CREATE TABLE tbl_settingstemp(emailid TEXT,user_Id TEXT, ipAddress TEXT, serverdb TEXT, createddate DATETIME DEFAULT CURRENT_TIMESTAMP , lastmodifieddate TEXT,customfield1 TEXT, customfield2 TEXT);
insert into tbl_settingstemp Select * from tbl_settings;
DROP table tbl_settings;
CREATE TABLE tbl_settings(emailid TEXT,user_Id TEXT, ipAddress TEXT, serverdb TEXT, createddate DATETIME DEFAULT CURRENT_TIMESTAMP , lastmodifieddate TEXT,customfield1 TEXT, customfield2 TEXT, evalEndDate DATETIME);
insert into tbl_settings Select emailid, user_Id, ipAddress, serverdb, createddate, lastmodifieddate, customfield1, customfield2, '' from tbl_settingstemp;
DROP table tbl_settingstemp;