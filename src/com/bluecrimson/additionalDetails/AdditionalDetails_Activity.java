package com.bluecrimson.additionalDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AdditionalDetails_Activity extends FragmentActivity implements OnClickListener {

	AQuery aq;
	Women_Profile_Pojo woman;
	Partograph_DB dbh;
	int ambulance = 0;
	int breastFeeding = 0;
	ImageButton imgbtnsave;
	int object_id = 13;
	String dateBreastFeeding;
	String strTime;
	int pos;
	ArrayList<String> adata;
	int id;
	String selecteddate;
	int year, mon, day;
	String strLastEntry;
	static final int TIME_DIALOG_ID = 999;
	String strbfTime;
	private int hour;
	private int minute;
	ArrayList<Add_value_pojo> val;
	public static MenuItem menuItem;
	public static int addtabpos;// 17Jan2017 Arpitha
	public static boolean additionalDetails;// 17Jan2017 Arpitha

	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.activity_addtionaldetails);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");

			addtabpos = Partograph_CommonClass.curr_tabpos;// 17JAn2017 Arpitha
			additionalDetails = true;// 17Jan2017 Arpitha

			dbh = Partograph_DB.getInstance(getApplicationContext());
			aq = new AQuery(this);

			ActionBar actionbar = getActionBar();
			actionbar.setDisplayHomeAsUpEnabled(true);
			initializeScreen(aq.id(R.id.rellay).getView());
			imgbtnsave = (ImageButton) findViewById(R.id.imgsave);

			dateBreastFeeding = Partograph_CommonClass.getTodaysDate();
			strTime = Partograph_CommonClass.getCurrentTime();

			/*
			 * aq.id(R.id.etdate).text(Partograph_CommonClass.
			 * getConvertedDateFormat( Partograph_CommonClass.getTodaysDate(),
			 * Partograph_CommonClass.defdateformat));
			 * aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());
			 */

			if (woman.getDel_type() == 0) {
				aq.id(R.id.trbreastfeedingheading).gone();
				aq.id(R.id.trbreastfeeding).gone();
				aq.id(R.id.viewbreastfeeding).gone();
				aq.id(R.id.viewbreastfeedingheading).gone();
			}

			dispalyData();

			imgbtnsave.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					saveAdditionalData();

				}
			});

			// dateBreastFeeding = Partograph_CommonClass.getTodaysDate();

			/*
			 * aq.id(R.id.etdate).getEditText().setOnTouchListener(new
			 * View.OnTouchListener() {
			 * 
			 * @Override public boolean onTouch(View v, MotionEvent event) { if
			 * (MotionEvent.ACTION_UP == event.getAction()) { try {
			 * 
			 * caldatepicker(); } catch (Exception e) { AppContext.addLog(new
			 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
			 * this.getClass().getSimpleName(), e); e.printStackTrace(); } }
			 * return true; } });
			 * 
			 * aq.id(R.id.ettime).getEditText().setOnTouchListener(new
			 * View.OnTouchListener() {
			 * 
			 * @Override public boolean onTouch(View v, MotionEvent event) { if
			 * (MotionEvent.ACTION_UP == event.getAction()) { strbfTime =
			 * aq.id(R.id.ettime).getText().toString();
			 * 
			 * if (strbfTime != null) { Calendar calendar =
			 * Calendar.getInstance(); Date d = null; d =
			 * dbh.getTime(strbfTime); calendar.setTime(d); hour =
			 * calendar.get(Calendar.HOUR_OF_DAY); minute =
			 * calendar.get(Calendar.MINUTE); } showDialog(TIME_DIALOG_ID); }
			 * return true; } });
			 */

			getwomanbasicdata();

			strLastEntry = dbh.getlastentrytime(woman.getWomenId(), 0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}

		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// TextView
			if (v.getClass() == TextView.class) {
				aq.id(v.getId()).getText();
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class || v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == TableRow.class) {

			}
			if (v.getClass() == View.class) {

			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.chkambulance:

			if (aq.id(R.id.chkambulance).isChecked()) {
				ambulance = 1;
				aq.id(R.id.trambulanceaddress).visible();
			} else {
				ambulance = 0;
				aq.id(R.id.trambulanceaddress).gone();
			}
			break;

		case R.id.rdbfyes:
			breastFeeding = 2;
			aq.id(R.id.trreason).gone();
			aq.id(R.id.trdate).visible();
			aq.id(R.id.trtime).visible();
			break;

		case R.id.rdbfno:
			breastFeeding = 1;
			aq.id(R.id.trreason).visible();
			aq.id(R.id.trdate).gone();
			aq.id(R.id.trtime).gone();
			break;

		case R.id.imgsave:
			saveAdditionalData();

		}
	}

	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));// 02Oct2016

			if (woman.getregtype() != 2) {// strings.xml
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = woman.getTime_of_admission();
				aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {

				aq.id(R.id.wdoa).text(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}
			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));

			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus)
						.text(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);
				tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				aq.id(R.id.wdeldate).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

			} else {
				aq.id(R.id.wdelstatus).gone();
				aq.id(R.id.wdeldate).gone();
			}

		}
	}

	boolean saveAdditionalData() {
		try {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (validateFileds()) {

				if (val.size() > 0) {
					if ((aq.id(R.id.rdbfyes).isChecked() || aq.id(R.id.rdbfno).isChecked())
							&& val.get(pos).getBreastFeeding() == 0) {
						adata = new ArrayList<String>();
						adata.add("" + breastFeeding);
						adata.add(aq.id(R.id.etreason).getText().toString());
						id = 1;
						strTime = aq.id(R.id.ettime).getText().toString();
						insertData();
					}

					if (aq.id(R.id.chkambulance).isChecked() && val.get(pos).getAmbulanceRequired() == 0) {
						adata = new ArrayList<String>();
						adata.add("" + ambulance);
						adata.add(aq.id(R.id.etambulanceaddress).getText().toString());
						id = 2;
						dateBreastFeeding = Partograph_CommonClass.getTodaysDate();
						strTime = Partograph_CommonClass.getCurrentTime();
						insertData();

					}
				} else {

					if (aq.id(R.id.rdbfyes).isChecked() || aq.id(R.id.rdbfno).isChecked()) {
						adata = new ArrayList<String>();
						adata.add("" + breastFeeding);
						adata.add(aq.id(R.id.etreason).getText().toString());
						id = 1;
						strTime = aq.id(R.id.ettime).getText().toString();
						insertData();
					}

					if (aq.id(R.id.chkambulance).isChecked()) {
						adata = new ArrayList<String>();
						adata.add("" + ambulance);
						adata.add(aq.id(R.id.etambulanceaddress).getText().toString());
						id = 2;
						dateBreastFeeding = Partograph_CommonClass.getTodaysDate();
						strTime = Partograph_CommonClass.getCurrentTime();
						insertData();

					}

				}
				dispalyData();
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return true;

	}

	/** Method to rollback the transaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the transaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

		Toast.makeText(getApplicationContext(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

	}

	void dispalyData() {

		try {

			val = new ArrayList<Add_value_pojo>();
			val = dbh.getAdditionalData(object_id, woman.getWomenId(), woman.getUserId());
			if (val.size() > 0) {

				if ((val.get(pos).getBreastFeeding()) == 1) {
					aq.id(R.id.rdbfno).checked(true);
					aq.id(R.id.etreason).text(val.get(pos).getResonForNotFeeding());
					aq.id(R.id.trdate).gone();
					aq.id(R.id.trtime).gone();
					aq.id(R.id.etdate).enabled(false);
					aq.id(R.id.ettime).enabled(false);
					aq.id(R.id.etreason).enabled(false);

					aq.id(R.id.rdbfno).enabled(false);
					aq.id(R.id.rdbfyes).enabled(false);
					aq.id(R.id.rdbfno).textColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.rdbfyes).textColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.etreason).textColor(getResources().getColor(R.color.fontcolor_disable));

				} else if ((val.get(pos).getBreastFeeding()) == 2) {
					aq.id(R.id.trdate).visible();
					aq.id(R.id.trtime).visible();
					aq.id(R.id.trreason).gone();
					aq.id(R.id.rdbfyes).checked(true);

					aq.id(R.id.etdate).enabled(false);
					aq.id(R.id.ettime).enabled(false);
					aq.id(R.id.etreason).enabled(false);
					aq.id(R.id.rdbfno).enabled(false);
					aq.id(R.id.rdbfyes).enabled(false);
					aq.id(R.id.rdbfno).textColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.rdbfyes).textColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.etdate).textColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.ettime).textColor(getResources().getColor(R.color.fontcolor_disable));

					aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
							val.get(pos).getBreastFeedingDate(), Partograph_CommonClass.defdateformat));
					aq.id(R.id.ettime).text(val.get(pos).getBreastFeedingTime());

				} else {
					aq.id(R.id.rdbfyes).checked(false);
					aq.id(R.id.rdbfno).checked(false);
					aq.id(R.id.trdate).gone();
					aq.id(R.id.trtime).gone();
					aq.id(R.id.trreason).gone();

				}

				if ((val.get(pos).getAmbulanceRequired()) == 1) {
					aq.id(R.id.chkambulance).checked(true);
					aq.id(R.id.trambulanceaddress).visible();
					aq.id(R.id.etambulanceaddress).visible();
					aq.id(R.id.etambulanceaddress).text(val.get(pos).getAmbulanceAddress());
					aq.id(R.id.chkambulance).enabled(false);
					aq.id(R.id.chkambulance).textColor(getResources().getColor(R.color.black));
					aq.id(R.id.etambulanceaddress).enabled(false);
					aq.id(R.id.etambulanceaddress).textColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.etambulanceaddress).getEditText().setFocusable(false);// 13April2017
																						// Arpitha

				}

				if (((val.get(pos).getBreastFeeding()) == 1 || (val.get(pos).getBreastFeeding()) == 2)
						&& (val.get(pos).getAmbulanceRequired()) == 1) {
					aq.id(R.id.imgsave).enabled(false);
					aq.id(R.id.imgsave).backgroundColor(getResources().getColor(R.color.darkgray));

				}

			}

			else {
				// 10Aug2017 Atpitha
				ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
				arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
				if (arrVal.size() > 0) {
					aq.id(R.id.txtdisable).visible();
					// txtdisabled.setVisibility(View.VISIBLE);
					// aq.id(R.id.txtdisable).gone();
					aq.id(R.id.tbl1).gone();
					// aq.id(R.id.scrapgarscore).gone();
					// aq.id(R.id.llparentapgar).gone();

				} // 10Aug2017 Atpitha
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	void insertData() {
		try {
			ArrayList<String> prop_id = new ArrayList<String>();
			ArrayList<String> prop_name = new ArrayList<String>();
			Property_pojo pdata = new Property_pojo();
			Cursor cursor = dbh.getAdditionalDetailPropertyIds(object_id, id);

			if (cursor.getCount() > 0) {
				cursor.moveToFirst();

				do {

					prop_id.add(cursor.getString(0));
					prop_name.add(cursor.getString(1));

				} while (cursor.moveToNext());

				if (id == 2) {
					Collections.reverse(prop_id);
					Collections.reverse(prop_name);
				}
				pdata.setObj_Id(object_id);
				pdata.setProp_id(prop_id);
				pdata.setProp_name(prop_name);
				pdata.setProp_value(adata);
				pdata.setStrdate(dateBreastFeeding);
				pdata.setStrTime(strTime);

				dbh.db.beginTransaction();
				int transId = dbh.iCreateNewTrans(woman.getUserId());

				boolean isAdded = dbh.insertData(pdata, woman.getWomenId(), woman.getUserId(), transId);

				if (isAdded) {
					dbh.iNewRecordTrans(woman.getUserId(), transId, Partograph_DB.TBL_PROPERTYVALUES);
					commitTrans();
				} else {
					rollbackTrans();
				}

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	boolean validateFileds() throws Exception {

		if (val.size() <= 0) {

			if (!(aq.id(R.id.chkambulance).isChecked())
					&& (!(aq.id(R.id.rdbfyes).isChecked() || aq.id(R.id.rdbfno).isChecked()))) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data_found_to_save),
						Toast.LENGTH_LONG).show();
				return false;

			}

			if (aq.id(R.id.rdbfno).isChecked() && aq.id(R.id.etreason).getText().toString().trim().length() <= 0) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.pz_enter_reason_for_not_breast_feeding), Toast.LENGTH_LONG)
						.show();
				return false;
			}

			if (aq.id(R.id.chkambulance).isChecked()
					&& aq.id(R.id.etambulanceaddress).getText().toString().trim().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.plz_enter_address),
						Toast.LENGTH_LONG).show();
				return false;
			}
		} else {

			if (val.get(pos).getBreastFeeding() != 0 && (!(aq.id(R.id.chkambulance).isChecked()))) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data_found_to_save),
						Toast.LENGTH_LONG).show();
				return false;
			}

			if (val.get(pos).getAmbulanceRequired() != 0
					&& (!(aq.id(R.id.rdbfyes).isChecked() || aq.id(R.id.rdbfno).isChecked()))) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_data_found_to_save),
						Toast.LENGTH_LONG).show();
				return false;
			}

			if (aq.id(R.id.rdbfno).isChecked() && aq.id(R.id.etreason).getText().toString().trim().length() <= 0) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.pz_enter_reason_for_not_breast_feeding), Toast.LENGTH_LONG)
						.show();
				return false;
			}

			if (aq.id(R.id.chkambulance).isChecked()
					&& aq.id(R.id.etambulanceaddress).getText().toString().trim().length() <= 0) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.plz_enter_address),
						Toast.LENGTH_LONG).show();
				return false;
			}

		}
		return true;
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getSupportFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? dateBreastFeeding : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(getResources().getString(R.string.pickadate));
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {

					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {

			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
			Date selected = date.parse(selecteddate);
			String deldate = woman.getDel_Date();
			Date delDate = null;
			Date lastEntry = null;
			if (deldate != null) {
				delDate = date.parse(deldate);
			}
			if (strLastEntry != null) {
				lastEntry = date.parse(strLastEntry);
			}
			if (selected.after(new Date())) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.date_cannot_be_greater_than_current_date), Toast.LENGTH_LONG)
						.show();
				aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
						Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
			} else if (delDate != null && selected.before(delDate)) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.breastfeeding_date_cannot_be_before_del_date),
						Toast.LENGTH_LONG).show();
				aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
						Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));

			} else if (lastEntry != null && selected.before(lastEntry)) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.breastfeeding_date_cannot_be_before_lastparto_date),
						Toast.LENGTH_LONG).show();
				aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
						Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
			} else {
				dateBreastFeeding = selecteddate;
				aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
						Partograph_CommonClass.defdateformat));
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		try {
			switch (id) {
			case TIME_DIALOG_ID:
				// set time picker as current time
				return new TimePickerDialog(this, timePickerListener, hour, minute, false);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;

			try {

				Date selected;
				String strselectedtime;
				Date currenttime;
				Date lastentry = null;
				Date del = null;
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				currenttime = date
						.parse(Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime());
				strselectedtime = dateBreastFeeding + " "
						+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));

				selected = date.parse(strselectedtime);

				if (woman.getDel_Date() != null) {

					del = date.parse(woman.getDel_Date() + " " + woman.getDel_Time());
				}

				if (selected.after(currenttime)) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_date),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

				} else if (del != null && selected.before(del)) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.breastfeeding_time_cannot_be_before_del_date),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

				} else if (lastentry != null && selected.before(lastentry)) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.breastfeeding_time_cannot_be_before_lastparto_date),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

				} else {
					aq.id(R.id.ettime).text(
							new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(AdditionalDetails_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(AdditionalDetails_Activity.this, woman.getUserId());
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));
				return true;
			// 22May2017 Arpitha - v2.6
			case R.id.summary:
				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);
				return true;
			case R.id.usermanual:
				Intent manual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(manual);
				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(this);
				return true;// 22May2017 Arpitha - v2.6

			case R.id.disch:
				Intent disch = new Intent(AdditionalDetails_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(AdditionalDetails_Activity.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(AdditionalDetails_Activity.this, woman);
					// loadWomendata(displayListCountddel);
					// if(adapter!=null)
					// adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(AdditionalDetails_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(AdditionalDetails_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(AdditionalDetails_Activity.this, woman);
				} else {
					Intent ref = new Intent(AdditionalDetails_Activity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				// Intent ref = new Intent(AdditionalDetails_Activity.this,
				// ReferralInfo_Activity.class);
				// ref.putExtra("woman", woman);
				// startActivity(ref);
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(AdditionalDetails_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(AdditionalDetails_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(AdditionalDetails_Activity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(AdditionalDetails_Activity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(AdditionalDetails_Activity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.lat:
				Intent lat = new Intent(AdditionalDetails_Activity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(AdditionalDetails_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.email).setVisible(false);// 09Jan2017 Arpitha

		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha
		// menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		// menu.findItem(R.id.view).setVisible(true);
		menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	/*
	 * @Override public void onBackPressed() { // TODO Auto-generated method
	 * stub super.onBackPressed(); try {
	 * displayConfirmationAlert(getResources().getString(R.string.exit_msg),
	 * getResources().getString(R.string.home)); } catch (NotFoundException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } catch
	 * (Exception e) { // TODO Auto-generated catch block
	 * 
	 * AppContext.addLog( new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } }
	 */

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.home));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void displayConfirmationAlert_exit(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(AdditionalDetails_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(AdditionalDetails_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {

					Intent i = new Intent(AdditionalDetails_Activity.this, LoginActivity.class);
					startActivity(i);

				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		dialog.setCancelable(false);

	}

}
