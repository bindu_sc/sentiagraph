package com.bluecrimson.additionalDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.regwomenview.ExpandableListAdapter;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Summary_Activity extends Activity {

	TableLayout tblsummary;
	Partograph_DB dbh;
	UserPojo user;
	String[] heading;

	int mon, year, day;
	public static int curryear;
	ExpandableListAdapter explistAdapter;
	Map<String, Integer> MonthList1;
	ExpandableListView exlistview;
	public static HashMap<String, ArrayList<Women_Profile_Pojo>> CategoryDataCollection1;
	public static int summary;
	public static boolean issummary;

	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);

		// 05Nov2016 Arpitha
		Locale locale = null;
		
		summary = Partograph_CommonClass.curr_tabpos;//03Sep2017 Arpitha
		issummary = true;//03Sep2017 Arpitha

		if (AppContext.prefs.getBoolean("isEnglish", false)) {
			locale = new Locale("en");
			Locale.setDefault(locale);
		} else if (AppContext.prefs.getBoolean("isHindi", false)) {
			locale = new Locale("hi");
			Locale.setDefault(locale);
		}

		Configuration config = new Configuration();
		config.locale = locale;
		getResources().updateConfiguration(config, getResources().getDisplayMetrics());

		AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
															// Arpitha

		setContentView(R.layout.activity_partographsummary);

		try {

			ActionBar actionbar = getActionBar();
			actionbar.setDisplayHomeAsUpEnabled(true);

			actionbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar))); // set
																											// your
																											// desired
																											// color

			dbh = Partograph_DB.getInstance(getApplicationContext());

			user = dbh.getUserProfile();

			heading = getResources().getStringArray(R.array.summary);

			exlistview = (ExpandableListView) findViewById(R.id.expListDataYearly);

			ArrayList<String> years = new ArrayList<String>();
			years = dbh.getYears();

			HashMap<String, ArrayList<Women_Profile_Pojo>> value = new HashMap<String, ArrayList<Women_Profile_Pojo>>();

			// ArrayList<Women_Profile_Pojo> pojo = new
			// ArrayList<Women_Profile_Pojo>();
			//
			// Women_Profile_Pojo wpojo = new Women_Profile_Pojo();
			// wpojo.setWomen_name("A");
			//
			// pojo.add(wpojo);

			SummaryAdapter adapter = new SummaryAdapter(this, years, value, dbh);
			exlistview.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
		 KillAllActivitesAndGoToLogin.addToStack(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(Summary_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			case R.id.sms:
				//if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(Summary_Activity.this, Partograph_CommonClass.user.getUserId());
//				} else
//					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
//							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));
				return true;
			case R.id.disch:
				Intent disch = new Intent(Summary_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.email).setVisible(false);
		menu.findItem(R.id.summary).setVisible(false);
		
		
		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
	//	menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
	//	menu.findItem(R.id.info).setVisible(false);

		menu.findItem(R.id.registration).setVisible(false);
		menu.findItem(R.id.changepass).setVisible(false);
		menu.findItem(R.id.usermanual).setVisible(false);
		// menu.findItem(R.id.sms).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);// 12may2017 Arpitha - v2.6
		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 12may2017
																				// Arpitha
																				// -
																				// v2.6
			menu.findItem(R.id.sms).setVisible(false);
		}
		menu.findItem(R.id.search).setVisible(false);//14Jun2017 Arpitha
		
		menu.findItem(R.id.save).setVisible(false);//08Aug2017 Arpitha
		return super.onPrepareOptionsMenu(menu);
	}

	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(Summary_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));// 08feb2017
																			// Arpitha
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {
					Intent i = new Intent(Summary_Activity.this, LoginActivity.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(Summary_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017
		dialog.show();

		dialog.setCancelable(false);

	}

}
