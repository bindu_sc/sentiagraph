package com.bluecrimson.partograph;

public class WomenReferral_pojo {
	String userid;
	String womenid;
	String womenname;
	String facility;
	String facilityname;
	int referredtofacilityid;
	String placeofreferral;
	String reasonforreferral;
	String dateofreferral;
	String timeofreferral;
	int transid;
	String created_date;
	String lastupdateddate;
	
//	04jan2016
	String descriptionofreferral;
	int conditionofmother;
	int conditionofbaby;
	String bp;
	String pulse;
	
//  updated by Arpitha on 23feb206
	String heartrate;
	String spo2;
	
	
	public String getHeartrate() {
		return heartrate;
	}
	public void setHeartrate(String heartrate) {
		this.heartrate = heartrate;
	}
	public String getSpo2() {
		return spo2;
	}
	public void setSpo2(String spo2) {
		this.spo2 = spo2;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getWomenid() {
		return womenid;
	}
	public void setWomenid(String womenid) {
		this.womenid = womenid;
	}
	public String getWomenname() {
		return womenname;
	}
	public void setWomenname(String womenname) {
		this.womenname = womenname;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacilityname() {
		return facilityname;
	}
	public void setFacilityname(String facilityname) {
		this.facilityname = facilityname;
	}
	public int getReferredtofacilityid() {
		return referredtofacilityid;
	}
	public void setReferredtofacilityid(int referredtofacilityid) {
		this.referredtofacilityid = referredtofacilityid;
	}
	public String getPlaceofreferral() {
		return placeofreferral;
	}
	public void setPlaceofreferral(String placeofreferral) {
		this.placeofreferral = placeofreferral;
	}
	public String getReasonforreferral() {
		return reasonforreferral;
	}
	public void setReasonforreferral(String reasonforreferral) {
		this.reasonforreferral = reasonforreferral;
	}
	public String getDateofreferral() {
		return dateofreferral;
	}
	public void setDateofreferral(String dateofreferral) {
		this.dateofreferral = dateofreferral;
	}
	public String getTimeofreferral() {
		return timeofreferral;
	}
	public void setTimeofreferral(String timeofreferral) {
		this.timeofreferral = timeofreferral;
	}
	public int getTransid() {
		return transid;
	}
	public void setTransid(int transid) {
		this.transid = transid;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
	public String getLastupdateddate() {
		return lastupdateddate;
	}
	public void setLastupdateddate(String lastupdateddate) {
		this.lastupdateddate = lastupdateddate;
	}
	public String getDescriptionofreferral() {
		return descriptionofreferral;
	}
	public void setDescriptionofreferral(String descriptionofreferral) {
		this.descriptionofreferral = descriptionofreferral;
	}
	public int getConditionofmother() {
		return conditionofmother;
	}
	public void setConditionofmother(int conditionofmother) {
		this.conditionofmother = conditionofmother;
	}
	public int getConditionofbaby() {
		return conditionofbaby;
	}
	public void setConditionofbaby(int conditionofbaby) {
		this.conditionofbaby = conditionofbaby;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getPulse() {
		return pulse;
	}
	public void setPulse(String pulse) {
		this.pulse = pulse;
	}
	
	
	
}
