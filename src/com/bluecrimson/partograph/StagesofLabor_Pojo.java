package com.bluecrimson.partograph;

public class StagesofLabor_Pojo {
	String durationofthirdstage;
	String ergometrine;
	String misoprost;
	String pph;
	String pgf2alpha;
	String uterinemassage;
	String urinepassed;
	String dateofentry;
	String timeofentry;
	String placenta;
	String oxytocin;
	String ebl;
	String retainedplacenta;
	String inversion;
	String thirdstagecomments;
	String pphproperties;
	String uterus;
	String bleeding;
	String pulse;
	String bpsystolic;
	String bpdiastolic;
	String fourthstagecomments;

	// 13Feb2017 Arpitha
	String breastFeeding;
	String breastFeedingDateTime;
	String reasonForNotFeeding;

	public String getDurationofthirdstage() {
		return durationofthirdstage;
	}

	public void setDurationofthirdstage(String durationofthirdstage) {
		this.durationofthirdstage = durationofthirdstage;
	}

	public String getErgometrine() {
		return ergometrine;
	}

	public void setErgometrine(String ergometrine) {
		this.ergometrine = ergometrine;
	}

	public String getMisoprost() {
		return misoprost;
	}

	public void setMisoprost(String misoprost) {
		this.misoprost = misoprost;
	}

	public String getPph() {
		return pph;
	}

	public void setPph(String pph) {
		this.pph = pph;
	}

	public String getPgf2alpha() {
		return pgf2alpha;
	}

	public void setPgf2alpha(String pgf2alpha) {
		this.pgf2alpha = pgf2alpha;
	}

	public String getUterinemassage() {
		return uterinemassage;
	}

	public void setUterinemassage(String uterinemassage) {
		this.uterinemassage = uterinemassage;
	}

	public String getUrinepassed() {
		return urinepassed;
	}

	public void setUrinepassed(String urinepassed) {
		this.urinepassed = urinepassed;
	}

	public String getDateofentry() {
		return dateofentry;
	}

	public void setDateofentry(String dateofentry) {
		this.dateofentry = dateofentry;
	}

	public String getTimeofentry() {
		return timeofentry;
	}

	public void setTimeofentry(String timeofentry) {
		this.timeofentry = timeofentry;
	}

	public String getPlacenta() {
		return placenta;
	}

	public void setPlacenta(String placenta) {
		this.placenta = placenta;
	}

	public String getOxytocin() {
		return oxytocin;
	}

	public void setOxytocin(String oxytocin) {
		this.oxytocin = oxytocin;
	}

	public String getEbl() {
		return ebl;
	}

	public void setEbl(String ebl) {
		this.ebl = ebl;
	}

	public String getRetainedplacenta() {
		return retainedplacenta;
	}

	public void setRetainedplacenta(String retainedplacenta) {
		this.retainedplacenta = retainedplacenta;
	}

	public String getInversion() {
		return inversion;
	}

	public void setInversion(String inversion) {
		this.inversion = inversion;
	}

	public String getThirdstagecomments() {
		return thirdstagecomments;
	}

	public void setThirdstagecomments(String thirdstagecomments) {
		this.thirdstagecomments = thirdstagecomments;
	}

	public String getPphproperties() {
		return pphproperties;
	}

	public void setPphproperties(String pphproperties) {
		this.pphproperties = pphproperties;
	}

	public String getUterus() {
		return uterus;
	}

	public void setUterus(String uterus) {
		this.uterus = uterus;
	}

	public String getBleeding() {
		return bleeding;
	}

	public void setBleeding(String bleeding) {
		this.bleeding = bleeding;
	}

	public String getPulse() {
		return pulse;
	}

	public void setPulse(String pulse) {
		this.pulse = pulse;
	}

	public String getBpsystolic() {
		return bpsystolic;
	}

	public void setBpsystolic(String bpsystolic) {
		this.bpsystolic = bpsystolic;
	}

	public String getBpdiastolic() {
		return bpdiastolic;
	}

	public void setBpdiastolic(String bpdiastolic) {
		this.bpdiastolic = bpdiastolic;
	}

	public String getFourthstagecomments() {
		return fourthstagecomments;
	}

	public void setFourthstagecomments(String fourthstagecomments) {
		this.fourthstagecomments = fourthstagecomments;
	}

	// 13Feb2017 Arpitha
	public String getBreastFeeding() {
		return breastFeeding;
	}

	public void setBreastFeeding(String breastFeeding) {
		this.breastFeeding = breastFeeding;
	}

	public String getBreastFeedingDateTime() {
		return breastFeedingDateTime;
	}

	public void setBreastFeedingDateTime(String breastFeedingDateTime) {
		this.breastFeedingDateTime = breastFeedingDateTime;
	}

	public String getReasonForNotFeeding() {
		return reasonForNotFeeding;
	}

	public void setReasonForNotFeeding(String reasonForNotFeeding) {
		this.reasonForNotFeeding = reasonForNotFeeding;
	}

}
