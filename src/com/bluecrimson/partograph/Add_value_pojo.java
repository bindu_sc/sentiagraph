package com.bluecrimson.partograph;

import java.util.ArrayList;

import android.R.bool;

public class Add_value_pojo {

	String strVal;
	String strTime;
	String strdate;
	String strPulseval;
	String strBPsystolicval;
	String strBPdiastolicval;
	String strProteinval;
	String strAcetoneval;
	String strVolumeval;
	String cervix;
	String desc_of_head;
	String contractions;
	int duration;
	String amnioticfluid;
	String moulding;
	String hours;
	int obj_Id;
	String obj_Type;
	String comments;
	int isDanger;
	int isDangerval2;

	// ADD FEB04
	String oxytocindrops;
	String oxytocinunits;

	// updated on 28Feb2016 by Arpitha
	String Afmcomments;
	String contactioncomments;
	String dilatationcomments;
	String Fhrcomments;
	String Drugcomments;
	String Tempcomments;
	String Urinecomments;
	String oxytocincomments;
	String pulsecomments;
	String Drug;

	int breastFeeding;// 07Dec2016 Arpitha
	String resonForNotFeeding;// 07Dec2016 Arpitha
	int ambulanceRequired;// 07Dec2016 Arpitha
	String ambulanceAddress;// 07Dec2016 Arpitha
	String breastFeedingDate;// 08Dec2016 Arpitha
	String breastFeedingTime;// 08Dec2016 Arpitha
	// 12Jun2017 Arpitha
	int fhrDanger;
	int amnioticFluidDanger;
	int cervixDanger;
	int descentofheadDanger;

	int contractionDanger;
	int pulseDanger;
	int bpSysDanger;
	int bpDiaDanger;
	int contrcationDuration;
	int tempDanger;// 12Jun2017 Arpitha

	ArrayList<Integer> danger;

	public int getContrcationDuration() {
		return contrcationDuration;
	}

	public void setContrcationDuration(int contrcationDuration) {
		this.contrcationDuration = contrcationDuration;
	}

	public ArrayList<Integer> getDanger() {
		return danger;
	}

	public void setDanger(ArrayList<Integer> danger) {
		this.danger = danger;
	}

	public int getFhrDanger() {
		return fhrDanger;
	}

	public void setFhrDanger(int fhrDanger) {
		this.fhrDanger = fhrDanger;
	}

	public int getAmnioticFluidDanger() {
		return amnioticFluidDanger;
	}

	public void setAmnioticFluidDanger(int amnioticFluidDanger) {
		this.amnioticFluidDanger = amnioticFluidDanger;
	}

	public int getCervixDanger() {
		return cervixDanger;
	}

	public void setCervixDanger(int cervixDanger) {
		this.cervixDanger = cervixDanger;
	}

	public int getDescentofheadDanger() {
		return descentofheadDanger;
	}

	public void setDescentofheadDanger(int descentofheadDanger) {
		this.descentofheadDanger = descentofheadDanger;
	}

	public int getContractionDanger() {
		return contractionDanger;
	}

	public void setContractionDanger(int contractionDanger) {
		this.contractionDanger = contractionDanger;
	}

	public int getPulseDanger() {
		return pulseDanger;
	}

	public void setPulseDanger(int pulseDanger) {
		this.pulseDanger = pulseDanger;
	}

	public int getBpSysDanger() {
		return bpSysDanger;
	}

	public void setBpSysDanger(int bpSysDanger) {
		this.bpSysDanger = bpSysDanger;
	}

	public int getBpDiaDanger() {
		return bpDiaDanger;
	}

	public void setBpDiaDanger(int bpDiaDanger) {
		this.bpDiaDanger = bpDiaDanger;
	}

	public int getTempDanger() {
		return tempDanger;
	}

	public void setTempDanger(int tempDanger) {
		this.tempDanger = tempDanger;
	}

	public int getBreastFeeding() {
		return breastFeeding;
	}

	public void setBreastFeeding(int breastFeeding) {
		this.breastFeeding = breastFeeding;
	}

	public String getResonForNotFeeding() {
		return resonForNotFeeding;
	}

	public void setResonForNotFeeding(String resonForNotFeeding) {
		this.resonForNotFeeding = resonForNotFeeding;
	}

	public int getAmbulanceRequired() {
		return ambulanceRequired;
	}

	public void setAmbulanceRequired(int ambulanceRequired) {
		this.ambulanceRequired = ambulanceRequired;
	}

	public String getAmbulanceAddress() {
		return ambulanceAddress;
	}

	public void setAmbulanceAddress(String ambulanceAddress) {
		this.ambulanceAddress = ambulanceAddress;
	}

	public String getBreastFeedingDate() {
		return breastFeedingDate;
	}

	public void setBreastFeedingDate(String breastFeedingDate) {
		this.breastFeedingDate = breastFeedingDate;
	}

	public String getBreastFeedingTime() {
		return breastFeedingTime;
	}

	public void setBreastFeedingTime(String breastFeedingTime) {
		this.breastFeedingTime = breastFeedingTime;
	}

	public String getContactioncomments() {
		return contactioncomments;
	}

	public void setContactioncomments(String contactioncomments) {
		this.contactioncomments = contactioncomments;
	}

	public String getDrug() {
		return Drug;
	}

	public void setDrug(String drug) {
		Drug = drug;
	}

	public String getFhrcomments() {
		return Fhrcomments;
	}

	public void setFhrcomments(String fhrcomments) {
		Fhrcomments = fhrcomments;
	}

	public String getDrugcomments() {
		return Drugcomments;
	}

	public void setDrugcomments(String drugcomments) {
		Drugcomments = drugcomments;
	}

	public String getTempcomments() {
		return Tempcomments;
	}

	public void setTempcomments(String tempcomments) {
		Tempcomments = tempcomments;
	}

	public String getUrinecomments() {
		return Urinecomments;
	}

	public void setUrinecomments(String urinecomments) {
		Urinecomments = urinecomments;
	}

	public String getOxytocincomments() {
		return oxytocincomments;
	}

	public void setOxytocincomments(String oxytocincomments) {
		this.oxytocincomments = oxytocincomments;
	}

	public String getPulsecomments() {
		return pulsecomments;
	}

	public void setPulsecomments(String pulsecomments) {
		this.pulsecomments = pulsecomments;
	}

	public String getDilatationcomments() {
		return dilatationcomments;
	}

	public void setDilatationcomments(String dilatationcomments) {
		this.dilatationcomments = dilatationcomments;
	}

	public String getAfmcomments() {
		return Afmcomments;
	}

	public void setAfmcomments(String afmcomments) {
		Afmcomments = afmcomments;
	}

	public int getIsDanger() {
		return isDanger;
	}

	public void setIsDanger(int isDanger) {
		this.isDanger = isDanger;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getObj_Id() {
		return obj_Id;
	}

	public void setObj_Id(int obj_Id) {
		this.obj_Id = obj_Id;
	}

	public String getObj_Type() {
		return obj_Type;
	}

	public void setObj_Type(String obj_Type) {
		this.obj_Type = obj_Type;
	}

	public String getHours() {
		return hours;
	}

	public void setHours(String hours) {
		this.hours = hours;
	}

	public String getStrVal() {
		return strVal;
	}

	public void setStrVal(String strVal) {
		this.strVal = strVal;
	}

	public String getStrTime() {
		return strTime;
	}

	public void setStrTime(String strTime) {
		this.strTime = strTime;
	}

	public String getStrdate() {
		return strdate;
	}

	public void setStrdate(String strdate) {
		this.strdate = strdate;
	}

	public String getStrPulseval() {
		return strPulseval;
	}

	public void setStrPulseval(String strPulseval) {
		this.strPulseval = strPulseval;
	}

	public String getStrBPsystolicval() {
		return strBPsystolicval;
	}

	public void setStrBPsystolicval(String strBPsystolicval) {
		this.strBPsystolicval = strBPsystolicval;
	}

	public String getStrBPdiastolicval() {
		return strBPdiastolicval;
	}

	public void setStrBPdiastolicval(String strBPdiastolicval) {
		this.strBPdiastolicval = strBPdiastolicval;
	}

	public String getStrProteinval() {
		return strProteinval;
	}

	public void setStrProteinval(String strProteinval) {
		this.strProteinval = strProteinval;
	}

	public String getStrAcetoneval() {
		return strAcetoneval;
	}

	public void setStrAcetoneval(String strAcetoneval) {
		this.strAcetoneval = strAcetoneval;
	}

	public String getStrVolumeval() {
		return strVolumeval;
	}

	public void setStrVolumeval(String strVolumeval) {
		this.strVolumeval = strVolumeval;
	}

	public String getCervix() {
		return cervix;
	}

	public void setCervix(String cervix) {
		this.cervix = cervix;
	}

	public String getDesc_of_head() {
		return desc_of_head;
	}

	public void setDesc_of_head(String desc_of_head) {
		this.desc_of_head = desc_of_head;
	}

	public String getContractions() {
		return contractions;
	}

	public void setContractions(String contractions) {
		this.contractions = contractions;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getAmnioticfluid() {
		return amnioticfluid;
	}

	public void setAmnioticfluid(String amnioticfluid) {
		this.amnioticfluid = amnioticfluid;
	}

	public String getMoulding() {
		return moulding;
	}

	public void setMoulding(String moulding) {
		this.moulding = moulding;
	}

	public String getOxytocindrops() {
		return oxytocindrops;
	}

	public void setOxytocindrops(String oxytocindrops) {
		this.oxytocindrops = oxytocindrops;
	}

	public String getOxytocinunits() {
		return oxytocinunits;
	}

	public void setOxytocinunits(String oxytocinunits) {
		this.oxytocinunits = oxytocinunits;
	}

	public int getIsDangerval2() {
		return isDangerval2;
	}

	public void setIsDangerval2(int isDangerval2) {
		this.isDangerval2 = isDangerval2;
	}

	// 12Jun2017 Arpitha
	int IsDangerval3;

	public int getIsDangerval3() {
		return IsDangerval3;
	}

	public void setIsDangerval3(int isDangerval3) {
		IsDangerval3 = isDangerval3;
	}// 12Jun2017 Arpitha

}
