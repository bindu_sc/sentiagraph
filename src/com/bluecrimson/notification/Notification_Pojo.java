package com.bluecrimson.notification;

import java.io.Serializable;

public class Notification_Pojo implements Serializable{
	
	
	String womenid;
	String userid;
	int objectid;
	int count;
	
	int fhr;
	int contraction;
	int dilatation;
	int pulse_bp;
	public int getFhr() {
		return fhr;
	}
	public void setFhr(int fhr) {
		this.fhr = fhr;
	}
	public int getContraction() {
		return contraction;
	}
	public void setContraction(int contraction) {
		this.contraction = contraction;
	}
	public int getDilatation() {
		return dilatation;
	}
	public void setDilatation(int dilatation) {
		this.dilatation = dilatation;
	}
	public int getPulse_bp() {
		return pulse_bp;
	}
	public void setPulse_bp(int pulse_bp) {
		this.pulse_bp = pulse_bp;
	}
	public String getWomenid() {
		return womenid;
	}
	public void setWomenid(String womenid) {
		this.womenid = womenid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getObjectid() {
		return objectid;
	}
	public void setObjectid(int objectid) {
		this.objectid = objectid;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

}
