//24July2017 Arpitha - v3.0
package com.bluecrimson.dischargedetails;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class DiscargeDetails_Activity extends Activity implements OnClickListener {

	static AQuery aq;
	Partograph_DB dbh;
	int womanDelStatus = 0;
	int womanReferral = 0;
	static Women_Profile_Pojo woman;
	DiscgargePojo dpojo;
	ArrayList<DiscgargePojo> dischValues;
	String selecteddate;
	int year, mon, day;
	static String todaysDate;
	static String dischargeDate;
	static String discTime;
	private static int hour;
	private static int minute;
	ArrayList<DiscgargePojo> dvalues;
	static Date doa;
	static Date dateOfDel;
	static Date lastpartoENtryDate;
	static Date lastpartoENtryTime;
	String strdateOfAdm;
	static String strlastpartoEntry;
	static Context context;
	static PdfPTable womanbasics;
	PdfPTable womandelInfo;
	PdfPTable childInfo;
	private static Font small = new Font(Font.HELVETICA, 10, Font.NORMAL);
	private static Font heading = new Font(Font.HELVETICA, 10, Font.BOLD);
	PdfPTable dischargeInfo;
	PdfPTable dischargeInfo2;
	static String strdelTime;
	static Date delTime;
	PdfPTable childInfo2;
	boolean discDate;
	String nextFollowUpDate;
	File file;
	static Date timeOfAdm;
	boolean isUpdateDetails = false;
	DiscgargePojo dpojoVal;
	public static boolean updateReferral;
	public static boolean updateDel;
	public static int discharge;// 03Sep2017 Arpitha
	public static boolean isdisc;// 03Sep2017 Arpitha
	// public static boolean
	static String strpostpartumdatetime;// 20Sep2017 Arpitha
	static Date postpartumdatetime;// 20Sep2017 Arpitha
	// 19Sep2017 Arpitha
	static String strrefdatetime = null;
	Date refdatetime;
	static String strlatdatetime;// 20Sep2017 Arpitha
	static Date latDatetime;// 20Sep2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			setContentView(R.layout.activity_dischargedetails);

			aq = new AQuery(this);
			dbh = Partograph_DB.getInstance(getApplicationContext());

			context = DiscargeDetails_Activity.this;

			getwomanbasicdata();

			discharge = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			isdisc = true;// 03Sep2017 Arpitha

			this.setTitle(getResources().getString(R.string.discharge_details));

			initializeScreen(aq.id(R.id.rldischargedetails).getView());
			intialView();

			aq.id(R.id.etdateofdicharge).getEditText().setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							discDate = true;
							v.performClick();

							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;

				}
			});

			aq.id(R.id.ettimeofdischarge).getEditText().setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						v.performClick();

						discTime = aq.id(R.id.ettimeofdischarge).getText().toString();

						if (discTime != null) {

							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(discTime);
							if (d != null)
								calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}

						showtimepicker();
					}

					return true;
				}
			});

			aq.id(R.id.etdateofnextfollowup).getEditText().setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							discDate = false;
							v.performClick();

							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;

				}
			});

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		KillAllActivitesAndGoToLogin.addToStack(this);

	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				// aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		if (v.getClass() == TableRow.class) {

		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		try {
			switch (v.getId()) {

			case R.id.rddip:
				womanDelStatus = 1;

				// aq.id(R.id.trdateofnextfollowup).gone();
				// aq.id(R.id.trplaceofnextfolowup).gone();
				aq.id(R.id.trchildinvestigation).gone();
				aq.id(R.id.trchildcondition).gone();
				if (!(aq.id(R.id.rdyes).isChecked()))
					aq.id(R.id.reasonfordischarge).visible();
				// aq.id(R.id.trreferred).visible();
				break;

			case R.id.rddelivered:
				womanDelStatus = 2;
				aq.id(R.id.reasonfordischarge).gone();
				// aq.id(R.id.trreferred).gone();
				// aq.id(R.id.trdateofnextfollowup).visible();
				// aq.id(R.id.trplaceofnextfolowup).visible();
				aq.id(R.id.trchildinvestigation).visible();
				aq.id(R.id.trchildcondition).visible();
				if (woman.getNo_of_child() == 2) {
					aq.id(R.id.trchild2investigation).visible();
					aq.id(R.id.trchild2condition).visible();
					aq.id(R.id.txtchildinvestigation)
							.text(getResources().getString(R.string.first_child_investigation));
					aq.id(R.id.txtconditionofchild).text(getResources().getString(R.string.condition_of_first_child));
				}
				break;

			case R.id.rdyes:
				womanReferral = 1;
				aq.id(R.id.reasonfordischarge).gone();
				break;

			case R.id.rdno:
				womanReferral = 2;
				if (aq.id(R.id.rddip).isChecked())
					aq.id(R.id.reasonfordischarge).visible();
				break;

			case R.id.imgpdf:
				Partograph_CommonClass.pdfAlertDialog(context, woman, "discharge", null, dvalues);
				// displayAlertDialog();
				break;
			case R.id.btndischargedetails1:
				aq.id(R.id.scrdischagedetails1).visible();
				aq.id(R.id.scrdischagedetails2).gone();
				aq.id(R.id.btndischargedetails1).backgroundColor(getResources().getColor(R.color.gamboge));
				aq.id(R.id.btndischargedetails2).backgroundColor(getResources().getColor(R.color.brown));
				break;
			case R.id.btndischargedetails2:
				aq.id(R.id.scrdischagedetails1).gone();
				aq.id(R.id.scrdischagedetails2).visible();
				aq.id(R.id.btndischargedetails2).backgroundColor(getResources().getColor(R.color.gamboge));
				aq.id(R.id.btndischargedetails1).backgroundColor(getResources().getColor(R.color.brown));
				break;
			case R.id.btnsave:

				saveDischargeDetails();
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	void intialView() throws Exception {

		aq.id(R.id.etwomaninvestigation).getEditText().setImeOptions(EditorInfo.IME_ACTION_NEXT);
		aq.id(R.id.btndischargedetails1).backgroundColor(getResources().getColor(R.color.gamboge));
		aq.id(R.id.scrdischagedetails1).visible();
		aq.id(R.id.scrdischagedetails2).gone();
		aq.id(R.id.etnamedisc).text(woman.getWomen_name());
		aq.id(R.id.etagedesc).text("" + woman.getAge());
		aq.id(R.id.etaddressdesc).text(woman.getAddress());
		aq.id(R.id.etfacilityname).text(woman.getFacility_name());
		if (woman.getregtype() != 2) {
			aq.id(R.id.etdoa)
					.text(Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
							Partograph_CommonClass.defdateformat) + " / "
							+ Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission()));
		} else {
			aq.id(R.id.etdoa)
					.text(Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_reg_entry().split("/")[0],
							Partograph_CommonClass.defdateformat) + " / "
							+ Partograph_CommonClass.gettimein12hrformat(woman.getDate_of_reg_entry().split("/")[1]));

		}
		if (woman.getDel_type() != 0) {
			aq.id(R.id.rddip).checked(false);
			aq.id(R.id.rddelivered).checked(true);

			aq.id(R.id.rddip).enabled(false);// 11Sep2017 Arpitha
			aq.id(R.id.rddelivered).enabled(false);// 11Sep2017 Arpitha

			womanDelStatus = 2;
			aq.id(R.id.reasonfordischarge).gone();
			// aq.id(R.id.trreferred).gone();
			// aq.id(R.id.trdateofnextfollowup).visible();
			// aq.id(R.id.trplaceofnextfolowup).visible();
			aq.id(R.id.trchildinvestigation).visible();
			aq.id(R.id.trchildcondition).visible();
			if (woman.getNo_of_child() == 2) {
				aq.id(R.id.trchild2investigation).visible();
				aq.id(R.id.trchild2condition).visible();
				aq.id(R.id.txtchildinvestigation).text(getResources().getString(R.string.first_child_investigation));
				aq.id(R.id.txtconditionofchild).text(getResources().getString(R.string.condition_of_first_child));
			}
		} else {
			aq.id(R.id.rddip).checked(true);
			aq.id(R.id.rddelivered).checked(false);
			womanDelStatus = 1;

			// aq.id(R.id.trdateofnextfollowup).gone();
			// aq.id(R.id.trplaceofnextfolowup).gone();
			aq.id(R.id.trchildinvestigation).gone();
			aq.id(R.id.trchildcondition).gone();
			// aq.id(R.id.trreferred).visible();

		}

		todaysDate = Partograph_CommonClass.getTodaysDate();
		dischargeDate = todaysDate;

		aq.id(R.id.etdateofdicharge).text(
				Partograph_CommonClass.getConvertedDateFormat(dischargeDate, Partograph_CommonClass.defdateformat));
		aq.id(R.id.ettimeofdischarge).text(Partograph_CommonClass.getCurrentTime());

		if (woman.getDel_type() != 0)
			aq.id(R.id.rddelivered).checked(true);
		else
			// {
			aq.id(R.id.rddip).checked(true);
		// aq.id(R.id.trreferred).visible();
		if (dbh.getisWomenReferred(woman.getWomenId(), woman.getUserId())) {
			aq.id(R.id.rdyes).checked(true);
			aq.id(R.id.rdyes).enabled(false);// 19Sep2017 Arpitha
			aq.id(R.id.rdno).enabled(false);// 19Sep2017 Arpitha
			womanReferral = 1;
			aq.id(R.id.reasonfordischarge).gone();
		} else {
			aq.id(R.id.rdno).checked(true);
			if (aq.id(R.id.rddip).isChecked())
				aq.id(R.id.reasonfordischarge).visible();
			womanReferral = 2;
		}

		// }

		displayDischrgeDetails();

		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		if (woman.getregtype() != 2)
			strdateOfAdm = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
		else {
			strdateOfAdm = woman.getDate_of_reg_entry();
			strdateOfAdm = strdateOfAdm.replace("/", " ");
		}

		timeOfAdm = formatTime.parse(strdateOfAdm);
		doa = formatDate.parse(strdateOfAdm);

		if (woman.getDel_type() != 0) {
			// strdelDate = woman.getDel_Date();
			strdelTime = woman.getDel_Date() + " " + woman.getDel_Time();
			dateOfDel = formatDate.parse(strdelTime);
			delTime = formatTime.parse(strdelTime);
		}
		strlastpartoEntry = dbh.getlastentrytime(woman.getWomenId(), 0);
		if (strlastpartoEntry != null)
			lastpartoENtryTime = formatTime.parse(strlastpartoEntry);

		if (strlastpartoEntry != null)
			lastpartoENtryDate = formatDate.parse(strlastpartoEntry);

		dpojoVal = dbh.getWomanDischargeData(woman.getUserId(), woman.getWomenId());
		if (dpojoVal != null)
			Partograph_CommonClass.dpojo = dpojoVal;

		strpostpartumdatetime = dbh.getPostPartumDateTime(woman.getWomenId(), woman.getUserId());// 20Sep2017
		// Arpitha

		// 19Sep2017 Arpitha
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		strrefdatetime = dbh.getrefdate(woman.getWomenId(), woman.getUserId());

		if (strrefdatetime != null) {
			refdatetime = sdf.parse(strrefdatetime);
		}

		strlatdatetime = dbh.getLatenPhaseDateTime(woman.getWomenId(), woman.getUserId());// 20Sep2017
		// Arpitha

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.email).setVisible(false);

		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(true);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);
		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		// menu.findItem(R.id.view).setVisible(true);
		menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			// case R.id.sync:
			// menuItem = item;
			// menuItem.setActionView(R.layout.progressbar);
			// menuItem.expandActionView();
			// calSyncMtd();
			// return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(DiscargeDetails_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(DiscargeDetails_Activity.this, woman.getUserId());
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));
				return true;
			// 22May2017 Arpitha - v2.6
			case R.id.summary:
				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);
				return true;
			case R.id.usermanual:
				Intent manual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(manual);
				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(this);
				return true;

			case R.id.disch:
				Intent disch = new Intent(DiscargeDetails_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(DiscargeDetails_Activity.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(DiscargeDetails_Activity.this, woman);
					// loadWomendata(displayListCountddel);
					// if(adapter!=null)
					// adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(DiscargeDetails_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(DiscargeDetails_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				// Intent ref = new Intent(DiscargeDetails_Activity.this,
				// ReferralInfo_Activity.class);
				// ref.putExtra("woman", woman);
				// startActivity(ref);
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(DiscargeDetails_Activity.this, woman);
				} else {
					Intent ref = new Intent(DiscargeDetails_Activity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(DiscargeDetails_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(DiscargeDetails_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(DiscargeDetails_Activity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(DiscargeDetails_Activity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(DiscargeDetails_Activity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.lat:
				Intent lat = new Intent(DiscargeDetails_Activity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.parto:
				Intent parto = new Intent(DiscargeDetails_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.home));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void displayConfirmationAlert_exit(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(DiscargeDetails_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(DiscargeDetails_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {

					Intent i = new Intent(DiscargeDetails_Activity.this, LoginActivity.class);
					startActivity(i);

				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		dialog.setCancelable(false);

	}

	void saveDischargeDetails() throws Exception {

		if (validateFileds()) {
			dpojo = new DiscgargePojo();

			if (isUpdateDetails) {
				dpojo.setWomanInvestigation(aq.id(R.id.etwomaninvestigation).getText().toString());
				dpojo.setConditionOfWoman(aq.id(R.id.etconditionofmother).getText().toString());
				dpojo.setChildInvestigation(aq.id(R.id.etchildinvestigation).getText().toString());
				dpojo.setChild2Investigation(aq.id(R.id.etchild2investigation).getText().toString());
				dpojo.setConditionOfChild(aq.id(R.id.etconditionofchild).getText().toString());
				dpojo.setConditionOfChild2(aq.id(R.id.etconditionofchild2).getText().toString());
				dpojo.setPlaceOfNextFOllowUp(aq.id(R.id.etplaceofnextfollowup).getText().toString());
				if (nextFollowUpDate != null && nextFollowUpDate.length() > 0)
					dpojo.setDateOfNextFollowUp(nextFollowUpDate);
				dpojo.setAdviceGiven(aq.id(R.id.etadvicegiven).getText().toString());
				dpojo.setDischargeDiagnosis(aq.id(R.id.etdiagnosis).getText().toString());
				dpojo.setProcedures(aq.id(R.id.etprocedures).getText().toString());
				dpojo.setComplicationObserved(aq.id(R.id.etcomplicationobserved).getText().toString());
				dpojo.setFindingHistorys(aq.id(R.id.etfindinghistory).getText().toString());
				dpojo.setActionTaken(aq.id(R.id.etactiontaken).getText().toString());
				dpojo.setPatientDisposition(aq.id(R.id.etdisposition).getText().toString());
				dpojo.setAdmissionReasons(aq.id(R.id.etadmreason).getText().toString());
				dpojo.setRemarks(aq.id(R.id.etremarks).getText().toString());
				dpojo.setHCProviderName(aq.id(R.id.etnameofhc).getText().toString());
				dpojo.setDesignation(aq.id(R.id.etdesignation).getText().toString());

				dpojo.setDischargeReasons(aq.id(R.id.etreasonfordischarge).getText().toString());

				dpojo.setUserId(woman.getUserId());
				dpojo.setWomanId(woman.getWomenId());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
				String currentDateandTime = sdf.format(new Date());

				dpojo.setDatelastupdated(currentDateandTime);

				dbh.db.beginTransaction();
				String discSql = "";
				int transId = dbh.iCreateNewTrans(woman.getUserId());

				discSql = dbh.updateDischargeData(dpojo, transId, Partograph_CommonClass.dpojo);

				if (discSql.length() > 0) {
					commitTrans();
					dbh.deletetblnotificationrow(woman.getWomenId(), woman.getUserId());// 12Oct2017
																						// Arpitha
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.discharge_details_updated_successfully),
							Toast.LENGTH_SHORT).show();
				} else
					// Partograph_CommonClass.
					rollbackTrans();

			} else {
				dpojo.setUserId(woman.getUserId());
				dpojo.setWomanId(woman.getWomenId());
				dpojo.setWomanName(woman.getWomen_name());
				dpojo.setWomanDeliveryStatus(womanDelStatus);
				dpojo.setReferred(womanReferral);
				dpojo.setDateTimeOfDischarge(dischargeDate + " " + aq.id(R.id.ettimeofdischarge).getText().toString());
				dpojo.setWomanInvestigation(aq.id(R.id.etwomaninvestigation).getText().toString());
				dpojo.setConditionOfWoman(aq.id(R.id.etconditionofmother).getText().toString());
				dpojo.setChildInvestigation(aq.id(R.id.etchildinvestigation).getText().toString());
				dpojo.setChild2Investigation(aq.id(R.id.etchild2investigation).getText().toString());
				dpojo.setConditionOfChild(aq.id(R.id.etconditionofchild).getText().toString());
				dpojo.setConditionOfChild2(aq.id(R.id.etconditionofchild2).getText().toString());
				dpojo.setPlaceOfNextFOllowUp(aq.id(R.id.etplaceofnextfollowup).getText().toString());
				if (nextFollowUpDate != null && nextFollowUpDate.length() > 0)
					dpojo.setDateOfNextFollowUp(nextFollowUpDate);
				dpojo.setAdviceGiven(aq.id(R.id.etadvicegiven).getText().toString());
				dpojo.setDischargeDiagnosis(aq.id(R.id.etdiagnosis).getText().toString());
				dpojo.setProcedures(aq.id(R.id.etprocedures).getText().toString());
				dpojo.setComplicationObserved(aq.id(R.id.etcomplicationobserved).getText().toString());
				dpojo.setFindingHistorys(aq.id(R.id.etfindinghistory).getText().toString());
				dpojo.setActionTaken(aq.id(R.id.etactiontaken).getText().toString());
				dpojo.setPatientDisposition(aq.id(R.id.etdisposition).getText().toString());
				dpojo.setAdmissionReasons(aq.id(R.id.etadmreason).getText().toString());
				dpojo.setRemarks(aq.id(R.id.etremarks).getText().toString());
				dpojo.setHCProviderName(aq.id(R.id.etnameofhc).getText().toString());
				dpojo.setDesignation(aq.id(R.id.etdesignation).getText().toString());

				dpojo.setDischargeReasons(aq.id(R.id.etreasonfordischarge).getText().toString());

				dbh.db.beginTransaction();
				int transId = dbh.iCreateNewTrans(woman.getUserId());
				boolean isDataInserted = dbh.addDischargeDetails(dpojo, transId);
				if (isDataInserted) {
					dbh.iNewRecordTrans(woman.getUserId(), transId, Partograph_DB.TBLdischargedetails);

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.discharge_details_added_successfully), Toast.LENGTH_SHORT)
							.show();
					commitTrans();

				} else {
					Partograph_CommonClass.rollbackTrans(DiscargeDetails_Activity.this);
				}
			}
		}

	}

	private boolean validateFileds() throws NotFoundException, Exception {
		// TODO Auto-generated method stub
		if (aq.id(R.id.etdateofdicharge).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_disch_date),
					DiscargeDetails_Activity.this);
			aq.id(R.id.etdateofdicharge).getEditText().requestFocus();
			return false;
		} else if (aq.id(R.id.ettimeofdischarge).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_disch_time),
					DiscargeDetails_Activity.this);
			aq.id(R.id.ettimeofdischarge).getEditText().requestFocus();
			return false;
		} else if (aq.id(R.id.etreasonfordischarge).getText().toString().trim().length() <= 0
				&& aq.id(R.id.rddip).isChecked() && aq.id(R.id.rdno).isChecked()) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_disch_reason),
					DiscargeDetails_Activity.this);
			aq.id(R.id.etreasonfordischarge).getEditText().requestFocus();
			return false;

		} else if (aq.id(R.id.etconditionofmother).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_condition_of_woman),
					DiscargeDetails_Activity.this);
			aq.id(R.id.etconditionofmother).getEditText().requestFocus();
			return false;
		}

		else if (aq.id(R.id.etconditionofchild).getText().toString().trim().length() <= 0
				&& aq.id(R.id.rddelivered).isChecked()) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_condition_of_child),
					DiscargeDetails_Activity.this);
			aq.id(R.id.etconditionofchild).getEditText().requestFocus();
			return false;
		}

		else if (aq.id(R.id.etconditionofchild2).getText().toString().trim().length() <= 0
				&& aq.id(R.id.rddelivered).isChecked() && woman.getNo_of_child() == 2) {
			Partograph_CommonClass.displayAlertDialog(
					getResources().getString(R.string.enter_condition_of_second_child), DiscargeDetails_Activity.this);
			aq.id(R.id.etconditionofchild2).getEditText().requestFocus();
			return false;
		}

		// 21Sep2017 Arpitha
		if(!isUpdateDetails)
		{
		if (aq.id(R.id.etdateofdicharge).getText().toString().trim().length() > 0
				&& aq.id(R.id.ettimeofdischarge).getText().toString().trim().length() > 0) {

			String strdiscdatetime = dischargeDate + " " + aq.id(R.id.ettimeofdischarge).getText().toString();

			if (strdateOfAdm != null && !(isDeliveryTimevalid(strdateOfAdm, strdiscdatetime, 1)))
				return false;

			if (strlastpartoEntry != null) {
				if (strlastpartoEntry.contains("_")) {
					strlastpartoEntry = strlastpartoEntry.replace("_", " ");
				}
				if (!isDeliveryTimevalid(strlastpartoEntry, strdiscdatetime, 2))
					return false;
			}

			String currenttime = Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime();

			if (!isDeliveryTimevalid(strdiscdatetime, currenttime, 3))
				return false;

			// 02Nov2016 Arpitha
			if (strrefdatetime != null) {
				if (!isDeliveryTimevalid(strrefdatetime, strdiscdatetime, 4))
					return false;
			} // 02Nov2016 Arpitha

			if (strpostpartumdatetime != null && !(isDeliveryTimevalid(strpostpartumdatetime, strdiscdatetime, 5)))
				return false;

			if (strdelTime != null && (!isDeliveryTimevalid(strdelTime, strdiscdatetime, 6)))
				return false;

			// 20Sep2017 Arpitha
			if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
				strlatdatetime = strlatdatetime.replace("/", " ");
				if (!isDeliveryTimevalid(strlatdatetime, strdiscdatetime, 7))
					return false;// 20Sep2017 Arpitha
			}

		}
		}

		return true;
	}

	public void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

		if (woman.getDel_type() == 0 && aq.id(R.id.rddelivered).isChecked()) {
			displayAlertDialog(getResources().getString(R.string.del_status_not_updated_would_you_like_to_update),
					"del");

		}

		else if (aq.id(R.id.rdyes).isChecked() && (!dbh.getisWomenReferred(woman.getWomenId(), woman.getUserId()))) {
			displayAlertDialog(getResources().getString(R.string.ref_info_not_updated_would_you_like_to_update), "ref");
		} else {
			Intent intent = new Intent(DiscargeDetails_Activity.this, DischargedWomanList_Activity.class);
			startActivity(intent);
		}
		dbh.deletetblnotificationrow(woman.getWomenId(), woman.getUserId());

		calSyncMtd();

	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	private class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.pickadate) + "</font>"));

			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {

					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date refdate = null;
			// 23oct2016 Arpitha
			if (strrefdatetime != null) {
				refdate = new SimpleDateFormat("yyyy-MM-dd").parse(strrefdatetime);
			}

			// 20Sep2017 Arpitha
			if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
				postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strpostpartumdatetime);
			} // 20Sep2017 Arpitha

			// 20Sep2017 Arpitha
			if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
				latDatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strlatdatetime);
			} // 20Sep2017 Arpitha

			Date selected = format.parse(selecteddate);
			if (discDate) {
				if (selected.after(new Date())) {
					Toast.makeText(context,
							getResources().getString(R.string.discharge_datetime_after_current_datetime) + " ",
							Toast.LENGTH_LONG).show();
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * discharge_datetime_after_current_datetime),
					 * DiscargeDetails_Activity.this);
					 */

				} else if (doa != null && selected.before(doa)) {
					Toast.makeText(context, getResources().getString(R.string.discharge_datetime_before_reg_datetime)
							+ " " + strdateOfAdm, Toast.LENGTH_LONG).show();
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * discharge_datetime_before_reg_datetime) + " " +
					 * timeOfAdm, DiscargeDetails_Activity.this);
					 */

				}

				// 20Sep2017 Arpitha
				else if (latDatetime != null && selected.before(latDatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.disc_datetime_cannot_be_beofre_latent_phase_datetime)
									+ " - " + latdate,
							Toast.LENGTH_SHORT).show();

					// etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
					// Partograph_CommonClass.defdateformat));
					// selected = todaysDate;

				} // 20Sep2017 Arpitha

				else if (dateOfDel != null && selected.before(dateOfDel)) {
					Toast.makeText(context, getResources().getString(R.string.discharge_datetime_before_del_datetime)
							+ " " + strdelTime, Toast.LENGTH_LONG).show();

				} else if (lastpartoENtryDate != null && selected.before(lastpartoENtryDate)) {
					Toast.makeText(context,
							getResources().getString(R.string.discharge_datetime_before_lastparto_datetime) + " "
									+ strlastpartoEntry,
							Toast.LENGTH_LONG).show();

				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && selected.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.discharge_datetime_cannot_be_before_postpartum_datetime)
									+ " - " + latdate,
							Toast.LENGTH_SHORT).show();

					/*
					 * etdateofreferral.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strRefDate =
					 * todaysDate;
					 */

				} // 20Sep2017 Arpitha

				// 19Sep2017 Arpitha
				else if (refdate != null && selected.before(refdate)) {

					String referraldate = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					Partograph_CommonClass.displayAlertDialog(getResources().getString(
							R.string.discharge_datetime_cannot_be_before_refer_dateral_datetime) + " " + referraldate,
							DiscargeDetails_Activity.this);

					/*
					 * etdeldate.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strDeldate =
					 * todaysDate;
					 */

				} // 19Sep2017 Arpitha

				else {
					aq.id(R.id.etdateofdicharge).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));

					dischargeDate = selecteddate;
				}
			} else {

				Date discDate = null;
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
				if (dvalues != null && dvalues.size() > 0) {

					discDate = date.parse(dvalues.get(0).getDateTimeOfDischarge().split("/")[0]);

				} else {
					if (dischargeDate != null && dischargeDate.length() > 0)
						discDate = date.parse(dischargeDate);
				}
				if (selected.before(doa)) {
					Toast.makeText(context, getResources().getString(R.string.nextfolowup_datetime_before_reg_datetime)
							+ " " + timeOfAdm, Toast.LENGTH_LONG).show();
					aq.id(R.id.etdateofnextfollowup).text("");
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * nextfolowup_datetime_before_reg_datetime) + " " +
					 * timeOfAdm, DiscargeDetails_Activity.this);
					 */

				} else if (dateOfDel != null && selected.before(dateOfDel)) {
					Toast.makeText(context, getResources().getString(R.string.nextfolowup_datetime_before_del_datetime)
							+ " " + strdelTime, Toast.LENGTH_LONG).show();
					aq.id(R.id.etdateofnextfollowup).text("");
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * nextfolowup_datetime_before_del_datetime) + " " +
					 * strdelTime, DiscargeDetails_Activity.this);
					 */

				} else if (lastpartoENtryDate != null && selected.before(lastpartoENtryDate)) {
					Toast.makeText(context,
							getResources().getString(R.string.nextfolowup_datetime_before_lastparto_datetime) + " "
									+ strlastpartoEntry,
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etdateofnextfollowup).text("");
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * nextfolowup_datetime_before_lastparto_datetime) + " " +
					 * strlastpartoEntry, DiscargeDetails_Activity.this);
					 */

				}

				// 19Sep2017 Arpitha
				else if (refdatetime != null && selected.before(refdatetime)) {

					String referraldate = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					Partograph_CommonClass.displayAlertDialog(
							getResources()
									.getString(R.string.discharge_datetime_cannot_be_before_refer_dateral_datetime)
									+ " " + referraldate + " " + strrefdatetime.split("_")[1],
							DiscargeDetails_Activity.this);

					/*
					 * etdeldate.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strDeldate =
					 * todaysDate;
					 */

				} // 19Sep2017 Arpitha

				else if ((discDate != null && selected.before(discDate)) || discDate.equals(selected)) {
					Toast.makeText(context,
							getResources().getString(R.string.date_of_nex_follow_up_cannot_be_before_disc_date) + " "
									+ discDate,
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etdateofnextfollowup).text("");
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * date_of_nex_follow_up_cannot_be_before_disc_date) + " " +
					 * discDate, DiscargeDetails_Activity.this);
					 */
				} else {
					aq.id(R.id.etdateofnextfollowup).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					nextFollowUpDate = selecteddate;
				}

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public void showtimepicker() {
		DialogFragment newFragment = new TimePickerFragment();
		FragmentManager fm = getFragmentManager();
		newFragment.show(fm, "timePicker");
	}

	private static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
		public TimePickerFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {

			// isTimeRefClicked = false;//19May2017 Arpitha - 2.6

			hour = hourOfDay;
			minute = minutes;

			try {

				// 20Sep2017 Arpitha
				if (strlatdatetime != null && strlatdatetime.trim().length() > 0) {
					strlatdatetime = strlatdatetime.replace("/", " ");
					latDatetime = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(strlatdatetime);
				} // 20Sep2017 Arpitha

				Date refdate = null;
				// 23oct2016 Arpitha
				if (strrefdatetime != null) {
					refdate = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(strrefdatetime);
				}

				discTime = dischargeDate + " "
						+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));

				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");

				Date selectedTime = format.parse(discTime);

				// 20Sep2017 Arpitha
				if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
					// strpostpartumdatetime =
					// strpostpartumdatetime.replace("/", newChar)
					postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strpostpartumdatetime);
				} // 20Sep2017 Arpitha

				if (selectedTime.after(new Date())) {
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * discharge_datetime_before_reg_datetime), context);
					 */
					Toast.makeText(context,
							getResources().getString(R.string.discharge_datetime_after_current_datetime),
							Toast.LENGTH_LONG).show();

				} else if (timeOfAdm != null && selectedTime.before(timeOfAdm)) {
					Toast.makeText(context,
							getResources().getString(R.string.discharge_datetime_before_reg_datetime) + " " + timeOfAdm,
							Toast.LENGTH_LONG).show();
					// Partograph_CommonClass.displayAlertDialog(
					// getResources().getString(R.string.discharge_datetime_before_del_datetime),
					// context);

				}

				// 20Sep2017 Arpitha
				else if (latDatetime != null && selectedTime.before(latDatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strlatdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(context,
							getResources().getString(R.string.disc_datetime_cannot_be_beofre_latent_phase_datetime)
									+ " - " + latdate + " " + strlatdatetime.split(" ")[1],
							Toast.LENGTH_SHORT).show();

					// etdeltime.setText(Partograph_CommonClass.getCurrentTime());

				} // 20Sep2017 Arpitha

				else if (delTime != null && selectedTime.before(delTime)) {
					Toast.makeText(context, getResources().getString(R.string.discharge_datetime_before_del_datetime)
							+ " " + strdelTime, Toast.LENGTH_LONG).show();
					// Partograph_CommonClass.displayAlertDialog(
					// getResources().getString(R.string.discharge_datetime_before_lastparto_datetime),
					// context);

				} else if (lastpartoENtryTime != null && selectedTime.before(lastpartoENtryTime)) {
					Toast.makeText(context,
							getResources().getString(R.string.discharge_datetime_before_lastparto_datetime) + " "
									+ strlastpartoEntry,
							Toast.LENGTH_LONG).show();
					// Partograph_CommonClass.displayAlertDialog(
					// getResources().getString(R.string.discharge_datetime_after_current_datetime),
					// context);

				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && selectedTime.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getActivity(),
							getResources().getString(R.string.discharge_datetime_cannot_be_before_postpartum_datetime)
									+ " - " + latdate + " " + strpostpartumdatetime.split(" ")[0],
							Toast.LENGTH_SHORT).show();

					/*
					 * etdateofreferral.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strRefDate =
					 * todaysDate;
					 */

				} // 20Sep2017 Arpitha

				// 19Sep2017 Arpitha
				else if (refdate != null && selectedTime.before(refdate)) {

					String referraldate = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					Partograph_CommonClass.displayAlertDialog(getResources()
							.getString(R.string.discharge_datetime_cannot_be_before_refer_dateral_datetime) + " "
							+ referraldate + " " + strrefdatetime.split(" ")[1], context);

					/*
					 * etdeldate.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strDeldate =
					 * todaysDate;
					 */

				} // 19Sep2017 Arpitha

				else {

					aq.id(R.id.ettimeofdischarge).getEditText().setText(
							new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));
				}
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

		}
	}

	// 26April2017 Arpitha
	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	void displayDischrgeDetails() {
		try {
			dvalues = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
			if (dvalues != null && dvalues.size() > 0) {
				isUpdateDetails = true;
				String discDate = dvalues.get(0).getDateTimeOfDischarge();
				aq.id(R.id.etdateofdicharge).text(Partograph_CommonClass
						.getConvertedDateFormat(discDate.split("\\s+")[0], Partograph_CommonClass.defdateformat));
				aq.id(R.id.ettimeofdischarge).text(discDate.split("\\s+")[1]);
				aq.id(R.id.etconditionofchild).text(dvalues.get(0).getConditionOfChild());
				aq.id(R.id.etconditionofchild2).text(dvalues.get(0).getConditionOfChild2());
				aq.id(R.id.etchildinvestigation).text(dvalues.get(0).getChildInvestigation());
				aq.id(R.id.etchild2investigation).text(dvalues.get(0).getChild2Investigation());
				aq.id(R.id.etwomaninvestigation).text(dvalues.get(0).getWomanInvestigation());
				aq.id(R.id.etconditionofmother).text(dvalues.get(0).getConditionOfWoman());
				aq.id(R.id.etadvicegiven).text(dvalues.get(0).getAdviceGiven());
				aq.id(R.id.etdiagnosis).text(dvalues.get(0).getDischargeDiagnosis());
				aq.id(R.id.etprocedures).text(dvalues.get(0).getProcedures());
				aq.id(R.id.etcomplicationobserved).text(dvalues.get(0).getComplicationObserved());
				aq.id(R.id.etfindinghistory).text(dvalues.get(0).getFindingHistorys());
				aq.id(R.id.etactiontaken).text(dvalues.get(0).getActionTaken());
				aq.id(R.id.etdisposition).text(dvalues.get(0).getPatientDisposition());
				aq.id(R.id.etadmreason).text(dvalues.get(0).getAdmissionReasons());
				aq.id(R.id.etremarks).text(dvalues.get(0).getRemarks());
				aq.id(R.id.etnameofhc).text(dvalues.get(0).getHCProviderName());
				aq.id(R.id.etdesignation).text(dvalues.get(0).getDesignation());
				aq.id(R.id.etreasonfordischarge).text(dvalues.get(0).getDischargeReasons());
				if (dvalues.get(0).getWomanDeliveryStatus() == 1)
					aq.id(R.id.rddip).checked(true);
				else if (dvalues.get(0).getWomanDeliveryStatus() == 2)
					aq.id(R.id.rddelivered).checked(true);

				aq.id(R.id.etplaceofnextfollowup).text(dvalues.get(0).getPlaceOfNextFOllowUp());
				if (dvalues.get(0).getDateOfNextFollowUp() != null
						&& dvalues.get(0).getDateOfNextFollowUp().trim().length() > 0)
					aq.id(R.id.etdateofnextfollowup).text(Partograph_CommonClass.getConvertedDateFormat(
							dvalues.get(0).getDateOfNextFollowUp(), Partograph_CommonClass.defdateformat));

				if (dvalues.get(0).getWomanDeliveryStatus() == 1) {
					// aq.id(R.id.trreferred).visible();
					aq.id(R.id.trchildcondition).gone();
					aq.id(R.id.trchildinvestigation).gone();
					if (dvalues.get(0).getReferred() == 2)
						aq.id(R.id.reasonfordischarge).visible();
					else
						aq.id(R.id.reasonfordischarge).gone();

				} else {
					// aq.id(R.id.trreferred).gone();
					aq.id(R.id.trchildcondition).visible();
					aq.id(R.id.trchildinvestigation).visible();
				}

				if (dvalues.get(0).getReferred() == 1) {
					// aq.id(R.id.trreferred).visible();
					aq.id(R.id.rdyes).checked(true);
				} else if (dvalues.get(0).getReferred() == 2) {
					// aq.id(R.id.trreferred).visible();
					aq.id(R.id.rdno).checked(true);

				}
				// else
				// aq.id(R.id.trreferred).gone();

				aq.id(R.id.etdateofdicharge).enabled(false);
				aq.id(R.id.ettimeofdischarge).enabled(false);

				aq.id(R.id.etdateofdicharge).textColor(getResources().getColor(R.color.fontcolor_disable));
				aq.id(R.id.ettimeofdischarge).textColor(getResources().getColor(R.color.fontcolor_disable));

				aq.id(R.id.rddip).enabled(false);
				aq.id(R.id.rddelivered).enabled(false);
				aq.id(R.id.rdyes).enabled(false);
				aq.id(R.id.rdno).enabled(false);

				// disableDischargeFields();

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	private void disableDischargeFields() {
		// TODO Auto-generated method stub
		aq.id(R.id.etdateofdicharge).enabled(false);
		aq.id(R.id.ettimeofdischarge).enabled(false);
		aq.id(R.id.etwomaninvestigation).enabled(false);
		aq.id(R.id.etchildinvestigation).enabled(false);
		aq.id(R.id.etchild2investigation).enabled(false);
		aq.id(R.id.etconditionofmother).enabled(false);
		aq.id(R.id.etconditionofchild).enabled(false);
		aq.id(R.id.etconditionofchild2).enabled(false);
		aq.id(R.id.etadvicegiven).enabled(false);
		aq.id(R.id.etdiagnosis).enabled(false);
		aq.id(R.id.etprocedures).enabled(false);
		aq.id(R.id.etcomplicationobserved).enabled(false);
		aq.id(R.id.etfindinghistory).enabled(false);
		aq.id(R.id.etactiontaken).enabled(false);
		aq.id(R.id.etdisposition).enabled(false);
		aq.id(R.id.etadmreason).enabled(false);
		aq.id(R.id.etremarks).enabled(false);
		aq.id(R.id.etnameofhc).enabled(false);
		aq.id(R.id.etdesignation).enabled(false);
		aq.id(R.id.etreasonfordischarge).enabled(false);
		aq.id(R.id.etplaceofnextfollowup).enabled(false);
		aq.id(R.id.etdateofnextfollowup).enabled(false);

		aq.id(R.id.rddip).enabled(false);
		aq.id(R.id.rddelivered).enabled(false);
		aq.id(R.id.rdyes).enabled(false);
		aq.id(R.id.rdno).enabled(false);

		aq.id(R.id.etdateofdicharge).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.ettimeofdischarge).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etwomaninvestigation).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etchildinvestigation).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etchild2investigation).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etconditionofmother).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etconditionofchild2).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etconditionofchild).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etadvicegiven).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etdiagnosis).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etprocedures).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etcomplicationobserved).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etfindinghistory).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etactiontaken).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etdisposition).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etadmreason).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etremarks).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etdesignation).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etreasonfordischarge).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etplaceofnextfollowup).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etdateofnextfollowup).textColor(getResources().getColor(R.color.fontcolor_disable));
		aq.id(R.id.etnameofhc).textColor(getResources().getColor(R.color.fontcolor_disable));

	}

	private void generateDischargeSlipinpdfFormat() {
		// TODO Auto-generated method stub
		Document doc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/DischargeSlip";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			file = new File(dir, woman.getWomen_name() + "_DischargeSlip.pdf");
			FileOutputStream fOut = new FileOutputStream(file);

			PdfWriter.getInstance(doc, fOut);

			doc.setMarginMirroring(false);
			// doc.setMargins(-25, -20, 65, -50);
			doc.setMargins(0, 10, 10, 0);

			doc.open();

			Paragraph p_heading = new Paragraph(context.getResources().getString(R.string.dischrage_slip), heading);
			// Font paraFont = new Font(Font.TIMES_ROMAN);
			// p_heading.setFont(paraFont);
			p_heading.setAlignment(Paragraph.ALIGN_CENTER);

			doc.add(p_heading);

			Drawable d = context.getResources().getDrawable(R.drawable.bcicon_small);

			BitmapDrawable bitDw = ((BitmapDrawable) d);

			Bitmap bmp = bitDw.getBitmap();

			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);

			Image image = Image.getInstance(stream.toByteArray());

			image.setIndentationLeft(10);
			doc.add(image);
			String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
					Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";
			Paragraph p1 = new Paragraph(strdatetime, small);
			p1.setAlignment(Paragraph.ALIGN_RIGHT);
			doc.add(p1);

			womanBasicInfo();

			// dischargeInformation();

			// dischargeInformation2();

			Paragraph basicInfo = new Paragraph("Woman Basic Information", heading);
			basicInfo.setAlignment(Paragraph.ALIGN_LEFT);
			basicInfo.setIndentationLeft(30);
			doc.add(basicInfo);
			doc.add(womanbasics);

			PdfPTable womanAddress = new PdfPTable(1);
			womanAddress.setWidthPercentage(80);
			womanAddress.setSpacingBefore(1);
			womanAddress.setSpacingAfter(1);
			womanAddress.addCell(getCellHeading(getResources().getString(R.string.address) + ": " + woman.getAddress(),
					PdfPCell.ALIGN_LEFT));
			// womanAddress.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
			// PdfPCell.ALIGN_CENTER));

			doc.add(womanAddress);

			if (woman.getDel_type() != 0) {
				Paragraph deliveryInfo = new Paragraph("Woman Delivery Information", heading);
				deliveryInfo.setAlignment(Paragraph.ALIGN_LEFT);
				deliveryInfo.setIndentationLeft(30);
				doc.add(deliveryInfo);
				DeliveryInformation();

				childInformation();

				doc.add(womandelInfo);

				Paragraph childInformation = new Paragraph("Child Information", heading);
				childInformation.setAlignment(Paragraph.ALIGN_LEFT);
				childInformation.setIndentationLeft(30);
				doc.add(childInformation);

				doc.add(childInfo);

				if (woman.getNo_of_child() == 2) {
					child2Information();
					Paragraph child2Information = new Paragraph("Second Child Information", heading);
					child2Information.setAlignment(Paragraph.ALIGN_LEFT);
					child2Information.setIndentationLeft(30);
					doc.add(child2Information);
					doc.add(childInfo2);
				}

			}

			Paragraph dischargedetials = new Paragraph("Discharge Details", heading);
			dischargedetials.setAlignment(Paragraph.ALIGN_LEFT);
			dischargedetials.setIndentationLeft(30);
			doc.add(dischargedetials);

			// doc.add(dischargeInfo);
			//
			// doc.add(dischargeInfo2);

			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(90);
			table.setSpacingBefore(10);
			// table.addCell(getCellHeading(getResources().getString(R.string.date_of_discharge),
			// PdfPCell.ALIGN_LEFT));
			// table.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
			// PdfPCell.ALIGN_CENTER));

			String date = "";
			if (dvalues.size() > 0) {
				date = Partograph_CommonClass
						.getConvertedDateFormat(dvalues.get(0).getDateTimeOfDischarge().split(" ")[0], "dd-MM-yyyy")
						+ "/" + dvalues.get(0).getDateTimeOfDischarge().split(" ")[1];
			}

			PdfPCell cellTwo = new PdfPCell(
					getCellHeading(getResources().getString(R.string.date_of_discharge), PdfPCell.ALIGN_LEFT));

			PdfPCell cellOne = new PdfPCell(getCell(date, PdfPCell.ALIGN_CENTER));

			cellOne.setPadding(5);
			cellTwo.setPadding(5);
			cellTwo.setBorder(Rectangle.BOX);
			cellOne.setBorder(Rectangle.BOX);

			table.addCell(cellTwo);
			table.addCell(cellOne);

			doc.add(table);

			PdfPTable womanInvestigation = new PdfPTable(2);
			// womanInvestigation.setSpacingBefore(5);
			womanInvestigation.setWidthPercentage(90);
			// womanInvestigation.addCell(
			// getCellHeading(getResources().getString(R.string.woman_investigations),
			// PdfPCell.ALIGN_LEFT));
			// womanInvestigation.addCell(getCell(dvalues.get(0).getWomanInvestigation(),
			// PdfPCell.ALIGN_CENTER));

			String investigation = "";

			if (dvalues.size() > 0) {

				investigation = dvalues.get(0).getWomanInvestigation();

			}

			PdfPCell cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.woman_investigations), PdfPCell.ALIGN_LEFT));

			PdfPCell cell2 = new PdfPCell(getCell(investigation, PdfPCell.ALIGN_CENTER));

			cell1.setPadding(5);
			cell2.setPadding(5);
			cell1.setBorder(Rectangle.BOX);
			cell2.setBorder(Rectangle.BOX);

			womanInvestigation.addCell(cell1);
			womanInvestigation.addCell(cell2);

			doc.add(womanInvestigation);

			/*
			 * PdfPTable childvestigation = new PdfPTable(2);
			 * childvestigation.setSpacingBefore(5);
			 * childvestigation.setWidthPercentage(90);
			 * childvestigation.addCell(
			 * getCellHeading(getResources().getString(R.string.
			 * child_investigation), PdfPCell.ALIGN_LEFT));
			 * childvestigation.addCell(getCell(dvalues.get(0).
			 * getChildInvestigation(), PdfPCell.ALIGN_CENTER));
			 * 
			 * doc.add(childvestigation);
			 */

			PdfPTable womanCondition = new PdfPTable(2);
			// womanCondition.setSpacingBefore(5);
			womanCondition.setWidthPercentage(90);
			// womanCondition.addCell(
			// getCellHeading(getResources().getString(R.string.condition_of_mother),
			// PdfPCell.ALIGN_LEFT));
			// womanCondition.addCell(getCell(dvalues.get(0).getConditionOfWoman(),
			// PdfPCell.ALIGN_CENTER));

			String womancondition = "";

			if (dvalues.size() > 0) {

				womancondition = dvalues.get(0).getConditionOfWoman();

			}

			PdfPCell womanConditioncell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.condition_of_mother), PdfPCell.ALIGN_LEFT));

			PdfPCell womanConditioncell2 = new PdfPCell(getCell(womancondition, PdfPCell.ALIGN_CENTER));

			womanConditioncell1.setPadding(5);
			womanConditioncell2.setPadding(5);
			womanConditioncell1.setBorder(Rectangle.BOX);
			womanConditioncell2.setBorder(Rectangle.BOX);

			womanCondition.addCell(womanConditioncell1);
			womanCondition.addCell(womanConditioncell2);

			doc.add(womanCondition);

			/*
			 * PdfPTable childCondition = new PdfPTable(2);
			 * childCondition.setSpacingBefore(5);
			 * childCondition.setWidthPercentage(90); childCondition.addCell(
			 * getCellHeading(getResources().getString(R.string.
			 * condition_of_child), PdfPCell.ALIGN_LEFT));
			 * childCondition.addCell(getCell(dvalues.get(0).getConditionOfChild
			 * (), PdfPCell.ALIGN_CENTER));
			 * 
			 * doc.add(childCondition);
			 */

			PdfPTable table10 = new PdfPTable(2);
			table10.setWidthPercentage(90);
			// table10.setSpacingBefore(5);
			// table10.addCell(
			// getCellHeading(getResources().getString(R.string.place_of_next_folow_up),
			// PdfPCell.ALIGN_LEFT));
			// table10.addCell(getCell(dvalues.get(0).getPlaceOfNextFOllowUp(),
			// PdfPCell.ALIGN_CENTER));
			String place = "";
			if (dvalues.size() > 0) {
				place = dvalues.get(0).getPlaceOfNextFOllowUp();

			}
			PdfPCell table10cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.place_of_next_folow_up), PdfPCell.ALIGN_LEFT));

			PdfPCell table10cell2 = new PdfPCell(getCell(place, PdfPCell.ALIGN_CENTER));

			table10cell1.setPadding(5);
			table10cell2.setPadding(5);
			table10cell1.setBorder(Rectangle.BOX);
			table10cell2.setBorder(Rectangle.BOX);

			table10.addCell(table10cell1);
			table10.addCell(table10cell2);
			doc.add(table10);

			PdfPTable table7 = new PdfPTable(2);
			table7.setWidthPercentage(90);
			// table7.setSpacingBefore(5);
			// table7.addCell(
			// getCellHeading(getResources().getString(R.string.date_of_next_folow_up),
			// PdfPCell.ALIGN_LEFT));
			// table7.addCell(getCell(dvalues.get(0).getDateOfNextFollowUp(),
			// PdfPCell.ALIGN_CENTER));
			String strdate = "";
			if (dvalues.size() > 0) {
				date = Partograph_CommonClass.getConvertedDateFormat(dvalues.get(0).getDateOfNextFollowUp(),
						"dd-MM-yyyy");
			}
			PdfPCell table7cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.date_of_next_folow_up), PdfPCell.ALIGN_LEFT));

			PdfPCell table7cell2 = new PdfPCell(getCell(strdate, PdfPCell.ALIGN_CENTER));

			table7cell1.setPadding(5);
			table7cell2.setPadding(5);
			table7cell1.setBorder(Rectangle.BOX);
			table7cell2.setBorder(Rectangle.BOX);

			table7.addCell(table7cell1);
			table7.addCell(table7cell2);
			doc.add(table7);

			PdfPTable table1 = new PdfPTable(2);
			table1.setWidthPercentage(90);
			// table1.setSpacingBefore(5);

			PdfPTable table9 = new PdfPTable(2);
			table9.setWidthPercentage(90);
			// table9.setSpacingBefore(5);
			// table9.addCell(getCellHeading(getResources().getString(R.string.advice_given),
			// PdfPCell.ALIGN_LEFT));
			// table9.addCell(getCell(dvalues.get(0).getAdviceGiven(),
			// PdfPCell.ALIGN_CENTER));
			String advice = "";

			PdfPCell table9cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.advice_given), PdfPCell.ALIGN_LEFT));

			if (dvalues.size() > 0) {
				advice = dvalues.get(0).getAdviceGiven();

			}
			PdfPCell table9cell2 = new PdfPCell(getCell(advice, PdfPCell.ALIGN_CENTER));

			table9cell1.setPadding(5);
			table9cell2.setPadding(5);
			table9cell1.setBorder(Rectangle.BOX);
			table9cell2.setBorder(Rectangle.BOX);

			table9.addCell(table9cell1);
			table9.addCell(table9cell2);
			doc.add(table9);

			// table1.addCell(getCellHeading(getResources().getString(R.string.procedures),
			// PdfPCell.ALIGN_LEFT));
			// table1.addCell(getCell(dvalues.get(0).getProcedures(),
			// PdfPCell.ALIGN_CENTER));
			String procdures = "";
			if (dvalues.size() > 0) {
				procdures = dvalues.get(0).getProcedures();

			}
			PdfPCell table1cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.procedures), PdfPCell.ALIGN_LEFT));

			PdfPCell table1cell2 = new PdfPCell(getCell(procdures, PdfPCell.ALIGN_CENTER));

			table1cell1.setPadding(5);
			table1cell2.setPadding(5);
			table1cell1.setBorder(Rectangle.BOX);
			table1cell2.setBorder(Rectangle.BOX);

			table1.addCell(table1cell1);
			table1.addCell(table1cell2);
			doc.add(table1);

			PdfPTable table11 = new PdfPTable(2);
			table11.setWidthPercentage(90);
			// table11.setSpacingBefore(5);
			// table11.addCell(
			// getCellHeading(getResources().getString(R.string.discharge_diagnosis),
			// PdfPCell.ALIGN_LEFT));
			// table11.addCell(getCell(dvalues.get(0).getDischargeDiagnosis(),
			// PdfPCell.ALIGN_CENTER));

			String diagnosis = "";
			if (dvalues.size() > 0) {
				diagnosis = dvalues.get(0).getDischargeDiagnosis();

			}

			PdfPCell table11cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.discharge_diagnosis), PdfPCell.ALIGN_LEFT));

			PdfPCell table11cell2 = new PdfPCell(getCell(diagnosis, PdfPCell.ALIGN_CENTER));

			table11cell1.setPadding(5);
			table11cell2.setPadding(5);
			table11cell1.setBorder(Rectangle.BOX);
			table11cell2.setBorder(Rectangle.BOX);

			table11.addCell(table11cell1);
			table11.addCell(table11cell2);
			doc.add(table11);

			PdfPTable table2 = new PdfPTable(2);
			// table2.setSpacingBefore(5);
			table2.setWidthPercentage(90);
			// table2.addCell(
			// getCellHeading(getResources().getString(R.string.complications_observed),
			// PdfPCell.ALIGN_LEFT));
			// table2.addCell(getCell(dvalues.get(0).getComplicationObserved(),
			// PdfPCell.ALIGN_CENTER));

			String compl = "";
			if (dvalues.size() > 0) {
				compl = dvalues.get(0).getComplicationObserved();
			}

			PdfPCell table2cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.complications_observed), PdfPCell.ALIGN_LEFT));

			PdfPCell table2cell2 = new PdfPCell(getCell(compl, PdfPCell.ALIGN_CENTER));

			table2cell1.setPadding(5);
			table2cell2.setPadding(5);
			table2cell1.setBorder(Rectangle.BOX);
			table2cell2.setBorder(Rectangle.BOX);

			table2.addCell(table2cell1);
			table2.addCell(table2cell2);
			doc.add(table2);

			PdfPTable table12 = new PdfPTable(2);
			// table12.setSpacingBefore(5);
			table12.setWidthPercentage(90);
			// table12.addCell(getCellHeading(getResources().getString(R.string.finding_history),
			// PdfPCell.ALIGN_LEFT));
			// table12.addCell(getCell(dvalues.get(0).getFindingHistorys(),
			// PdfPCell.ALIGN_CENTER));

			String history = "";
			if (dvalues.size() > 0) {
				history = dvalues.get(0).getFindingHistorys();

				// }
			}
			PdfPCell table12cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.finding_history), PdfPCell.ALIGN_LEFT));

			PdfPCell table12cell2 = new PdfPCell(getCell(history, PdfPCell.ALIGN_CENTER));

			table12cell1.setPadding(5);
			table12cell2.setPadding(5);
			table12cell1.setBorder(Rectangle.BOX);
			table12cell2.setBorder(Rectangle.BOX);

			table12.addCell(table12cell1);
			table12.addCell(table12cell2);
			doc.add(table12);

			PdfPTable table8 = new PdfPTable(2);
			// table8.setSpacingBefore(5);
			table8.setWidthPercentage(90);
			// table8.addCell(getCellHeading(getResources().getString(R.string.patient_disposition),
			// PdfPCell.ALIGN_LEFT));
			// table8.addCell(getCell(dvalues.get(0).getPatientDisposition(),
			// PdfPCell.ALIGN_CENTER));
			String dsposition = "";
			if (dvalues.size() > 0) {
				dsposition = dvalues.get(0).getPatientDisposition();
				// }
			}
			PdfPCell table8cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.patient_disposition), PdfPCell.ALIGN_LEFT));

			PdfPCell table8cell2 = new PdfPCell(getCell(dsposition, PdfPCell.ALIGN_CENTER));

			table8cell1.setPadding(5);
			table8cell2.setPadding(5);
			table8cell1.setBorder(Rectangle.BOX);
			table8cell2.setBorder(Rectangle.BOX);

			table8.addCell(table8cell1);
			table8.addCell(table8cell2);
			doc.add(table8);

			PdfPTable table13 = new PdfPTable(2);
			// table13.setSpacingBefore(5);
			table13.setWidthPercentage(90);
			// table13.addCell(getCellHeading(getResources().getString(R.string.admisiion_reasons),
			// PdfPCell.ALIGN_LEFT));
			// table13.addCell(getCell(dvalues.get(0).getAdmissionReasons(),
			// PdfPCell.ALIGN_CENTER));
			String reason = " ";
			if (dvalues.size() > 0) {
				reason = dvalues.get(0).getAdmissionReasons();
			}
			PdfPCell table13cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.admisiion_reasons), PdfPCell.ALIGN_LEFT));

			PdfPCell table13cell2 = new PdfPCell(getCell(reason, PdfPCell.ALIGN_CENTER));

			table13cell1.setPadding(5);
			table13cell2.setPadding(5);
			table13cell1.setBorder(Rectangle.BOX);
			table13cell2.setBorder(Rectangle.BOX);

			table13.addCell(table13cell1);
			table13.addCell(table13cell2);
			doc.add(table13);

			PdfPTable table3 = new PdfPTable(2);
			table3.setWidthPercentage(90);
			// table3.setSpacingBefore(5);
			// table3.addCell(getCellHeading(getResources().getString(R.string.overall_remarks),
			// PdfPCell.ALIGN_LEFT));
			// table3.addCell(getCell(dvalues.get(0).getRemarks(),
			// PdfPCell.ALIGN_CENTER));
			String remarks = " ";
			if (dvalues.size() > 0) {
				remarks = dvalues.get(0).getRemarks();

			}
			PdfPCell table3cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.overall_remarks), PdfPCell.ALIGN_LEFT));

			PdfPCell table3cell2 = new PdfPCell(getCell(remarks, PdfPCell.ALIGN_CENTER));

			table3cell1.setPadding(5);
			table3cell2.setPadding(5);
			table3cell1.setBorder(Rectangle.BOX);
			table3cell2.setBorder(Rectangle.BOX);

			table3.addCell(table3cell1);
			table3.addCell(table3cell2);
			doc.add(table3);

			PdfPTable nameofHC = new PdfPTable(2);
			nameofHC.setWidthPercentage(90);
			// nameofHC.setSpacingBefore(5);
			// nameofHC.addCell(getCellHeading(getResources().getString(R.string.name_of_healthcare_provider),
			// PdfPCell.ALIGN_LEFT));
			// nameofHC.addCell(getCell(dvalues.get(0).getHCProviderName(),
			// PdfPCell.ALIGN_CENTER));
			String name = " ";
			if (dvalues.size() > 0) {
				name = dvalues.get(0).getHCProviderName();

			}
			PdfPCell nameofHCcell1 = new PdfPCell(getCellHeading(
					getResources().getString(R.string.name_of_healthcare_provider), PdfPCell.ALIGN_LEFT));

			PdfPCell nameofHCcell2 = new PdfPCell(getCell(name, PdfPCell.ALIGN_CENTER));

			nameofHCcell1.setPadding(5);
			nameofHCcell2.setPadding(5);
			nameofHCcell1.setBorder(Rectangle.BOX);
			nameofHCcell2.setBorder(Rectangle.BOX);

			nameofHC.addCell(nameofHCcell1);
			nameofHC.addCell(nameofHCcell2);
			doc.add(nameofHC);

			PdfPTable designationofHC = new PdfPTable(2);
			designationofHC.setWidthPercentage(90);

			// designationofHC.setSpacingBefore(5);
			// designationofHC.addCell(getCellHeading(
			// getResources().getString(R.string.designation_of_healthcare_provider),
			// PdfPCell.ALIGN_LEFT));
			// designationofHC.addCell(getCell(dvalues.get(0).getDesignation(),
			// PdfPCell.ALIGN_CENTER));

			String desig = " ";
			if (dvalues.size() > 0) {
				desig = dvalues.get(0).getDesignation();

			}
			PdfPCell designationofHCcell1 = new PdfPCell(getCellHeading(
					getResources().getString(R.string.designation_of_healthcare_provider), PdfPCell.ALIGN_LEFT));

			PdfPCell designationofHCcell2 = new PdfPCell(getCell(desig, PdfPCell.ALIGN_CENTER));

			designationofHCcell1.setPadding(5);
			designationofHCcell2.setPadding(5);
			designationofHCcell1.setBorder(Rectangle.BOX);
			designationofHCcell2.setBorder(Rectangle.BOX);

			designationofHC.addCell(designationofHCcell1);
			designationofHC.addCell(designationofHCcell2);
			doc.add(designationofHC);

			PdfPTable signatureofHC = new PdfPTable(1);
			signatureofHC.setWidthPercentage(90);
			signatureofHC.setSpacingBefore(45);
			signatureofHC.addCell(getCellHeading(getResources().getString(R.string.signature_of_health_care_provider),
					PdfPCell.ALIGN_RIGHT));

			doc.add(signatureofHC);

			/*
			 * PdfPTable table = new PdfPTable(2); table.setWidthPercentage(98);
			 * table.addCell(getCell(getResources().getString(R.string.
			 * date_of_discharge) + ": " +
			 * dvalues.get(0).getDateTimeOfDischarge(), PdfPCell.ALIGN_LEFT));
			 * table.addCell(getCell(getResources().getString(R.string.
			 * place_of_next_folow_up) + ": " +
			 * dvalues.get(0).getPlaceOfNextFOllowUp(), PdfPCell.ALIGN_RIGHT));
			 * 
			 * doc.add(table);
			 * 
			 * PdfPTable table7 = new PdfPTable(2);
			 * table7.setWidthPercentage(98);
			 * table7.addCell(getCell(getResources().getString(R.string.
			 * date_of_next_folow_up) + ": " +
			 * dvalues.get(0).getDateOfNextFollowUp(), PdfPCell.ALIGN_LEFT));
			 * table7.addCell(
			 * getCell(getResources().getString(R.string.advice_given) + ": " +
			 * dvalues.get(0).getAdviceGiven(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table7);
			 * 
			 * PdfPTable table1 = new PdfPTable(2);
			 * table1.setWidthPercentage(98);
			 * 
			 * table1.addCell(
			 * getCell(getResources().getString(R.string.procedures) + ": " +
			 * dvalues.get(0).getProcedures(), PdfPCell.ALIGN_LEFT));
			 * table1.addCell(getCell(getResources().getString(R.string.
			 * discharge_diagnosis) + ": " +
			 * dvalues.get(0).getDischargeDiagnosis(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table1);
			 * 
			 * PdfPTable table2 = new PdfPTable(2);
			 * table2.setWidthPercentage(98);
			 * table2.addCell(getCell(getResources().getString(R.string.
			 * complications_observed) + ": " +
			 * dvalues.get(0).getComplicationObserved(), PdfPCell.ALIGN_LEFT));
			 * table2.addCell(getCell(
			 * getResources().getString(R.string.finding_history) + ": " +
			 * dvalues.get(0).getFindingHistorys(), PdfPCell.ALIGN_RIGHT));
			 * 
			 * doc.add(table2);
			 * 
			 * PdfPTable table8 = new PdfPTable(2);
			 * table8.setWidthPercentage(98);
			 * table8.addCell(getCell(getResources().getString(R.string.
			 * patient_disposition) + ": " +
			 * dvalues.get(0).getPatientDisposition(), PdfPCell.ALIGN_LEFT));
			 * table8.addCell(getCell(
			 * getResources().getString(R.string.admisiion_reasons) + ": " +
			 * dvalues.get(0).getAdmissionReasons(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table8);
			 * 
			 * PdfPTable table3 = new PdfPTable(1);
			 * table3.setWidthPercentage(98);
			 * 
			 * table3.addCell(
			 * getCell(getResources().getString(R.string.overall_remarks) + ": "
			 * + dvalues.get(0).getRemarks(), PdfPCell.ALIGN_LEFT)); //
			 * table3.addCell(getCell(getResources().getString(R.string.
			 * patient_disposition) // + ": " // +
			 * dvalues.get(0).getPatientDisposition(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table3);
			 */

			// Rectangle rect = new Rectangle(100, 100, 900, 500);
			// Rectangle rect = new Rectangle(5, 5, 500, 500);
			// Rectangle rect = new Rectangle(5, 5, 585, 770);
			Rectangle rect = new Rectangle(10, 20, 585, 808);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.setBorder(2);
			// rect.setBorderColor(context.getResources().getColor(R.color.black));
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(1);
			doc.add(rect);

			doc.close();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	void womanBasicInfo() {
		try {
			String delstatus;
			ArrayList<String> val = new ArrayList<String>();
			val.add(context.getResources().getString(R.string.name) + ":" + woman.getWomen_name());
			val.add(context.getResources().getString(R.string.age) + ":" + woman.getAge()
					+ context.getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {// 12Jan2017 Arpitha
				String strDOA = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				val.add(context.getResources().getString(R.string.doadm) + ":" + strDOA + "/"
						+ woman.getTime_of_admission());
			} else {// 12Jan2017 Arpitha
				val.add(context.getResources().getString(R.string.doadm) + ":" + woman.getDate_of_reg_entry());
			}
			if (woman.getGestationage() == 0)
				val.add(context.getResources().getString(R.string.gest_short_label) + " "
						+ context.getResources().getString(R.string.notknown));
			else
				val.add(woman.getGestationage() + context.getResources().getString(R.string.wks) + " "
						+ woman.getGest_age_days() + context.getResources().getString(R.string.days));

			womanbasics = new PdfPTable(val.size());

			womanbasics.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < val.size(); k++) {
				String header = val.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			ArrayList<String> arrVal = new ArrayList<String>();
			arrVal.add(context.getResources().getString(R.string.gravida) + ":" + woman.getGravida());
			arrVal.add(context.getResources().getString(R.string.para) + ":" + woman.getPara());
			arrVal.add(context.getResources().getString(R.string.facility) + ": " + woman.getFacility_name() + "["
					+ woman.getFacility() + "]");
			if (woman.getDel_type() != 0) {
				delstatus = context.getResources().getString(R.string.delivered);
			} else
				delstatus = context.getResources().getString(R.string.del_prog);
			arrVal.add(delstatus);

			for (int k = 0; k < arrVal.size(); k++) {
				String header = arrVal.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.BOLD)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			womanbasics.setSpacingBefore(3);

			womanbasics.setWidthPercentage(90);

			womanbasics.completeRow();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	void DeliveryInformation() throws NotFoundException, Exception {

		if (woman.getDel_type() != 0) {
			ArrayList<String> deliveryInfo = new ArrayList<String>();
			String delType = "";
			if (woman.getDel_type() == 1)
				delType = "Normal";
			else if (woman.getDel_type() == 2)
				delType = "Cesarean";
			else if (woman.getDel_type() == 3)
				delType = "Instrumental";

			deliveryInfo.add(delType);

			if (woman.getDelTypeReason() != null) {
				String[] delTypeRes = woman.getDelTypeReason().split(",");
				String[] delTypeResArr = null;
				if (woman.getDel_type() == 2)
					delTypeResArr = getResources().getStringArray(R.array.spnreasoncesareanValues);
				else if (woman.getDel_type() == 3)
					delTypeResArr = getResources().getStringArray(R.array.spnreasoninstrumental);

				String delTypeResDisplay = "";
				if (delTypeResArr != null) {
					if (delTypeRes != null && delTypeRes.length > 0) {
						for (String str : delTypeRes) {
							if (str != null && str.length() > 0)
								delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
						}
					}
				}

				deliveryInfo.add(delTypeResDisplay);
			}

			deliveryInfo.add(context.getResources().getString(R.string.dod) + ": " + Partograph_CommonClass
					.getConvertedDateFormat(woman.getDel_Date(), Partograph_CommonClass.defdateformat) + " "
					+ woman.getDel_Time());

			deliveryInfo.add(context.getResources().getString(R.string.noofchild) + ": " + "" + woman.getNo_of_child());

			womandelInfo = new PdfPTable(deliveryInfo.size());

			// womandelInfo.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < deliveryInfo.size(); k++) {
				String header = deliveryInfo.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.BOLD, 10, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womandelInfo.addCell(cell);

				womandelInfo.setSpacingBefore(6);

			}

			womandelInfo.setWidthPercentage(90);
			womandelInfo.completeRow();

			// }

		}

	}

	void childInformation() throws DocumentException {

		ArrayList<String> valChildInfo = new ArrayList<String>();

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());
		if (dvalues.size() > 0) {
			valChildInfo.add(context.getResources().getString(R.string.condition_of_child) + ": "
					+ dvalues.get(0).getConditionOfChild());
			valChildInfo.add(context.getResources().getString(R.string.child_investigation) + ": "
					+ dvalues.get(0).getChildInvestigation());
		}

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());

		childInfo = new PdfPTable(valChildInfo.size());

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valChildInfo.size(); k++) {
			String header = valChildInfo.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			childInfo.addCell(cell);

			childInfo.setSpacingBefore(6);

		}
		childInfo.setWidthPercentage(90);
		childInfo.completeRow();

	}

	void child2Information() throws DocumentException {

		ArrayList<String> valChildInfo = new ArrayList<String>();

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());
		if (dvalues.size() > 0) {
			valChildInfo.add(context.getResources().getString(R.string.condition_of_child) + ": "
					+ dvalues.get(0).getConditionOfChild2());
			valChildInfo.add(context.getResources().getString(R.string.child_investigation) + ": "
					+ dvalues.get(0).getChild2Investigation());
		}

		// valChildInfo.add(context.getResources().getString(R.string.noofchild)
		// + ": " + "" + woman.getNo_of_child());

		childInfo2 = new PdfPTable(valChildInfo.size());

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valChildInfo.size(); k++) {
			String header = valChildInfo.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			childInfo2.addCell(cell);

			childInfo2.setSpacingBefore(6);

		}
		childInfo2.setWidthPercentage(90);
		childInfo2.completeRow();

	}

	void dischargeInformation() throws DocumentException {

		ArrayList<String> valDischargeInfo = new ArrayList<String>();

		if (dvalues.size() > 0) {
			valDischargeInfo.add(context.getResources().getString(R.string.date_of_discharge) + ": " + ""
					+ dvalues.get(0).getDateTimeOfDischarge());

			valDischargeInfo.add(context.getResources().getString(R.string.place_of_next_folow_up) + ": " + ""
					+ dvalues.get(0).getConditionOfChild());
			valDischargeInfo.add(context.getResources().getString(R.string.date_of_next_folow_up) + ": " + ""
					+ dvalues.get(0).getChildInvestigation());
		}

		valDischargeInfo
				.add(context.getResources().getString(R.string.advice_given) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.procedures) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo.add(
				context.getResources().getString(R.string.discharge_diagnosis) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo.add(
				context.getResources().getString(R.string.complications_observed) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.finding_history) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo.add(
				context.getResources().getString(R.string.patient_disposition) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.admisiion_reasons) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.overall_remarks) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo
				.add(context.getResources().getString(R.string.advice_given) + ": " + "" + woman.getNo_of_child());

		dischargeInfo = new PdfPTable(valDischargeInfo.size());

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valDischargeInfo.size(); k++) {
			String header = valDischargeInfo.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			dischargeInfo.addCell(cell);

			dischargeInfo.setSpacingBefore(6);

		}
		dischargeInfo.completeRow();

	}

	void dischargeInformation2() throws DocumentException {

		ArrayList<String> valDischargeInfo2 = new ArrayList<String>();

		/*
		 * if (dvalues.size() > 0) {
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * date_of_discharge) + ": " + "" +
		 * dvalues.get(0).getDateTimeOfDischarge());
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * place_of_next_folow_up) + ": " + "" +
		 * dvalues.get(0).getConditionOfChild());
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * date_of_next_folow_up) + ": " + "" +
		 * dvalues.get(0).getChildInvestigation()); }
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * advice_given) + ": " + "" + woman.getNo_of_child());
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * procedures) + ": " + "" + woman.getNo_of_child());
		 * 
		 * valDischargeInfo.add(context.getResources().getString(R.string.
		 * discharge_diagnosis) + ": " + "" + woman.getNo_of_child());
		 */

		valDischargeInfo2.add(
				context.getResources().getString(R.string.complications_observed) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.finding_history) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2.add(
				context.getResources().getString(R.string.patient_disposition) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.admisiion_reasons) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.overall_remarks) + ": " + "" + woman.getNo_of_child());

		valDischargeInfo2
				.add(context.getResources().getString(R.string.advice_given) + ": " + "" + woman.getNo_of_child());

		dischargeInfo2 = new PdfPTable(valDischargeInfo2.size());

		// childInfo.setWidths(new int[] { 7, 5, 11, 11 });

		for (int k = 0; k < valDischargeInfo2.size(); k++) {
			String header = valDischargeInfo2.get(k);
			PdfPCell cell = new PdfPCell();
			// valChildInfo.add(context.getResources().getString(R.string.noofchild)
			// + ": " + "" + woman.getNo_of_child());
			cell.setPhrase(new Phrase(header, new Font(Font.HELVETICA, 10, Font.NORMAL)));
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// cell.setPadding(2);
			cell.setBorder(0);

			dischargeInfo2.addCell(cell);

			dischargeInfo2.setSpacingBefore(6);

		}
		dischargeInfo2.completeRow();

	}

	public PdfPCell getCellHeading(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.HELVETICA, 10, Font.BOLD)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.HELVETICA, 10, Font.NORMAL)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	// Alert dialog
	public void displayAlertDialog(String message, final String classname) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
						if (classname.equalsIgnoreCase("del")) {
							updateDel = true;
							Intent del = new Intent(DiscargeDetails_Activity.this, DeliveryInfo_Activity.class);
							del.putExtra("woman", woman);
							startActivity(del);
						}
						if (classname.equalsIgnoreCase("ref")) {
							updateReferral = true;
							Intent ref = new Intent(DiscargeDetails_Activity.this, ReferralInfo_Activity.class);
							ref.putExtra("woman", woman);
							startActivity(ref);
						}
					}
				});

		alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing

						dialog.cancel();
						Intent home = new Intent(DiscargeDetails_Activity.this, DischargedWomanList_Activity.class);
						startActivity(home);

					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show ite
		alertDialog.show();
	}

	void displayAlertDialog() {

		final Dialog dialog = new Dialog(DiscargeDetails_Activity.this);

		dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dialog_action);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.dischrage_slip));

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		final RadioButton rdsavepdf = (RadioButton) dialog.findViewById(R.id.rdsave);
		final RadioButton rdviewpdf = (RadioButton) dialog.findViewById(R.id.rdview);
		Button btndone = (Button) dialog.findViewById(R.id.btndone);

		final RadioButton rdshare = (RadioButton) dialog.findViewById(R.id.rdshare);

		btndone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (rdsavepdf.isChecked()) {
					try {
						dialog.cancel();
						generateDischargeSlipinpdfFormat();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.dischrage_slip_is_saved), Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} else if (rdviewpdf.isChecked())

				{
					try {
						dialog.cancel();
						generateDischargeSlipinpdfFormat();
						// to view pdf file from appliction
						if (file.exists()) {

							Uri pdfpath = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfpath, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {// 24Jan2017
																	// Arpitha
								// No application to view, ask to download one
								AlertDialog.Builder builder = new AlertDialog.Builder(DiscargeDetails_Activity.this);
								builder.setTitle("No Application Found");
								builder.setMessage("Download one from Android Market?");
								builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketIntent = new Intent(Intent.ACTION_VIEW);
										marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
										startActivity(marketIntent);
									}
								});
								builder.setNegativeButton("No, Thanks", null);
								builder.create().show();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} // 24Jan2017
				/*
				 * else if (rdshare.isChecked()) { SendviaBluetoth();
				 * 
				 * }
				 */

				else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_one_option),
							Toast.LENGTH_LONG).show();
				}

			}
		});
		dialog.show();

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {
			dateofreg = sdf.parse(date1);
			dateofdelivery = sdf.parse(date2);

			String strDate1 = Partograph_CommonClass.getConvertedDateFormat(date1,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha
			strDate1 = strDate1 + " " + date1.substring(11);

			String strDate2 = Partograph_CommonClass.getConvertedDateFormat(date2,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha
			strDate2 = strDate2 + " " + date2.substring(11);

			if (dateofdelivery.before(dateofreg)) {
				if (i == 1)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.discharge_datetime_before_reg_datetime) + " - "
									+ strDate1,
							context);
				else if (i == 2)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.discharge_datetime_before_lastparto_datetime) + " - "
									+ strDate1,
							context);
				else if (i == 3)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.discharge_datetime_after_current_datetime), context);
				// 02Nov2016 Arpitha
				else if (i == 4)
					Partograph_CommonClass.displayAlertDialog(getResources()
							.getString(R.string.discharge_datetime_cannot_be_before_refer_dateral_datetime), context);
				else if (i == 5)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.discharge_datetime_cannot_be_before_postpartum_datetime),
							context);
				else if (i == 6)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.discharge_datetime_before_del_datetime), context);
				else if (i == 7)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.disc_datetime_cannot_be_beofre_latent_phase_datetime),
							context);
				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				// toa =
				// Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoa)
						.text(getResources().getString(R.string.reg) + ": " + doa + "/" + woman.getTime_of_admission());
			} else {

				aq.id(R.id.wdoa).text(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}

			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));
			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				aq.id(R.id.trdelstatus).visible();

				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);

				String gender = "";
				if (woman.getBabysex1() == 0)
					gender = getResources().getString(R.string.male);
				else
					gender = getResources().getString(R.string.female);

				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus).text(getResources().getString(R.string.del) + ":"
						+ deltype[woman.getDel_type() - 1] + "," + dod + "/" + woman.getDel_Time() + "," + gender);

				String gender2 = "";
				if (woman.getBabysex2() == 0)
					gender = getResources().getString(R.string.male);
				else
					gender = getResources().getString(R.string.female);
				if (woman.getNo_of_child() == 2)
					aq.id(R.id.wdeldate).text(getResources().getString(R.string.second_baby) + ": "
							+ woman.getDel_time2() + "," + gender2);

				// tod =
				// Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				// aq.id(R.id.wdeldate)
				// .text(getResources().getString(R.string.del) + ": " + dod +
				// "/" + woman.getDel_Time());

			}

		}
	}

}
