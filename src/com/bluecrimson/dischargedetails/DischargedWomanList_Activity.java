package com.bluecrimson.dischargedetails;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.comprehensiveview.View_Partograph;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.regwomenlist.RecentCustomListAdapterSearch;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.AdditionalDetails_Activity;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.partograph.ActionItem;
import com.bluecrimson.partograph.QuickAction;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.SearchView;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DischargedWomanList_Activity extends Activity implements SearchView.OnQueryTextListener {

	AQuery aq;
	Partograph_DB dbh;
	UserPojo user;
	private int totalCount = 0;
	int displayListCount = 10;
	ArrayList<Women_Profile_Pojo> rowItems;
	QuickAction mQuickAction;
	Cursor cursor;
	RecentCustomListAdapterSearch adapterDisc;
	SearchView searchDiscahrgedwomen;
	public static final int ID_PROFILE = 1;
	public static final int ID_GRAPH = 2;
	public static final int ID_DELSTATUS = 3;
	public static final int ID_APGAR = 4;
	public static final int ID_COMPREHENSIVEVIEW = 5;
	public static final int ID_REFERRAL = 6;
	public static final int ID_STAGEOFLABOR = 7;
	public static final int ID_PRINTPARTO = 8;
	public static final int ID_ADDITIONALDETAILS = 9;
	public static final int ID_DISCHARGEDETAILS = 10;
	Women_Profile_Pojo woman;
	public static final int ID_LATENTPHASE = 11;// 10Aug2017 Arpitha
	public static final int ID_POSTPARTUM = 12;// 18Aug2017 Arpitha
	public static final int ID_PDF = 13;// 10Aug2017 Arpitha
	public static int disclist;
	public static boolean isdisclist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dischargedwomanlist);

		try {

			aq = new AQuery(this);
			dbh = Partograph_DB.getInstance(this);

			disclist = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			isdisclist = true;// 03Sep2017 Arpitha

			initializeScreen(aq.id(R.id.rltoday).getView());
			user = Partograph_CommonClass.user;
			// searchDiscahrgedwomen = (SearchView)
			// findViewById(R.id.search_view_dischargedwomen);
			// searchDiscahrgedwomen.setOnQueryTextListener(this);
			initialView();

			aq.id(R.id.listdischargedwomenlist).getListView().setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
					try {

						woman = (Women_Profile_Pojo) adapter.getItemAtPosition(pos);

						mQuickAction.show(v);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			mQuickAction = new QuickAction(this);
			displayOptions();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 KillAllActivitesAndGoToLogin.addToStack(this);
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}
		return result;
	}

	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// 01Jun2017 Arpitha
		String mainSql = "Select * from " + Partograph_DB.TBL_REGISTEREDWOMEN + " where  user_Id = '" + user.getUserId()
				+ "'  and women_id  IN(Select discwomanId from tbldischargedetails) ";

		totalCount = dbh.getDisplayWomanListCount(mainSql);// 01Jun2017 Arpitha

		loadWomendata(displayListCount);

	}

	// Load women Data//01Jun2017 Arpitha - add argument
	private void loadWomendata(int displayListCount) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		rowItems = new ArrayList<Women_Profile_Pojo>();
		Women_Profile_Pojo wdata;

		cursor = dbh.getdischargewomenList(user.getUserId(), displayListCount);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {

				// int pos = cursor.getPosition();
				// if (pos <= displayListCount)
				{
					wdata = new Women_Profile_Pojo();
					wdata.setWomenId(cursor.getString(1));

					if (cursor.getType(2) > 0) {
						String b = (cursor.getString(2).length() > 1) ? cursor.getString(2) : null;
						byte[] decoded = (b != null) ? Base64.decode(b, Base64.DEFAULT) : null;
						wdata.setWomen_Image(decoded);
					}

					wdata.setWomen_name(cursor.getString(3));
					wdata.setDate_of_admission(cursor.getString(4));
					wdata.setTime_of_admission(cursor.getString(5));
					wdata.setAge(cursor.getInt(6));
					wdata.setAddress(cursor.getString(7));
					wdata.setPhone_No(cursor.getString(8));
					wdata.setDel_type(cursor.getInt(9));
					wdata.setDoc_name(cursor.getString(10));
					wdata.setNurse_name(cursor.getString(11));
					wdata.setW_attendant(cursor.getString(12));
					wdata.setGravida(cursor.getInt(13));
					wdata.setPara(cursor.getInt(14));
					wdata.setHosp_no(cursor.getString(15));
					wdata.setFacility(cursor.getString(16));
					wdata.setWomenId(cursor.getString(1));
					wdata.setSpecial_inst(cursor.getString(17));
					wdata.setUserId(cursor.getString(0));
					wdata.setComments(cursor.getString(19));
					wdata.setRisk_category(cursor.getInt(20));
					wdata.setDel_Comments(cursor.getString(21));
					wdata.setDel_Time(cursor.getString(22));
					wdata.setDel_Date(cursor.getString(23));
					wdata.setDel_result1(cursor.getInt(24));
					wdata.setNo_of_child(cursor.getInt(25));
					wdata.setBabywt1(cursor.getInt(26));
					wdata.setBabywt2(cursor.getInt(27));
					wdata.setBabysex1(cursor.getInt(28));
					wdata.setBabysex2(cursor.getInt(29));
					wdata.setGestationage(cursor.getInt(30));
					wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
					wdata.setDel_result2(cursor.getInt(31));
					wdata.setAdmitted_with(cursor.getString(34));
					wdata.setMemb_pres_abs(cursor.getInt(35));
					wdata.setMothers_death_reason(cursor.getString(36));
					wdata.setState(cursor.getString(38));
					wdata.setDistrict(cursor.getString(39));
					wdata.setTaluk(cursor.getString(40));
					wdata.setFacility_name(cursor.getString(41));
					wdata.setGest_age_days(cursor.getInt(45));

					// updated on 23Nov2015
					wdata.setDel_time2(cursor.getString(33));

					// updated on 30Nov2015
					wdata.setThayicardnumber(cursor.getString(46));
					wdata.setDelTypeReason(cursor.getString(47));
					wdata.setDeltype_otherreasons(cursor.getString(48));

					// 27dec2015
					wdata.setLmp(cursor.getString(49));
					wdata.setRiskoptions(cursor.getString(50));

					// 04jan2016
					wdata.setTears(cursor.getString(51));
					wdata.setEpisiotomy(cursor.getInt(52));
					wdata.setSuturematerial(cursor.getInt(53));

					// 19jan2016
					wdata.setBloodgroup(cursor.getInt(54));
					// updated on 23/2/16 by Arpitha
					wdata.setEdd(cursor.getString(55));
					wdata.setHeight(cursor.getString(56));
					wdata.setW_weight(cursor.getString(57));
					wdata.setOther(cursor.getString(58));

					// updated on 29May by Arpitha
					wdata.setHeight_unit(cursor.getInt(59));

					wdata.setExtracomments(cursor.getString(60));// 13Oct2016
																	// Arpitha
					wdata.setregtype(cursor.getInt(61));// 16Oct2016 Arpitha

					wdata.setRegtypereason(cursor.getString(62));// 18Oct2016
																	// Arpitha
					wdata.setDate_of_reg_entry(cursor.getString(63));// 10Nov2016
																		// Arpitha

					wdata.setNormaltype(cursor.getInt(64));// 20Nov2016 Arpitha

					wdata.setRegtypeOtherReason(cursor.getString(65));// 21Nov2016
																		// Arpitha

					boolean isDanger = dbh.chkIsDanger(cursor.getString(1), user.getUserId());

					wdata.setDanger(isDanger);

					rowItems.add(wdata);
				}
			} while (cursor.moveToNext());

			adapterDisc = new RecentCustomListAdapterSearch(this, R.layout.activity_womenlist, rowItems, dbh);
			aq.id(R.id.listdischargedwomenlist).adapter(adapterDisc);

		}

		if (rowItems.size() <= 0) {
			aq.id(R.id.listdischargedwomenlist).gone();
			aq.id(R.id.txtnowomendischarged).visible();
			// searchDiscahrgedwomen.setVisibility(View.GONE);// 05Jun2017
			// Arpitha
		} else {
			// aq.id(R.id.listwomentoday).setSelection(displayListCount - 10);
			aq.id(R.id.listdischargedwomenlist).visible();
			aq.id(R.id.txtnowomendischarged).gone();
			// searchDiscahrgedwomen.setVisibility(View.VISIBLE);// 05Jun2017
			// Arpitha
		}

	}

	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		if (adapterDisc != null) {
			adapterDisc.getFilter().filter(newText);
		}
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String arg0) {
		// TODO Auto-generated method stub
		return true;
	}

	// Display options on click of list item
	private void displayOptions() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {

			ActionItem viewprofile_Item = new ActionItem(ID_PROFILE, getResources().getString(R.string.view_profile),
					getResources().getDrawable(R.drawable.ic_viewprofile_menu));
			ActionItem graph_Item = new ActionItem(ID_GRAPH, getResources().getString(R.string.partograph),
					getResources().getDrawable(R.drawable.ic_add_parto_menu));

			ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
					getResources().getString(R.string.delivery_status),
					getResources().getDrawable(R.drawable.ic_del_sttaus_menu));
			ActionItem apgarscore_Item = new ActionItem(ID_APGAR, getResources().getString(R.string.apgar_score),
					getResources().getDrawable(R.drawable.ic_apgar_menu));

			// updated on 16Nov2015
			ActionItem referredto_Item = new ActionItem(ID_REFERRAL, getResources().getString(R.string.referrral_info),
					getResources().getDrawable(R.drawable.ic_referral_menu));

			// updated on 26Nov2015
			ActionItem stagesoflabor_Item = new ActionItem(ID_STAGEOFLABOR,
					getResources().getString(R.string.stagesoflabor),
					getResources().getDrawable(R.drawable.thid_fourth_menu));

			// updated on 23dec2015
			ActionItem print_partoitem = new ActionItem(ID_PRINTPARTO, getResources().getString(R.string.printparto),
					getResources().getDrawable(R.drawable.ic_view_graph_menu));
			// updated on 17Oct2016 Arpitha

			ActionItem additonal_details = new ActionItem(ID_ADDITIONALDETAILS,
					getResources().getString(R.string.additional_details),
					getResources().getDrawable(R.drawable.ic_additional));// 06Dec2016
			// Arpitha

			// 21July2017 Arpitha
			ActionItem dischargeDetails = new ActionItem(ID_DISCHARGEDETAILS,
					getResources().getString(R.string.discharge_details),
					getResources().getDrawable(R.drawable.discharge));

			// 10Agu2017 Arpitha
			ActionItem latentphase = new ActionItem(ID_LATENTPHASE, getResources().getString(R.string.latent_phase),
					getResources().getDrawable(R.drawable.discharge));

			// 18Agu2017 Arpitha
			ActionItem postpartumcare = new ActionItem(ID_POSTPARTUM, "PostPartum",
					getResources().getDrawable(R.drawable.discharge));

			// 31Agu2017 Arpitha
			ActionItem pdf = new ActionItem(ID_PDF, getResources().getString(R.string.export_pdf),
					getResources().getDrawable(R.drawable.pdf5));

			// updated on 23dec2015
			mQuickAction.addActionItem(latentphase);// 10Aug2017 Arpitha
			mQuickAction.addActionItem(print_partoitem);
			mQuickAction.addActionItem(graph_Item);
			mQuickAction.addActionItem(deliverystatus_Item);
			mQuickAction.addActionItem(apgarscore_Item);

			// updated on 26Nov2015
			mQuickAction.addActionItem(stagesoflabor_Item);
			
			mQuickAction.addActionItem(postpartumcare);// 18Aug2017 Arpitha

			// changed by Arpitha 26Feb2016
			mQuickAction.addActionItem(referredto_Item);

			mQuickAction.addActionItem(additonal_details);// 06Dec2016 Arpitha

			mQuickAction.addActionItem(viewprofile_Item);

			// Arpitha

			mQuickAction.addActionItem(dischargeDetails);// 21July2017 Arpitha

			

			

			mQuickAction.addActionItem(pdf);// 31Aug2017 Arpitha

			// setup the action item click listener
			mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
				@Override
				public void onItemClick(QuickAction quickAction, int pos, int actionId) {

					try {

						if (actionId == ID_PROFILE) { // Message item
														// selected

							if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {// 27Sep2016
								// Arpitha
								Intent view = new Intent(DischargedWomanList_Activity.this, ViewProfile_Activity.class);
								view.putExtra("woman", woman);
								startActivity(view);
							}

						}
						if (actionId == ID_GRAPH) {

							if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {// 27Sep2016
								// Arpitha

								Intent graph = new Intent(DischargedWomanList_Activity.this, SlidingActivity.class);
								graph.putExtra("woman", woman);
								startActivity(graph);
							}

						}
						if (actionId == ID_DELSTATUS) {

							if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {// 27Sep2016
								// Arpitha

								if (woman != null) {
									if (woman.getDel_type() == 0) {

										Intent graph = new Intent(DischargedWomanList_Activity.this,
												DeliveryInfo_Activity.class);
										graph.putExtra("woman", woman);
										startActivity(graph);
									} else {
										Partograph_CommonClass
												.showDialogToUpdateStatus(DischargedWomanList_Activity.this, woman);

									}

								}

							}
						}
						if (actionId == ID_APGAR) {

							if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {// 27Sep2016
								// Arpitha

								if (woman.getDel_type() != 0 &&

										(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent viewapgar = new Intent(DischargedWomanList_Activity.this,
											Activity_Apgar.class);
									viewapgar.putExtra("woman", woman);
									startActivity(viewapgar);
								} else {
									Toast.makeText(DischargedWomanList_Activity.this,
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated on 16Nov2015
						if (actionId == ID_REFERRAL) {

							try {
								if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {// 27Sep2016
									// Arpitha
									Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
									if (cur != null && cur.getCount() > 0) {
										Partograph_CommonClass
												.showDialogToUpdateReferral(DischargedWomanList_Activity.this, woman);
									} else {
										Intent ref = new Intent(DischargedWomanList_Activity.this,
												ReferralInfo_Activity.class);
										ref.putExtra("woman", woman);
										startActivity(ref);
									}

								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName(), e);
							}

							/*
							 * 
							 * if (Partograph_CommonClass.autodatetime(
							 * DischargedWomanList_Activity.this)) {// 27Sep2016
							 * // Arpitha
							 * 
							 * Intent ref = new
							 * Intent(DischargedWomanList_Activity.this,
							 * ReferralInfo_Activity.class);
							 * ref.putExtra("woman", woman); startActivity(ref);
							 * }
							 */}

						// updated on 26Nov2015
						if (actionId == ID_STAGEOFLABOR) {

							if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {// 27Sep2016
								// Arpitha
								if (woman.getDel_type() != 0 &&

										(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent stagesoflabor = new Intent(DischargedWomanList_Activity.this,
											StageofLabor.class);
									stagesoflabor.putExtra("woman", woman);
									startActivity(stagesoflabor);

								} else {
									Toast.makeText(DischargedWomanList_Activity.this,
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated bindu - 26july2016
						// updated on 23Dec2015
						if (actionId == ID_PRINTPARTO) {
							try {
								Intent addtionalDetails = new Intent(DischargedWomanList_Activity.this,
										View_Partograph.class);
								addtionalDetails.putExtra("woman", woman);

								startActivity(addtionalDetails);

								// Partograph_CommonClass.exportPDFs(DischargedWomanList_Activity.this,
								// woman);
								/*
								 * Intent view_partograph = new
								 * Intent(DischargedWomanList_Activity.this,
								 * View_Partograph.class);
								 * view_partograph.putExtra("woman", woman);
								 * 
								 * startActivity(view_partograph);
								 */

							} catch (Exception e) {
								AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName());
								e.printStackTrace();

							}
						}

						// 06Dec2016 Arpitha
						if (actionId == ID_ADDITIONALDETAILS) {

							Intent addtionalDetails = new Intent(DischargedWomanList_Activity.this,
									AdditionalDetails_Activity.class);
							addtionalDetails.putExtra("woman", woman);

							startActivity(addtionalDetails);
						} // 06Dec2016 Arpitha

						// 21July2017 Arpitha
						if (actionId == ID_DISCHARGEDETAILS) {
							Intent dischargeDetails = new Intent(DischargedWomanList_Activity.this,
									DiscargeDetails_Activity.class);
							dischargeDetails.putExtra("woman", woman);
							startActivity(dischargeDetails);

						} // 21July2017 Arpitha

						// 10Aug2017 Arpitha
						if (actionId == ID_LATENTPHASE) {
							Intent latent = new Intent(DischargedWomanList_Activity.this, LatentPhase_Activity.class);
							latent.putExtra("woman", woman);
							startActivity(latent);
						}

						// 18Aug2017 Arpitha
						if (actionId == ID_POSTPARTUM) {
							if (woman.getDel_type() != 0) {
								Intent postpartum = new Intent(DischargedWomanList_Activity.this,
										PostPartumCare_Activity.class);
								postpartum.putExtra("woman", woman);
								startActivity(postpartum);
							} else
								Toast.makeText(DischargedWomanList_Activity.this,
										getResources().getString(R.string.applicable_only_for_delivered_woman),
										Toast.LENGTH_SHORT).show();
						}

						// 31Aug2017 Arpitha
						if (actionId == ID_PDF) {
							Partograph_CommonClass.exportPDFs(DischargedWomanList_Activity.this, woman);

						}

					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName());
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName());
						e.printStackTrace();
					}

				}
			});

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.email).setVisible(false);

		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);
		menu.findItem(R.id.save).setVisible(false);
		menu.findItem(R.id.disch).setVisible(false);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			// case R.id.sync:
			// menuItem = item;
			// menuItem.setActionView(R.layout.progressbar);
			// menuItem.expandActionView();
			// calSyncMtd();
			// return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(DischargedWomanList_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(DischargedWomanList_Activity.this, woman.getUserId());
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));
				return true;
			// 22May2017 Arpitha - v2.6
			case R.id.summary:
				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);
				return true;
			case R.id.usermanual:
				Intent manual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(manual);
				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(this);
				return true;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.home));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void displayConfirmationAlert_exit(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(DischargedWomanList_Activity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(DischargedWomanList_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {

					Intent i = new Intent(DischargedWomanList_Activity.this, LoginActivity.class);
					startActivity(i);

				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		dialog.setCancelable(false);

	}

}
