package com.bluecrimson.postpartumcare;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class PostPartumCare_Activity extends Activity implements OnClickListener {

	static AQuery aq;
	Partograph_DB dbh;
	PostPartum_Pojo postpojo;
	Women_Profile_Pojo woman;
	MultiSelectionSpinner mspnpresentigcomplaints;
	LinkedHashMap<String, Integer> presentingcomplaintsMap;

	TableLayout tbldata;
	TableLayout tbladd;
	public static Cursor cur;
	static Context context;
	File file;
	private static Font small = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
	private static Font heading = new Font(Font.TIMES_ROMAN, 15, Font.BOLD);
	static PdfPTable womanbasics;
	PdfPTable womandelInfo;
	public static int postpartum;// 03Sep2017 Arpitha
	public static boolean ispostpartum;// 03Sep2017 Arpitha
	int bpsysval = 0, bpdiaval = 0, pulval = 0;
	String selecteddate;
	static String todaysDate;
	int year, mon, day;
	static Date doa;
	static Date dateOfDel;
	static Date lastpartoENtry;
	String strdateOfAdm;
	static String strlastpartoEntry;
	static Date timeOfAdm;
	private static int hour;
	private static int minute;
	static String posttime;
	static String strdelTime;
	static Date delTime;
	static String postpartumdate;
	// 19Sep2017 Arpitha
	static String strrefdatetime = null;
	Date refdatetime;
	static String strpostpartumdatetime;// 20Sep2017 Arpitha
	static Date postpartumdatetime;// 20Sep2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_postpartumcare);
		try {
			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			aq = new AQuery(this);
			dbh = Partograph_DB.getInstance(getApplicationContext());

			postpartum = Partograph_CommonClass.curr_tabpos;// 03Sep2017 Arpitha
			ispostpartum = true;// 03Sep2017 Arpitha

			initializeScreen(aq.id(R.id.relpostpartum).getView());

			context = PostPartumCare_Activity.this;

			getwomanbasicdata();

			mspnpresentigcomplaints = (MultiSelectionSpinner) findViewById(R.id.mspnpresentigcomplaints);

			this.setTitle(getResources().getString(R.string.postpartum_care));

			intialView();

			aq.id(R.id.etdatepost).getEditText().setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							v.performClick();

							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;

				}
			});

			aq.id(R.id.ettimepost).getEditText().setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						v.performClick();

						posttime = aq.id(R.id.ettimepost).getText().toString();

						if (posttime != null) {

							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(posttime);
							if (d != null)
								calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}

						showtimepicker();
					}

					return true;
				}
			});

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} KillAllActivitesAndGoToLogin.addToStack(this);
	}

	private void intialView() throws Exception {
		// TODO Auto-generated method stub

		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		if (woman.getregtype() != 2)
			strdateOfAdm = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
		else {
			strdateOfAdm = woman.getDate_of_reg_entry();
			strdateOfAdm = strdateOfAdm.replace("/", " ");
		}

		timeOfAdm = formatTime.parse(strdateOfAdm);
		doa = formatDate.parse(strdateOfAdm);

		if (woman.getDel_type() != 0) {
			// strdelDate = woman.getDel_Date();
			strdelTime = woman.getDel_Date() + " " + woman.getDel_Time();
			dateOfDel = formatDate.parse(strdelTime);
			delTime = formatTime.parse(strdelTime);
		}
		strlastpartoEntry = dbh.getlastentrytime(woman.getWomenId(), 0);
		if (strlastpartoEntry != null)
			lastpartoENtry = formatTime.parse(strlastpartoEntry);

		ArrayList<DiscgargePojo> arrValDisc = new ArrayList<DiscgargePojo>();
		arrValDisc = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (arrValDisc.size() > 0) {
			aq.id(R.id.scrladd).gone();
			aq.id(R.id.txtdisable).visible();
			aq.id(R.id.btnsave).enabled(false);
			aq.id(R.id.btnsave).backgroundColor(R.color.ashgray);
		}

		todaysDate = Partograph_CommonClass.getTodaysDate();

		postpartumdate = Partograph_CommonClass.getTodaysDate();
		aq.id(R.id.etdatepost).text(Partograph_CommonClass.getTodaysDate(Partograph_CommonClass.defdateformat));
		aq.id(R.id.ettimepost).text(Partograph_CommonClass.getCurrentTime());
		setPresentingcomplaints();

		// tbldata = (TableLayout) findViewById(R.id.tblpostpartum);

		tbladd = (TableLayout) findViewById(R.id.tbladd);

		// ArrayList<PostPartum_Pojo> values = new ArrayList<PostPartum_Pojo>();
		// PostPartum_Pojo postpojo;

		cur = dbh.getPostPartumData(woman.getUserId(), woman.getWomenId());
		if (cur != null && cur.getCount() <= 0) {

			// tbldata.setVisibility(View.VISIBLE);
			// tbladd.setVisibility(View.GONE);
			aq.id(R.id.btnview).enabled(false);
			aq.id(R.id.btnview).backgroundColor(R.color.ashgray);

			/*
			 * cur.moveToFirst(); do { postpojo = new PostPartum_Pojo();
			 * postpojo.setUserId(cur.getString(0));
			 * postpojo.setWomanId(cur.getString(1));
			 * postpojo.setWomanName(cur.getString(2));
			 * postpojo.setComplaints(cur.getString(3));
			 * postpojo.setOther(cur.getString(4));
			 * postpojo.setExamination(cur.getString(5));
			 * postpojo.setPallor(cur.getString(6));
			 * postpojo.setPulse(cur.getInt(7));
			 * postpojo.setBp(cur.getString(8));
			 * postpojo.setBreastExamination(cur.getString(9));
			 * postpojo.setInvolution(cur.getString(10));
			 * postpojo.setLochia(cur.getString(11));
			 * postpojo.setPerineal(cur.getString(12));
			 * postpojo.setAdvice(cur.getString(13));
			 * postpojo.setDateTimeOfEntry(cur.getString(15));
			 * values.add(postpojo);
			 * 
			 * } while (cur.moveToNext());
			 * 
			 */}

		/*
		 * if (values.size() <= 0) {
		 * 
		 * // tbldata.setVisibility(View.VISIBLE); //
		 * tbladd.setVisibility(View.GONE); aq.id(R.id.btnview).enabled(false);
		 * aq.id(R.id.btnview).backgroundColor(R.color.ashgray);
		 * 
		 * for (int i = 0; i < values.size(); i++) {
		 * 
		 * tbldata.removeAllViews();
		 * 
		 * TableRow trlabel = new TableRow(this); trlabel.setLayoutParams(new
		 * LayoutParams(LayoutParams.WRAP_CONTENT, 40));
		 * 
		 * TextView textExamination = new TextView(this);
		 * textExamination.setTextSize(15); //
		 * textPulse.setText(getResources().getString(R.string.examination));
		 * textExamination.setPadding(10, 10, 10, 10);
		 * textExamination.setTextColor(getResources().getColor(R.color.black));
		 * // textPulse.setTextSize(18);
		 * textExamination.setGravity(Gravity.CENTER);
		 * trlabel.addView(textExamination);
		 * 
		 * TextView textcomplaints = new TextView(this);
		 * textcomplaints.setTextSize(15); textcomplaints.setPadding(10, 10, 10,
		 * 10); textcomplaints.setText(getResources().getString(R.string.
		 * presenting_complaints));
		 * textcomplaints.setTextColor(getResources().getColor(R.color.black));
		 * // textbp.setTextSize(15); textcomplaints.setGravity(Gravity.CENTER);
		 * trlabel.addView(textcomplaints);
		 * 
		 * TextView textOthers = new TextView(this); textOthers.setTextSize(15);
		 * textOthers.setText(getResources().getString(R.string.others));
		 * textOthers.setPadding(10, 10, 10, 10);
		 * textOthers.setTextColor(getResources().getColor(R.color.black)); //
		 * textcontractions.setTextSize(15);
		 * textOthers.setGravity(Gravity.LEFT); trlabel.addView(textOthers);
		 * 
		 * TextView textpallor = new TextView(this); textpallor.setTextSize(15);
		 * textpallor.setText(getResources().getString(R.string.pallor));
		 * textpallor.setPadding(10, 10, 10, 10);
		 * textpallor.setTextColor(getResources().getColor(R.color.black)); //
		 * textFHS.setTextSize(15); textpallor.setGravity(Gravity.LEFT);
		 * trlabel.addView(textpallor);
		 * 
		 * TextView textpulse = new TextView(this); textpulse.setTextSize(15);
		 * textpulse.setText(getResources().getString(R.string.pulse));
		 * textpulse.setPadding(10, 10, 10, 10);
		 * textpulse.setTextColor(getResources().getColor(R.color.black)); //
		 * textPV.setTextSize(15); textpulse.setGravity(Gravity.LEFT);
		 * trlabel.addView(textpulse);
		 * 
		 * TextView textbp = new TextView(this); textbp.setTextSize(15);
		 * textbp.setText(getResources().getString(R.string.txtbpvalue));
		 * textbp.setPadding(10, 10, 10, 10);
		 * textbp.setTextColor(getResources().getColor(R.color.black)); //
		 * textPV.setTextSize(15); textbp.setGravity(Gravity.LEFT);
		 * trlabel.addView(textbp);
		 * 
		 * TextView textbreast = new TextView(this); textbreast.setTextSize(15);
		 * textbreast.setText(getResources().getString(R.string.
		 * breast_examination)); textbreast.setPadding(10, 10, 10, 10);
		 * textbreast.setTextColor(getResources().getColor(R.color.black)); //
		 * textPV.setTextSize(15); textbreast.setGravity(Gravity.LEFT);
		 * trlabel.addView(textbreast);
		 * 
		 * TextView textinvolution = new TextView(this);
		 * textinvolution.setTextSize(15);
		 * textinvolution.setText(getResources().getString(R.string.
		 * involution_of_uterus)); textinvolution.setPadding(10, 10, 10, 10);
		 * textinvolution.setTextColor(getResources().getColor(R.color.black));
		 * // textPV.setTextSize(15); textinvolution.setGravity(Gravity.LEFT);
		 * trlabel.addView(textinvolution);
		 * 
		 * TextView textlochia = new TextView(this); textlochia.setTextSize(15);
		 * textlochia.setText(getResources().getString(R.string.lochia));
		 * textlochia.setPadding(10, 10, 10, 10);
		 * textlochia.setTextColor(getResources().getColor(R.color.black)); //
		 * textPV.setTextSize(15); textlochia.setGravity(Gravity.LEFT);
		 * trlabel.addView(textlochia);
		 * 
		 * TextView textperinealcare = new TextView(this);
		 * textperinealcare.setTextSize(15);
		 * textperinealcare.setText(getResources().getString(R.string.
		 * perineal_care)); textperinealcare.setPadding(10, 10, 10, 10);
		 * textperinealcare.setTextColor(getResources().getColor(R.color.black))
		 * ; // textPV.setTextSize(15);
		 * textperinealcare.setGravity(Gravity.LEFT);
		 * trlabel.addView(textperinealcare);
		 * 
		 * TextView textAdvice = new TextView(this); textAdvice.setTextSize(15);
		 * textAdvice.setText(getResources().getString(R.string.advice));
		 * textAdvice.setPadding(10, 10, 10, 10);
		 * textAdvice.setTextColor(getResources().getColor(R.color.black)); //
		 * textAdvice.setTextSize(18); textAdvice.setGravity(Gravity.LEFT);
		 * trlabel.addView(textAdvice);
		 * 
		 * TextView textdatetime = new TextView(this);
		 * textdatetime.setTextSize(15); textdatetime.setPadding(10, 10, 10,
		 * 10);
		 * textdatetime.setTextColor(getResources().getColor(R.color.black)); //
		 * textdatetime.setTextSize(18);
		 * textdatetime.setText(getResources().getString(R.string.date));
		 * textdatetime.setGravity(Gravity.CENTER);
		 * trlabel.addView(textdatetime);
		 * 
		 * for (int j = 0; j < values.size(); j++) { TableRow tr = new
		 * TableRow(this); tr.setLayoutParams(new
		 * LayoutParams(LayoutParams.WRAP_CONTENT, 40));
		 * 
		 * TextView textExaminationval = new TextView(this);
		 * textExaminationval.setTextSize(15);
		 * textExaminationval.setText(values.get(j).getExamination());
		 * textExaminationval.setPadding(10, 10, 10, 10);
		 * textExaminationval.setTextColor(getResources().getColor(R.color.black
		 * )); // textPulse.setTextSize(18);
		 * textExaminationval.setGravity(Gravity.CENTER);
		 * tr.addView(textExaminationval);
		 * 
		 * TextView textcomplaintsval = new TextView(this);
		 * textcomplaintsval.setTextSize(15); textcomplaintsval.setPadding(10,
		 * 10, 10, 10);
		 * textcomplaintsval.setText(values.get(j).getComplaints());
		 * textcomplaintsval.setTextColor(getResources().getColor(R.color.black)
		 * ); // textbp.setTextSize(15);
		 * textcomplaintsval.setGravity(Gravity.CENTER);
		 * tr.addView(textcomplaintsval);
		 * 
		 * TextView textOthersval = new TextView(this);
		 * textOthersval.setTextSize(15);
		 * textOthersval.setText(values.get(j).getOther());
		 * textOthersval.setPadding(10, 10, 10, 10);
		 * textOthersval.setTextColor(getResources().getColor(R.color.black));
		 * // textcontractions.setTextSize(15);
		 * textOthersval.setGravity(Gravity.LEFT); tr.addView(textOthersval);
		 * 
		 * TextView textpallorval = new TextView(this);
		 * textpallorval.setTextSize(15);
		 * textpallorval.setText(values.get(j).getPallor());
		 * textpallorval.setPadding(10, 10, 10, 10);
		 * textpallorval.setTextColor(getResources().getColor(R.color.black));
		 * // textFHS.setTextSize(15); textpallorval.setGravity(Gravity.LEFT);
		 * tr.addView(textpallorval);
		 * 
		 * TextView textpulseval = new TextView(this);
		 * textpulseval.setTextSize(15); textpulseval.setText("" +
		 * values.get(j).getPulse()); textpulseval.setPadding(10, 10, 10, 10);
		 * textpulseval.setTextColor(getResources().getColor(R.color.black)); //
		 * textPV.setTextSize(15); textpulseval.setGravity(Gravity.LEFT);
		 * tr.addView(textpulseval);
		 * 
		 * TextView textbpval = new TextView(this); textbpval.setTextSize(15);
		 * textbpval.setText(values.get(j).getBp()); textbpval.setPadding(10,
		 * 10, 10, 10);
		 * textbpval.setTextColor(getResources().getColor(R.color.black)); //
		 * textPV.setTextSize(15); textbpval.setGravity(Gravity.LEFT);
		 * tr.addView(textbpval);
		 * 
		 * TextView textbreastval = new TextView(this);
		 * textbreastval.setTextSize(15);
		 * textbreastval.setText(values.get(j).getBreastExamination());
		 * textbreastval.setPadding(10, 10, 10, 10);
		 * textbreastval.setTextColor(getResources().getColor(R.color.black));
		 * // textPV.setTextSize(15); textbreastval.setGravity(Gravity.LEFT);
		 * tr.addView(textbreastval);
		 * 
		 * TextView textinvolutionval = new TextView(this);
		 * textinvolutionval.setTextSize(15);
		 * textinvolutionval.setText(values.get(j).getInvolution());
		 * textinvolutionval.setPadding(10, 10, 10, 10);
		 * textinvolutionval.setTextColor(getResources().getColor(R.color.black)
		 * ); // textPV.setTextSize(15);
		 * textinvolutionval.setGravity(Gravity.LEFT);
		 * tr.addView(textinvolutionval);
		 * 
		 * TextView textlochiaval = new TextView(this);
		 * textlochiaval.setTextSize(15);
		 * textlochiaval.setText(values.get(j).getLochia());
		 * textlochiaval.setPadding(10, 10, 10, 10);
		 * textlochiaval.setTextColor(getResources().getColor(R.color.black));
		 * // textPV.setTextSize(15); textlochiaval.setGravity(Gravity.LEFT);
		 * tr.addView(textlochiaval);
		 * 
		 * TextView textperinealcareval = new TextView(this);
		 * textperinealcareval.setTextSize(15);
		 * textperinealcareval.setText(values.get(j).getPerineal());
		 * textperinealcareval.setPadding(10, 10, 10, 10);
		 * textperinealcareval.setTextColor(getResources().getColor(R.color.
		 * black)); // textPV.setTextSize(15);
		 * textperinealcareval.setGravity(Gravity.LEFT);
		 * tr.addView(textperinealcareval);
		 * 
		 * TextView textAdviceval = new TextView(this);
		 * textAdviceval.setTextSize(15);
		 * textAdviceval.setText(values.get(j).getAdvice());
		 * textAdviceval.setPadding(10, 10, 10, 10);
		 * textAdviceval.setTextColor(getResources().getColor(R.color.black));
		 * // textAdvice.setTextSize(18);
		 * textAdviceval.setGravity(Gravity.LEFT); tr.addView(textAdviceval);
		 * 
		 * View view3 = new View(this); view3.setLayoutParams(new
		 * LayoutParams(LayoutParams.WRAP_CONTENT, 1));
		 * view3.setBackgroundColor(Color.DKGRAY); tbldata.addView(view3);
		 * 
		 * String date = Partograph_CommonClass
		 * .getConvertedDateFormat(values.get(j).getDateTimeOfEntry().split("/")
		 * [0], "dd-MM-yyyy") + "/" +
		 * values.get(j).getDateTimeOfEntry().split("/")[1];
		 * 
		 * TextView textdatetimeval = new TextView(this);
		 * textdatetimeval.setTextSize(15); textdatetimeval.setPadding(10, 10,
		 * 10, 10);
		 * textdatetimeval.setTextColor(getResources().getColor(R.color.black));
		 * // textdatetimeval.setTextSize(18); textdatetimeval.setText(date);
		 * textdatetimeval.setGravity(Gravity.CENTER);
		 * tr.addView(textdatetimeval);
		 * 
		 * if (i == 0) { tbldata.addView(trlabel); View view1 = new View(this);
		 * view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
		 * 1)); view1.setBackgroundColor(Color.DKGRAY); tbldata.addView(view1);
		 * }
		 * 
		 * tbldata.addView(tr); }
		 * 
		 * 
		 * * aq.id(R.id.etexamination).text(values.get(i).getExamination() );
		 * aq.id(R.id.etpallor).text(values.get(i).getPallor());
		 * aq.id(R.id.etinvolution).text(values.get(i).getInvolution());
		 * aq.id(R.id.etadvicepost).text(values.get(i).getAdvice());
		 * aq.id(R.id.etbreastexamination).text(values.get(i).
		 * getBreastExamination());
		 * aq.id(R.id.etdatepost).text(values.get(i).getDateTimeOfEntry(
		 * ).split("/")[0]);
		 * aq.id(R.id.ettimepost).text(values.get(i).getDateTimeOfEntry(
		 * ).split("/")[1]); aq.id(R.id.etpulsepost).text("" +
		 * values.get(i).getPulse());
		 * aq.id(R.id.etbpsyspost).text(values.get(i).getBp().split("/") [0]);
		 * aq.id(R.id.etbpdiapost).text(values.get(i).getBp().split("/") [1]);
		 * aq.id(R.id.etlochia).text(values.get(i).getLochia());
		 * aq.id(R.id.etother).text(values.get(i).getOther());
		 * aq.id(R.id.etperinealcare).text(values.get(i).getPerineal()); }
		 * 
		 * } }
		 */

		// aq.id(R.id.imgpdfpost).gone();

		// 19Sep2017 Arpitha
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		strrefdatetime = dbh.getrefdate(woman.getWomenId(), woman.getUserId());

		if (strrefdatetime != null) {
			refdatetime = sdf.parse(strrefdatetime);
		}

		strpostpartumdatetime = dbh.getPostPartumDateTime(woman.getWomenId(), woman.getUserId());// 20Sep2017
		// Arpitha

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		try {
			switch (v.getId()) {

			case R.id.btnsave:
				savePostPartumData();
				break;
			case R.id.btnview:
				Intent i = new Intent(PostPartumCare_Activity.this, PostpartumCare_View_Activity.class);
				i.putExtra("woman", woman);
				startActivity(i);
				break;
			case R.id.imgpdfpost:
				Partograph_CommonClass.pdfAlertDialog(PostPartumCare_Activity.this, woman, "postpartum", null,
						null);
//				displayAlertDialog();
				break;

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	private void savePostPartumData() throws NotFoundException, Exception {
		// TODO Auto-generated method stub

		if (validateFields()) {

			pulval = Integer.parseInt(aq.id(R.id.etpulsepost).getText().toString());

			bpsysval = Integer.parseInt(aq.id(R.id.etbpsyspost).getText().toString());

			bpdiaval = Integer.parseInt(aq.id(R.id.etbpdiapost).getText().toString());

			if (!((pulval > 120 || pulval < 50) || (bpsysval < 80 || bpsysval > 160)
					|| (bpdiaval < 50 || bpdiaval > 110))) {

				setData();

			} else
				displayConfirmationAlert("", "confirmation");

		}

	}

	public void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

		Intent i = new Intent(PostPartumCare_Activity.this, PostpartumCare_View_Activity.class);
		i.putExtra("woman", woman);
		startActivity(i);

		calSyncMtd();

	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				// aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		if (v.getClass() == TableRow.class) {

		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	boolean validateFields() throws NotFoundException, Exception {

		if (aq.id(R.id.etpulsepost).getText().toString().trim().length() <= 0) {

			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_pulse),
					PostPartumCare_Activity.this);
			return false;

		}

		if (aq.id(R.id.etbpsyspost).getText().toString().trim().length() <= 0
				&& aq.id(R.id.etbpdiapost).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_bp),
					PostPartumCare_Activity.this);
			return false;

		}

		if (aq.id(R.id.etdatepost).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_date),
					PostPartumCare_Activity.this);
			return false;

		}
		if (aq.id(R.id.ettimepost).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_time),
					PostPartumCare_Activity.this);
			return false;

		}

		if (aq.id(R.id.etbpsyspost).getText().toString().trim().length() > 0
				|| aq.id(R.id.etbpdiapost).getText().toString().trim().length() > 0) {

			if (aq.id(R.id.etbpsyspost).getText().toString().trim().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterbpsystolicval),
						PostPartumCare_Activity.this);
				return false;

			} else if (aq.id(R.id.etbpdiapost).getText().toString().trim().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterbpdiastolicval),
						PostPartumCare_Activity.this);
				return false;

			}
		}

		// 21Sep2017 Arpitha
		if (aq.id(R.id.etdatepost).getText().toString().trim().length() > 0
				&& aq.id(R.id.ettimepost).getText().toString().trim().length() > 0) {

			String postpartumdatetime = postpartumdate + " " + aq.id(R.id.ettimepost).getText().toString();

			if (strdateOfAdm != null && !(isDeliveryTimevalid(strdateOfAdm, postpartumdatetime, 1)))
				return false;

			if (strlastpartoEntry != null) {
				if (strlastpartoEntry.contains("_")) {
					strlastpartoEntry = strlastpartoEntry.replace("_", " ");
				}
				if (!isDeliveryTimevalid(strlastpartoEntry, postpartumdatetime, 2))
					return false;
			}

			String currenttime = Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime();

			if (!isDeliveryTimevalid(postpartumdatetime, currenttime, 3))
				return false;

			// 02Nov2016 Arpitha
			if (strrefdatetime != null) {
				if (!isDeliveryTimevalid(strrefdatetime, postpartumdatetime, 4))
					return false;
			} // 02Nov2016 Arpitha

			if (strpostpartumdatetime != null && !(isDeliveryTimevalid(strpostpartumdatetime, postpartumdatetime, 5)))
				return false;

			if (strdelTime != null && (!isDeliveryTimevalid(strdelTime, postpartumdatetime, 6)))
				return false;

		}

		return true;

	}

	// Set options for admitted with - 9jan2016
	protected void setPresentingcomplaints() throws Exception {
		int i = 0;
		presentingcomplaintsMap = new LinkedHashMap<String, Integer>();
		List<String> complaints = null;

		complaints = Arrays.asList(getResources().getStringArray(R.array.presenting_complaints));

		if (complaints != null) {
			for (String str : complaints) {
				presentingcomplaintsMap.put(str, i);
				i++;
			}
			mspnpresentigcomplaints.setItems(complaints);
		}
	}

	void displayAlertDialog() {

		final Dialog dialog = new Dialog(PostPartumCare_Activity.this);

		dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dialog_action);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.dischrage_slip));

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});

		final RadioButton rdsavepdf = (RadioButton) dialog.findViewById(R.id.rdsave);
		final RadioButton rdviewpdf = (RadioButton) dialog.findViewById(R.id.rdview);
		Button btndone = (Button) dialog.findViewById(R.id.btndone);

		final RadioButton rdshare = (RadioButton) dialog.findViewById(R.id.rdshare);

		btndone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (rdsavepdf.isChecked()) {
					try {
						dialog.cancel();
						Partograph_CommonClass.pdfAlertDialog(PostPartumCare_Activity.this, woman, "postpartum", null,
								null);
						// Partograph_CommonClass.generatePostPartumCareDatainpdfFormat(context,
						// woman);
						// PostpartumCare_View_Activity.generatePostPartumCareDatainpdfFormat();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.latent_phase_is_saved), Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} else if (rdviewpdf.isChecked())

				{
					try {
						dialog.cancel();
						Partograph_CommonClass.pdfAlertDialog(PostPartumCare_Activity.this, woman, "postpartum", null,
								null);
//						PostpartumCare_View_Activity.generatePostPartumCareDatainpdfFormat();
						// to view pdf file from appliction
						if (file.exists()) {

							Uri pdfpath = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(pdfpath, "application/pdf");
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

							try {
								startActivity(intent);
							} catch (ActivityNotFoundException e) {// 24Jan2017
																	// Arpitha
								// No application to view, ask to download one
								AlertDialog.Builder builder = new AlertDialog.Builder(PostPartumCare_Activity.this);
								builder.setTitle("No Application Found");
								builder.setMessage("Download one from Android Market?");
								builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketIntent = new Intent(Intent.ACTION_VIEW);
										marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
										startActivity(marketIntent);
									}
								});
								builder.setNegativeButton("No, Thanks", null);
								builder.create().show();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				} // 24Jan2017
				/*
				 * else if (rdshare.isChecked()) { SendviaBluetoth();
				 * 
				 * }
				 */

				else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_one_option),
							Toast.LENGTH_LONG).show();
				}

			}

		});
		dialog.show();

	}

	private void generatePostPartumCareDatainpdfFormat() {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		Document doc = new Document(PageSize.A4);

		try {

			String path = AppContext.mainDir + "/PDF/DischargeSlip";

			File dir = new File(path);
			if (!dir.exists())
				dir.mkdirs();

			Log.d("PDFCreator", "PDF Path: " + path);

			file = new File(dir, woman.getWomen_name() + "_DischargeSlip.pdf");
			FileOutputStream fOut = new FileOutputStream(file);

			PdfWriter.getInstance(doc, fOut);

			doc.setMarginMirroring(false);
			// doc.setMargins(-25, -20, 65, -50);
			doc.setMargins(0, 10, 0, 0);

			doc.open();

			Paragraph p_heading = new Paragraph(context.getResources().getString(R.string.dischrage_slip), heading);
			// Font paraFont = new Font(Font.TIMES_ROMAN);
			// p_heading.setFont(paraFont);
			p_heading.setAlignment(Paragraph.ALIGN_CENTER);

			doc.add(p_heading);

			Drawable d = context.getResources().getDrawable(R.drawable.bcicon_small);

			BitmapDrawable bitDw = ((BitmapDrawable) d);

			Bitmap bmp = bitDw.getBitmap();

			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			bmp.compress(Bitmap.CompressFormat.PNG, 0, stream);

			Image image = Image.getInstance(stream.toByteArray());

			image.setIndentationLeft(10);
			doc.add(image);
			String strdatetime = Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
					Partograph_CommonClass.defdateformat) + " " + Partograph_CommonClass.getCurrentTime() + " ";
			Paragraph p1 = new Paragraph(strdatetime, small);
			p1.setAlignment(Paragraph.ALIGN_RIGHT);
			doc.add(p1);

			womanBasicInfo();

			// dischargeInformation();

			// dischargeInformation2();

			Paragraph basicInfo = new Paragraph("Woman Basic Information", heading);
			basicInfo.setAlignment(Paragraph.ALIGN_LEFT);
			basicInfo.setIndentationLeft(30);
			doc.add(basicInfo);
			doc.add(womanbasics);

			PdfPTable womanAddress = new PdfPTable(1);
			womanAddress.setWidthPercentage(80);
			womanAddress.setSpacingBefore(3);
			womanAddress.addCell(getCellHeading(getResources().getString(R.string.address) + ": " + woman.getAddress(),
					PdfPCell.ALIGN_LEFT));
			// womanAddress.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
			// PdfPCell.ALIGN_CENTER));

			doc.add(womanAddress);

			if (woman.getDel_type() != 0) {
				Paragraph deliveryInfo = new Paragraph("Woman Delivery Information", heading);
				deliveryInfo.setAlignment(Paragraph.ALIGN_LEFT);
				deliveryInfo.setIndentationLeft(30);
				doc.add(deliveryInfo);
				DeliveryInformation();

				// childInformation();

				doc.add(womandelInfo);

				/*
				 * Paragraph childInformation = new Paragraph(
				 * "Child Information", heading);
				 * childInformation.setAlignment(Paragraph.ALIGN_LEFT);
				 * childInformation.setIndentationLeft(30);
				 * doc.add(childInformation);
				 * 
				 * doc.add(childInfo);
				 * 
				 * if (woman.getNo_of_child() == 2) { child2Information();
				 * Paragraph child2Information = new Paragraph(
				 * "Second Child Information", heading);
				 * child2Information.setAlignment(Paragraph.ALIGN_LEFT);
				 * child2Information.setIndentationLeft(30);
				 * doc.add(child2Information); doc.add(childInfo2); }
				 */

			}

			Paragraph dischargedetials = new Paragraph(getResources().getString(R.string.postpartum_care_details),
					heading);
			dischargedetials.setAlignment(Paragraph.ALIGN_LEFT);
			dischargedetials.setIndentationLeft(30);
			doc.add(dischargedetials);

			// doc.add(dischargeInfo);
			//
			// doc.add(dischargeInfo2);

			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(90);
			table.setSpacingBefore(10);
			// table.addCell(getCellHeading(getResources().getString(R.string.date_of_discharge),
			// PdfPCell.ALIGN_LEFT));
			// table.addCell(getCell(dvalues.get(0).getDateTimeOfDischarge(),
			// PdfPCell.ALIGN_CENTER));

			/*
			 * String prsentingcomplaints = ""; if (valu.size() > 0) {
			 * 
			 * date = Partograph_CommonClass
			 * .getConvertedDateFormat(dvalues.get(0).getDateTimeOfDischarge().
			 * split(" ")[0], "dd-MM-yyyy") + "/" +
			 * dvalues.get(0).getDateTimeOfDischarge().split(" ")[1]; }
			 */

			PdfPCell cellTwo = new PdfPCell(
					getCellHeading(getResources().getString(R.string.date_of_discharge), PdfPCell.ALIGN_LEFT));

			PdfPCell cellOne = new PdfPCell(getCell("", PdfPCell.ALIGN_CENTER));

			cellOne.setPadding(5);
			cellTwo.setPadding(5);
			cellTwo.setBorder(Rectangle.BOX);
			cellOne.setBorder(Rectangle.BOX);

			table.addCell(cellTwo);
			table.addCell(cellOne);

			doc.add(table);

			PdfPTable womanInvestigation = new PdfPTable(2);
			// womanInvestigation.setSpacingBefore(5);
			womanInvestigation.setWidthPercentage(90);
			// womanInvestigation.addCell(
			// getCellHeading(getResources().getString(R.string.woman_investigations),
			// PdfPCell.ALIGN_LEFT));
			// womanInvestigation.addCell(getCell(dvalues.get(0).getWomanInvestigation(),
			// PdfPCell.ALIGN_CENTER));

			String investigation = "";

			/*
			 * if (dvalues.size() > 0) {
			 * 
			 * investigation = dvalues.get(0).getWomanInvestigation();
			 * 
			 * }
			 */

			PdfPCell cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.woman_investigations), PdfPCell.ALIGN_LEFT));

			PdfPCell cell2 = new PdfPCell(getCell(investigation, PdfPCell.ALIGN_CENTER));

			cell1.setPadding(5);
			cell2.setPadding(5);
			cell1.setBorder(Rectangle.BOX);
			cell2.setBorder(Rectangle.BOX);

			womanInvestigation.addCell(cell1);
			womanInvestigation.addCell(cell2);

			doc.add(womanInvestigation);

			/*
			 * PdfPTable childvestigation = new PdfPTable(2);
			 * childvestigation.setSpacingBefore(5);
			 * childvestigation.setWidthPercentage(90);
			 * childvestigation.addCell(
			 * getCellHeading(getResources().getString(R.string.
			 * child_investigation), PdfPCell.ALIGN_LEFT));
			 * childvestigation.addCell(getCell(dvalues.get(0).
			 * getChildInvestigation(), PdfPCell.ALIGN_CENTER));
			 * 
			 * doc.add(childvestigation);
			 */

			PdfPTable womanCondition = new PdfPTable(2);
			// womanCondition.setSpacingBefore(5);
			womanCondition.setWidthPercentage(90);
			// womanCondition.addCell(
			// getCellHeading(getResources().getString(R.string.condition_of_mother),
			// PdfPCell.ALIGN_LEFT));
			// womanCondition.addCell(getCell(dvalues.get(0).getConditionOfWoman(),
			// PdfPCell.ALIGN_CENTER));

			String womancondition = "";

			/*
			 * if (dvalues.size() > 0) {
			 * 
			 * womancondition = dvalues.get(0).getConditionOfWoman();
			 * 
			 * }
			 */

			PdfPCell womanConditioncell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.condition_of_mother), PdfPCell.ALIGN_LEFT));

			PdfPCell womanConditioncell2 = new PdfPCell(getCell(womancondition, PdfPCell.ALIGN_CENTER));

			womanConditioncell1.setPadding(5);
			womanConditioncell2.setPadding(5);
			womanConditioncell1.setBorder(Rectangle.BOX);
			womanConditioncell2.setBorder(Rectangle.BOX);

			womanCondition.addCell(womanConditioncell1);
			womanCondition.addCell(womanConditioncell2);

			doc.add(womanCondition);

			/*
			 * PdfPTable childCondition = new PdfPTable(2);
			 * childCondition.setSpacingBefore(5);
			 * childCondition.setWidthPercentage(90); childCondition.addCell(
			 * getCellHeading(getResources().getString(R.string.
			 * condition_of_child), PdfPCell.ALIGN_LEFT));
			 * childCondition.addCell(getCell(dvalues.get(0).getConditionOfChild
			 * (), PdfPCell.ALIGN_CENTER));
			 * 
			 * doc.add(childCondition);
			 */

			PdfPTable table10 = new PdfPTable(2);
			table10.setWidthPercentage(90);
			// table10.setSpacingBefore(5);
			// table10.addCell(
			// getCellHeading(getResources().getString(R.string.place_of_next_folow_up),
			// PdfPCell.ALIGN_LEFT));
			// table10.addCell(getCell(dvalues.get(0).getPlaceOfNextFOllowUp(),
			// PdfPCell.ALIGN_CENTER));
			String place = "";
			/*
			 * if (dvalues.size() > 0) { place =
			 * dvalues.get(0).getPlaceOfNextFOllowUp();
			 * 
			 * }
			 */
			PdfPCell table10cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.place_of_next_folow_up), PdfPCell.ALIGN_LEFT));

			PdfPCell table10cell2 = new PdfPCell(getCell(place, PdfPCell.ALIGN_CENTER));

			table10cell1.setPadding(5);
			table10cell2.setPadding(5);
			table10cell1.setBorder(Rectangle.BOX);
			table10cell2.setBorder(Rectangle.BOX);

			table10.addCell(table10cell1);
			table10.addCell(table10cell2);
			doc.add(table10);

			PdfPTable table7 = new PdfPTable(2);
			table7.setWidthPercentage(90);
			// table7.setSpacingBefore(5);
			// table7.addCell(
			// getCellHeading(getResources().getString(R.string.date_of_next_folow_up),
			// PdfPCell.ALIGN_LEFT));
			// table7.addCell(getCell(dvalues.get(0).getDateOfNextFollowUp(),
			// PdfPCell.ALIGN_CENTER));
			String strdate = "";
			/*
			 * if (dvalues.size() > 0) { date =
			 * Partograph_CommonClass.getConvertedDateFormat(dvalues.get(0).
			 * getDateOfNextFollowUp(), "dd-MM-yyyy"); }
			 */
			PdfPCell table7cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.date_of_next_folow_up), PdfPCell.ALIGN_LEFT));

			PdfPCell table7cell2 = new PdfPCell(getCell(strdate, PdfPCell.ALIGN_CENTER));

			table7cell1.setPadding(5);
			table7cell2.setPadding(5);
			table7cell1.setBorder(Rectangle.BOX);
			table7cell2.setBorder(Rectangle.BOX);

			table7.addCell(table7cell1);
			table7.addCell(table7cell2);
			doc.add(table7);

			PdfPTable table1 = new PdfPTable(2);
			table1.setWidthPercentage(90);
			// table1.setSpacingBefore(5);

			PdfPTable table9 = new PdfPTable(2);
			table9.setWidthPercentage(90);
			// table9.setSpacingBefore(5);
			// table9.addCell(getCellHeading(getResources().getString(R.string.advice_given),
			// PdfPCell.ALIGN_LEFT));
			// table9.addCell(getCell(dvalues.get(0).getAdviceGiven(),
			// PdfPCell.ALIGN_CENTER));
			String advice = "";

			PdfPCell table9cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.advice_given), PdfPCell.ALIGN_LEFT));

			/*
			 * if (dvalues.size() > 0) { advice =
			 * dvalues.get(0).getAdviceGiven();
			 * 
			 * }
			 */
			PdfPCell table9cell2 = new PdfPCell(getCell(advice, PdfPCell.ALIGN_CENTER));

			table9cell1.setPadding(5);
			table9cell2.setPadding(5);
			table9cell1.setBorder(Rectangle.BOX);
			table9cell2.setBorder(Rectangle.BOX);

			table9.addCell(table9cell1);
			table9.addCell(table9cell2);
			doc.add(table9);

			// table1.addCell(getCellHeading(getResources().getString(R.string.procedures),
			// PdfPCell.ALIGN_LEFT));
			// table1.addCell(getCell(dvalues.get(0).getProcedures(),
			// PdfPCell.ALIGN_CENTER));
			String procdures = "";
			/*
			 * if (dvalues.size() > 0) { procdures =
			 * dvalues.get(0).getProcedures();
			 * 
			 * }
			 */
			PdfPCell table1cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.procedures), PdfPCell.ALIGN_LEFT));

			PdfPCell table1cell2 = new PdfPCell(getCell(procdures, PdfPCell.ALIGN_CENTER));

			table1cell1.setPadding(5);
			table1cell2.setPadding(5);
			table1cell1.setBorder(Rectangle.BOX);
			table1cell2.setBorder(Rectangle.BOX);

			table1.addCell(table1cell1);
			table1.addCell(table1cell2);
			doc.add(table1);

			PdfPTable table11 = new PdfPTable(2);
			table11.setWidthPercentage(90);
			// table11.setSpacingBefore(5);
			// table11.addCell(
			// getCellHeading(getResources().getString(R.string.discharge_diagnosis),
			// PdfPCell.ALIGN_LEFT));
			// table11.addCell(getCell(dvalues.get(0).getDischargeDiagnosis(),
			// PdfPCell.ALIGN_CENTER));

			String diagnosis = "";
			/*
			 * if (dvalues.size() > 0) { diagnosis =
			 * dvalues.get(0).getDischargeDiagnosis();
			 * 
			 * }
			 */

			PdfPCell table11cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.discharge_diagnosis), PdfPCell.ALIGN_LEFT));

			PdfPCell table11cell2 = new PdfPCell(getCell(diagnosis, PdfPCell.ALIGN_CENTER));

			table11cell1.setPadding(5);
			table11cell2.setPadding(5);
			table11cell1.setBorder(Rectangle.BOX);
			table11cell2.setBorder(Rectangle.BOX);

			table11.addCell(table11cell1);
			table11.addCell(table11cell2);
			doc.add(table11);

			PdfPTable table2 = new PdfPTable(2);
			// table2.setSpacingBefore(5);
			table2.setWidthPercentage(90);
			// table2.addCell(
			// getCellHeading(getResources().getString(R.string.complications_observed),
			// PdfPCell.ALIGN_LEFT));
			// table2.addCell(getCell(dvalues.get(0).getComplicationObserved(),
			// PdfPCell.ALIGN_CENTER));

			String compl = "";
			/*
			 * if (dvalues.size() > 0) { compl =
			 * dvalues.get(0).getComplicationObserved(); }
			 */

			PdfPCell table2cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.complications_observed), PdfPCell.ALIGN_LEFT));

			PdfPCell table2cell2 = new PdfPCell(getCell(compl, PdfPCell.ALIGN_CENTER));

			table2cell1.setPadding(5);
			table2cell2.setPadding(5);
			table2cell1.setBorder(Rectangle.BOX);
			table2cell2.setBorder(Rectangle.BOX);

			table2.addCell(table2cell1);
			table2.addCell(table2cell2);
			doc.add(table2);

			PdfPTable table12 = new PdfPTable(2);
			// table12.setSpacingBefore(5);
			table12.setWidthPercentage(90);
			// table12.addCell(getCellHeading(getResources().getString(R.string.finding_history),
			// PdfPCell.ALIGN_LEFT));
			// table12.addCell(getCell(dvalues.get(0).getFindingHistorys(),
			// PdfPCell.ALIGN_CENTER));

			String history = "";
			/*
			 * if (dvalues.size() > 0) { history =
			 * dvalues.get(0).getFindingHistorys();
			 * 
			 * // } }
			 */
			PdfPCell table12cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.finding_history), PdfPCell.ALIGN_LEFT));

			PdfPCell table12cell2 = new PdfPCell(getCell(history, PdfPCell.ALIGN_CENTER));

			table12cell1.setPadding(5);
			table12cell2.setPadding(5);
			table12cell1.setBorder(Rectangle.BOX);
			table12cell2.setBorder(Rectangle.BOX);

			table12.addCell(table12cell1);
			table12.addCell(table12cell2);
			doc.add(table12);

			PdfPTable table8 = new PdfPTable(2);
			// table8.setSpacingBefore(5);
			table8.setWidthPercentage(90);
			// table8.addCell(getCellHeading(getResources().getString(R.string.patient_disposition),
			// PdfPCell.ALIGN_LEFT));
			// table8.addCell(getCell(dvalues.get(0).getPatientDisposition(),
			// PdfPCell.ALIGN_CENTER));
			String dsposition = "";
			/*
			 * if (dvalues.size() > 0) { dsposition =
			 * dvalues.get(0).getPatientDisposition(); // } }
			 */
			PdfPCell table8cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.patient_disposition), PdfPCell.ALIGN_LEFT));

			PdfPCell table8cell2 = new PdfPCell(getCell(dsposition, PdfPCell.ALIGN_CENTER));

			table8cell1.setPadding(5);
			table8cell2.setPadding(5);
			table8cell1.setBorder(Rectangle.BOX);
			table8cell2.setBorder(Rectangle.BOX);

			table8.addCell(table8cell1);
			table8.addCell(table8cell2);
			doc.add(table8);

			PdfPTable table13 = new PdfPTable(2);
			// table13.setSpacingBefore(5);
			table13.setWidthPercentage(90);
			// table13.addCell(getCellHeading(getResources().getString(R.string.admisiion_reasons),
			// PdfPCell.ALIGN_LEFT));
			// table13.addCell(getCell(dvalues.get(0).getAdmissionReasons(),
			// PdfPCell.ALIGN_CENTER));
			String reason = " ";
			/*
			 * if (dvalues.size() > 0) { reason =
			 * dvalues.get(0).getAdmissionReasons(); }
			 */
			PdfPCell table13cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.admisiion_reasons), PdfPCell.ALIGN_LEFT));

			PdfPCell table13cell2 = new PdfPCell(getCell(reason, PdfPCell.ALIGN_CENTER));

			table13cell1.setPadding(5);
			table13cell2.setPadding(5);
			table13cell1.setBorder(Rectangle.BOX);
			table13cell2.setBorder(Rectangle.BOX);

			table13.addCell(table13cell1);
			table13.addCell(table13cell2);
			doc.add(table13);

			PdfPTable table3 = new PdfPTable(2);
			table3.setWidthPercentage(90);
			// table3.setSpacingBefore(5);
			// table3.addCell(getCellHeading(getResources().getString(R.string.overall_remarks),
			// PdfPCell.ALIGN_LEFT));
			// table3.addCell(getCell(dvalues.get(0).getRemarks(),
			// PdfPCell.ALIGN_CENTER));
			String remarks = " ";
			/*
			 * if (dvalues.size() > 0) { remarks = dvalues.get(0).getRemarks();
			 * 
			 * }
			 */
			PdfPCell table3cell1 = new PdfPCell(
					getCellHeading(getResources().getString(R.string.overall_remarks), PdfPCell.ALIGN_LEFT));

			PdfPCell table3cell2 = new PdfPCell(getCell(remarks, PdfPCell.ALIGN_CENTER));

			table3cell1.setPadding(5);
			table3cell2.setPadding(5);
			table3cell1.setBorder(Rectangle.BOX);
			table3cell2.setBorder(Rectangle.BOX);

			table3.addCell(table3cell1);
			table3.addCell(table3cell2);
			doc.add(table3);

			PdfPTable nameofHC = new PdfPTable(2);
			nameofHC.setWidthPercentage(90);
			// nameofHC.setSpacingBefore(5);
			// nameofHC.addCell(getCellHeading(getResources().getString(R.string.name_of_healthcare_provider),
			// PdfPCell.ALIGN_LEFT));
			// nameofHC.addCell(getCell(dvalues.get(0).getHCProviderName(),
			// PdfPCell.ALIGN_CENTER));
			String name = " ";
			/*
			 * if (dvalues.size() > 0) { name =
			 * dvalues.get(0).getHCProviderName();
			 * 
			 * }
			 */
			PdfPCell nameofHCcell1 = new PdfPCell(getCellHeading(
					getResources().getString(R.string.name_of_healthcare_provider), PdfPCell.ALIGN_LEFT));

			PdfPCell nameofHCcell2 = new PdfPCell(getCell(name, PdfPCell.ALIGN_CENTER));

			nameofHCcell1.setPadding(5);
			nameofHCcell2.setPadding(5);
			nameofHCcell1.setBorder(Rectangle.BOX);
			nameofHCcell2.setBorder(Rectangle.BOX);

			nameofHC.addCell(nameofHCcell1);
			nameofHC.addCell(nameofHCcell2);
			doc.add(nameofHC);

			PdfPTable designationofHC = new PdfPTable(2);
			designationofHC.setWidthPercentage(90);

			// designationofHC.setSpacingBefore(5);
			// designationofHC.addCell(getCellHeading(
			// getResources().getString(R.string.designation_of_healthcare_provider),
			// PdfPCell.ALIGN_LEFT));
			// designationofHC.addCell(getCell(dvalues.get(0).getDesignation(),
			// PdfPCell.ALIGN_CENTER));

			String desig = " ";
			/*
			 * if (dvalues.size() > 0) { desig =
			 * dvalues.get(0).getDesignation();
			 * 
			 * }
			 */
			PdfPCell designationofHCcell1 = new PdfPCell(getCellHeading(
					getResources().getString(R.string.designation_of_healthcare_provider), PdfPCell.ALIGN_LEFT));

			PdfPCell designationofHCcell2 = new PdfPCell(getCell(desig, PdfPCell.ALIGN_CENTER));

			designationofHCcell1.setPadding(5);
			designationofHCcell2.setPadding(5);
			designationofHCcell1.setBorder(Rectangle.BOX);
			designationofHCcell2.setBorder(Rectangle.BOX);

			designationofHC.addCell(designationofHCcell1);
			designationofHC.addCell(designationofHCcell2);
			doc.add(designationofHC);

			PdfPTable signatureofHC = new PdfPTable(1);
			signatureofHC.setWidthPercentage(90);
			signatureofHC.setSpacingBefore(45);
			signatureofHC.addCell(getCellHeading(getResources().getString(R.string.signature_of_health_care_provider),
					PdfPCell.ALIGN_RIGHT));

			doc.add(signatureofHC);

			/*
			 * PdfPTable table = new PdfPTable(2); table.setWidthPercentage(98);
			 * table.addCell(getCell(getResources().getString(R.string.
			 * date_of_discharge) + ": " +
			 * dvalues.get(0).getDateTimeOfDischarge(), PdfPCell.ALIGN_LEFT));
			 * table.addCell(getCell(getResources().getString(R.string.
			 * place_of_next_folow_up) + ": " +
			 * dvalues.get(0).getPlaceOfNextFOllowUp(), PdfPCell.ALIGN_RIGHT));
			 * 
			 * doc.add(table);
			 * 
			 * PdfPTable table7 = new PdfPTable(2);
			 * table7.setWidthPercentage(98);
			 * table7.addCell(getCell(getResources().getString(R.string.
			 * date_of_next_folow_up) + ": " +
			 * dvalues.get(0).getDateOfNextFollowUp(), PdfPCell.ALIGN_LEFT));
			 * table7.addCell(
			 * getCell(getResources().getString(R.string.advice_given) + ": " +
			 * dvalues.get(0).getAdviceGiven(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table7);
			 * 
			 * PdfPTable table1 = new PdfPTable(2);
			 * table1.setWidthPercentage(98);
			 * 
			 * table1.addCell(
			 * getCell(getResources().getString(R.string.procedures) + ": " +
			 * dvalues.get(0).getProcedures(), PdfPCell.ALIGN_LEFT));
			 * table1.addCell(getCell(getResources().getString(R.string.
			 * discharge_diagnosis) + ": " +
			 * dvalues.get(0).getDischargeDiagnosis(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table1);
			 * 
			 * PdfPTable table2 = new PdfPTable(2);
			 * table2.setWidthPercentage(98);
			 * table2.addCell(getCell(getResources().getString(R.string.
			 * complications_observed) + ": " +
			 * dvalues.get(0).getComplicationObserved(), PdfPCell.ALIGN_LEFT));
			 * table2.addCell(getCell(
			 * getResources().getString(R.string.finding_history) + ": " +
			 * dvalues.get(0).getFindingHistorys(), PdfPCell.ALIGN_RIGHT));
			 * 
			 * doc.add(table2);
			 * 
			 * PdfPTable table8 = new PdfPTable(2);
			 * table8.setWidthPercentage(98);
			 * table8.addCell(getCell(getResources().getString(R.string.
			 * patient_disposition) + ": " +
			 * dvalues.get(0).getPatientDisposition(), PdfPCell.ALIGN_LEFT));
			 * table8.addCell(getCell(
			 * getResources().getString(R.string.admisiion_reasons) + ": " +
			 * dvalues.get(0).getAdmissionReasons(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table8);
			 * 
			 * PdfPTable table3 = new PdfPTable(1);
			 * table3.setWidthPercentage(98);
			 * 
			 * table3.addCell(
			 * getCell(getResources().getString(R.string.overall_remarks) + ": "
			 * + dvalues.get(0).getRemarks(), PdfPCell.ALIGN_LEFT)); //
			 * table3.addCell(getCell(getResources().getString(R.string.
			 * patient_disposition) // + ": " // +
			 * dvalues.get(0).getPatientDisposition(), PdfPCell.ALIGN_RIGHT));
			 * doc.add(table3);
			 */

			// Rectangle rect = new Rectangle(100, 100, 900, 500);
			// Rectangle rect = new Rectangle(5, 5, 500, 500);
			// Rectangle rect = new Rectangle(5, 5, 585, 770);
			Rectangle rect = new Rectangle(10, 20, 585, 808);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.enableBorderSide(1);
			rect.setBorder(2);
			// rect.setBorderColor(context.getResources().getColor(R.color.black));
			rect.setBorder(Rectangle.BOX);
			rect.setBorderWidth(1);
			doc.add(rect);

			doc.close();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	void womanBasicInfo() {
		try {
			String delstatus;
			ArrayList<String> val = new ArrayList<String>();
			val.add(context.getResources().getString(R.string.name) + ":" + woman.getWomen_name());
			val.add(context.getResources().getString(R.string.age) + ":" + woman.getAge()
					+ context.getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {// 12Jan2017 Arpitha
				String strDOA = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				val.add(context.getResources().getString(R.string.doadm) + ":" + strDOA + "/"
						+ woman.getTime_of_admission());
			} else {// 12Jan2017 Arpitha
				val.add(context.getResources().getString(R.string.doadm) + ":" + woman.getDate_of_reg_entry());
			}
			if (woman.getGestationage() == 0)
				val.add(context.getResources().getString(R.string.gest_short_label) + " "
						+ context.getResources().getString(R.string.notknown));
			else
				val.add(woman.getGestationage() + context.getResources().getString(R.string.wks) + " "
						+ woman.getGest_age_days() + context.getResources().getString(R.string.days));

			womanbasics = new PdfPTable(val.size());

			womanbasics.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < val.size(); k++) {
				String header = val.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.TIMES_ROMAN, 12, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			ArrayList<String> arrVal = new ArrayList<String>();
			arrVal.add(context.getResources().getString(R.string.gravida) + ":" + woman.getGravida());
			arrVal.add(context.getResources().getString(R.string.para) + ":" + woman.getPara());
			arrVal.add(context.getResources().getString(R.string.facility) + ": " + woman.getFacility_name() + "["
					+ woman.getFacility() + "]");
			if (woman.getDel_type() != 0) {
				delstatus = context.getResources().getString(R.string.delivered);
			} else
				delstatus = context.getResources().getString(R.string.del_prog);
			arrVal.add(delstatus);

			for (int k = 0; k < arrVal.size(); k++) {
				String header = arrVal.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.TIMES_ROMAN, 12, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womanbasics.addCell(cell);
			}

			womanbasics.setSpacingBefore(3);

			womanbasics.completeRow();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public PdfPCell getCellHeading(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.TIMES_ROMAN, 12, Font.BOLD)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	public PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text, new Font(Font.TIMES_ROMAN, 12, Font.NORMAL)));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}

	void DeliveryInformation() throws NotFoundException, Exception {

		if (woman.getDel_type() != 0) {
			ArrayList<String> deliveryInfo = new ArrayList<String>();
			String delType = "";
			if (woman.getDel_type() == 1)
				delType = "Normal";
			else if (woman.getDel_type() == 2)
				delType = "Cesarean";
			else if (woman.getDel_type() == 3)
				delType = "Instrumental";

			deliveryInfo.add(delType);

			if (woman.getDelTypeReason() != null) {
				String[] delTypeRes = woman.getDelTypeReason().split(",");
				String[] delTypeResArr = null;
				if (woman.getDel_type() == 2)
					delTypeResArr = getResources().getStringArray(R.array.spnreasoncesareanValues);
				else if (woman.getDel_type() == 3)
					delTypeResArr = getResources().getStringArray(R.array.spnreasoninstrumental);

				String delTypeResDisplay = "";
				if (delTypeResArr != null) {
					if (delTypeRes != null && delTypeRes.length > 0) {
						for (String str : delTypeRes) {
							if (str != null && str.length() > 0)
								delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
						}
					}
				}

				deliveryInfo.add(delTypeResDisplay);
			}

			deliveryInfo.add(context.getResources().getString(R.string.dod) + ": " + Partograph_CommonClass
					.getConvertedDateFormat(woman.getDel_Date(), Partograph_CommonClass.defdateformat) + " "
					+ woman.getDel_Time());

			deliveryInfo.add(context.getResources().getString(R.string.noofchild) + ": " + "" + woman.getNo_of_child());

			womandelInfo = new PdfPTable(deliveryInfo.size());

			// womandelInfo.setWidths(new int[] { 7, 5, 11, 11 });

			for (int k = 0; k < deliveryInfo.size(); k++) {
				String header = deliveryInfo.get(k);
				PdfPCell cell = new PdfPCell();
				cell.setPhrase(new Phrase(header, new Font(Font.TIMES_ROMAN, 12, Font.NORMAL)));
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// cell.setPadding(2);
				cell.setBorder(0);

				womandelInfo.addCell(cell);

				womandelInfo.setSpacingBefore(6);

			}
			womandelInfo.completeRow();

			// }

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.email).setVisible(false);

		menu.findItem(R.id.registration).setVisible(false);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		 menu.findItem(R.id.lat).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.disc).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				/*
				 * displayConfirmationAlert(getResources().getString(R.string.
				 * exit_msg), getResources().getString(R.string.logout));
				 */
				Intent login = new Intent(PostPartumCare_Activity.this, LoginActivity.class);
				startActivity(login);
				return true;

			// case R.id.sync:
			// menuItem = item;
			// menuItem.setActionView(R.layout.progressbar);
			// menuItem.expandActionView();
			// calSyncMtd();
			// return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(PostPartumCare_Activity.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;
				
			case R.id.lat:
				Intent lat = new Intent(PostPartumCare_Activity.this, LatentPhase_Activity.class);
				lat.putExtra("woman", woman);
				startActivity(lat);
				return true;

			case R.id.sms:
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					Partograph_CommonClass.display_messagedialog(PostPartumCare_Activity.this, woman.getUserId());
				} else
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_sim),
							Toast.LENGTH_LONG).show();
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.home:
				/*
				 * displayConfirmationAlert(getResources().getString(R.string.
				 * exit_msg), getResources().getString(R.string.home));
				 */
				Intent home = new Intent(PostPartumCare_Activity.this, Activity_WomenView.class);
				startActivity(home);
				return true;
			// 22May2017 Arpitha - v2.6
			case R.id.summary:
				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);
				return true;
			case R.id.usermanual:
				Intent manual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(manual);
				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(this);
				return true;

			case R.id.disch:
				Intent disch = new Intent(PostPartumCare_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;
				
			case R.id.disc:
				Intent disc = new Intent(PostPartumCare_Activity.this, DiscargeDetails_Activity.class);
				disc.putExtra("woman", woman);
				startActivity(disc);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
//				Intent disc = new Intent(PostPartumCare_Activity.this, DiscargeDetails_Activity.class);
//				disc.putExtra("woman", woman);
//				startActivity(disc);

				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(PostPartumCare_Activity.this, woman);
//					loadWomendata(displayListCountddel);
//					if(adapter!=null)
//					adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(PostPartumCare_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
				return true;
			case R.id.viewprofile:
				Intent view = new Intent(PostPartumCare_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				Intent ref = new Intent(PostPartumCare_Activity.this, ReferralInfo_Activity.class);
				ref.putExtra("woman", woman);
				startActivity(ref);
				return true;

			case R.id.stages:
				Intent stages = new Intent(PostPartumCare_Activity.this, StageofLabor.class);
				stages.putExtra("woman", woman);
				startActivity(stages);
				return true;

			case R.id.apgar:
				Intent apgar = new Intent(PostPartumCare_Activity.this, Activity_Apgar.class);
				apgar.putExtra("woman", woman);
				startActivity(apgar);
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(PostPartumCare_Activity.this, woman);
				return true;

			case R.id.parto:
				Intent parto = new Intent(PostPartumCare_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha
				
				

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public void onBackPressed() {
		try {
			/*
			 * displayConfirmationAlert(getResources().getString(R.string.
			 * exit_msg), getResources().getString(R.string.home));
			 */
			Intent home = new Intent(PostPartumCare_Activity.this, Activity_WomenView.class);
			startActivity(home);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/*
	 * private void displayConfirmationAlert_exit(String exit_msg, final String
	 * classname) throws Exception { AppContext.addToTrace( new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName()); final Dialog dialog = new
	 * Dialog(PostPartumCare_Activity.this); dialog.setTitle(Html.fromHtml(
	 * "<font color='" + getResources().getColor(R.color.appcolor) + "'>" +
	 * getResources().getString(R.string.alert) + "</font>"));
	 * dialog.setContentView(R.layout.temp_alertdialog);
	 * 
	 * dialog.show();
	 * 
	 * final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
	 * Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes); Button
	 * imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno); // Arpitha
	 * 27may16 final TextView txtdialog1 = (TextView)
	 * dialog.findViewById(R.id.txtpulseval);
	 * 
	 * final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);
	 * 
	 * txtdialog1.setVisibility(View.GONE); txtdialog.setVisibility(View.GONE);
	 * txtdialog6.setText(exit_msg);
	 * imgbtnyes.setText(getResources().getString(R.string.yes));
	 * imgbtnno.setText(getResources().getString(R.string.no));
	 * 
	 * imgbtnyes.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View v) {
	 * 
	 * dialog.cancel();
	 * 
	 * if (classname.equalsIgnoreCase(getResources().getString(R.string.home)))
	 * { Intent i = new Intent(PostPartumCare_Activity.this,
	 * Activity_WomenView.class); startActivity(i); }
	 * 
	 * if
	 * (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {
	 * 
	 * Intent i = new Intent(PostPartumCare_Activity.this, LoginActivity.class);
	 * startActivity(i);
	 * 
	 * }
	 * 
	 * } });
	 * 
	 * imgbtnno.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { dialog.cancel();
	 * 
	 * } });
	 * 
	 * int dividerId = dialog.getContext().getResources().getIdentifier(
	 * "android:id/titleDivider", null, null); View divider =
	 * dialog.findViewById(dividerId);
	 * divider.setBackgroundColor(getResources().getColor(R.color.appcolor));//
	 * 08Feb2017
	 * 
	 * dialog.show();
	 * 
	 * dialog.setCancelable(false);
	 * 
	 * }
	 */

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(PostPartumCare_Activity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		String alertmsg = "";
		String values = "";

		imgbtnyes.setText(getResources().getString(R.string.save));
		imgbtnno.setText(getResources().getString(R.string.recheck));

		if (pulval > 120 || pulval < 50) {
			alertmsg = alertmsg + getResources().getString(R.string.pul_val) + "\n";
			values = values + "" + pulval + "\n";

		}
		if (bpsysval < 80 || bpsysval > 160) {
			alertmsg = alertmsg + getResources().getString(R.string.bp_sys) + "\n";
			values = values + "" + bpsysval + "\n";

		}
		if (bpdiaval < 50 || bpdiaval >= 110) {
			alertmsg = alertmsg + getResources().getString(R.string.bp_dia) + "\n";
			values = values + "" + bpdiaval + "\n";

		}
		txtdialog6.setText(getResources().getString(R.string.bp_val));

		txtdialog.setText(alertmsg);
		txtdialog1.setText(values);
		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("confirmation")) {
					try {
						setData();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(PostPartumCare_Activity.this, Activity_WomenView.class);
					startActivity(i);
				}

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {

					Intent i = new Intent(PostPartumCare_Activity.this, LoginActivity.class);
					startActivity(i);

				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	void setData() throws Exception {
		postpojo = new PostPartum_Pojo();
		postpojo.setWomanId(woman.getWomenId());
		postpojo.setUserId(woman.getUserId());
		postpojo.setWomanName(woman.getWomen_name());
		postpojo.setBp(
				aq.id(R.id.etbpsyspost).getText().toString() + "/" + aq.id(R.id.etbpdiapost).getText().toString());
		postpojo.setPulse(Integer.parseInt(aq.id(R.id.etpulsepost).getText().toString()));

		postpojo.setExamination(aq.id(R.id.etexamination).getText().toString());
		postpojo.setPallor(aq.id(R.id.etpallor).getText().toString());
		postpojo.setLochia(aq.id(R.id.etlochia).getText().toString());
		postpojo.setPerineal(aq.id(R.id.etperinealcare).getText().toString());
		postpojo.setBreastExamination(aq.id(R.id.etbreastexamination).getText().toString());
		postpojo.setInvolution(aq.id(R.id.etinvolution).getText().toString());
		postpojo.setAdvice(aq.id(R.id.etadvicepost).getText().toString());
		postpojo.setDateTimeOfEntry(postpartumdate + " " + aq.id(R.id.ettimepost).getText().toString());

		postpojo.setOther(aq.id(R.id.etother).getText().toString());

		String seladmittedwithIds = "";
		List<String> selectedadmittedwith = mspnpresentigcomplaints.getSelectedStrings();

		if (selectedadmittedwith != null && selectedadmittedwith.size() > 0) {
			for (String str : selectedadmittedwith) {
				if (selectedadmittedwith.indexOf(str) != selectedadmittedwith.size() - 1)
					seladmittedwithIds = seladmittedwithIds + presentingcomplaintsMap.get(str) + ",";
				else
					seladmittedwithIds = seladmittedwithIds + presentingcomplaintsMap.get(str);
			}
		}

		postpojo.setComplaints(seladmittedwithIds);

		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(woman.getUserId());
		boolean isDataInserted = dbh.addPostPartumData(postpojo, transId);
		if (isDataInserted) {
			dbh.iNewRecordTrans(woman.getUserId(), transId, Partograph_DB.TBL_POSTPARTUM);

			commitTrans();
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.data_added_successfully),
					Toast.LENGTH_SHORT).show();

		}
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	private class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.pickadate) + "</font>"));

			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {

					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		Date refdate = null;

		// 23oct2016 Arpitha
		if (strrefdatetime != null) {
			refdate = new SimpleDateFormat("yyyy-MM-dd").parse(strrefdatetime);
		}

		// 20Sep2017 Arpitha
		if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
			postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strpostpartumdatetime);
		} // 20Sep2017 Arpitha

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date selected = format.parse(selecteddate);
			// if (discDate) {
			if (selected.after(new Date())) {
				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.postpartumcare_datetime_after_current_datetime),
						PostPartumCare_Activity.this);

			} else if (doa != null && selected.before(doa)) {
				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.postpartumcare_datetime_before_reg_datetime) + " "
								+ strdateOfAdm,
						PostPartumCare_Activity.this);

			} else if (dateOfDel != null && selected.before(dateOfDel)) {
				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.postpartumcare_datetime_before_del_datetime) + " "
								+ strdelTime,
						PostPartumCare_Activity.this);

			} else if (lastpartoENtry != null && selected.before(lastpartoENtry)) {
				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.postpartumcare_datetime_before_lastparto_datetime) ,
						PostPartumCare_Activity.this);

			}

			// 20Sep2017 Arpitha
			else if (postpartumdatetime != null && selected.before(postpartumdatetime)) {

				String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
						Partograph_CommonClass.defdateformat);
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.postpartum_datetime_cannot_be_before_postpartum_datetime)
								+ " - " + latdate,
						Toast.LENGTH_SHORT).show();

				/*
				 * etdateofreferral.setText(Partograph_CommonClass.
				 * getConvertedDateFormat(todaysDate,
				 * Partograph_CommonClass.defdateformat)); strRefDate =
				 * todaysDate;
				 */

			} // 20Sep2017 Arpitha

			// 19Sep2017 Arpitha
			else if (refdate != null && selected.before(refdate)) {

				String referraldate = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime,
						Partograph_CommonClass.defdateformat);// 17April2017
																// Arpitha

				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.postpartum_datetime_cannot_be_before_refedatetime) + " "
								+ referraldate,
						PostPartumCare_Activity.this);

				/*
				 * etdeldate.setText(Partograph_CommonClass.
				 * getConvertedDateFormat(todaysDate,
				 * Partograph_CommonClass.defdateformat)); strDeldate =
				 * todaysDate;
				 */

			} // 19Sep2017 Arpitha

			else {
				aq.id(R.id.etdatepost).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
						Partograph_CommonClass.defdateformat));

				postpartumdate = selecteddate;
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public void showtimepicker() {
		DialogFragment newFragment = new TimePickerFragment();
		FragmentManager fm = getFragmentManager();
		newFragment.show(fm, "timePicker");
	}

	private static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
		public TimePickerFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {

			// isTimeRefClicked = false;//19May2017 Arpitha - 2.6

			hour = hourOfDay;
			minute = minutes;

			try {

				// 20Sep2017 Arpitha
				if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
					// strpostpartumdatetime =
					// strpostpartumdatetime.replace("/", newChar)
					postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strpostpartumdatetime);
				} // 20Sep2017 Arpitha

				posttime = postpartumdate + " "
						+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));

				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");

				Date refdate = null;

				// 23oct2016 Arpitha
				if (strrefdatetime != null) {
					refdate = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(strrefdatetime);
				}

				Date selectedTime = format.parse(posttime);

				if (selectedTime.after(new Date())) {
					/*
					 * Partograph_CommonClass.displayAlertDialog(
					 * getResources().getString(R.string.
					 * discharge_datetime_before_reg_datetime), context);
					 */
					Toast.makeText(context,
							getResources().getString(R.string.postpartumcare_datetime_after_current_datetime),
							Toast.LENGTH_LONG).show();

				} else if (selectedTime.before(timeOfAdm)) {
					Toast.makeText(context,
							getResources().getString(R.string.postpartumcare_datetime_before_reg_datetime) + " "
									+ timeOfAdm,
							Toast.LENGTH_LONG).show();
					// Partograph_CommonClass.displayAlertDialog(
					// getResources().getString(R.string.discharge_datetime_before_del_datetime),
					// context);

				} else if (delTime != null && selectedTime.before(delTime)) {
					Toast.makeText(context,
							getResources().getString(R.string.postpartumcare_datetime_before_del_datetime) + " "
									+ strdelTime,
							Toast.LENGTH_LONG).show();
					// Partograph_CommonClass.displayAlertDialog(
					// getResources().getString(R.string.discharge_datetime_before_lastparto_datetime),
					// context);

				} else if (lastpartoENtry != null && selectedTime.before(lastpartoENtry)) {
					Toast.makeText(context,
							getResources().getString(R.string.postpartumcare_datetime_before_lastparto_datetime) + " "
									,
							Toast.LENGTH_LONG).show();
					// Partograph_CommonClass.displayAlertDialog(
					// getResources().getString(R.string.discharge_datetime_after_current_datetime),
					// context);

				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && selectedTime.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getActivity(),
							getResources().getString(R.string.postpartum_datetime_cannot_be_before_postpartum_datetime)
									+ " - " + latdate + " " + strpostpartumdatetime.split(" ")[1],
							Toast.LENGTH_SHORT).show();

					/*
					 * etdateofreferral.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strRefDate =
					 * todaysDate;
					 */

				} // 20Sep2017 Arpitha

				// 19Sep2017 Arpitha
				else if (refdate != null && selectedTime.before(refdate)) {

					String referraldate = Partograph_CommonClass.getConvertedDateFormat(strrefdatetime.split(" ")[0],
							Partograph_CommonClass.defdateformat);// 17April2017
																	// Arpitha

					Toast.makeText(context,
							getResources().getString(R.string.postpartum_datetime_cannot_be_before_refedatetime) + " - "
									+ referraldate + " " + strrefdatetime.split(" ")[1],
							Toast.LENGTH_SHORT).show();

					/*
					 * etdeldate.setText(Partograph_CommonClass.
					 * getConvertedDateFormat(todaysDate,
					 * Partograph_CommonClass.defdateformat)); strDeldate =
					 * todaysDate;
					 */

				} // 19Sep2017 Arpitha

				else {

					aq.id(R.id.ettimepost).getEditText().setText(
							new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));
				}
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

		}
	}

	// 26April2017 Arpitha
	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {
			dateofreg = sdf.parse(date1);
			dateofdelivery = sdf.parse(date2);

			String strDate1 = Partograph_CommonClass.getConvertedDateFormat(date1,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha
			strDate1 = strDate1 + " " + date1.substring(11);

			String strDate2 = Partograph_CommonClass.getConvertedDateFormat(date2,
					Partograph_CommonClass.defdateformat);// 17April2017 Arpitha
			strDate2 = strDate2 + " " + date2.substring(11);

			if (dateofdelivery.before(dateofreg)) {
				if (i == 1)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.postpartumcare_datetime_before_reg_datetime) + " - "
									+ strDate1,
							context);
				else if (i == 2)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.postpartumcare_datetime_before_lastparto_datetime) + " - "
									+ strDate1,
							context);
				else if (i == 3)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.postpartumcare_datetime_after_current_datetime), context);
				// 02Nov2016 Arpitha
				else if (i == 4)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.postpartum_datetime_cannot_be_before_refedatetime),
							context);
				else if (i == 5)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.postpartum_datetime_cannot_be_before_postpartum_datetime),
							context);
				else if (i == 6)
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.postpartumcare_datetime_before_del_datetime), context);

				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				// toa =
				// Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoa)
						.text(getResources().getString(R.string.reg) + ": " + doa + "/" + woman.getTime_of_admission());
			} else {

				aq.id(R.id.wdoa).text(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}

			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));
			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				aq.id(R.id.trdelstatus).visible();

				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);

				String gender = "";
				if (woman.getBabysex1() == 0)
					gender = getResources().getString(R.string.male);
				else
					gender = getResources().getString(R.string.female);

				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus).text(getResources().getString(R.string.del) + ":"
						+ deltype[woman.getDel_type() - 1] + "," + dod + "/" + woman.getDel_Time() + "," + gender);

				String gender2 = "";
				if (woman.getBabysex2() == 0)
					gender = getResources().getString(R.string.male);
				else
					gender = getResources().getString(R.string.female);
				if (woman.getNo_of_child() == 2)
					aq.id(R.id.wdeldate).text(getResources().getString(R.string.second_baby) + ": "
							+ woman.getDel_time2() + "," + gender2);

				// tod =
				// Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				// aq.id(R.id.wdeldate)
				// .text(getResources().getString(R.string.del) + ": " + dod +
				// "/" + woman.getDel_Time());

			}

		}
	}

}
