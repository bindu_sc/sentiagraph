package com.bluecrimson.viewprofile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bc.partograph.womenview.Fragment_DIP;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.usermanual.UseManual;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class ViewProfile_Activity extends FragmentActivity implements OnClickListener, OnFocusChangeListener {

	AQuery aq;
	Partograph_DB dbh;
	String path = null;
	ByteArrayOutputStream baos;
	File imgfileBeforeResult;
	Uri outputFileUri;
	private static final int IMAGE_CAPTURE = 0;
	Bitmap bitmap;
	Women_Profile_Pojo wpojo;
	int womanAge;
	UserPojo user;
	String womenID;
	int babysex1, babysex2;
	RadioButton rdmale1, rdfemale1, rdmale2, rdfemale2;
	String todaysDate, selecteddate;
	int year, mon, day;
	static final int TIME_DIALOG_ID = 999;
	private int hour;
	private int minute;
	int delivery_type, delivery_result1, delivery_result2;
	String no_of_child;
	TableRow trchilddet1, tr_childdetsex1, tr_childdet2, tr_childsex2, tr_secondchilddetails, tr_result2, tr_deltime2;
	String delTime;
	int numofchildren;
	boolean isDelTime1;
	byte[] image1;
	int risk, memb_pres_abs;
	boolean isDelinfoadd = false;
	public static int viewtabpos = 0;
	public static boolean isViewactivity;
	boolean isremovephotoClicked;
	RadioButton rdmotheralive, rdmotherdead; // changed on 27Mar2015
	String strDeldate, strRegdate;
	int igravida, ipara, igest_age_days;
	// 04Dec2015
	TableRow tr_deltypereason, tr_deltypeotherreasons, tr_deltypereasonc;
	EditText txtdeltypereason, txtdeltypeotherreasons;
	MultiSelectionSpinner mspndeltypereason;
	LinkedHashMap<String, Integer> deltypereasonMap;
	// 25dec2015
	MultiSelectionSpinner mspnriskoptions;
	TableRow trriskoptions;
	// 05jan2016
	RadioButton rdepiyes, rdepino, rdcatgut, rdvicryl;
	MultiSelectionSpinner mspntears;
	LinkedHashMap<String, Integer> tearsMap;
	TableRow trsuturematerial;
	int episiotomy = 0, suturematerial = 0;
	EditText ettears;
	// 9Jan0216
	MultiSelectionSpinner mspnadmittedwith;
	// 19jan2016
	int bldgrp = 0;
	// 25Jan2016
	boolean isLmp = false;
	boolean isupdatedel = false;
	// Changed by Arpita
	boolean islmpdate = false;
	String iweight = "";
	// changed by Arpitha on 20/05/16
	RadioButton rd_feet;
	RadioButton rd_cm, rd_htdontknow; // 25Aug2016 bindu
	int height_numeric = 0;
	int height_decimal = 0;
	int height_unit;
	// updated on 6/6/16 by Arpitha
	String time_of_adm;
	String strlmpdate, stredddate;
	ImageView imgphoto;
	ImageView imgwt1_warning, imgwt2_warning;
	RadioButton rd_highrisk;
	CheckBox chkgest;
	String stredd = "";
	String strlmp = "";
	// updated on25july2016 by Arpitha
	ArrayList<String> risk_observed;
	String robserved = "";
	boolean isage = false;
	boolean isheight = false;
	boolean isweight = false;
	RadioButton rd_lowrisk;
	int para;
	// changed on 18july2016 by Arpitha
	static String strlastentrytime;
	// 28Sep2016 Arpitha
	int age = 0;
	double height = 0;
	double weight = 0;
	String height_feet;
	String feet_height;
	Double d;
	int baby1weight;
	int baby2weight;
	int duration = 0;
	Women_Profile_Pojo woman;
	RadioButton rdvertext, rdbreech;// 20NOv2016 Arpitha
	int normaltype = 1;// 20NOv2016 Arpitha
	RadioButton rd_memb_pre;
	RadioButton rd_memb_abs;
	RadioGroup rdnormaltype;// 27Feb2017 Arpitha
	static String strrefdatetime = null;// 12April2017 Arpitha
	Date refdatetime;// 12April2017 Arpitha
	String selectedriskOptions = "";// 19April2017 Arpitha
	RadioGroup rgheight;// 25April2017 Arpitha
	RadioGroup rgbabypresentation;// 05may2017 Arpitha- v2.6 mantis id 0000231
	RadioButton rd_vertex_pres, rd_breech_pres, rd_others_pres;// 05may2017
																// Arpitha- v2.6
																// mantis id
																// 0000231
	int presentation = 0;// 05may2017 Arpitha- v2.6 mantis id 0000231
	boolean vertex;
	RadioGroup rdsuture;// 11Sep2017 Arpitha
	Date refdate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_newviewprofile);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			viewtabpos = Partograph_CommonClass.curr_tabpos;
			isViewactivity = true;

			aq = new AQuery(this);
			dbh = Partograph_DB.getInstance(getApplicationContext());
			user = Partograph_CommonClass.user;

			// get the action bar
			ActionBar actionBar = getActionBar();

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);
			initializeScreen(aq.id(R.id.rlregistration1).getView());
			// updated on 11july2016 by Arpitha
			imgwt1_warning = (ImageView) findViewById(R.id.warweight1);
			imgwt2_warning = (ImageView) findViewById(R.id.warweight2);

			imgwt1_warning.setVisibility(View.INVISIBLE);
			imgwt2_warning.setVisibility(View.INVISIBLE);

			rdmale1 = (RadioButton) findViewById(R.id.rd_male1);
			rdfemale1 = (RadioButton) findViewById(R.id.rd_female1);
			rdmale2 = (RadioButton) findViewById(R.id.rd_male2);
			rdfemale2 = (RadioButton) findViewById(R.id.rd_female2);
			tr_childdet2 = (TableRow) findViewById(R.id.tr_childdet2);
			tr_childsex2 = (TableRow) findViewById(R.id.tr_childsex2);
			trchilddet1 = (TableRow) findViewById(R.id.tr_childdet1);
			tr_childdetsex1 = (TableRow) findViewById(R.id.tr_childdetsex1);
			tr_secondchilddetails = (TableRow) findViewById(R.id.tr_secondchilddetails);
			tr_result2 = (TableRow) findViewById(R.id.tr_result2);
			tr_deltime2 = (TableRow) findViewById(R.id.tr_deltime2);

			// changed on 27Mar2015
			rdmotheralive = (RadioButton) findViewById(R.id.rdmotheralive);
			rdmotherdead = (RadioButton) findViewById(R.id.rdmotherdead);

			// 04Dec2015
			tr_deltypereason = (TableRow) findViewById(R.id.tr_deltypereason);
			tr_deltypereasonc = (TableRow) findViewById(R.id.tr_deltypereasonc);
			tr_deltypeotherreasons = (TableRow) findViewById(R.id.tr_deltypeotherreasons);

			txtdeltypereason = (EditText) findViewById(R.id.txtdeltypereasonval);
			txtdeltypeotherreasons = (EditText) findViewById(R.id.etdeltypeotherreasons);

			mspndeltypereason = (MultiSelectionSpinner) findViewById(R.id.mspndeltypereason);

			// 25dec2015
			mspnriskoptions = (MultiSelectionSpinner) findViewById(R.id.mspnriskoptions);
			trriskoptions = (TableRow) findViewById(R.id.tr_riskoptions);

			// updated on 20july2016 by Arpitha
			chkgest = (CheckBox) findViewById(R.id.chk);

			// 05jan2016
			mspntears = (MultiSelectionSpinner) findViewById(R.id.mspntears);
			rdepiyes = (RadioButton) findViewById(R.id.rd_epiyes);
			rdepino = (RadioButton) findViewById(R.id.rd_epino);
			rdcatgut = (RadioButton) findViewById(R.id.rd_catgut);
			rdvicryl = (RadioButton) findViewById(R.id.rd_vicryl);
			trsuturematerial = (TableRow) findViewById(R.id.tr_suturematerial);

			// 9jan2016
			mspnadmittedwith = (MultiSelectionSpinner) findViewById(R.id.mspnadmittedwith);
			mspnadmittedwith.setVisibility(View.GONE);
			// updated on 20may16
			rd_feet = (RadioButton) findViewById(R.id.rd_feet);
			rd_cm = (RadioButton) findViewById(R.id.rd_cm);
			imgphoto = (ImageView) findViewById(R.id.imgphoto);

			// 25Aug2016 - bindu
			rd_htdontknow = (RadioButton) findViewById(R.id.rd_htdontknow);

			rdbreech = (RadioButton) findViewById(R.id.rd_breech);// 20Nov2016
			// Arpitha
			rdvertext = (RadioButton) findViewById(R.id.rd_vertex);// 20Nov2016
			// Arpitha

			// 05may2017 Arpitha- v2.6 mantis id 0000231
			rd_vertex_pres = (RadioButton) findViewById(R.id.rd_vertexpresentation);
			rd_breech_pres = (RadioButton) findViewById(R.id.rd_breechpresentation);
			// 05may2017 Arpitha- v2.6 mantis id 0000231

			rdsuture = (RadioGroup) findViewById(R.id.rdsuturematerial);// 11Sep2017
																		// Arpitha

			setData();
			initialView();
			// updated on 26may2016 by Arpitha
			aq.id(R.id.etage).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etheight).getEditText().addTextChangedListener(watcher);
			// updated on 8july2016 by Arpitha
			aq.id(R.id.etweight).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etchildwt1).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etchildwt2).getEditText().addTextChangedListener(watcher);

			// updated on 01Oct2106 by Arpitha
			aq.id(R.id.etgestationage).getEditText().addTextChangedListener(watcher);
			// updated on 01Oct2106 by Arpitha
			aq.id(R.id.etgestationagedays).getEditText().addTextChangedListener(watcher);

			// 26Nov2016 Arpitha
			aq.id(R.id.etgravida).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etchildwt1).getEditText().addTextChangedListener(watcher);// 26Nov2016
																					// Arpitha

			aq.id(R.id.etpara).getEditText().addTextChangedListener(watcher);

			rdmale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex1 = 0;
				}
			});

			rdfemale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex1 = 1;

				}
			});

			rdmale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex2 = 0;

				}
			});

			rdfemale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					babysex2 = 1;
				}
			});

			aq.id(R.id.etdeldate).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						isLmp = false; // 25Jan2016

						// 11Sep2016 - bindu
						islmpdate = true;
						// isdoa = false;// 10Nov2016
						caldatepicker();
					}
					return true;
				}
			});

			aq.id(R.id.etdeltime).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						isDelTime1 = true;
						delTime = aq.id(R.id.etdeltime).getText().toString();

						if (delTime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(delTime);
							// istoa = false;// 10Nov2016
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			aq.id(R.id.etdeltime2).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						isDelTime1 = false;
						delTime = aq.id(R.id.etdeltime2).getText().toString();

						if (delTime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(delTime);
							// istoa = false;// 10Nov2016
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						showDialog(TIME_DIALOG_ID);
					}
					return true;
				}
			});

			rd_highrisk = (RadioButton) findViewById(R.id.rd_hishrisk);
			rd_lowrisk = (RadioButton) findViewById(R.id.rd_lowrisk);
			rd_memb_pre = (RadioButton) findViewById(R.id.rd_mempresent);
			rd_memb_abs = (RadioButton) findViewById(R.id.rd_memabsent);

			rd_highrisk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					risk = 1;
				}
			});

			rd_lowrisk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					risk = 0;
				}
			});

			rd_memb_pre.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					memb_pres_abs = 1;
				}
			});

			rd_memb_abs.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					memb_pres_abs = 0;
				}
			});
			rd_cm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// 17Aug2016 - Arpitha
					height_unit = 2;
					aq.id(R.id.tr_heightcm).visible();
					aq.id(R.id.tr_height).gone();
					aq.id(R.id.etheight).visible();// 11oct2016 Arpitha
					aq.id(R.id.spnHeightDecimal).gone();// 11oct2016 Arpitha
					aq.id(R.id.spnHeightNumeric).gone();// 11oct2016 Arpitha

					aq.id(R.id.etheight).getEditText().requestFocus();// 14Nov2016
																		// Arpitha
					aq.id(R.id.warheight).invisible();// 17Nov2016 Arpitha

					aq.id(R.id.warheight_ft).gone();// 17July2017 Arpitha

				}
			});
			rd_feet.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					height_unit = 1;
					aq.id(R.id.tr_height).visible();
					aq.id(R.id.tr_heightcm).gone();
					aq.id(R.id.spnHeightDecimal).visible();
					aq.id(R.id.spnHeightNumeric).visible();
					aq.id(R.id.etheight).text("");
					aq.id(R.id.spnHeightDecimal).setSelection(0);
					aq.id(R.id.spnHeightNumeric).setSelection(5);
					aq.id(R.id.warheight).gone();// 26Nov2016 Arpitha
					aq.id(R.id.etheight).gone();// 26Nov2016 Arpitha
					aq.id(R.id.warheight_ft).invisible();// 17July2017 Arpitha

				}
			});

			// updated 25Aug2016 - bindu
			rd_htdontknow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					height_unit = 3;
					aq.id(R.id.tr_heightcm).gone();
					aq.id(R.id.tr_height).gone();
					aq.id(R.id.etheight).text("");
					aq.id(R.id.etheight).text("");// 05oct201 Arpitha
					aq.id(R.id.etheight).gone();// 05oct201 Arpitha
					aq.id(R.id.spnHeightDecimal).gone();// 05oct201 Arpitha
					aq.id(R.id.spnHeightNumeric).gone();// 05oct201 Arpitha
					aq.id(R.id.etHeightDot).gone();// 05oct201 Arpitha
					aq.id(R.id.warheight).gone();// 05oct201 Arpitha
					aq.id(R.id.warheight_ft).gone();// 05oct201 Arpitha
				}
			});

			setInputFiltersForEdittext();

			rdmotheralive.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					aq.id(R.id.txtmothersdeathreason).gone();
					aq.id(R.id.etmothersdeathreason).gone();

					// Bindu - Aug082016
					aq.id(R.id.etmothersdeathreason).text("");
				}
			});

			rdmotherdead.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					aq.id(R.id.txtmothersdeathreason).visible();
					aq.id(R.id.etmothersdeathreason).visible();

					aq.id(R.id.etmothersdeathreason).getEditText().requestFocus();
				}
			});

			// 05jan2016
			rdepiyes.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.VISIBLE);
					episiotomy = 1;

					rdcatgut.setVisibility(View.VISIBLE);// 24Sep2016 -Arpitha
					rdvicryl.setVisibility(View.VISIBLE);// 24Sept2016 -Arpitha
				}
			});

			rdepino.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.GONE);
					episiotomy = 0;

					rdcatgut.setVisibility(View.GONE);// 24Sep2016 -Arpitha
					rdvicryl.setVisibility(View.GONE);// 24Sept2016 -Arpitha
					suturematerial = 0;// 19Nov2016 Arpitha
					// rdcatgut.setEnabled(false);// 19Nov2016 Arpitha
					// rdvicryl.setEnabled(false);// 19Nov2016 Arpitha
					/*
					 * rdcatgut.setSelected(false);//08Sep2017 Arpitha
					 * rdvicryl.setSelected(false);//08Sep2017 Arpitha
					 */
					rdsuture.clearCheck();// 11Sep2017 Arpitha
				}
			});

			rdcatgut.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					suturematerial = 1;
				}
			});

			rdvicryl.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					suturematerial = 2;
				}
			});

			// 29Sep2016 ARpitha
			mspndeltypereason.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Fragment_DIP.istears = false;// 29Sep2016 ARpitha
					return false;
				}
			});
			// 29Sep2016 ARpitha
			mspntears.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Fragment_DIP.istears = true;// 29Sep2016 ARpitha
					return false;
				}
			});
			Fragment_DIP.istears = false;// 06Nov2016 ARpitha

			// 25jan2015
			aq.id(R.id.etlmp).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							isLmp = true;
							// changed by Arpitha
							islmpdate = true;
							// isdoa = false;// 10Nov2016
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			// updated on 28Feb2016 by Arpitha
			aq.id(R.id.etedd).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							isLmp = false;
							// changed by Arpitha
							islmpdate = false;
							// isdoa = false;// 10Nov2016
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			imgphoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						selectImage();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			// updated on 19August2016 by Arpitha
			strlastentrytime = dbh.getlastentrytime(womenID, 0);

			// 20Nov2016 Arpitha
			rdbreech.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// 11May2017 Arpitha -v2.6
					// presentation = getResources().getString(R.string.vertex);
					if (woman.getDel_type() == 0 && vertex && woman.getNormaltype() == 1)
						displayAlertDialog(getResources().getString(R.string.you_have_selected) + " "
								+ getResources().getString(R.string.vertex) + " "
								+ getResources().getString(R.string.during_reg) + " "
								+ getResources().getString(R.string.are_you_sure_want_to_change), "breech");
					// Toast.makeText(getApplicationContext(),
					// getResources().getString(R.string.you_have_selected) + "
					// "
					// + getResources().getString(R.string.vertex) + " "
					// + getResources().getString(R.string.during_reg),
					// Toast.LENGTH_LONG).show();// 11May2017
					// // Arpitha -v2.6
					else // 18May2017 Arpitha - v2.6
						normaltype = 2;

				}
			});

			rdvertext.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (woman.getDel_type() == 0 && (!vertex) && woman.getNormaltype() == 2)
						displayAlertDialog(getResources().getString(R.string.you_have_selected) + " "
								+ getResources().getString(R.string.breech) + " "
								+ getResources().getString(R.string.during_reg) + " "
								+ getResources().getString(R.string.are_you_sure_want_to_change), "vertex");
					/*
					 * Toast.makeText(getApplicationContext(),
					 * getResources().getString(R.string.you_have_selected) +
					 * " " + getResources().getString(R.string.breech) + " " +
					 * getResources().getString(R.string.during_reg),
					 * Toast.LENGTH_LONG).show();// 11May2017 // Arpitha -v2.6
					 */ else
						normaltype = 1;

				}
			});// 20Nov2016 Arpitha

			rdnormaltype = (RadioGroup) findViewById(R.id.rdnormaltype);// 27Feb2017

			rgheight = (RadioGroup) findViewById(R.id.rdheight);// 25April2017
																// Arpitha

			setInputFiltersForEdittext();

			// 05may2017 Arpitha- v2.6 mantis id 0000231
			rgbabypresentation = (RadioGroup) findViewById(R.id.rdbabypresentation);
			rd_vertex_pres.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					presentation = 1;

				}
			});

			rd_breech_pres.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					presentation = 2;

				}
			});// 05may2017 Arpitha- v2.6 mantis id 0000231

			// KillAllActivitesAndGoToLogin.activity_stack.add(this);
			KillAllActivitesAndGoToLogin.addToStack(this);

		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

		}
		KillAllActivitesAndGoToLogin.addToStack(this);

	}

	// change made on 01Apr2015
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// 08Sep2016 - bindu - set max char length
		aq.id(R.id.etwname).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etcomments).getEditText()
				.setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(400) });
		aq.id(R.id.etattendant).getEditText()
				.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etdocname).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etnursename).getEditText()
				.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		// 10April2017 Arpitha
		aq.id(R.id.etother).getEditText().setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etspecialinstructions).getEditText()
				.setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(200) });
		aq.id(R.id.etextracoments).getEditText()
				.setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(200) });
		aq.id(R.id.etaddress).getEditText().setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(60) });

		/*
		 * // 08Sep2016 - bindu - set max char length
		 * aq.id(R.id.etwname).getEditText().setFilters(new InputFilter[] {
		 * filter1, new InputFilter.LengthFilter(45) });
		 * aq.id(R.id.etcomments).getEditText() .setFilters(new InputFilter[] {
		 * filter, new InputFilter.LengthFilter(400) });
		 * aq.id(R.id.etattendant).getEditText() .setFilters(new InputFilter[] {
		 * filter1, new InputFilter.LengthFilter(45) });
		 * aq.id(R.id.etdocname).getEditText().setFilters(new InputFilter[] {
		 * filter1, new InputFilter.LengthFilter(45) });
		 * aq.id(R.id.etnursename).getEditText() .setFilters(new InputFilter[] {
		 * filter1, new InputFilter.LengthFilter(45) }); // 10April2017 Arpitha
		 * aq.id(R.id.etother).getEditText().setFilters(new InputFilter[] {
		 * filter });
		 * aq.id(R.id.etspecialinstructions).getEditText().setFilters(new
		 * InputFilter[] { filter });
		 * aq.id(R.id.etextracoments).getEditText().setFilters(new InputFilter[]
		 * { filter }); aq.id(R.id.etaddress).getEditText().setFilters(new
		 * InputFilter[] { filter ,new InputFilter.LengthFilter(60)});
		 */}

	// / To avoid special characters in Input type
	public static InputFilter filter1 = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*.[]1234567890Â¶";
			String blockCharacterSet = "0123456789~#^|$%*!@/()-'\":;,?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// Initial View
	private void initialView() {
		try {
			if (woman.getregtype() != 2)// 10Nov2016 Arpitha

			{
				aq.id(R.id.ettoa).enabled(false);
				aq.id(R.id.etdoa).enabled(false);
			} else// 16Nov2016 Arpitha
			{
				aq.id(R.id.ettoa).textColor(getResources().getColor(R.color.black));// 16Nov2016
																					// Arpitha
				aq.id(R.id.etdoa).textColor(getResources().getColor(R.color.black));// 16Nov2016
																					// Arpitha
			} // 16Nov2016 Arpitha

			aq.id(R.id.etadmittedwith).enabled(false);
			aq.id(R.id.etadmittedwith).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
			aq.id(R.id.etother).enabled(false);
			aq.id(R.id.etother).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));
			aq.id(R.id.btnregister).text(getResources().getString(R.string.save));

			aq.id(R.id.scradditionalinfo).gone();
			aq.id(R.id.scrdeliverystatus).gone();
			aq.id(R.id.scrwomendetails).visible();

			aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.gamboge));

			todaysDate = Partograph_CommonClass.getTodaysDate();
			delTime = aq.id(R.id.etdeltime).getText().toString();

			// 07Apr2015
			strDeldate = todaysDate;
			selecteddate = todaysDate;

			if (delTime != null) {
				Calendar calendar = Calendar.getInstance();
				Date d = null;
				d = dbh.getTime(delTime);
				calendar.setTime(d);
				hour = calendar.get(Calendar.HOUR_OF_DAY);
				minute = calendar.get(Calendar.MINUTE);
			}

			aq.id(R.id.etfacility).enabled(false);
			aq.id(R.id.etfacilityname).enabled(false);
			aq.id(R.id.etstate).enabled(false);
			aq.id(R.id.etdistrict).enabled(false);
			aq.id(R.id.ettaluk).enabled(false);
			aq.id(R.id.etcomments).enabled(false);
			aq.id(R.id.etcomments).getEditText().setTextColor(getResources().getColor(R.color.fontcolor_disable));

			aq.id(R.id.rd_hishrisk).enabled(false);
			aq.id(R.id.rd_lowrisk).enabled(false);

			aq.id(R.id.rd_mempresent).enabled(false);
			aq.id(R.id.rd_memabsent).enabled(false);

			if (woman != null && woman.getDel_type() == 0) {
				aq.id(R.id.btndelstatus).enabled(false);
				aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.lightgray));
			}

			// 04jan2016
			setTears();

			// 24Sep2016 -Arpitha
			mspndeltypereason.setVisibility(View.GONE);
			aq.id(R.id.etdeltypeotherreasons).getEditText().setVisibility(View.GONE);
			aq.id(R.id.spndelresult2).getSpinner().setVisibility(View.GONE);
			aq.id(R.id.etchildwt2).getEditText().setVisibility(View.GONE);
			imgwt2_warning.setVisibility(View.GONE);
			rdfemale2.setVisibility(View.GONE);
			rdmale2.setVisibility(View.GONE);
			aq.id(R.id.etdeltime2).getEditText().setVisibility(View.GONE);
			aq.id(R.id.delothrrason).getImageView().setVisibility(View.GONE);// 24Sep2016
																				// -Arpitha

			aq.id(R.id.warage).invisible();
			aq.id(R.id.war_gravida).invisible();
			// 24Sep2016 -Arpitha made as gone
			// aq.id(R.id.warheight).gone();
			aq.id(R.id.warheight_feet).gone();

			if (Integer.parseInt(aq.id(R.id.etage).getText().toString()) < 18
					|| Integer.parseInt(aq.id(R.id.etage).getText().toString()) > 40) {
				aq.id(R.id.warage).visible();
				isage = true;// 13April2017 Arpitha
			}

			/*
			 * if (height_unit == 2) { if
			 * (aq.id(R.id.etheight).getText().length() > 0) { if
			 * (Double.parseDouble(aq.id(R.id.etheight).getText().toString()) >
			 * 176) { aq.id(R.id.warheight).visible(); } } else
			 * aq.id(R.id.warheight).invisible(); } else if (height_unit == 1) {
			 * String height = woman.getHeight(); Double heightval =
			 * Double.valueOf(height); if (heightval > 6.0) {
			 * aq.id(R.id.warheight_ft).visible(); } else {
			 * aq.id(R.id.warheight_ft).invisible(); } } else {
			 * aq.id(R.id.tr_heightin).visible(); aq.id(R.id.tr_height).gone();
			 * aq.id(R.id.tr_heightcm).gone(); aq.id(R.id.etheight).gone();//
			 * 05Oct2016 Arpitha aq.id(R.id.spnHeightDecimal).gone();//
			 * 05Oct2016 Arpitha aq.id(R.id.spnHeightNumeric).gone();//
			 * 05Oct2016 Arpitha aq.id(R.id.etHeightDot).gone();// 05Oct2016
			 * Arpitha aq.id(R.id.warheight_ft).gone();// 05Oct2016 Arpitha
			 * aq.id(R.id.warheight).gone();// 05Oct2016 Arpitha }
			 */
			aq.id(R.id.imgweight).invisible();

			// updated on 18August2016 by Arpitha
			if (woman.getW_weight() != null && woman.getW_weight().length() > 0) {
				double weight = Double.parseDouble(woman.getW_weight());
				if ((weight <= 35 || weight > 80) && weight > 0) {
					aq.id(R.id.imgweight).visible();
					isweight = true;// 13April2017 Arpitha
				} else {
					aq.id(R.id.imgweight).invisible();
				}
			}

			// updated on 13july2016 by Arpitha
			int babyweight1 = woman.getBabywt1();
			if (babyweight1 < 2500 || babyweight1 > 4000) {
				imgwt1_warning.setVisibility(View.VISIBLE);
			} else
				imgwt1_warning.setVisibility(View.INVISIBLE);

			int bbayweight2 = woman.getBabywt2();
			if (bbayweight2 < 2500 || bbayweight2 > 4000) {
				imgwt2_warning.setVisibility(View.VISIBLE);
			} else
				// changed 0n 04Sep2016 Arpitha
				imgwt2_warning.setVisibility(View.GONE);

			// 01nov2016 Arpitha
			if (woman.getregtype() == 2) {
				aq.id(R.id.txtretrospective).visible();
				aq.id(R.id.txtretrospective).text(getResources().getString(R.string.retrospective_case));
				aq.id(R.id.etother).gone();
				aq.id(R.id.tr_other).gone();
				aq.id(R.id.etadmittedwith).gone();
				aq.id(R.id.tr_admittedwith).gone();
				aq.id(R.id.tr_doa).gone();
				aq.id(R.id.tr_timeofadm).gone();
				aq.id(R.id.txt_heightin).invisible();
				aq.id(R.id.ettoa).gone();
				aq.id(R.id.etdoa).gone();
				aq.id(R.id.imgadmittedwith).gone();
				aq.id(R.id.imgadmittedwithother).gone();
				aq.id(R.id.imgconditionwhileadmission).gone();
				aq.id(R.id.imgriskoptions).gone();
				aq.id(R.id.imgotherrisk).gone();
				mspnadmittedwith.setVisibility(View.GONE);
				aq.id(R.id.tr_retrospectivereason).visible();
				aq.id(R.id.txtretrospectivereason).visible();

				aq.id(R.id.ettxtretroreason).visible();

				aq.id(R.id.txtretroreason).visible();// 26Nov2016 Arpitha

				aq.id(R.id.imgreason).visible();

				if (Integer.parseInt(woman.getRegtypereason()) == 4) {// 15May2017
																		// Arpitha
																		// -
																		// v2.6
					aq.id(R.id.tr_retrospectivereasonother).visible();
					aq.id(R.id.ettxtretroreasonother).visible();
					aq.id(R.id.imgotherreason).visible();
					aq.id(R.id.ettxtretroreasonother).text(woman.getRegtypeOtherReason());// 15May2017
																							// Arpitha
																							// -
																							// v2.6
					aq.id(R.id.txtretroreasonother).visible();// 26Nov2016
																// Arpitha
					// aq.id(R.id.ettxtretroreasonother).textColor(getResources().getColor(R.color.fontcolor_disable));//
					// 23Nov2016
					// Arpitha

					aq.id(R.id.txtmandretreason).visible();// 15May2017 Arpitha
															// - v2.6
				}
				String[] retroreason = getResources().getStringArray(R.array.retrospective_reason);
				// String reason = woman.getRegtypereason();
				aq.id(R.id.ettxtretroreason).text(retroreason[Integer.parseInt(woman.getRegtypereason())]);// 15May2017
																											// Arpitha
																											// -
																											// v2.6

			} else {
				aq.id(R.id.txtretrospective).gone();
			} // 01nov2016 Arpitha

			aq.id(R.id.etdoe).enabled(false);// 14Nov2016 Arpitha

			strrefdatetime = dbh.getrefdate(woman.getWomenId(), woman.getUserId());

			if (strrefdatetime != null) {
				refdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strrefdatetime);
				refdate = new SimpleDateFormat("yyyy-MM-dd").parse(strrefdatetime);
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}
	}

	// Set the data from database
	private void setData() throws Exception {
		try {
			if (woman != null) {
				aq.id(R.id.etwname).text(woman.getWomen_name() == null ? "" : woman.getWomen_name());
				aq.id(R.id.etage).text("" + woman.getAge());

				aq.id(R.id.etaddress).text(woman.getAddress() == null ? "" : woman.getAddress());
				aq.id(R.id.etattendant).text(woman.getW_attendant() == null ? "" : woman.getW_attendant());
				aq.id(R.id.etphone).text(woman.getPhone_No() == null ? "" : woman.getPhone_No());

				aq.id(R.id.etgravida).text("" + woman.getGravida());
				aq.id(R.id.etpara).text("" + woman.getPara());
				aq.id(R.id.ethospno).text(woman.getHosp_no() == null ? "" : woman.getHosp_no());

				// doa = woman.getDate_of_admission();
				if (woman.getregtype() != 2)// 21Nov2016 Arpitha
				{
					aq.id(R.id.etdoa)
							.text(woman.getDate_of_admission() == null ? ""
									: Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
											Partograph_CommonClass.defdateformat));

					strRegdate = "" + woman.getDate_of_admission();
					time_of_adm = "" + woman.getTime_of_admission();

					aq.id(R.id.ettoa).text(woman.getTime_of_admission() == null ? "" : woman.getTime_of_admission());
				} // 21Nov2016 Arpitha
				aq.id(R.id.etdocname).text(woman.getDoc_name() == null ? "" : woman.getDoc_name());
				aq.id(R.id.etnursename).text(woman.getNurse_name() == null ? "" : woman.getNurse_name());

				image1 = woman.getWomen_Image();
				if (image1 != null) {
					Bitmap btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64,
							64, false);
					aq.id(R.id.imgphoto).getImageView().setImageBitmap(btmp);
				}

				aq.id(R.id.etspecialinstructions).text(woman.getSpecial_inst() == null ? "" : woman.getSpecial_inst());
				womenID = woman.getWomenId();

				memb_pres_abs = woman.getMemb_pres_abs();
				if (memb_pres_abs == 0)
					aq.id(R.id.rd_memabsent).checked(true);
				else
					aq.id(R.id.rd_mempresent).checked(true);

				aq.id(R.id.etcomments).text(woman.getComments() == null ? "" : woman.getComments());
				aq.id(R.id.etdelcomments).text(woman.getDel_Comments() == null ? "" : woman.getDel_Comments());
				aq.id(R.id.etdeltime).text(
						woman.getDel_Time() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_Time());

				aq.id(R.id.etdeltime2).text(
						woman.getDel_time2() == null ? Partograph_CommonClass.getCurrentTime() : woman.getDel_time2());

				// changed on 07Apr2015
				aq.id(R.id.etdeldate)
						.text(woman.getDel_Date() == null
								? Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(),
										Partograph_CommonClass.defdateformat)
								: Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
										Partograph_CommonClass.defdateformat));

				aq.id(R.id.etchildwt1).text("" + woman.getBabywt1());

				aq.id(R.id.etchildwt2).text("" + woman.getBabywt2());

				babysex1 = woman.getBabysex1();
				babysex2 = woman.getBabysex2();

				if (babysex1 == 0)
					aq.id(R.id.rd_male1).checked(true);
				else
					aq.id(R.id.rd_female1).checked(true);

				if (babysex2 == 0)
					aq.id(R.id.rd_male2).checked(true);
				else
					aq.id(R.id.rd_female2).checked(true);

				int ges = woman.getGestationage();
				int gestdays = woman.getGest_age_days();
				if (ges == 0 && gestdays == 0) {
					chkgest.setChecked(true);
				} else {
					chkgest.setChecked(false);
					chkgest.setEnabled(false);
					aq.id(R.id.etgestationage).text("" + ges);
					aq.id(R.id.etgestationagedays).text("" + woman.getGest_age_days());
				}

				if (woman.isMothersdeath() == 1) {

					aq.id(R.id.txtmothersdeathreason).visible();
					aq.id(R.id.etmothersdeathreason).visible();
					// 24Sep2016 Arpitha
					aq.id(R.id.imgmotherdeathreason).getImageView().setVisibility(View.INVISIBLE);

					aq.id(R.id.rdmotherdead).checked(true);
				} else {

					aq.id(R.id.txtmothersdeathreason).gone();
					aq.id(R.id.etmothersdeathreason).gone();

					// changed on 27Mar2015
					aq.id(R.id.rdmotherdead).checked(false);
					// 24Sep2016 Arpitha
					aq.id(R.id.imgmotherdeathreason).getImageView().setVisibility(View.GONE);
					aq.id(R.id.etmothersdeathreason).getEditText().setVisibility(View.GONE);
				}

				// 23jan2016
				aq.id(R.id.ettears).gone();
				aq.id(R.id.mspntears).visible();

				if (woman.getDel_type() != 0) {

					int dtypepos = woman.getDel_type() - 1;
					aq.id(R.id.spndeltype).setSelection(dtypepos);

					delivery_type = dtypepos;

					disableDelFields();
				}

				// No of child
				ArrayList<Integer> listnoofchild = new ArrayList(
						Arrays.asList(getResources().getStringArray(R.array.num_of_children))); // array
																								// id
																								// of
																								// string
				// resource

				int noofchildpos = listnoofchild.indexOf(String.valueOf(woman.getNo_of_child()));
				aq.id(R.id.spnnoofchild).setSelection(noofchildpos);
				no_of_child = String.valueOf(woman.getNo_of_child());

				if (noofchildpos == 0) {
					trchilddet1.setVisibility(View.VISIBLE);
					tr_childdetsex1.setVisibility(View.VISIBLE);
					tr_secondchilddetails.setVisibility(View.GONE);
					tr_result2.setVisibility(View.GONE);
					tr_childdet2.setVisibility(View.GONE);
					tr_childsex2.setVisibility(View.GONE);
					tr_deltime2.setVisibility(View.GONE);

					// 30Dec2015
					aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.child_details));
				} else {
					tr_childdet2.setVisibility(View.VISIBLE);
					tr_childsex2.setVisibility(View.VISIBLE);
					tr_secondchilddetails.setVisibility(View.VISIBLE);
					tr_result2.setVisibility(View.VISIBLE);
					trchilddet1.setVisibility(View.VISIBLE);
					tr_childdetsex1.setVisibility(View.VISIBLE);
					tr_deltime2.setVisibility(View.VISIBLE);
					// 30Dec2015
					aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.first_child_details));
				}

				aq.id(R.id.spndelresult).setSelection(woman.getDel_result1() - 1);

				aq.id(R.id.spndelresult2).setSelection(woman.getDel_result2() - 1);

				aq.id(R.id.etfacility).text(woman.getFacility());
				aq.id(R.id.etfacilityname).text(woman.getFacility_name());
				aq.id(R.id.etstate).text(woman.getState());
				aq.id(R.id.etdistrict).text(woman.getDistrict());
				aq.id(R.id.ettaluk).text(woman.getTaluk());

				aq.id(R.id.etmothersdeathreason).text(woman.getMothers_death_reason());

				// 02Dec2015
				if (woman.getDel_type() > 1) {
					tr_deltypereason.setVisibility(View.GONE);
					tr_deltypereasonc.setVisibility(View.VISIBLE);
					tr_deltypeotherreasons.setVisibility(View.VISIBLE);
					txtdeltypeotherreasons.setEnabled(false);

					mspndeltypereason.setVisibility(View.GONE);// 11oct2016
																// Arpitha

					// 22Aug2016 - check for null in deltype reason
					if (woman.getDelTypeReason() != null) {
						String[] delTypeRes = woman.getDelTypeReason().split(",");
						String[] delTypeResArr = null;
						if (woman.getDel_type() == 2)
							delTypeResArr = getResources().getStringArray(R.array.spnreasoncesareanValues);
						else if (woman.getDel_type() == 3)
							delTypeResArr = getResources().getStringArray(R.array.spnreasoninstrumental);

						String delTypeResDisplay = "";
						if (delTypeResArr != null) {
							if (delTypeRes != null && delTypeRes.length > 0) {
								for (String str : delTypeRes) {
									if (str != null && str.length() > 0)
										delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)]
												+ "\n";
								}
							}
						}

						txtdeltypereason.setText(delTypeResDisplay);
						txtdeltypeotherreasons.setText(woman.getDeltype_otherreasons());
					}
				} else {
					tr_deltypereason.setVisibility(View.GONE);
					tr_deltypereasonc.setVisibility(View.GONE);
					tr_deltypeotherreasons.setVisibility(View.GONE);
					// 24Sep2016 -Arpitha
					txtdeltypereason.setVisibility(View.GONE);

					// woman.getd
					// rdbreech.setEnabled(false);// 26Nov2016 Arpitha
					// rdvertext.setEnabled(false);// 26Nov2016 Arpitha

					// 20Nov2016 Arpitha
					if (woman.getNormaltype() == 2) {
						rdbreech.setChecked(true);

					} else if (woman.getNormaltype() == 1) {
						rdvertext.setChecked(true);// 20Nov2016 Arpitha

					}
				}

				risk = woman.getRisk_category();
				if (risk == 0) {
					aq.id(R.id.rd_lowrisk).checked(true);
					// 25dce2015
					trriskoptions.setVisibility(View.GONE);
					// 24Sep2016 Arpitha
					mspnriskoptions.setVisibility(View.GONE);
					aq.id(R.id.etriskoptions).getEditText().setVisibility(View.GONE);
				}

				else if (risk == 99)// 21Nov2016 Arpitha
				{
					trriskoptions.setVisibility(View.GONE);
					aq.id(R.id.tr_condwhileadm).gone();
					mspnriskoptions.setVisibility(View.GONE);
					aq.id(R.id.rdrisk).gone();
					aq.id(R.id.etriskoptions).gone();
					aq.id(R.id.tr_comments).gone();
					aq.id(R.id.etcomments).gone();

				} // 21Nov2016 Arpitha

				else {
					aq.id(R.id.rd_hishrisk).checked(true);
					trriskoptions.setVisibility(View.VISIBLE);
					mspnriskoptions.setVisibility(View.GONE);
					// 24Sep2016 Arpitha
					mspnriskoptions.setVisibility(View.GONE);
					aq.id(R.id.txtmand).getTextView().setVisibility(View.VISIBLE);
					aq.id(R.id.txtriskoptions).getTextView().setVisibility(View.VISIBLE);
					aq.id(R.id.etriskoptions).getEditText().setVisibility(View.VISIBLE);

					// 22Aug check for null risk options
					// 25dec2015
					String[] riskoptions = woman.getRiskoptions().split(",");
					String[] riskoptArr = null;

					// getResources().getStringArray(R.array.riskoptions);
					// 06jul2016 - use riskoptionsvalues instead of riskoptions
					riskoptArr = getResources().getStringArray(R.array.riskoptionsvalues);

					String riskoptionDisplay = "";
					if (riskoptArr != null) {
						if (riskoptions != null && riskoptions.length > 0) {
							for (String str : riskoptions) {
								if (str != null && str.length() > 0)
									riskoptionDisplay = riskoptionDisplay + riskoptArr[Integer.parseInt(str)] + "\n";
							}
						}
					}

					aq.id(R.id.etriskoptions).enabled(false);
					aq.id(R.id.etriskoptions).getEditText()
							.setTextColor(getResources().getColor(R.color.fontcolor_disable));
					aq.id(R.id.etriskoptions).text("" + riskoptionDisplay);

					// aq.id(R.id.etcomments).text(woman.getComments() == null ?
					// "" : woman.getComments());//19April2017 Arpitha
				}

				if (woman.getEdd() != null) {
					String edd = woman.getEdd();
					if (edd != null && edd.trim().length() > 0) {
						edd = Partograph_CommonClass.getConvertedDateFormat(edd, Partograph_CommonClass.defdateformat);
						aq.id(R.id.etedd).text(edd);
					}
				}

				if (woman.getLmp() != null) {
					String s = woman.getLmp();
					if (s != null && s.trim().length() > 0) {
						s = Partograph_CommonClass.getConvertedDateFormat(s, Partograph_CommonClass.defdateformat);
						aq.id(R.id.etlmp).text(s);
					}
				}

				// updated on 28Feb2016 by Arpitha

				// 07jan2016
				String tearsResDisplay = "";
				if (woman.getTears() != null) {
					String[] tearsres = woman.getTears().split(",");
					String[] tearsResArr = null;

					// updated on 6july2016
					tearsResArr = getResources().getStringArray(R.array.tearsvalues);

					if (tearsResArr != null) {
						if (tearsres != null && tearsres.length > 0) {
							for (String str : tearsres) {
								if (str != null && str.length() > 0)
									tearsResDisplay = tearsResDisplay + tearsResArr[Integer.parseInt(str)] + "\n";
							}
						}
					}
				}
				aq.id(R.id.ettears).text(tearsResDisplay);

				if (woman.getEpisiotomy() == 1) {

					rdepiyes.setChecked(true);
					trsuturematerial.setVisibility(View.VISIBLE);

					if (woman.getSuturematerial() == 1) {
						rdcatgut.setChecked(true);
						rdcatgut.setVisibility(View.VISIBLE);// 31oct2016
																// Arpitha
						rdvicryl.setVisibility(View.VISIBLE);// 31oct2016
																// Arpitha
						trsuturematerial.setVisibility(View.VISIBLE);// 23Nov2016
																		// Arpitha

					} else if (woman.getSuturematerial() == 2) {
						rdvicryl.setChecked(true);
						rdcatgut.setVisibility(View.VISIBLE);// 31oct2016
																// Arpitha
						rdvicryl.setVisibility(View.VISIBLE);// 31oct2016
																// Arpitha
						trsuturematerial.setVisibility(View.VISIBLE);// 23Nov2016
																		// Arpitha
					} else {
						trsuturematerial.setVisibility(View.GONE);
					}
				} else {
					rdepino.setChecked(true);
					trsuturematerial.setVisibility(View.GONE);
					rdcatgut.setVisibility(View.GONE);// 31oct2016 Arpitha
					rdvicryl.setVisibility(View.GONE);// 31oct2016 Arpitha
				}

				// 9jan2016
				String admResDisplay = "";
				if (woman.getAdmitted_with() != null) {
					String[] admres = woman.getAdmitted_with().split(",");
					String[] admResArr = null;

					admResArr = getResources().getStringArray(R.array.admittedarraylist);

					if (admResArr != null) {
						if (admres != null && admres.length > 0) {
							for (String str : admres) {
								if (str != null && str.length() > 0)
									admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + "\n";
							}
						}
					}
				}
				aq.id(R.id.etadmittedwith).text(admResDisplay);

				// 11jan2016
				aq.id(R.id.etthayicardno).text("" + woman.getThayicardnumber());

				// 19jan2016
				int n = woman.getBloodgroup();
				aq.id(R.id.spnbloodgroup).setSelection(n);

				// updated on 28feb16 by Arpitha

				String womenweight = woman.getW_weight();
				if (womenweight == null || womenweight.equalsIgnoreCase("null")) {
					aq.id(R.id.etweight).text("");
				} else {
					aq.id(R.id.etweight).text(womenweight);
				}

				aq.id(R.id.etother).text(woman.getOther() == null ? "" : woman.getOther());

				// updated on 20May16 by Arpitha

				height_unit = woman.getHeight_unit();
				String height_in_cm;
				String height_in_feet;
				if (height_unit == 1) {
					height_in_feet = woman.getHeight();
					if (height_in_feet.contains(".")) {
						String[] heightval = height_in_feet.split("\\.");
						String numeric = heightval[0];
						String decimal = heightval[1];
						aq.id(R.id.spnHeightNumeric).setSelection(Integer.parseInt(numeric));
						aq.id(R.id.spnHeightDecimal).setSelection(Integer.parseInt(decimal));
					} else {
						aq.id(R.id.spnHeightNumeric).setSelection(Integer.parseInt(height_in_feet));
					}
					aq.id(R.id.tr_height).visible();
					aq.id(R.id.tr_heightcm).gone();
					rd_feet.setChecked(true);
					Double heightval = Double.valueOf(height_in_feet);
					if (heightval > 5.6 || heightval < 4.8) {
						aq.id(R.id.warheight_ft).visible();
					} else {
						aq.id(R.id.warheight_ft).invisible();
					}

				} else if (height_unit == 2) {

					height_in_cm = woman.getHeight();
					aq.id(R.id.etheight).text(height_in_cm);
					aq.id(R.id.tr_height).gone();
					aq.id(R.id.tr_heightcm).visible();
					rd_cm.setChecked(true);
					Double height = null;
					if (height_in_cm != null && height_in_cm.length() > 0)// 19April2017
																			// Arpitha
						height = Double.parseDouble(height_in_cm);// 17May2017
																	// Arpitha
																	// -v2.6

					if (height != null && (height.doubleValue() > 0.0)
							&& (height.doubleValue() > 176.0 || height.doubleValue() < 146.0)) {
						aq.id(R.id.warheight).visible();
						isheight = true;// 13April2017 Arpitha
					} else {
						aq.id(R.id.warheight).invisible();
					}
					// 17July2017 Arpitha - 2.6.1
					aq.id(R.id.spnHeightDecimal).gone();
					aq.id(R.id.spnHeightNumeric).gone();
					aq.id(R.id.etheight).visible();// 17July2017 Arpitha - 2.6.1

				} else {
					aq.id(R.id.tr_height).gone();
					aq.id(R.id.tr_heightcm).gone();
					rd_htdontknow.setChecked(true);
					// 17July2017 Arpitha - 2.6.1
					aq.id(R.id.etheight).gone();
					aq.id(R.id.etHeightDot).gone();
					aq.id(R.id.warheight).gone();
					aq.id(R.id.warheight_ft).gone();
					aq.id(R.id.spnHeightDecimal).gone();
					aq.id(R.id.spnHeightNumeric).gone();// 17July2017 Arpitha -
														// 2.6.1
				}

				// bindu- 29jul2016 - getWomanDetails
				Women_Profile_Pojo wdataold = new Women_Profile_Pojo();

				wdataold = dbh.getWomanDetails(woman.getUserId(), woman.getWomenId());

				if (wdataold != null)
					Partograph_CommonClass.oldWomanObj = wdataold;

				aq.id(R.id.etextracoments).text(woman.getExtracomments());// 13Oct2016
				// Arpitha

				// aq.id(R.id.etdoe).text(woman.getDate_of_reg_entry());//
				// 10Nov2016
				// Arpitha

				String entryDate = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_reg_entry(),
						Partograph_CommonClass.defdateformat);// 10April2017
																// Arpitha
				String entryTime = woman.getDate_of_reg_entry().substring(11);// 10April2017
																				// Arpitha

				aq.id(R.id.etdoe).text(entryDate + "/" + entryTime);// 10April2017
																	// Arpitha
				// 05May2017 Arpitha - v2.6 mantis id-0000231
				presentation = woman.getNormaltype();
				if (presentation == 1) {
					rd_vertex_pres.setChecked(true);

				} else if (presentation == 2)
					rd_breech_pres.setChecked(true);// 05May2017 Arpitha - v2.6
													// mantis id-0000231

			}

		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}

	}

	// Disable delivery fields
	private void disableDelFields() {
		try {
			aq.id(R.id.spndeltype).enabled(false);
			aq.id(R.id.spnnoofchild).enabled(false);
			aq.id(R.id.spndelresult).enabled(false);
			aq.id(R.id.spndelresult2).enabled(false);

			aq.id(R.id.etdeldate).enabled(false);
			aq.id(R.id.etdeltime).enabled(false);
			aq.id(R.id.etdeltime2).enabled(false);
			// bindu
			// - Aug082016-

			aq.id(R.id.btndelstatus).enabled(true);
			aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);

			// Change on 27Mar2015
			aq.id(R.id.chkdelinfo).enabled(false);
			aq.id(R.id.chkdelinfo).checked(true);

			// 23jan2016
			aq.id(R.id.ettears).visible();
			aq.id(R.id.mspntears).gone();

			// 25jan2016
			rdepiyes.setEnabled(false);
			rdepino.setEnabled(false);
			rdcatgut.setEnabled(false);
			rdvicryl.setEnabled(false);

			isupdatedel = true;

			rdbreech.setEnabled(false);// 26Nov2016 Arpitha
			rdvertext.setEnabled(false);// 26Nov2016 Arpitha
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// TextView
			if (v.getClass() == TextView.class) {
				aq.id(v.getId()).getText();
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class || v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.imgphoto:
				selectImage();

				break;
			case R.id.btnregister:
				updateWomenDetails();
				break;

			case R.id.btnwomendetails:
				try {
					Fragment_DIP.istears = false;// 28Sep2016 Arpitha
					aq.id(R.id.scradditionalinfo).gone();
					aq.id(R.id.scrdeliverystatus).gone();
					aq.id(R.id.scrwomendetails).visible();
					aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.gamboge));
					aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.brown));

					if (woman != null && woman.getDel_type() == 0) {
						aq.id(R.id.btndelstatus).enabled(false);
						aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.lightgray));
					}

					if (isDelinfoadd) {
						aq.id(R.id.btndelstatus).enabled(true);
						aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);
					}
					// 17Aug2017 Arpitha
					aq.id(R.id.btnregister).enabled(true);
					aq.id(R.id.btnregister).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btnclear).enabled(true);
					aq.id(R.id.btnclear).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.txtdisable).gone();// 17Aug2017 Arpitha
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
				break;
			case R.id.btnother:
				try {
					Fragment_DIP.istears = false;// 28Sep2016 Arpitha
					aq.id(R.id.scradditionalinfo).visible();
					aq.id(R.id.scrdeliverystatus).gone();
					aq.id(R.id.scrwomendetails).gone();

					aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.gamboge));
					aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.brown));

					if (woman != null && woman.getDel_type() == 0) {
						aq.id(R.id.btndelstatus).enabled(false);
						aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.lightgray));
					}

					if (isDelinfoadd) {
						aq.id(R.id.btndelstatus).enabled(true);
						aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);
					}
					// 17Aug2017 Arpitha
					aq.id(R.id.btnregister).enabled(true);
					aq.id(R.id.btnregister).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btnclear).enabled(true);
					aq.id(R.id.btnclear).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.txtdisable).gone();// 17Aug2017 Arpitha
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
				}

				break;

			case R.id.btndelstatus:
				try {
					Fragment_DIP.istears = true;// 28Sep2016 Arpitha
					aq.id(R.id.scradditionalinfo).gone();
					aq.id(R.id.scrdeliverystatus).visible();
					aq.id(R.id.scrwomendetails).gone();

					aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.gamboge));

					// 15Aug2017 Arpitha
					ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
					arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());// 15Aug2017
																							// Arpitha
					if (arrVal.size() > 0) {
						if (woman.getDel_type() == 0) {

							aq.id(R.id.txtdisable).visible();
							aq.id(R.id.scrdeliverystatus).gone();

						}
						aq.id(R.id.btnregister).enabled(false);
						aq.id(R.id.btnregister).backgroundColor(getResources().getColor(R.color.ashgray));
						aq.id(R.id.btnclear).enabled(false);
						aq.id(R.id.btnclear).backgroundColor(getResources().getColor(R.color.ashgray));

						aq.id(R.id.etchildwt1).enabled(false);
						aq.id(R.id.etchildwt2).enabled(false);
						aq.id(R.id.etdelcomments).enabled(false);
						aq.id(R.id.etmothersdeathreason).enabled(false);
						rdmotherdead.setEnabled(false);
						rdmotheralive.setEnabled(false);
						rdmotheralive.setEnabled(false);
						rdmotherdead.setEnabled(false);
						rdmotherdead.setEnabled(false);
						rdmotherdead.setEnabled(false);
						rdmotherdead.setEnabled(false);
						rdmale2.setEnabled(false);
						rdmale1.setEnabled(false);
						rdfemale1.setEnabled(false);
						rdfemale2.setEnabled(false);

					} // 17Aug2017 Arpitha
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
				break;

			case R.id.chkdelinfo:
				if (((CheckBox) v).isChecked()) {
					isDelinfoadd = true;
					aq.id(R.id.btndelstatus).enabled(true);
					aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);
				} else {
					isDelinfoadd = false;
					aq.id(R.id.btndelstatus).enabled(false);
					aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.lightgray));
				}

				break;

			case R.id.btnclear:
				displayConfirmationAlert(getResources().getString(R.string.clear_msg),
						getResources().getString(R.string.clear));

				break;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Update Women Details
	private void updateWomenDetails() throws Exception {

		if (!(no_of_child.equals("") || (no_of_child == null)))
			numofchildren = Integer.parseInt(no_of_child);

		if (validateRegFields()) {

			age = 0;
			height = 0;
			d = 0.0;
			weight = 0;
			duration = 0;

			if (aq.id(R.id.etage).getText().length() > 0) {
				age = Integer.parseInt(aq.id(R.id.etage).getText().toString());
			}
			if (aq.id(R.id.etheight).getText().length() > 0 && rd_cm.isChecked()) {
				height = Double.parseDouble(aq.id(R.id.etheight).getText().toString());
			}
			if (aq.id(R.id.etweight).getText().length() > 0) {
				weight = Double.parseDouble(aq.id(R.id.etweight).getText().toString());
			}
			int height_numeric = Integer.parseInt((String) aq.id(R.id.spnHeightNumeric).getSelectedItem());
			int height_decimal = Integer.parseInt((String) aq.id(R.id.spnHeightDecimal).getSelectedItem());
			feet_height = height_numeric + "." + height_decimal;
			if (height_decimal < 10) {
				height_feet = "" + height_numeric + ".0" + height_decimal;
			} else {
				height_feet = "" + height_numeric + "." + height_decimal;
			}
			d = Double.parseDouble(height_feet);

			if (isDelinfoadd || isupdatedel) {
				if (aq.id(R.id.etchildwt1).getText().toString().trim().length() > 0) {
					baby1weight = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());
				}
				if (Integer.parseInt(no_of_child) == 2) {
					baby2weight = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());
				}

				String del_datetime = strDeldate + "_" + aq.id(R.id.etdeltime).getText().toString();

				if (strlastentrytime != null && isDelinfoadd) {
					strlastentrytime = strlastentrytime.replace(" ", "_");
					duration = Partograph_CommonClass.getHoursBetDates(del_datetime, strlastentrytime);
				}

			}

			if ((age > 0 && (age < 18 || age > 40)) || (height > 0 && (height > 176 || height < 146))
					|| (weight > 0 && (weight <= 35 || weight > 80)) || (d > 0 && (d > 5.08 || d < 4.08))
					|| (baby1weight > 0 && (baby1weight < 2500 || baby1weight > 4000))
					|| (baby2weight > 0 && (baby2weight < 2500 || baby2weight > 4000))
					|| (duration > 0 && duration > 12)) {
				displayConfirmationAlert("", getResources().getString(R.string.warning));
			} else
				savewomendata();

		}

	}

	// Options to set Image
	private void selectImage() {
		final CharSequence[] options = { getResources().getString(R.string.take_photo),
				getResources().getString(R.string.remove_photo) };

		AlertDialog.Builder builder = new AlertDialog.Builder(ViewProfile_Activity.this);

		builder.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.profile_photo) + "</font>"));// 08Feb2017
																					// Arpitha
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals(getResources().getString(R.string.take_photo))) {
					isremovephotoClicked = false;
					CapturingImage();
					dialog.dismiss();
				} else if (options[item].equals(getResources().getString(R.string.remove_photo))) {
					isremovephotoClicked = true;
					aq.id(R.id.imgphoto).getImageView().setImageResource(R.drawable.women);
					bitmap = null;
					dialog.dismiss();
				}
			}
		});

		// 08Feb2017 Arpitha
		Dialog d = builder.show();
		int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = d.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017
		d.show();
		builder.setCancelable(false);// 31oct2016 Arpitha
	}

	/** This method invokes camera intent */
	@SuppressLint("SimpleDateFormat")
	protected void CapturingImage() {
		try {
			// intent to start device camera
			Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
			Date xDate = new Date();
			String lasmod = new SimpleDateFormat("dd-hh-mm-ss").format(xDate);
			String fileName = "profile_img" + lasmod;
			File imageDir = new File(AppContext.imgDirRef);
			if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
				if (imageDir != null) {
					if (!imageDir.mkdirs()) {
						if (!imageDir.exists()) {
							Log.d("CameraSample", "failed to create directory");
						} else {
							Log.d("CameraSample", "created directory");
						}
					}
				}
			} else {
				Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
			}
			path = imageDir + "/" + fileName + ".jpg";
			imgfileBeforeResult = new File(path);
			outputFileUri = Uri.fromFile(imgfileBeforeResult);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
			startActivityForResult(intent, IMAGE_CAPTURE);
		} catch (NullPointerException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** This method get the result from Camera Intent, gets the image */
	@SuppressLint("SimpleDateFormat")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if (requestCode == IMAGE_CAPTURE) {
				if (resultCode == RESULT_OK) {
					baos = new ByteArrayOutputStream();

					bitmap = BitmapFactory.decodeFile(path);
					Bitmap resizedBmp = Bitmap.createScaledBitmap(bitmap, 230, 210, true);
					resizedBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
					aq.id(R.id.imgphoto).getImageView().setImageBitmap(resizedBmp);

				} else if (resultCode == RESULT_CANCELED) {
					deleteImages();
				}
			}
		} catch (NullPointerException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Delete the temporarily stored Images */
	private void deleteImages() throws Exception {
		if (imgfileBeforeResult != null)
			imgfileBeforeResult.delete();
	}

	/**
	 * This method invokes when Item clicked in Spinner
	 * 
	 * @throws Exception
	 */
	public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {
		try {
			switch (adapter.getId()) {

			case R.id.spnHeightNumeric:
				height_numeric = adapter.getSelectedItemPosition();
				checkHeightValue();

				break;
			case R.id.spnHeightDecimal:
				height_decimal = adapter.getSelectedItemPosition();
				checkHeightValue();

				break;

			// num of children
			case R.id.spnnoofchild: {
				no_of_child = (String) adapter.getSelectedItem();

				if (position == 0) {
					trchilddet1.setVisibility(View.VISIBLE);
					tr_childdetsex1.setVisibility(View.VISIBLE);
					tr_secondchilddetails.setVisibility(View.GONE);
					tr_result2.setVisibility(View.GONE);
					tr_childdet2.setVisibility(View.GONE);
					tr_childsex2.setVisibility(View.GONE);
					tr_deltime2.setVisibility(View.GONE);
					aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.child_details));

					if (rdmale1.isChecked())
						babysex1 = 0;
					else
						babysex1 = 1;

					// 24Sep2016 Arpitha
					aq.id(R.id.etdeltime2).getEditText().setVisibility(View.GONE);
					rdfemale2.setVisibility(View.GONE);
					rdmale2.setVisibility(View.GONE);
					aq.id(R.id.etchildwt2).getEditText().setVisibility(View.GONE);
					imgwt2_warning.setVisibility(View.GONE);
					aq.id(R.id.spndelresult2).getSpinner().setVisibility(View.GONE);
					aq.id(R.id.imgdeltime2).getImageView().setVisibility(View.GONE);
					aq.id(R.id.imgdelresult).getImageView().setVisibility(View.GONE);
					aq.id(R.id.imgsex2).getImageView().setVisibility(View.GONE);// 24Sep2016
																				// Arpitha

				} else {
					tr_childdet2.setVisibility(View.VISIBLE);
					tr_childsex2.setVisibility(View.VISIBLE);
					tr_secondchilddetails.setVisibility(View.VISIBLE);
					tr_result2.setVisibility(View.VISIBLE);
					trchilddet1.setVisibility(View.VISIBLE);
					tr_childdetsex1.setVisibility(View.VISIBLE);
					tr_deltime2.setVisibility(View.VISIBLE);
					aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.first_child_details));

					if (rdmale2.isChecked())
						babysex2 = 0;
					else
						babysex2 = 1;

					// 24Sep2016 Arpitha
					aq.id(R.id.etdeltime2).getEditText().setVisibility(View.VISIBLE);
					rdfemale2.setVisibility(View.VISIBLE);
					rdmale2.setVisibility(View.VISIBLE);
					aq.id(R.id.etchildwt2).getEditText().setVisibility(View.VISIBLE);
					if (woman.getDel_type() == 0)// 25April2017 Arpitha - 2.5
													// testing bug fixing
						imgwt2_warning.setVisibility(View.INVISIBLE);// changed
																		// visible
																		// to
																		// invisible
																		// -
																		// 01Oct2016
																		// Arpitha
					aq.id(R.id.spndelresult2).getSpinner().setVisibility(View.VISIBLE);
					aq.id(R.id.imgdeltime2).getImageView().setVisibility(View.INVISIBLE);
					aq.id(R.id.imgdelresult).getImageView().setVisibility(View.INVISIBLE);
					aq.id(R.id.imgsex2).getImageView().setVisibility(View.INVISIBLE);// 24Sep2016
																						// Arpitha

				}
			}
				break;

			// Del result1
			case R.id.spndelresult: {

				delivery_result1 = adapter.getSelectedItemPosition() + 1;

			}
				break;

			// del result2
			case R.id.spndelresult2: {
				delivery_result2 = adapter.getSelectedItemPosition() + 1;
			}
				break;

			// del type
			case R.id.spndeltype: {
				delivery_type = adapter.getSelectedItemPosition() + 1;
				// 04Dec2015
				try {
					if (delivery_type > 1) {
						if (!(woman.getDel_type() > 1))// 11oct2016
														// ARpitha
						{
							tr_deltypereason.setVisibility(View.VISIBLE);// 11oct2016
																			// ARpitha
							mspndeltypereason.setVisibility(View.VISIBLE);// 11oct2016
																			// ARpitha
						}

						// tr_deltypereason.setVisibility(View.VISIBLE);
						tr_deltypeotherreasons.setVisibility(View.VISIBLE);
						setDelTypeReason(delivery_type);
						// 24Sep2016 -Arpitha
						// mspndeltypereason.setVisibility(View.VISIBLE);
						aq.id(R.id.etdeltypeotherreasons).getEditText().setVisibility(View.VISIBLE);
						aq.id(R.id.wardeltype).getImageView().setVisibility(View.INVISIBLE);
						aq.id(R.id.delothrrason).getImageView().setVisibility(View.INVISIBLE);// 24Sep2016
																								// -Arpitha

						// 20Nov2016 Arpitha
						rdbreech.setVisibility(View.GONE);
						rdvertext.setVisibility(View.GONE);
						rdbreech.setChecked(false);
						rdvertext.setChecked(false);
						if (woman.getNormaltype() != 2)// 11Aug2017 Arpitha
							normaltype = 0;// 20Nov2016 Arpitha

						aq.id(R.id.txtnormaltype).gone();// 22Nov2016 Arpitha

						// 11May2017 Arpitha -v2.6 mantis id-0000231
						if (woman.getNormaltype() == 2) {
							if (delivery_type == 2) {
								mspndeltypereason.setSelection(5);
							}
						} // 11May2017 Arpitha -v2.6 mantis id-0000231

					} else {
						tr_deltypereason.setVisibility(View.GONE);
						tr_deltypeotherreasons.setVisibility(View.GONE);

						// 24Sep2016 -Arpitha
						mspndeltypereason.setVisibility(View.GONE);
						aq.id(R.id.etdeltypeotherreasons).getEditText().setVisibility(View.GONE);
						aq.id(R.id.wardeltype).getImageView().setVisibility(View.GONE);
						aq.id(R.id.delothrrason).getImageView().setVisibility(View.GONE);// 24Sep2016
																							// -Arpitha

						rdbreech.setVisibility(View.VISIBLE);// 20Nov2016
																// Arpitha
						rdvertext.setVisibility(View.VISIBLE);// 20Nov2016
																// Arpitha
						if (woman.getDel_type() == 0)// 12April2017 Arpitha
						{
							if (woman.getNormaltype() != 2)// 05May2017 Arpitha
															// -v2.6 mantis
															// id-0000231
							{
								rdvertext.setChecked(true);// 20Nov2016 Arpitha
								rdbreech.setChecked(false);
								normaltype = 1;
							} else// 05May2017 Arpitha -v2.6 mantis id-0000231
							{
								rdvertext.setChecked(false);
								rdbreech.setChecked(true);
								normaltype = 2;
							} // 05May2017 Arpitha -v2.6 mantis id-0000231
						}

						aq.id(R.id.txtnormaltype).visible();// 22Nov2016 Arpitha

					}
					vertex = rdvertext.isChecked();// 11May2017 Arpitha
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
				break;
			}

			case R.id.spnbloodgroup: {
				bldgrp = adapter.getSelectedItemPosition();
				break;
			}

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void checkHeightValue() throws Exception {

		String feet;

		if (height_decimal < 10) {
			feet = "" + height_numeric + ".0" + height_decimal;
		} else {
			feet = "" + height_numeric + "." + height_decimal;
		}
		Double f = Double.parseDouble(feet);

		String height = woman.getHeight();

		String[] heightVal = height.split("\\.");// 19April2017 Arpitha

		if (f > 0 && (f < 4.08 || f > 5.08)) {
			aq.id(R.id.warheight_ft).visible();
			if (woman.getHeight_unit() == height_unit && woman.getRisk_category() == 0
					&& Integer.parseInt(heightVal[0]) == height_numeric
					&& Integer.parseInt(heightVal[1]) == height_decimal) {
				isheight = false;
			} else
				isheight = true;
		} else {
			aq.id(R.id.warheight_ft).gone();
			isheight = false;
		}

		setrisk();

	}

	// Validate mandatory fields
	private boolean validateRegFields() throws NotFoundException, Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (aq.id(R.id.etwname).getEditText().getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.pls_ent_wname), "");
			aq.id(R.id.etwname).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etage).getText().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.pls_ent_age), "");
			aq.id(R.id.etage).getEditText().requestFocus();
			return false;
		}
		// changed on 1/7/16 by Arpitha
		if (Integer.parseInt(aq.id(R.id.etage).getText().toString()) == 0) {
			displayAlertDialog(getResources().getString(R.string.invalid_age), "");
			return false;
		}

		if (aq.id(R.id.etgestationage).getText().length() <= 0 && (!chkgest.isChecked())
				&& aq.id(R.id.etgestationagedays).getText().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.pls_ent_gestationage), "");
			aq.id(R.id.etgestationage).getEditText().requestFocus();
			return false;
		}

		if ((aq.id(R.id.etgestationage).getText().toString().length()) > 0) {
			if (Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) < 27
					|| Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) > 44) {
				displayAlertDialog(getResources().getString(R.string.gestationvalid), "");
				aq.id(R.id.etgestationage).getEditText().requestFocus();
				return false;
			}
		}

		// updated on 16july2016 by Arpitha
		if (aq.id(R.id.etgravida).getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.entr_gravida), "");
			aq.id(R.id.etgravida).getEditText().requestFocus();
			return false;
		}

		// updated on 16july2016 by Arpitha
		if (aq.id(R.id.etpara).getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.enter_para), "");
			aq.id(R.id.etpara).getEditText().requestFocus();
			return false;
		}

		// updated on 24july2016 by Arpitha
		if (Integer.parseInt(aq.id(R.id.etgravida).getText().toString()) == 0) {
			displayAlertDialog(getResources().getString(R.string.gravida_invalid), "");
			aq.id(R.id.etgravida).getEditText().requestFocus();
			return false;
		}

		// updated on 16jul2016 by Arpitha
		if (aq.id(R.id.spnbloodgroup).getSelectedItemPosition() == 0) {
			displayAlertDialog(getResources().getString(R.string.select_bloodgroup), "");
			aq.id(R.id.etweight).getEditText().requestFocus();
			return false;
		}

		// updated on 2/3/16 by Arpitha

		if (aq.id(R.id.etthayicardno).getText().toString().length() > 0) {
			if (aq.id(R.id.etthayicardno).getText().toString().length() < 7) {
				displayAlertDialog(getResources().getString(R.string.thayicardno_mustcontain_sevendigits), "");
				aq.id(R.id.etthayicardno).getEditText().requestFocus();
				return false;
			}
			// updated on 13july2016 by Arpitha
			if (Long.parseLong((aq.id(R.id.etthayicardno).getText().toString())) == 0) {
				displayAlertDialog(getResources().getString(R.string.invalid_thayicard), "");
				aq.id(R.id.etthayicardno).getEditText().requestFocus();
				return false;
			}
		}

		// updated 25Aug2016 - bindu - height mandatory

		// 26Nov2016 Arpitha - height is not mandatory for retrospective cases
		if (height_unit == 0 && woman.getregtype() != 2) {
			displayAlertDialog(getResources().getString(R.string.plsenterheight), "");
			aq.id(R.id.etpara).getEditText().requestFocus();
			return false;
		}

		// if (aq.id(R.id.etheight).getText().length() <= 0 &&
		// rd_cm.isChecked())
		if (aq.id(R.id.etheight).getText().length() <= 0 && rd_cm.isChecked()
				|| (rd_cm.isChecked() && aq.id(R.id.etheight).getText().length() > 0
						&& Double.parseDouble(aq.id(R.id.etheight).getText().toString()) == 0)) {
			displayAlertDialog(getResources().getString(R.string.plsenterheight), "");
			aq.id(R.id.etheight).getEditText().requestFocus();
			return false;
		}

		// 08Oct2017 Arpitha
		if (rd_feet.isChecked() && aq.id(R.id.spnHeightNumeric).getSelectedItemPosition() == 0
				&& aq.id(R.id.spnHeightDecimal).getSelectedItemPosition() == 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.plsenterheight),
					ViewProfile_Activity.this);
			return false;
		} // 08Oct2017 Arpitha

		if (isDelinfoadd || isupdatedel) { // 25jan2016

			if (aq.id(R.id.etchildwt1).getText().length() < 4) {
				displayAlertDialog(getResources().getString(R.string.child_wt), "");
				aq.id(R.id.etchildwt1).getEditText().requestFocus();
				return false;
			}

			// updated on 6/6/16 by Arpitha
			if (Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString()) == 0) {
				displayAlertDialog(getResources().getString(R.string.child_wt_val), "");
				aq.id(R.id.etchildwt1).getEditText().requestFocus();
				return false;
			}

			if (isDelinfoadd)// 12Jan2017 Arpitha
			{
				if (aq.id(R.id.etdeldate).getText().length() <= 0) {
					displayAlertDialog(getResources().getString(R.string.ent_deldate), "");
					aq.id(R.id.etdeldate).getEditText().requestFocus();
					return false;
				}

				if (aq.id(R.id.etdeltime).getText().length() <= 0) {
					displayAlertDialog(getResources().getString(R.string.ent_deltime), "");
					aq.id(R.id.etdeltime).getEditText().requestFocus();
					return false;
				}

				// updated on 13nov2015 - validating delivery date and time with
				// reg
				// date

				if (aq.id(R.id.etdeltime).getText().length() >= 1) {

					String date1, date2;
					String toa = aq.id(R.id.ettoa).getText().toString();
					date1 = strRegdate + " " + toa;
					String dtime = aq.id(R.id.etdeltime).getText().toString();
					date2 = strDeldate + " " + dtime;
					aq.id(R.id.etdeltime).getEditText().requestFocus();
					if (woman.getregtype() != 2) {
						if (!isDeliveryTimevalid(date1, date2, 1))
							return false;
						// 24August Arpitha
						if (strlastentrytime != null) { // 11Sep2016 bindu -
														// check
														// for
														// null
							strlastentrytime = strlastentrytime.replace("_", " ");// 09Aug2017
																					// Arpitha
							if (!isDeliveryTimevalid(strlastentrytime, date2, 2))
								return false;
						}
					} else {
						// 21Nov2016 Arpitha
						int days;
						if (woman.getLmp() != null) {
							days = Partograph_CommonClass.getDaysBetweenDates(strDeldate, woman.getLmp());
							if (days < 196) {
								displayAlertDialog(getResources()
										.getString(R.string.deldate_cannot_be_before_sevenmonths_from_lmp), "");
								return false;
							} else if (days > 301) {
								displayAlertDialog(
										getResources().getString(R.string.deldate_cannot_be_after_ninemonths_from_lmp),
										"");
								return false;
							}
						} // 21Nov2016 Arpitha
					}
					// 22August2016 Arpitha
					String currenttime = Partograph_CommonClass.getTodaysDate() + " "
							+ Partograph_CommonClass.getCurrentTime();
					if (!isDeliveryTimevalid(date2, currenttime, 3)) // 11Sep2016
																		// -
																		// bindu
																		// reversed
																		// arguments
						return false;
				}
			} // 12Jan2017 Arpitha

			if (numofchildren == 2) {
				if (aq.id(R.id.etchildwt2).getText().length() < 4) {
					displayAlertDialog(getResources().getString(R.string.child_wt), "");
					aq.id(R.id.etchildwt2).getEditText().requestFocus();
					return false;
				}

				// updated on 13nov2015 - validating delivery date and time with
				// reg date
				if (isDelinfoadd)// 12Jan2017 Arpitha
				{
					if (aq.id(R.id.etdeltime2).getText().length() >= 1) {
						String date1, date2;
						String toa = aq.id(R.id.ettoa).getText().toString();
						date1 = strRegdate + " " + toa;
						String dtime = aq.id(R.id.etdeltime2).getText().toString();
						date2 = strDeldate + " " + dtime;
						aq.id(R.id.etdeltime2).getEditText().requestFocus();
						// 24August2016 Arpitha
						if (woman.getregtype() != 2) {
							if (!isDeliveryTimevalid(date1, date2, 1))
								return false;
							// 19Aug2016

							if (strlastentrytime != null) { // 11Sep2016 bindu -
															// check
															// for null
								if (!isDeliveryTimevalid(strlastentrytime, date2, 2))
									return false;
							}
						}
						// 22August2016 Arpitha
						String currenttime = Partograph_CommonClass.getTodaysDate() + " "
								+ Partograph_CommonClass.getCurrentTime();
						if (!isDeliveryTimevalid(date2, currenttime, 3)) // 11Sep2016
																			// -
																			// bindu
																			// -
																			// reversed
																			// arguments
							return false;
					}
				} // 12Jan2017 Arpitha
			}

			if (aq.id(R.id.rdmotherdead).isChecked()) {
				if (aq.id(R.id.etmothersdeathreason).getText().toString().trim().length() <= 0) {// 17May2017
																									// Arpitha
																									// -
																									// v2.6
																									// trim()
					displayAlertDialog(getResources().getString(R.string.enter_mothersdeathreason), "");
					aq.id(R.id.etmothersdeathreason).getEditText().requestFocus();
					return false;
				}
			}
		}

		return true;
	}

	// Alert dialog
	private void displayAlertDialog(String message, final String classname) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
						if (classname.equalsIgnoreCase("breech"))// 18May2017
																	// v2.6
						{
							normaltype = 2;

						} else if (classname.equalsIgnoreCase("vertex")) {
							normaltype = 1;

						}
					}
				});// 18May2017 Arpitha - v2.6

		// create alert dialog

		// 18May2017 Arpitha - v2.6
		if (classname.equalsIgnoreCase("breech") || classname.equalsIgnoreCase("vertex")) {
			alertDialogBuilder.setMessage(message).setNegativeButton(getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
							if (classname.equalsIgnoreCase("breech")) {
								normaltype = 1;
								rdvertext.setChecked(true);
								rdbreech.setChecked(false);
							} else if (classname.equalsIgnoreCase("vertex")) {
								normaltype = 2;
								rdvertext.setChecked(false);
								rdbreech.setChecked(true);
							}

						}
					});
		}

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
		alertDialog.setCancelable(false);// 31oct2016 Arpitha
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		// 25jan2016
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.failedtoupdate), Toast.LENGTH_SHORT)
				.show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();
		if (isDelinfoadd) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.del_sucess), Toast.LENGTH_SHORT)
					.show();
			Fragment_DIP.isdelstatus = true;// 02nov2016 Arpitha
			// commented by bindu - 10Aug2016
		} else {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.woman_update), Toast.LENGTH_SHORT)
					.show();
		}
		// finish();

		calSyncMtd();
		Intent womActivity = new Intent(this, Activity_WomenView.class);
		startActivity(womActivity);

	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	private void caldatepicker() {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getSupportFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(getResources().getString(R.string.pickadate));
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {
					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);
		String strlmpdate = null;

		// updated on 19August2016 by Arpitha
		Date lastentrydate = null;
		if (strlastentrytime != null) {
			lastentrydate = new SimpleDateFormat("yyyy-MM-dd").parse(strlastentrytime);
		}

		try {

			// updated by Arpitha
			if (islmpdate) {

				Date taken = new SimpleDateFormat("yyyy-MM-dd").parse(selecteddate);
				Date regdate = null;
				if (woman.getDate_of_admission() != null)
					regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_admission());
				else
					regdate = new SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_reg_entry());

				if (taken.after(new Date())) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.curr_date_validation),
							Toast.LENGTH_SHORT).show();
					if (isLmp) {
						aq.id(R.id.etlmp).text(" ");
						aq.id(R.id.etedd).text("");// 23Nov2016 Arpitha
					} else {
						aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
								Partograph_CommonClass.defdateformat));
						strDeldate = todaysDate;
					}
				}

				if (isLmp) {
					aq.id(R.id.etlmp).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));

					if (validateLmpADDDateSet(todaysDate, selecteddate)) {
						strlmpdate = selecteddate;
						String gestation = Partograph_CommonClass.getNumberOfWeeksDays(selecteddate,
								Partograph_CommonClass.getTodaysDate());// 08Nov2016
						String[] gest = gestation.split(",");// 08Nov2016
						aq.id(R.id.etgestationage).text(gest[0]);// 08Nov2016
						aq.id(R.id.etgestationagedays).text(gest[1]);// 08Nov2016

						populateEDD(strlmpdate);

					}
					// updated on 1Aug2016 by Arpitha
					if (aq.id(R.id.etgestationage).getText().length() > 0
							|| aq.id(R.id.etgestationagedays).getText().length() > 0) {
						chkgest.setChecked(false);
						chkgest.setEnabled(false);
					}
				} else {
					// curr date val - 11Sep2016 - bindu
					if (woman.getregtype() != 2) {
						if (taken.after(new Date())) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.curr_date_validation), Toast.LENGTH_SHORT).show();
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
									Partograph_CommonClass.defdateformat));
							strDeldate = todaysDate;
						} else if (regdate != null && taken.before(regdate)) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.date_cant_be_before_reg_date), Toast.LENGTH_SHORT)
									.show();
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
									Partograph_CommonClass.defdateformat));
							strDeldate = todaysDate;
						}
						// 19August2016 by Arpitha
						else if (lastentrydate != null && taken.before(lastentrydate)) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.deldate_before_lastpartodate) + " - "
											+ strlastentrytime,
									Toast.LENGTH_SHORT).show();
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
									Partograph_CommonClass.defdateformat)); // 11Sep2016-
																			// bindu
						} else {
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
									Partograph_CommonClass.defdateformat));
							strDeldate = selecteddate;
						}

					} else// 31Oct2016 Arpitha
					{
						Date lmp = null;
						int days = 0;
						int months = 0;

						if (strlmpdate != null) {
							lmp = new SimpleDateFormat("yyyy-MM-dd").parse(strlmpdate);
							months = taken.getMonth() - lmp.getMonth();

							days = Partograph_CommonClass.getDaysBetweenDates(strlmpdate, selecteddate);
						}
						if (taken.after(new Date())) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.curr_date_validation), Toast.LENGTH_SHORT).show();
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
									Partograph_CommonClass.defdateformat));
							strDeldate = todaysDate;
						} else if (strlmpdate != null && days < 196) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.deldate_cannot_be_before_sevenmonths_from_lmp),
									Toast.LENGTH_LONG).show();
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
									Partograph_CommonClass.defdateformat));
							strDeldate = todaysDate;
						} else if ((strlmpdate != null && days > 301)) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.deldate_cannot_be_after_ninemonths_from_lmp),
									Toast.LENGTH_LONG).show();
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
									Partograph_CommonClass.defdateformat));
							strDeldate = todaysDate;

						}

						else if (refdate != null && taken.before(refdate)) {// 12April2017
																					// Arpitha
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.deltim_bef_reftim), Toast.LENGTH_SHORT).show();

						} // 12April2017 Arpitha

						else {
							aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
									Partograph_CommonClass.defdateformat));
							strDeldate = selecteddate;
						}
					} // 31Oct2016 Arpitha
				}

			}

			else
			// updated on 28Feb2016 by Arpitha
			{
				// strlmpdate = aq.id(R.id.etlmp).getText().toString();//
				// 18April2017
				// Arpitha

				// strlmpdate =
				// Partograph_CommonClass.getConvertedDateFormat(strlmpdate,
				// "dd-MM-yyyy");
				if (woman.getLmp() != null && woman.getLmp().trim().length() > 0) {
					if (validateEDDADDDateSet(woman.getLmp(), selecteddate)) {
						aq.id(R.id.etedd).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
								Partograph_CommonClass.defdateformat));
						stredddate = selecteddate;
					}
				} else {
					aq.id(R.id.etedd).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					stredddate = selecteddate;
				}
			}
			// }

		} catch (

		Exception e)

		{
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_ID:
			// set time picker as current time
			return new TimePickerDialog(this, timePickerListener, hour, minute, false);

		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {

			try {

				if (view.isShown()) {
					hour = selectedHour;
					minute = selectedMinute;

					// updated on 19Augu st2016 by Arpitha
					Date regdatetime = null;
					String regdate = null;
					try {

						// 11Sep2016 - bindu -
						regdate = woman.getDate_of_admission() + " " + woman.getTime_of_admission();
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (regdate != null) {
						try {
							regdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(regdate);
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
					Date lastentrydate = null;
					if (strlastentrytime != null) {
						try {
							lastentrydate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strlastentrytime);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					String strselecteddate = strDeldate + " "
							+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));
					Date selecteddate = null;
					try {
						selecteddate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strselecteddate);
					} catch (ParseException e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

					if (isDelTime1) {

						// 31oct2016 Arpitha
						if (woman.getregtype() != 2) {

							if (selecteddate.before(regdatetime)) {

								String admDate = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
										Partograph_CommonClass.defdateformat);// 25April2017
																				// Arpitha
								Toast.makeText(
										getApplicationContext(), getResources().getString(R.string.deltime_before_reg)
												+ " - " + admDate + " " + woman.getTime_of_admission(),
										Toast.LENGTH_SHORT).show();

							} else if (lastentrydate != null && selecteddate.before(lastentrydate)) {

								String lastEntryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
										Partograph_CommonClass.defdateformat);// 25April2017
																				// Arpitha

								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_before_lastpartime) + " - "
												+ lastEntryDate + " " + strlastentrytime.substring(11),
										Toast.LENGTH_SHORT).show();

							} else if (selecteddate.after(new Date())) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_after_currenttime),
										Toast.LENGTH_SHORT).show();

							}

							else if (refdatetime != null && selecteddate.before(refdatetime)) {// 12April2017
																								// Arpitha

								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltim_bef_reftim), Toast.LENGTH_SHORT)
										.show();

							} // 12April2017 Arpitha

							else
								aq.id(R.id.etdeltime).getEditText().setText(new StringBuilder()
										.append(padding_str(hour)).append(":").append(padding_str(minute)));
						}
						// 31oct2016 Arpitha
						else {
							if (selecteddate.after(new Date())) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_after_currenttime),
										Toast.LENGTH_SHORT).show();

							} else
								aq.id(R.id.etdeltime).getEditText().setText(new StringBuilder()
										.append(padding_str(hour)).append(":").append(padding_str(minute)));
						}
					} else {

						// 31oct2016 Arpitha
						if (woman.getregtype() != 2) {
							if (selecteddate.before(regdatetime)) {

								String admDate = Partograph_CommonClass.getConvertedDateFormat(strRegdate,
										Partograph_CommonClass.defdateformat);// 25April2017
																				// Arpitha

								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_before_reg) + " - " + admDate + " "
												+ strRegdate.substring(11),
										Toast.LENGTH_SHORT).show();

							} else if (lastentrydate != null && selecteddate.before(lastentrydate)) {

								String lastEntryDate = Partograph_CommonClass.getConvertedDateFormat(strlastentrytime,
										Partograph_CommonClass.defdateformat);// 25April2017
																				// Arpitha

								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_before_lastpartime) + " - "
												+ lastEntryDate + " " + strlastentrytime.substring(11),
										Toast.LENGTH_SHORT).show();

							} else if (selecteddate.after(new Date())) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_after_currenttime),
										Toast.LENGTH_SHORT).show();

							} else

								aq.id(R.id.etdeltime2).getEditText().setText(new StringBuilder()
										.append(padding_str(hour)).append(":").append(padding_str(minute)));
						}
						// 31oct2016 Arpitha
						else {
							if (selecteddate.after(new Date())) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.deltime_after_currenttime),
										Toast.LENGTH_SHORT).show();

							} else
								aq.id(R.id.etdeltime).getEditText().setText(new StringBuilder()
										.append(padding_str(hour)).append(":").append(padding_str(minute)));
						} // 31oct2016
							// Arpitha
					}
				}
				// }

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	};

	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {

			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				// NavUtils.navigateUpFromSameTask(this);

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.home));

				return true;

			case R.id.home:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.main));
				return true;

			case R.id.logout:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.login));
				return true;

			// 10May2017 Arpitha - v2.6
			case R.id.info:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.info));

				return true;

			case R.id.about:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.about));

				return true;

			case R.id.settings:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.settings));

				return true;

			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(ViewProfile_Activity.this, user.getUserId());
				return true;

			case R.id.comments:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.comments));
				return true;

			case R.id.summary:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.summary));

				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(ViewProfile_Activity.this);
				return true;

			case R.id.usermanual:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.user_manual));
				return true;// 10May2017 Arpitha - v2.6

			case R.id.disch:
				Intent disch = new Intent(ViewProfile_Activity.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			// 03Oct2017 Arpitha
			case R.id.del:
				// Intent disc = new Intent(ViewProfile_Activity.this,
				// DeliveryInfo_Activity.class);
				// disc.putExtra("woman", woman);
				// startActivity(disc);
				// 27Sep2016
				// Arpitha



				if (woman.getDel_type() != 0)// 21Oct2016
												// Arpitha
				{// 21Oct2016 Arpitha
					// 05Jan2016
					// / showDialogToUpdateStatus();
					Partograph_CommonClass.showDialogToUpdateStatus(ViewProfile_Activity.this, woman);
//					loadWomendata(displayListCountddel);
//					if(adapter!=null)
//					adapter.notifyDataSetChanged();
					// loadWomendata(displayListCountddel);
				} else// 21Oct2016 Arpitha
				{

					Intent graph = new Intent(ViewProfile_Activity.this, DeliveryInfo_Activity.class);
					graph.putExtra("woman", woman);
					startActivity(graph);
				} // 21Oct2016 Arpitha
			

				return true;
			case R.id.viewprofile:
				Intent view = new Intent(ViewProfile_Activity.this, ViewProfile_Activity.class);
				view.putExtra("woman", woman);
				startActivity(view);
				return true;

			case R.id.ref:
				// Intent ref = new Intent(ViewProfile_Activity.this,
				// ReferralInfo_Activity.class);
				// ref.putExtra("woman", woman);
				// startActivity(ref);
				Cursor cur = dbh.getReferralDetails(woman.getUserId(), woman.getWomenId());
				if (cur != null && cur.getCount() > 0) {
					Partograph_CommonClass.showDialogToUpdateReferral(ViewProfile_Activity.this, woman);
				} else {
					Intent ref = new Intent(ViewProfile_Activity.this, ReferralInfo_Activity.class);
					ref.putExtra("woman", woman);
					startActivity(ref);
				}
				return true;

			case R.id.pdf:
				Partograph_CommonClass.exportPDFs(ViewProfile_Activity.this, woman);
				return true;

			case R.id.disc:
				Intent discharge = new Intent(ViewProfile_Activity.this, DiscargeDetails_Activity.class);
				discharge.putExtra("woman", woman);
				startActivity(discharge);
				return true;

			case R.id.apgar:
				if (woman.getDel_type() != 0) {
					Intent apgar = new Intent(ViewProfile_Activity.this, Activity_Apgar.class);
					apgar.putExtra("woman", woman);
					startActivity(apgar);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.stages:
				if (woman.getDel_type() != 0) {
					Intent stages = new Intent(ViewProfile_Activity.this, StageofLabor.class);
					stages.putExtra("woman", woman);
					startActivity(stages);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.post:
				if (woman.getDel_type() != 0) {
					Intent post = new Intent(ViewProfile_Activity.this, PostPartumCare_Activity.class);
					post.putExtra("woman", woman);
					startActivity(post);
				} else
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.msg_applicableafterdelivery), Toast.LENGTH_LONG).show();
				return true;

			case R.id.parto:
				Intent parto = new Intent(ViewProfile_Activity.this, SlidingActivity.class);
				parto.putExtra("woman", woman);
				startActivity(parto);
				return true;// 03Oct2017 Arpitha

			}
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	// changes made on 26Mar2015
	// Display Confirmation to exit the screen
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(ViewProfile_Activity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// updated on 26May16 by Arpitha
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);

		txtdialog.setText(exit_msg);
		txt1.setVisibility(View.GONE);
		txt2.setVisibility(View.GONE);

		if (classname.equalsIgnoreCase(getResources().getString(R.string.warning))) {
			String strwarningmessage = "";
			String strval = "";

			txt1.setVisibility(View.VISIBLE);

			txt2.setVisibility(View.VISIBLE);

			if (age < 18 || age > 40) {
				strwarningmessage = strwarningmessage + getResources().getString(R.string.age_is) + "\n";
				strval = strval + "" + age + getResources().getString(R.string.years) + "\n";

			}
			if ((height > 0 && (height > 176 || height < 146))) {
				strwarningmessage = strwarningmessage + getResources().getString(R.string.height_is) + "\n";
				strval = strval + "" + height + getResources().getString(R.string.centimeter) + "\n";
			}
			if (d > 0 && (d > 5.08 || d < 4.08)) {
				strwarningmessage = strwarningmessage + getResources().getString(R.string.height_is) + "\n";
				strval = strval + "" + feet_height + getResources().getString(R.string.feet) + "\n";
			}
			txt2.setText(getResources().getString(R.string.bp_val));

			// 22Sep2016 Arpitha
			if (weight > 0 && (weight <= 35 || weight > 80)) {
				strwarningmessage = strwarningmessage + getResources().getString(R.string.weight_is);
				strval = strval + "" + weight + getResources().getString(R.string.kg);

			}
			if (baby1weight > 0 && (baby1weight < 2500 || baby1weight > 4000)) {
				strval = strval + "" + baby1weight + "\n";
				if (baby2weight > 0)

					strwarningmessage = strwarningmessage + getResources().getString(R.string.first_baby_weight) + "\n";
				else
					strwarningmessage = strwarningmessage + getResources().getString(R.string.baby_weight) + "\n";

			}

			if (baby2weight > 0 && (baby2weight < 2500 || baby2weight > 4000)) {
				strwarningmessage = strwarningmessage + getResources().getString(R.string.second_baby_weight) + "\n";
				strval = strval + "" + baby2weight;

			}

			if (duration > 0 && duration > 12) {
				strwarningmessage = strwarningmessage
						+ getResources().getString(R.string.deldatetime_twelvehour_after_lastparto_entry_datetime)
						+ "\n";
				// strval = strval + "" + duration;

			}

			txtdialog.setText(strwarningmessage);
			txt1.setText(strval);
		}

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.warning))) {
					savewomendata();
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.clear))) {
					clearAllFields();
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.home))) {
					Intent i = new Intent(getApplicationContext(), Activity_WomenView.class);
					startActivity(i);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.main))) {
					Intent i = new Intent(getApplicationContext(), Activity_WomenView.class);
					startActivity(i);
				}
				if (classname.equalsIgnoreCase(getResources().getString(R.string.login))) {
					Intent i = new Intent(getApplicationContext(), LoginActivity.class);
					startActivity(i);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.summary))) {
					Intent i = new Intent(getApplicationContext(), Summary_Activity.class);
					startActivity(i);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.user_manual))) {
					Intent i = new Intent(getApplicationContext(), UseManual.class);
					startActivity(i);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.about))) {
					Intent i = new Intent(getApplicationContext(), About.class);
					startActivity(i);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.settings))) {
					Intent i = new Intent(getApplicationContext(), Settings_parto.class);
					startActivity(i);
				} else if (classname.equalsIgnoreCase(getResources().getString(R.string.info))) {
					Intent i = new Intent(getApplicationContext(), GraphInformation.class);
					startActivity(i);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		dialog.show();// 31Oct2016 Arpitha
		dialog.setCancelable(false);// 31Oct2016 Arpitha
	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	/*
	 * // To avoid special characters in Input type public static InputFilter
	 * filter1 = new InputFilter() {
	 * 
	 * @Override public CharSequence filter(CharSequence source, int start, int
	 * end, Spanned dest, int dstart, int dend) { String blockCharacterSet =
	 * "~#^|$%*!@/()-'\":;?{}=!$^';?Ãƒâ€”ÃƒÂ·<>{}Ã¢â€šÂ¬Ã‚Â£Ã‚Â¥Ã¢â€šÂ©%&+*.[]1234567890";
	 * if (source != null && blockCharacterSet.contains(("" + source))) { return
	 * ""; } return null; } };
	 */

	// updated on 13nov2015
	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {
			dateofreg = sdf.parse(date1);
			dateofdelivery = sdf.parse(date2);

			String strDate1 = Partograph_CommonClass.getConvertedDateFormat(date1,
					Partograph_CommonClass.defdateformat);// 25April2017 Arpitha

			String strDate2 = Partograph_CommonClass.getConvertedDateFormat(date2,
					Partograph_CommonClass.defdateformat);// 25April2017 Arpitha

			if (dateofdelivery.before(dateofreg)) {

				// 19August2016 included if condition
				if (i == 1)
					displayAlertDialog(getResources().getString(R.string.deltime_before_reg) + " - " + strDate1 + " "
							+ strDate1.substring(11), "");
				// 19August2016 included if condition
				else if (i == 2)
					displayAlertDialog(getResources().getString(R.string.deltime_before_lastpartotime) + " - "
							+ strDate2 + " " + date2.substring(11), "");
				else if (i == 3)
					displayAlertDialog(getResources().getString(R.string.invaliddeltime), "");
				return false;

			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
			return false;
		}
	}

	// Set Junk Items
	private void setDelTypeReason(int dtype) throws Exception {
		int i = 0;
		deltypereasonMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;
		if (dtype == 2) {
			// reasonStrArr =
			// Arrays.asList(getResources().getStringArray(R.array.spnreasoncesarean));
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoncesareanValues));// 29Sep2016
																											// Arpitha
		} else if (dtype == 3) {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoninstrumental));
		}

		if (reasonStrArr != null) {
			for (String str : reasonStrArr) {
				deltypereasonMap.put(str, i);
				i++;
			}
			// mspndeltypereason.setItems(reasonStrArr);
		}

		// 29Sep2016 Arpitha
		if (dtype == 2)

		{
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoncesarean));
		} else {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoninstrumental));
		}
		if (reasonStrArr != null) {
			mspndeltypereason.setItems(reasonStrArr);
		}
	}

	// Set options for tears - 05jan2016
	protected void setTears() throws Exception {

		int i = 0;
		tearsMap = new LinkedHashMap<String, Integer>();
		List<String> tearsStrArr = null;

		// updated on 6july2016

		List<String> tearsStrArrvalues = null;

		tearsStrArrvalues = Arrays.asList(getResources().getStringArray(R.array.tearsvalues));

		if (tearsStrArrvalues != null) {
			for (String str : tearsStrArrvalues) {
				tearsMap.put(str, i);
				i++;
			}
		}

		tearsStrArr = Arrays.asList(getResources().getStringArray(R.array.tears));

		if (tearsStrArr != null) {
			mspntears.setItems(tearsStrArr);
		}
	}

	/**
	 * Validate Lmp 25jan2016
	 * 
	 * @param sdate
	 * @param tdate
	 */
	private boolean validateLmpADDDateSet(String tdate, String sdate) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		int days = Partograph_CommonClass.getDaysBetweenDates(tdate, sdate);
		if (days < 196) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg010), Toast.LENGTH_LONG)
					.show();
			aq.id(R.id.etlmp).text("").getEditText().requestFocus();
			aq.id(R.id.etedd).text("");// 19Nov2016 Arpitha
			return false;
		}
		if (days > 301) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg011), Toast.LENGTH_LONG)
					.show();
			aq.id(R.id.etlmp).text("").getEditText().requestFocus();
			aq.id(R.id.etedd).text("");// 19Nov2016 Arpitha
			return false;
		}

		return true;
	}

	// updated on 30May2016 by Arpitha
	private boolean validateEDDADDDateSet(String tdate, String sdate) throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// 19July2017 Arpitha - 2.6.1 bug fixing
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date lmpDate = null;
		Date edddate = null;
		if (tdate != null & tdate.length() > 0)
			lmpDate = format.parse(tdate);

		if (sdate != null & sdate.length() > 0)
			edddate = format.parse(sdate);

		if (edddate.before(lmpDate)) {
			displayAlertDialog(getResources().getString(R.string.edd_cannnot_be_lmp), "");
			return false;
		} // 19July2017 Arpitha - 2.6.1 bug fixing

		int days = Partograph_CommonClass.getDaysBetweenDates(sdate, tdate);
		if (days < 212) {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.edd_cannot_be_sevenmonths_before_lmp), Toast.LENGTH_LONG).show();
			aq.id(R.id.etedd).text("").getEditText().requestFocus();
			return false;
		}

		int no_of_days = Partograph_CommonClass.getDaysBetweenDates(Partograph_CommonClass.getTodaysDate(), tdate);// 25April2017
																													// Arpitha
		if (aq.id(R.id.etlmp).getText().toString().trim().length() <= 0 && no_of_days > 180) {// 18May2017
																								// Arpitha
																								// -
																								// v2.6
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.edd_cannot_be_after_sixmonths_from_doa), Toast.LENGTH_LONG)
					.show();
			aq.id(R.id.etedd).text("").getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etlmp).getText().toString().trim().length() <= 0 && no_of_days < -180)// 18May2017
																								// Arpitha
																								// -
																								// v2.6

		{
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.edd_cannot_be_after_sixmonths_from_doa), Toast.LENGTH_LONG)
					.show();
			aq.id(R.id.etedd).text("").getEditText().requestFocus();
			return false;
		}

		// 18May2017 Arpitha - v2.6
		if (aq.id(R.id.etlmp).getText().toString().trim().length() >= 0) {
			int no_of_days_lmp_edd = Partograph_CommonClass.getDaysBetweenDates(tdate, sdate);
			if ((no_of_days_lmp_edd > 300 || no_of_days_lmp_edd < -300)) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.edd_cannot_be_after_sixmonths_from_doa), Toast.LENGTH_LONG)
						.show();
				aq.id(R.id.etedd).text("").getEditText().requestFocus();
				return false;
			}
		} // 18May2017 Arpitha - v2.6

		return true;
	}

	public void savewomendata() {
		try {

			// 27Sep2016 Arpitha
			if (Partograph_CommonClass.autodatetime(ViewProfile_Activity.this)) {

				Date lastregdate = null;
				String strlastregdate = dbh.getlastmaxdate(user.getUserId(),
						getResources().getString(R.string.viewprofile));
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
				if (strlastregdate != null) {
					lastregdate = format.parse(strlastregdate);
				}
				Date currentdatetime = new Date();
				if (lastregdate != null && currentdatetime.before(lastregdate)) {
					Partograph_CommonClass.showSettingsAlert(ViewProfile_Activity.this,
							getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
				} else {// 27Sep2016 Arpitha

					String age = aq.id(R.id.etage).getText().toString();
					if (!(age.equals("") || (age == null)))
						womanAge = Integer.parseInt(age);

					String gravida = aq.id(R.id.etgravida).getText().toString();
					if (!(gravida.equals("") || (gravida == null)))
						igravida = Integer.parseInt(gravida);

					String para = aq.id(R.id.etpara).getText().toString();
					if (!(para.equals("") || (para == null)))
						ipara = Integer.parseInt(para);

					wpojo = new Women_Profile_Pojo();
					wpojo.setAddress(aq.id(R.id.etaddress).getText().toString());
					wpojo.setAge(womanAge);
					wpojo.setDoc_name(aq.id(R.id.etdocname).getText().toString());
					wpojo.setGravida(igravida);
					wpojo.setHosp_no(aq.id(R.id.ethospno).getText().toString());
					wpojo.setNurse_name(aq.id(R.id.etnursename).getText().toString());
					wpojo.setPara(ipara);
					wpojo.setPhone_No(aq.id(R.id.etphone).getText().toString());
					wpojo.setUserId(user.getUserId());
					wpojo.setW_attendant(aq.id(R.id.etattendant).getText().toString());

					if (!isremovephotoClicked)
						wpojo.setWomen_Image(baos == null ? image1 : baos.toByteArray());
					else
						wpojo.setWomen_Image(baos == null ? null : baos.toByteArray());

					wpojo.setWomen_name(aq.id(R.id.etwname).getText().toString());
					wpojo.setSpecial_inst(aq.id(R.id.etspecialinstructions).getText().toString());
					wpojo.setWomenId(womenID);

					// 19Aug2016 - bindu

					if (woman.getregtype() != 2)// 18April2017 Arpitha
					{
						wpojo.setRisk_category(risk);

						if (risk == 0) {
							wpojo.setRiskoptions("");
						} else {
							String riskOptions = null;

							if (!woman.getRiskoptions().contains("12"))// 13April2017
																		// Arpitha
								riskOptions = woman.getRiskoptions() + "," + selectedriskOptions;// 13April2017
							// Arpitha
							wpojo.setRiskoptions(riskOptions);
						}
					} else
						wpojo.setRisk_category(99);// 17May2017 Arpitha - v2.6

					wpojo.setComments(aq.id(R.id.etcomments).getText().toString());

					if (isDelinfoadd) {

						wpojo.setDel_Comments(aq.id(R.id.etdelcomments).getText().toString());
						wpojo.setDel_Time(aq.id(R.id.etdeltime).getText().toString());
						wpojo.setDel_time2(aq.id(R.id.etdeltime2).getText().toString());
						wpojo.setDel_Date(strDeldate); // changed 07Apr2015

						int motherdead;

						if (aq.id(R.id.rdmotheralive).isChecked())
							motherdead = 0;
						else
							motherdead = 1;

						wpojo.setMothersdeath(motherdead);

						int babywt1 = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());
						int babywt2 = 0;
						if (aq.id(R.id.etchildwt2).getText().toString().trim().length() > 0)// 19April2017
																							// Arpitha
							babywt2 = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());

						wpojo.setBabywt1(babywt1);
						wpojo.setBabywt2(babywt2);
						wpojo.setBabysex1(babysex1);
						wpojo.setBabysex2(babysex2);
						wpojo.setDel_result1(delivery_result1);
						wpojo.setDel_result2(delivery_result2);
						wpojo.setDel_type(delivery_type);
						wpojo.setNo_of_child(numofchildren);
						wpojo.setMothers_death_reason(aq.id(R.id.etmothersdeathreason).getText().toString());

						// 04Dec2015
						String selIds = "";
						if (delivery_type > 1) {
							List<String> selecteddeltypereason = mspndeltypereason.getSelectedStrings();

							if (selecteddeltypereason != null && selecteddeltypereason.size() > 0) {
								for (String str : selecteddeltypereason) {
									if (selecteddeltypereason.indexOf(str) != selecteddeltypereason.size() - 1)
										selIds = selIds + deltypereasonMap.get(str) + ",";
									else
										selIds = selIds + deltypereasonMap.get(str);
								}
							}
						}
						wpojo.setDelTypeReason(selIds);

						wpojo.setDeltype_otherreasons("" + aq.id(R.id.etdeltypeotherreasons).getText().toString());

						// 05jan2016
						String seltearsIds = "";
						List<String> selectedtears = mspntears.getSelectedStrings();
						if (selectedtears != null && selectedtears.size() > 0) {
							for (String str : selectedtears) {
								if (selectedtears.indexOf(str) != selectedtears.size() - 1)
									seltearsIds = seltearsIds + tearsMap.get(str) + ",";
								else
									seltearsIds = seltearsIds + tearsMap.get(str);
							}
						}

						wpojo.setTears(seltearsIds);
						wpojo.setEpisiotomy(episiotomy);
						wpojo.setSuturematerial(suturematerial);

						wpojo.setNormaltype(normaltype);// 20Nov2016 Arpitha

						if (normaltype == 0)// 17July2017 Arpitha - bug fixing
											// 2.6.1
							wpojo.setNormaltype(presentation);// 17July2017
																// Arpitha - bug
																// fixing 2.6.1

					} else// 05may2017 Arpitha- v2.6 mantis id 0000231
						wpojo.setNormaltype(presentation);// 05may2017 Arpitha-
															// v2.6 mantis id
															// 0000231

					// Aug 17 2015
					String gestationage = aq.id(R.id.etgestationage).getText().toString();

					if (!(gestationage.equals("") || (gestationage == null))) {
						int gest_age = Integer.parseInt(gestationage);
						wpojo.setGestationage(gest_age);
					}

					String gestationagedays = aq.id(R.id.etgestationagedays).getText().toString();
					if (!(gestationagedays.equals("") || (gestationagedays == null))) {
						int gest_age_days = Integer.parseInt(gestationagedays);
						wpojo.setGest_age_days(gest_age_days);
					}

					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
					String currentDateandTime = sdf.format(new Date());

					wpojo.setDatelastupdated(currentDateandTime);
					// 19jan2016
					wpojo.setBloodgroup(bldgrp);

					// updated on 30Nov2015
					wpojo.setThayicardnumber("" + aq.id(R.id.etthayicardno).getText().toString());

					// SimpleDateFormat a = new SimpleDateFormat("dd-MM-yyyy");
					SimpleDateFormat a = new SimpleDateFormat(Partograph_CommonClass.defdateformat);// 26Feb2017
																									// Arpitha
					Calendar cal = Calendar.getInstance();

					Date LmpDate;
					String lmp = aq.id(R.id.etlmp).getText().toString();
					if (lmp.trim().length() > 0) {
						LmpDate = a.parse(lmp);
						cal.setTime(LmpDate);
						cal.add(Calendar.DAY_OF_MONTH, 0);

						int day = cal.get(Calendar.DAY_OF_MONTH);
						int month = cal.get(Calendar.MONTH) + 1;
						int year = cal.get(Calendar.YEAR);

						strlmp = String.valueOf(year) + "-" + String.format("%02d", month) + "-"
								+ String.format("%02d", day);

					}
					wpojo.setLmp(strlmp);

					if (aq.id(R.id.etedd).getText().length() > 0) {
						String edd = aq.id(R.id.etedd).getText().toString();
						if (edd.length() > 0) {
							Date edddate = a.parse(edd);
							cal.setTime(edddate);
							cal.add(Calendar.DAY_OF_MONTH, 0);

							int eddday = cal.get(Calendar.DAY_OF_MONTH);
							int eddmonth = cal.get(Calendar.MONTH) + 1;
							int eddyear = cal.get(Calendar.YEAR);

							stredd = String.valueOf(eddyear) + "-" + String.format("%02d", eddmonth) + "-"
									+ String.format("%02d", eddday);

						}
					}
					wpojo.setEdd(stredd);

					String weight = aq.id(R.id.etweight).getText().toString();
					if (!(weight.equals("") || (weight == null))) {
						iweight = weight;
						if ((double) Double.parseDouble(weight) == 0.0) {
							aq.id(R.id.etweight).text("");
						}
					}
					wpojo.setW_weight(iweight);

					// updated on 19may16 by Arpitha
					wpojo.setHeight_unit(height_unit);
					String heightval = "";

					if (height_unit == 1) {
						heightval = "" + (height_numeric + "." + height_decimal);
						wpojo.setHeight(heightval);

					} else if (height_unit == 2) {
						wpojo.setHeight(aq.id(R.id.etheight).getText().toString());

					} else {
						wpojo.setHeight(heightval);
					}

					wpojo.setOther(aq.id(R.id.etother).getText().toString());

					wpojo.setExtracomments(aq.id(R.id.etextracoments).getText().toString());// 13Oct2016
																							// Arpitha

					if (isupdatedel) {
						wpojo.setDel_Comments(aq.id(R.id.etdelcomments).getText().toString());

						int motherdead;
						if (aq.id(R.id.rdmotheralive).isChecked())
							motherdead = 0;
						else
							motherdead = 1;

						wpojo.setMothersdeath(motherdead);

						int babywt1 = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());
						int babywt2 = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());

						wpojo.setBabywt1(babywt1);
						wpojo.setBabywt2(babywt2);
						wpojo.setBabysex1(babysex1);
						wpojo.setBabysex2(babysex2);
						wpojo.setMothers_death_reason(aq.id(R.id.etmothersdeathreason).getText().toString());
						wpojo.setMothers_death_reason(aq.id(R.id.etmothersdeathreason).getText().toString());

					}

					// wpojo.setRiskoptions(riskoptions);

					dbh.db.beginTransaction();
					String regSql = "";
					int transId = dbh.iCreateNewTrans(user.getUserId());

					if (isDelinfoadd) {
						// bindu - 29july2016 - add oldwomanobj to check for
						// audit
						// trail
						regSql = dbh.updateWomenRegData(wpojo, transId, Partograph_CommonClass.oldWomanObj);

						dbh.deletetblnotificationrow(womenID, user.getUserId());// 17Oct2016
																				// Arpitha
					} else
						regSql = dbh.updateProfileData(wpojo, transId, isupdatedel, Partograph_CommonClass.oldWomanObj); // Bindu
																															// -Aug
																															// 08
																															// 2016
																															// -
																															// added
																															// parameter
																															// oldwomanobj

					if (regSql.length() > 0) {
						commitTrans();
					} else
						rollbackTrans();

				}
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	TextWatcher watcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			try {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				if (s == aq.id(R.id.etage).getEditable()) {

					if (aq.id(R.id.etage).getText().toString().length() > 0) {// changed
																				// count
																				// to
																				// aq.id(R.id.etage).getText().toString().length()
																				// -
																				// 23Nov2016
																				// Arpitha
						int age = Integer.parseInt(aq.id(R.id.etage).getText().toString());// 09Oct2016
																							// Arpitha
						if (age > 0 && (age < 18 || age > 40)) {
							aq.id(R.id.warage).visible();
							isage = true;
						} else {
							aq.id(R.id.warage).invisible();
							isage = false;

						}

					} else {
						aq.id(R.id.warage).invisible();
						isage = false;

					}
					if (woman.getregtype() != 2)
						setrisk();

				} else if (s == aq.id(R.id.etheight).getEditable()) {

					if (aq.id(R.id.etheight).getText().toString().trim().length() > 0) {
						// int height =
						// Integer.parseInt(aq.id(R.id.etheight).getText().toString());//
						// 09Oct2016
						// Arpitha
						if (Double.parseDouble(aq.id(R.id.etheight).getText().toString()) > 275) {// 13April2017
																									// Arpitha
							displayAlertDialog(getResources().getString(R.string.max_height), "");
							aq.id(R.id.etheight).text("");
						} else {
							// if (height > 0 && (height > 176 || height < 146))
							// {
							if (Double.parseDouble(aq.id(R.id.etheight).getText().toString()) > 176
									|| Double.parseDouble(aq.id(R.id.etheight).getText().toString()) < 146) {// 13April2017
																												// Arpitha
								aq.id(R.id.warheight).visible();

								isheight = true;
							} else {
								aq.id(R.id.warheight).invisible();
								isheight = false;
							}
						}
					} else {
						aq.id(R.id.warheight).invisible();
						isheight = false;

					}
					if (woman.getregtype() != 2)
						setrisk();

				}

				else if (s == aq.id(R.id.etweight).getEditable()) {

					if (aq.id(R.id.etweight).getText().toString().trim().length() > 0) {
						double weight = Double.parseDouble(aq.id(R.id.etweight).getText().toString());

						if (weight < 35 || weight == 35 || weight > 80) {
							aq.id(R.id.imgweight).visible();

							isweight = true;
						} else {
							aq.id(R.id.imgweight).invisible();
							isweight = false;

						}

					} else {
						// updated on 1Aug2016 by Arpitha
						isweight = false;
						aq.id(R.id.imgweight).invisible();
					}
					if (woman.getregtype() != 2)
						setrisk();

				} else if (s == aq.id(R.id.etpara).getEditable()) {
					int para = Integer.parseInt(aq.id(R.id.etpara).getText().toString());
					int gravida = Integer.parseInt(aq.id(R.id.etgravida).getText().toString());
					if (aq.id(R.id.etpara).getText().toString().trim().length() > 0) {
						if (para >= gravida) {
							Toast.makeText(getApplicationContext(), getResources().getText(R.string.para_validation),
									Toast.LENGTH_LONG).show();
							aq.id(R.id.etpara).text("");
						}
					}
				} else if (s == aq.id(R.id.etgestationage).getEditable()) {
					if (aq.id(R.id.etgestationage).getText().toString().trim().length() > 0
							|| (aq.id(R.id.etgestationagedays).getText().length()) > 0) {
						chkgest.setEnabled(false);
						chkgest.setChecked(false);
					} else
						chkgest.setEnabled(true);
				} else if (s == aq.id(R.id.etgestationagedays).getEditable()) {
					if (aq.id(R.id.etgestationagedays).getText().toString().trim().length() > 0
							|| (aq.id(R.id.etgestationage).getText().length()) > 0) {
						chkgest.setEnabled(false);
						chkgest.setChecked(false);
					} else
						chkgest.setEnabled(true);

				} else if (s == aq.id(R.id.etgravida).getEditable()) {
					if (aq.id(R.id.etpara).getText().length() > 0) {
						para = Integer.parseInt(aq.id(R.id.etpara).getText().toString());
					}
					int gravida = Integer.parseInt(aq.id(R.id.etgravida).getText().toString());
					if (aq.id(R.id.etgravida).getText().toString().trim().length() > 0) {
						if (para >= gravida) {
							Toast.makeText(getApplicationContext(), getResources().getText(R.string.para_validation),
									Toast.LENGTH_LONG).show();
							aq.id(R.id.etpara).text("");
						}

					}
				}

				else if (s == aq.id(R.id.etchildwt1).getEditable()) {
					if (aq.id(R.id.etchildwt1).getText().toString().trim().length() > 0) {
						int babywt1 = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());

						if (babywt1 > 0 && (babywt1 < 2500 || babywt1 > 4000)) {
							imgwt1_warning.setVisibility(View.VISIBLE);
						} else {
							imgwt1_warning.setVisibility(View.INVISIBLE);
						}
					} else
						imgwt1_warning.setVisibility(View.INVISIBLE);

				} else if (s == aq.id(R.id.etchildwt2).getEditable()) {
					if (aq.id(R.id.etchildwt2).getText().toString().trim().length() > 0) {
						int babywt2 = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());

						if (babywt2 > 0 && (babywt2 < 2500 || babywt2 > 4000)) {
							imgwt2_warning.setVisibility(View.VISIBLE);
						} else {
							imgwt2_warning.setVisibility(View.INVISIBLE);
						}
					} else
						imgwt2_warning.setVisibility(View.INVISIBLE);
				}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};

	public void clearAllFields() {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		try {

			aq.id(R.id.etaddress).text("");
			aq.id(R.id.etattendant).text("");
			aq.id(R.id.etphone).text("");
			aq.id(R.id.ethospno).text("");
			aq.id(R.id.etdocname).text("");
			aq.id(R.id.etnursename).text("");

			// updated by Arpitha
			aq.id(R.id.etlmp).text("");
			aq.id(R.id.etedd).text("");

			strlmpdate = "";// 17July2017 Arpitha - bug fixing - v2.6.1
			stredddate = "";// 17July2017 Arpitha - bug fixing - v2.6.1

			aq.id(R.id.etweight).text("");
			aq.id(R.id.etthayicardno).text("");

			aq.id(R.id.etspecialinstructions).text("");

			// 18Feb2017 Arpitha
			aq.id(R.id.etextracoments).text("");

			rd_memb_pre.setChecked(false);
			rd_memb_abs.setChecked(true);

			if (woman.getregtype() == 2) {
				aq.id(R.id.spnHeightDecimal).setSelection(0);
				aq.id(R.id.spnHeightNumeric).setSelection(0);
				rd_htdontknow.setChecked(false);
				rd_feet.setChecked(false);
				rd_cm.setChecked(false);
				rgheight.clearCheck();// 25April2017 Arpitha
				aq.id(R.id.tr_heightcm).gone();
				aq.id(R.id.tr_height).gone();
				aq.id(R.id.etheight).text("");
			}

			if (isDelinfoadd || isupdatedel)// 12April2017
			// Arpitha
			{
				aq.id(R.id.spndeltype).setSelection(0);
				mspndeltypereason.setSelected(false);
				aq.id(R.id.etdeltypeotherreasons).text("");
				aq.id(R.id.spnnoofchild).setSelection(0);
				aq.id(R.id.spndelresult).setSelection(0);
				aq.id(R.id.spndelresult2).setSelection(0);

				aq.id(R.id.etdeltime).text(Partograph_CommonClass.getCurrentTime());
				aq.id(R.id.etdeltime2).text(Partograph_CommonClass.getCurrentTime());
				aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(
						Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
				rdmale1.setChecked(true);
				rdmale2.setChecked(true);
				rdfemale1.setChecked(false);
				rdfemale2.setChecked(false);
				mspntears.setSelected(false);
				mspntears.setSelection(9);
				rdepino.setChecked(true);
				rdepiyes.setChecked(false);
				rdvicryl.setChecked(false);
				rdcatgut.setChecked(false);
				rdvicryl.setVisibility(View.GONE);
				episiotomy = 0;
				suturematerial = 0;
				rdcatgut.setVisibility(View.GONE);
				rdmotheralive.setChecked(true);
				rdmotherdead.setChecked(false);
				aq.id(R.id.etmothersdeathreason).text("");
				aq.id(R.id.etmothersdeathreason).gone();

				normaltype = 0;
				presentation = 0;

				rdmale2.setVisibility(View.GONE);
				rdfemale2.setVisibility(View.GONE);

				tr_secondchilddetails.setVisibility(View.GONE);
				tr_result2.setVisibility(View.GONE);
				tr_childdet2.setVisibility(View.GONE);
				tr_childsex2.setVisibility(View.GONE);
				tr_deltime2.setVisibility(View.GONE);

				aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.child_details));

				aq.id(R.id.etdeltime2).gone();

				aq.id(R.id.etchildwt2).gone();
				imgwt2_warning.setVisibility(View.GONE);
				aq.id(R.id.spndelresult2).gone();
				aq.id(R.id.imgdelresult).gone();
				aq.id(R.id.imgsex2).gone();
				aq.id(R.id.imgdeltime2).gone();

				aq.id(R.id.etdelcomments).text("");
				trsuturematerial.setVisibility(View.GONE);
				aq.id(R.id.txtmothersdeathreason).gone();

				rdbreech.setChecked(false);
				rdnormaltype.clearCheck();// 28April2017 Arpitha
				rdvertext.setChecked(true);
				mspntears.setSelection(20);// 12April2017 Arpitha

			}

			rgbabypresentation.clearCheck();// 17May2017 Arpitha - v2.6
			presentation = 0;// 22May2017 Arpitha
			// memb_pre_abs = 0;// 22May2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Calculate EDD and display in EDD Edittext
	private void populateEDD(String xLMP) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date LmpDate;
		LmpDate = sdf.parse(xLMP);
		Calendar cal = Calendar.getInstance();
		cal.setTime(LmpDate);
		cal.add(Calendar.DAY_OF_MONTH, 280);

		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		stredddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);
		aq.id(R.id.etedd)
				.text(Partograph_CommonClass.getConvertedDateFormat(stredddate, Partograph_CommonClass.defdateformat));

	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.main));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	void setrisk1() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		risk_observed = new ArrayList<String>();
		if (woman.getRisk_category() == 0) {
			if (isage || isheight || isweight) {
				risk = 1;
				rd_highrisk.setChecked(true);

				String pos = woman.getRiskoptions();// 13April2017 Arpitha
				String[] otheroption = { pos, getResources().getString(R.string.otherrisk) };// 13April2017
																								// Arpitha
				// mspnriskoptions.setSelection(otheroption);
				for (int i = 0; i < otheroption.length; i++)// 13April2017
															// Arpitha
				{
					aq.id(R.id.etriskoptions).text(otheroption[i]);// 13April2017
																	// Arpitha
				}
			}
			// updated on 18August2016 by Arpitha
			else {
				risk = 0;
				rd_highrisk.setChecked(false);
				rd_lowrisk.setChecked(true);
				aq.id(R.id.etriskoptions).text("");// 13April2017 Arpitha
			}

		}
		// updated on 18Aug2016 by Arpitha
		else {
			if (woman.getRiskoptions().length() > 0 && woman.getRiskoptions().contains("12")) {// 13April2017
																								// Arpitha
				// String riskOptions =
				// aq.id(R.id.etriskoptions).getText().toString();

				// 22Aug check for null risk options
				// 25dec2015
				String[] riskoptions = woman.getRiskoptions().split(",");
				String[] riskoptArr = null;

				// getResources().getStringArray(R.array.riskoptions);
				// 06jul2016 - use riskoptionsvalues instead of riskoptions
				riskoptArr = getResources().getStringArray(R.array.riskoptionsvalues);

				String riskOptions = "";
				if (riskoptArr != null) {
					if (riskoptions != null && riskoptions.length > 0) {
						for (String str : riskoptions) {
							if (str != null && str.length() > 0)
								riskOptions = riskOptions + riskoptArr[Integer.parseInt(str)] + "\n";
						}
					}
				}

				if (!(riskOptions.contains(getResources().getString(R.string.otherrisk)))) {

					risk = 1;
					rd_highrisk.setChecked(true);
					rd_lowrisk.setChecked(false);

				}
				// 17Feb2017 Arpitha
				else {

					if (!(aq.id(R.id.etcomments).getText().toString().contains("Age")
							|| aq.id(R.id.etcomments).getText().toString().contains("Height")
							|| aq.id(R.id.etcomments).getText().toString().contains("Weight"))) {
						risk = 1;
						rd_highrisk.setChecked(true);
						rd_lowrisk.setChecked(false);

					} else {
						risk = 0;
						rd_highrisk.setChecked(false);
						rd_lowrisk.setChecked(true);
						// aq.id(R.id.etriskoptions).text("")
						// wpojo.setRiskoptions(selIds);
						aq.id(R.id.etcomments).text("");
					}

				} // 17Feb2017 Arpitha
			} else {
				risk = 0;
				rd_highrisk.setChecked(false);
				rd_lowrisk.setChecked(true);
				aq.id(R.id.etriskoptions).text("");
				aq.id(R.id.etcomments).text("");
			}
		}

		// if()
		if (isage) {
			risk_observed.add(getResources().getString(R.string.age));
		} else {
			risk_observed.remove(getResources().getString(R.string.age));
		}
		if (isheight) {
			risk_observed.add(getResources().getString(R.string.height_riskobserved));
		} else {
			risk_observed.remove(getResources().getString(R.string.height_riskobserved));
		}

		if (isweight) {
			risk_observed.add(getResources().getString(R.string.weight_riskobserved));
		} else {
			risk_observed.remove(getResources().getString(R.string.weight_riskobserved));
		}

		if (risk_observed != null && risk_observed.size() > 0) {
			robserved = "";
			for (int i = 0; i < risk_observed.size(); i++) {
				robserved = robserved + risk_observed.get(i) + " ";
			}

		} else {
			robserved = "";
		}
		// updated on 18August2016 by Arpitha
		String riskobserved = woman.getComments();
		String newriskobsereved = riskobserved;
		if (riskobserved.contains(getResources().getString(R.string.weight_riskobserved))) {
			newriskobsereved = riskobserved.replace(getResources().getString(R.string.weight_riskobserved), "");
		}
		if (riskobserved.contains(getResources().getString(R.string.height_riskobserved))) {
			newriskobsereved = riskobserved.replace(getResources().getString(R.string.height_riskobserved), "");
		}
		if (riskobserved.contains(getResources().getString(R.string.age))) {
			newriskobsereved = riskobserved.replace(getResources().getString(R.string.age), "");
		}
		if (riskobserved.length() > 0) {
			robserved = robserved + newriskobsereved;
		}
		aq.id(R.id.etcomments).text(robserved);
	}

	void setrisk() {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		risk_observed = new ArrayList<String>();
		if (woman.getRisk_category() == 0) {
			if (isage || isheight || isweight) {
				risk = 1;
				rd_highrisk.setChecked(true);

				aq.id(R.id.etriskoptions).visible();
				aq.id(R.id.tr_riskoptions).visible();// 13April2017 Arpitha
				String pos = woman.getRiskoptions();// 13April2017 Arpitha
				String[] otheroption = { pos, getResources().getString(R.string.otherrisk) };// 13April2017
																								// Arpitha
				// mspnriskoptions.setSelection(otheroption);
				for (int i = 0; i < otheroption.length; i++)// 13April2017
															// Arpitha
				{
					aq.id(R.id.etriskoptions).text(otheroption[i]);// 13April2017
																	// Arpitha
				}

				selectedriskOptions = "12";// 19April2017 Arpitha
			}
			// updated on 18August2016 by Arpitha
			else {
				risk = 0;
				rd_highrisk.setChecked(false);
				rd_lowrisk.setChecked(true);
				aq.id(R.id.etriskoptions).text("");// 13April2017 Arpitha
			}

		}
		// updated on 18Aug2016 by Arpitha
		else {

			if (isage || isheight || isweight) {
				risk = 1;
				rd_highrisk.setChecked(true);
				rd_lowrisk.setChecked(false);
				String pos = woman.getRiskoptions();// 13April2017 Arpitha
				String[] otheroption = { pos, getResources().getString(R.string.otherrisk) };// 13April2017
																								// Arpitha
				for (int i = 0; i < otheroption.length; i++)// 13April2017
															// Arpitha
				{
					aq.id(R.id.etriskoptions).text(otheroption[i]);// 13April2017
																	// Arpitha
				}
				selectedriskOptions = "12";// 19April2017 Arpitha

			} else {

				if (woman.getRiskoptions().contains("12")) {
					if (!((woman.getComments().contains("Age")) || woman.getComments().contains("Height")
							|| woman.getComments().contains("Weight"))) {

						risk = 1;
						rd_highrisk.setChecked(true);
						rd_lowrisk.setChecked(false);
						String pos = woman.getRiskoptions();// 13April2017
															// Arpitha
						String[] otheroption = { pos, getResources().getString(R.string.otherrisk) };// 13April2017
																										// Arpitha
						for (int i = 0; i < otheroption.length; i++)// 13April2017
																	// Arpitha
						{
							aq.id(R.id.etriskoptions).text(otheroption[i]);// 13April2017
																			// Arpitha
						}
						selectedriskOptions = "12";// 19April2017 Arpitha

					} else {

						risk = 0;
						rd_highrisk.setChecked(false);
						rd_lowrisk.setChecked(true);
						aq.id(R.id.etriskoptions).text("");

					}

				} else {
					/*
					 * risk = 0; rd_highrisk.setChecked(false);
					 * rd_lowrisk.setChecked(true);
					 * aq.id(R.id.etriskoptions).text("");
					 */
				}

			}

		}

		// if()
		if (isage) {
			risk_observed.add(getResources().getString(R.string.age));
		} else {
			risk_observed.remove(getResources().getString(R.string.age));
		}
		if (isheight) {
			risk_observed.add(getResources().getString(R.string.height_riskobserved));
		} else {
			risk_observed.remove(getResources().getString(R.string.height_riskobserved));
		}

		if (isweight) {
			risk_observed.add(getResources().getString(R.string.weight_riskobserved));
		} else {
			risk_observed.remove(getResources().getString(R.string.weight_riskobserved));
		}

		if (risk_observed != null && risk_observed.size() > 0) {
			robserved = "";
			for (int i = 0; i < risk_observed.size(); i++) {
				robserved = robserved + risk_observed.get(i) + " ";
			}

		} else {
			robserved = "";
		}
		// updated on 18August2016 by Arpitha
		String riskobserved = woman.getComments();
		// String newriskobsereved = riskobserved;
		if (riskobserved.contains(getResources().getString(R.string.weight_riskobserved))) {
			riskobserved = riskobserved.replace(getResources().getString(R.string.weight_riskobserved), "");
		}
		if (riskobserved.contains(getResources().getString(R.string.height_riskobserved))) {
			riskobserved = riskobserved.replace(getResources().getString(R.string.height_riskobserved), "");
		}
		if (riskobserved.contains(getResources().getString(R.string.age))) {
			riskobserved = riskobserved.replace(getResources().getString(R.string.age), "");
		}
		if (riskobserved.length() > 0) {
			robserved = robserved + riskobserved;
		}
		aq.id(R.id.etcomments).text(robserved);

	}

	// 10May2017 Arpitha - v2.6
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha
		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		// 03Oct2017 Arpitha
		menu.findItem(R.id.del).setVisible(true);
		menu.findItem(R.id.ref).setVisible(true);
		menu.findItem(R.id.pdf).setVisible(true);
		menu.findItem(R.id.stages).setVisible(true);
		menu.findItem(R.id.apgar).setVisible(true);
		menu.findItem(R.id.post).setVisible(true);
		menu.findItem(R.id.parto).setVisible(true);// 03Oct2017 Arpitha
		return super.onPrepareOptionsMenu(menu);
	}

	// 12Oct2017 Arpitha
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub

		if (v == aq.id(R.id.etgestationage).getEditText()) {
			if ((aq.id(R.id.etgestationage).getText().toString().length()) > 0) {
				if (Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) < 27
						|| Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) > 44) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.gestationvalid),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etgestationage).text("");
					// aq.id(R.id.etgestationage).getEditText().requestFocus();

					// displayAlertDialog(getResources().getString(R.string.gestationvalid),
					// "");
					// aq.id(R.id.etgestationage).getEditText().requestFocus();
					// return false;
				}
			}
		}

	}

}