package com.bluecrimson.apgar;

public class Apgar_Pojo {

	String appearance;
	String pulse;
	String grimace;
	String activity;
	String respiration;
	String dateOfEntry;
	String timeOfEntry;
	
	String appearance2;
	String pulse2;
	String grimace2;
	String activity2;
	String respiration2;
	String dateOfEntry2;
	String timeOfEntry2;
	
	// updated on feb28 by Arpitha
	String apgarcomments;
	String apgarcomments2;
	
	public String getApgarcomments2() {
		return apgarcomments2;
	}
	public void setApgarcomments2(String apgarcomments2) {
		this.apgarcomments2 = apgarcomments2;
	}
	public String getApgarcomments() {
		return apgarcomments;
	}
	public void setApgarcomments(String apgarcomments) {
		this.apgarcomments = apgarcomments;
	}
	public String getAppearance2() {
		return appearance2;
	}
	public void setAppearance2(String appearance2) {
		this.appearance2 = appearance2;
	}
	public String getPulse2() {
		return pulse2;
	}
	public void setPulse2(String pulse2) {
		this.pulse2 = pulse2;
	}
	public String getGrimace2() {
		return grimace2;
	}
	public void setGrimace2(String grimace2) {
		this.grimace2 = grimace2;
	}
	public String getActivity2() {
		return activity2;
	}
	public void setActivity2(String activity2) {
		this.activity2 = activity2;
	}
	public String getRespiration2() {
		return respiration2;
	}
	public void setRespiration2(String respiration2) {
		this.respiration2 = respiration2;
	}
	public String getDateOfEntry2() {
		return dateOfEntry2;
	}
	public void setDateOfEntry2(String dateOfEntry2) {
		this.dateOfEntry2 = dateOfEntry2;
	}
	public String getTimeOfEntry2() {
		return timeOfEntry2;
	}
	public void setTimeOfEntry2(String timeOfEntry2) {
		this.timeOfEntry2 = timeOfEntry2;
	}
	public String getDateOfEntry() {
		return dateOfEntry;
	}
	public void setDateOfEntry(String dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}
	public String getTimeOfEntry() {
		return timeOfEntry;
	}
	public void setTimeOfEntry(String timeOfEntry) {
		this.timeOfEntry = timeOfEntry;
	}
	public String getAppearance() {
		return appearance;
	}
	public void setAppearance(String appearance) {
		this.appearance = appearance;
	}
	public String getPulse() {
		return pulse;
	}
	public void setPulse(String pulse) {
		this.pulse = pulse;
	}
	public String getGrimace() {
		return grimace;
	}
	public void setGrimace(String grimace) {
		this.grimace = grimace;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getRespiration() {
		return respiration;
	}
	public void setRespiration(String respiration) {
		this.respiration = respiration;
	}
}
