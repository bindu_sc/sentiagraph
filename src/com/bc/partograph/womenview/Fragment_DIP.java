package com.bc.partograph.womenview;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.FragmentState;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.comprehensiveview.View_Partograph;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.regwomenlist.RecentCustomListAdapterDIP;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bluecrimson.additionalDetails.AdditionalDetails_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.partograph.ActionItem;
import com.bluecrimson.partograph.QuickAction;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

public class Fragment_DIP extends Fragment implements FragmentState, OnClickListener, SearchView.OnQueryTextListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	UserPojo user;
	ArrayList<Women_Profile_Pojo> rowItems;
	QuickAction mQuickAction;
	Thread myThread;
	public static final int ID_PROFILE = 1;
	public static final int ID_GRAPH = 2;
	public static final int ID_DELSTATUS = 3;
	public static final int ID_APGAR = 4;
	public static final int ID_COMPREHENSIVEVIEW = 5;
	public static final int ID_REFERRAL = 6;
	public static final int ID_STAGEOFLABOR = 7;
	public static final int ID_PRINTPARTO = 8;
	RecentCustomListAdapterDIP adapterDIP;
	public static boolean isreferral_updated = false;
	public static boolean isdelstatus;
	// 26Sep2016 Arpitha
	public static boolean istears = false;
	// 27oct2016 - bindu
	private int totalCount = 0;
	Women_Profile_Pojo woman;// 03Nov2016 Arpitha
	public static final int ID_ADDITIONALDETAILS = 9;// 06Dec2016 Arpitha
	int displayListCount;// 01Jun2017 Arpitha
	Cursor cursor;
	SearchView searchDIP;// 05Jun2017 Arpitha
	public static final int ID_DISCHARGEDETAILS = 10;// 08Aug2017 Arpitha
	public static final int ID_LATENTPHASE = 11;// 10Aug2017 Arpitha
	public static final int ID_PDF = 12;// 10Aug2017 Arpitha

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.fragment_dip, container, false);
		// 05Jun2017 Arpitha
		searchDIP = (SearchView) rootView.findViewById(R.id.search_view_dip);
		searchDIP.setOnQueryTextListener(this);// 05Jun2017 Arpitha
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			user = Partograph_CommonClass.user;

			initializeScreen(aq.id(R.id.rltoday).getView());

			displayListCount = 10;

			initialView();

			aq.id(R.id.listwomentoday).getListView().setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
					try {

						woman = (Women_Profile_Pojo) adapter.getItemAtPosition(pos);

						mQuickAction.show(v);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			mQuickAction = new QuickAction(getActivity());
			displayOptions();

			// 26dec2015
			myThread = null;
			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

			// 01Jun2017 Arpitha - load more data on scroll
			aq.id(R.id.listwomentoday).getListView().setOnScrollListener(new OnScrollListener() {

				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					// TODO Auto-generated method stub
					try {
						if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0
								&& totalCount > displayListCount) {
							displayListCount = displayListCount + 10;

							loadWomendata(displayListCount);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});// 01Jun2017 Arpitha - load more data on scroll

		} catch (

		Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}

	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}
		return result;
	}

	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// 01Jun2017 Arpitha
		String mainSql = "Select * from " + Partograph_DB.TBL_REGISTEREDWOMEN + " where " + Partograph_DB.DEL_STATUS
				+ " is null  and regtype!=2 and user_Id = '" + user.getUserId()
				+ "'  and women_id NOT IN(Select women_id from tbl_referral) and women_id IN(Select women_id from tbl_propertyValues where object_id<=9) and women_id NOT IN (Select discWomanId from tbldischargedetails) order by dateinserted desc";

		totalCount = dbh.getDisplayWomanListCount(mainSql);// 01Jun2017 Arpitha

		loadWomendata(displayListCount);

	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(5000); // Pause of 5 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
				}
			}
		}
	}

	public void doWork() throws Exception {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						if (adapterDIP != null) {
							adapterDIP.notifyDataSetChanged();
						}

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	// Load women Data//01Jun2017 Arpitha - add argument
	private void loadWomendata(int displayListCount) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		rowItems = new ArrayList<Women_Profile_Pojo>();
		Women_Profile_Pojo wdata;

		cursor = dbh.getdipwomen(user.getUserId(), displayListCount);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {

				int pos = cursor.getPosition();
				if (pos <= displayListCount) {
					wdata = new Women_Profile_Pojo();
					wdata.setWomenId(cursor.getString(1));

					if (cursor.getType(2) > 0) {
						String b = (cursor.getString(2).length() > 1) ? cursor.getString(2) : null;
						byte[] decoded = (b != null) ? Base64.decode(b, Base64.DEFAULT) : null;
						wdata.setWomen_Image(decoded);
					}

					wdata.setWomen_name(cursor.getString(3));
					wdata.setDate_of_admission(cursor.getString(4));
					wdata.setTime_of_admission(cursor.getString(5));
					wdata.setAge(cursor.getInt(6));
					wdata.setAddress(cursor.getString(7));
					wdata.setPhone_No(cursor.getString(8));
					wdata.setDel_type(cursor.getInt(9));
					wdata.setDoc_name(cursor.getString(10));
					wdata.setNurse_name(cursor.getString(11));
					wdata.setW_attendant(cursor.getString(12));
					wdata.setGravida(cursor.getInt(13));
					wdata.setPara(cursor.getInt(14));
					wdata.setHosp_no(cursor.getString(15));
					wdata.setFacility(cursor.getString(16));
					wdata.setWomenId(cursor.getString(1));
					wdata.setSpecial_inst(cursor.getString(17));
					wdata.setUserId(cursor.getString(0));
					wdata.setComments(cursor.getString(19));
					wdata.setRisk_category(cursor.getInt(20));
					wdata.setDel_Comments(cursor.getString(21));
					wdata.setDel_Time(cursor.getString(22));
					wdata.setDel_Date(cursor.getString(23));
					wdata.setDel_result1(cursor.getInt(24));
					wdata.setNo_of_child(cursor.getInt(25));
					wdata.setBabywt1(cursor.getInt(26));
					wdata.setBabywt2(cursor.getInt(27));
					wdata.setBabysex1(cursor.getInt(28));
					wdata.setBabysex2(cursor.getInt(29));
					wdata.setGestationage(cursor.getInt(30));
					wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
					wdata.setDel_result2(cursor.getInt(31));
					wdata.setAdmitted_with(cursor.getString(34));
					wdata.setMemb_pres_abs(cursor.getInt(35));
					wdata.setMothers_death_reason(cursor.getString(36));
					wdata.setState(cursor.getString(38));
					wdata.setDistrict(cursor.getString(39));
					wdata.setTaluk(cursor.getString(40));
					wdata.setFacility_name(cursor.getString(41));
					wdata.setGest_age_days(cursor.getInt(45));

					// updated on 23Nov2015
					wdata.setDel_time2(cursor.getString(33));

					// updated on 30Nov2015
					wdata.setThayicardnumber(cursor.getString(46));
					wdata.setDelTypeReason(cursor.getString(47));
					wdata.setDeltype_otherreasons(cursor.getString(48));

					// 27dec2015
					wdata.setLmp(cursor.getString(49));
					wdata.setRiskoptions(cursor.getString(50));

					// 04jan2016
					wdata.setTears(cursor.getString(51));
					wdata.setEpisiotomy(cursor.getInt(52));
					wdata.setSuturematerial(cursor.getInt(53));

					// 19jan2016
					wdata.setBloodgroup(cursor.getInt(54));
					// updated on 23/2/16 by Arpitha
					wdata.setEdd(cursor.getString(55));
					wdata.setHeight(cursor.getString(56));
					wdata.setW_weight(cursor.getString(57));
					wdata.setOther(cursor.getString(58));

					// updated on 29May by Arpitha
					wdata.setHeight_unit(cursor.getInt(59));

					wdata.setExtracomments(cursor.getString(60));// 13Oct2016
																	// Arpitha
					wdata.setregtype(cursor.getInt(61));// 16Oct2016 Arpitha

					wdata.setRegtypereason(cursor.getString(62));// 18Oct2016
																	// Arpitha
					wdata.setDate_of_reg_entry(cursor.getString(63));// 10Nov2016
																		// Arpitha

					wdata.setNormaltype(cursor.getInt(64));// 20Nov2016 Arpitha

					wdata.setRegtypeOtherReason(cursor.getString(65));// 21Nov2016
																		// Arpitha

					boolean isDanger = dbh.chkIsDanger(cursor.getString(1), user.getUserId());

					wdata.setDanger(isDanger);

					rowItems.add(wdata);
				}
			} while (cursor.moveToNext());

			adapterDIP = new RecentCustomListAdapterDIP(getActivity(), R.layout.activity_womenlist, rowItems, dbh);
			aq.id(R.id.listwomentoday).adapter(adapterDIP);

		}

		if (rowItems.size() <= 0) {
			aq.id(R.id.listwomentoday).gone();
			aq.id(R.id.txtnowoman).visible();
			searchDIP.setVisibility(View.GONE);// 05Jun2017 Arpitha
		} else {
			aq.id(R.id.listwomentoday).visible();
			aq.id(R.id.txtnowoman).gone();
			searchDIP.setVisibility(View.VISIBLE);// 05Jun2017 Arpitha
		}

	}

	// Display options on click of list item
	private void displayOptions() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {

			ActionItem viewprofile_Item = new ActionItem(ID_PROFILE, getResources().getString(R.string.view_profile),
					getResources().getDrawable(R.drawable.ic_viewprofile_menu));
			ActionItem graph_Item = new ActionItem(ID_GRAPH, getResources().getString(R.string.partograph),
					getResources().getDrawable(R.drawable.ic_add_parto_menu));

			ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
					getResources().getString(R.string.delivery_status),
					getResources().getDrawable(R.drawable.ic_del_sttaus_menu));
			ActionItem apgarscore_Item = new ActionItem(ID_APGAR, getResources().getString(R.string.apgar_score),
					getResources().getDrawable(R.drawable.ic_apgar_menu));

			// updated on 16Nov2015
			ActionItem referredto_Item = new ActionItem(ID_REFERRAL, getResources().getString(R.string.referrral_info),
					getResources().getDrawable(R.drawable.ic_referral_menu));

			// updated on 26Nov2015
			ActionItem stagesoflabor_Item = new ActionItem(ID_STAGEOFLABOR,
					getResources().getString(R.string.stagesoflabor),
					getResources().getDrawable(R.drawable.thid_fourth_menu));

			// updated on 23dec2015
			ActionItem print_partoitem = new ActionItem(ID_PRINTPARTO, getResources().getString(R.string.printparto),
					getResources().getDrawable(R.drawable.ic_view_graph_menu));
			// updated on 17Oct2016 Arpitha

			ActionItem additonal_details = new ActionItem(ID_ADDITIONALDETAILS,
					getResources().getString(R.string.additional_details),
					getResources().getDrawable(R.drawable.ic_additional));// 06Dec2016
			// Arpitha

			// 21July2017 Arpitha
			ActionItem dischargeDetails = new ActionItem(ID_DISCHARGEDETAILS,
					getResources().getString(R.string.discharge_details),
					getResources().getDrawable(R.drawable.discharge));

			// 10Agu2017 Arpitha
			ActionItem latentphase = new ActionItem(ID_LATENTPHASE, getResources().getString(R.string.latent_phase),
					getResources().getDrawable(R.drawable.latentphase));

			// 31Agu2017 Arpitha
			ActionItem pdf = new ActionItem(ID_PDF, getResources().getString(R.string.export_pdf),
					getResources().getDrawable(R.drawable.pdf5));

			mQuickAction.addActionItem(latentphase);// 10Aug2017 Arpitha
			// updated on 23dec2015
			mQuickAction.addActionItem(print_partoitem);
			mQuickAction.addActionItem(graph_Item);
			mQuickAction.addActionItem(deliverystatus_Item);
			mQuickAction.addActionItem(apgarscore_Item);

			// updated on 26Nov2015
			mQuickAction.addActionItem(stagesoflabor_Item);

			// mQuickAction.addActionItem(postpartumcare);// 18Aug2017 Arpitha

			// changed by Arpitha 26Feb2016
			mQuickAction.addActionItem(referredto_Item);

			mQuickAction.addActionItem(additonal_details);// 06Dec2016 Arpitha

			mQuickAction.addActionItem(viewprofile_Item);

			mQuickAction.addActionItem(dischargeDetails);// 21July2017 Arpitha

			mQuickAction.addActionItem(pdf);// 31Aug2017 Arpitha

			// setup the action item click listener
			mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
				@Override
				public void onItemClick(QuickAction quickAction, int pos, int actionId) {

					try {

						if (actionId == ID_PROFILE) { // Message item
														// selected

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha
								Intent view = new Intent(getActivity(), ViewProfile_Activity.class);
								view.putExtra("woman", woman);
								startActivity(view);
							}

						}
						if (actionId == ID_GRAPH) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha

								Intent graph = new Intent(getActivity(), SlidingActivity.class);
								graph.putExtra("woman", woman);
								startActivity(graph);
							}

						}
						if (actionId == ID_DELSTATUS) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha

								if (woman != null) {

									Intent graph = new Intent(getActivity(), DeliveryInfo_Activity.class);
									graph.putExtra("woman", woman);
									startActivity(graph);
								}

							}
						}
						if (actionId == ID_APGAR) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha

								if (woman.getDel_type() != 0 &&

										(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent viewapgar = new Intent(getActivity(), Activity_Apgar.class);
									viewapgar.putExtra("woman", woman);
									startActivity(viewapgar);
								} else {
									Toast.makeText(getActivity(),
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated on 16Nov2015
						if (actionId == ID_REFERRAL) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha
								Intent ref = new Intent(getActivity(), ReferralInfo_Activity.class);
								ref.putExtra("woman", woman);
								startActivity(ref);
							}
						}

						// updated on 26Nov2015
						if (actionId == ID_STAGEOFLABOR) {

							if (Partograph_CommonClass.autodatetime(getActivity())) {// 27Sep2016
																						// Arpitha
								if (woman.getDel_type() != 0 &&

										(woman.getDel_result1() != 0) || (woman.getDel_result2() != 0)) {
									Intent stagesoflabor = new Intent(getActivity(), StageofLabor.class);
									stagesoflabor.putExtra("woman", woman);
									startActivity(stagesoflabor);

								} else {
									Toast.makeText(getActivity(),
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

						}

						// updated bindu - 26july2016
						// updated on 23Dec2015
						if (actionId == ID_PRINTPARTO) {
							try {

								Intent view_partograph = new Intent(getActivity(), View_Partograph.class);
								view_partograph.putExtra("woman", woman);

								startActivity(view_partograph);

							} catch (Exception e) {
								AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName());
								e.printStackTrace();

							}
						}

						// 06Dec2016 Arpitha
						if (actionId == ID_ADDITIONALDETAILS) {

							Intent addtionalDetails = new Intent(getActivity(), AdditionalDetails_Activity.class);
							addtionalDetails.putExtra("woman", woman);

							startActivity(addtionalDetails);
						} // 06Dec2016 Arpitha

						// 21July2017 Arpitha
						if (actionId == ID_DISCHARGEDETAILS) {
							Intent dischargeDetails = new Intent(getActivity(), DiscargeDetails_Activity.class);
							dischargeDetails.putExtra("woman", woman);
							startActivity(dischargeDetails);

						} // 21July2017 Arpitha

						// 10Aug2017 Arpitha
						if (actionId == ID_LATENTPHASE) {
							Intent latent = new Intent(getActivity(), LatentPhase_Activity.class);
							latent.putExtra("woman", woman);
							startActivity(latent);
						}

						// 31Aug2017 Arpitha
						if (actionId == ID_PDF) {
							Partograph_CommonClass.exportPDFs(getActivity(), woman);

						}

					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName());
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName());
						e.printStackTrace();
					}

				}
			});

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	@Override
	public void fragmentVisible() {
		// TODO Auto-generated method stub
		try {
			if (adapterDIP != null) {
				adapterDIP.notifyDataSetChanged();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	// 05Jun2017 Arpitha
	@Override
	public boolean onQueryTextChange(String newText) {
		// TODO Auto-generated method stub
		if (adapterDIP != null) {
			adapterDIP.getFilter().filter(newText);
		}
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		// TODO Auto-generated method stub
		return true;
	}// 05Jun2017 Arpitha

}
