package com.bc.partograph.womenview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class TabsPager_Adapter extends FragmentStatePagerAdapter {

	public TabsPager_Adapter(FragmentManager fm) {
		super(fm);
	}

	
	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Today fragment activity
			return new Fragment_RegistredWomenList();
		/*case 1:
			// Weekly fragment activity
			return new Fragment_REGg24hrs();*/
		case 1:
			// Today fragment activity
			return new Fragment_DIP();
		case 2:
			//Monthly fragment activity
			return new Fragment_Referred();
			
		case 3:
			//Yearly fragment activity
			return new Fragment_Delivered();
			//19Oct2016 Arpitha
		case 4:
			//Retrospective Case 
			return new Fragment_Retrospective();
		
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 5;
	}

}
