package com.bc.partograph.womenview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.FragmentState;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS_ChangePassword;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.SendSMS;
import com.bc.partograph.comprehensiveview.View_Partograph;
import com.bc.partograph.deliveryinfo.DeliveryInfo_Activity;
import com.bc.partograph.latentphase.LatentPhase_Activity;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.registration.RegistrationActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.additionalDetails.AdditionalDetails_Activity;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.apgar.Activity_View_Apgar;
import com.bluecrimson.dischargedetails.DiscargeDetails_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.postpartumcare.PostPartumCare_Activity;
import com.bluecrimson.postpartumcare.PostpartumCare_View_Activity;
import com.bluecrimson.usermanual.UseManual;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_WomenView extends FragmentActivity implements ActionBar.TabListener, FragmentState {

	private ViewPager viewPager;
	private TabsPager_Adapter mAdapter;
	public static ActionBar actionBar;
	private final String TAB_KEY_INDEX = "tab_key";
	static Partograph_DB dbh;
	UserPojo user;
	public String[] tabs;
	public static MenuItem menuItem;
	FragmentState fragment = null;
	String user_id;
	public static int regtype;// 13oct2016 Arpitha
	public static String regtypereason;
	public static boolean isretrospective = false;// 24Oct2016 Arpitha
	public static int retroreason;
	// 23Dec2016 Arpitha
	Button btnchangepass;
	EditText etoldpass, etretypepass, etnewpass;
	// 25Dec2016 Arpitha
	TextView txtdialogtitle;// 27Dec2016 Arpitha
	UserPojo newUser;
	String serverResult;// 03April2017 Arpitha
	boolean isErrorFound = false;// 13April2017 Arpitha
	boolean inetOn = false;// 03May2017 Arpitha
	CountDownTimer commentsTimer = null;// 03May2017 Arpitha
	public static final int START_AFTER_SECONDS = 10000;// 03May2017 Arpitha
	public static ArrayList<String> referredWomen;

	static EditText etphn;
	 static final int PICK_CONTACT=1;

	@Override
	protected void onCreate(Bundle saved) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			super.onCreate(saved);
			setContentView(R.layout.activity_view);
			dbh = Partograph_DB.getInstance(getApplicationContext());
			user = Partograph_CommonClass.user;
			user_id = user.getUserId();
			// 05Nov2016 Arpitha
			Locale locale = null;
			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			referredWomen = dbh.getreferredwomen(user_id);// 26May2017 Arpitha

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			// changed on 8August2016 by Arpitha
			// include reg, reg>24 and remove dip>24
			// removed reg>24hrs tab on 14Jun2017 Arpitha
			tabs = new String[] { getResources().getString(R.string.reg), getResources().getString(R.string.dip),
					getResources().getString(R.string.ref), getResources().getString(R.string.del),
					getResources().getString(R.string.retr) };
			// Initilization
			viewPager = (ViewPager) findViewById(R.id.pager);
			viewPager.setOffscreenPageLimit(4);
			actionBar = getActionBar();

			mAdapter = new TabsPager_Adapter(getSupportFragmentManager());
			viewPager.setAdapter(mAdapter);
			// updated on 17Nov2015
			Partograph_CommonClass.isRecordExistsForSync = dbh.GetSyncCountRecords();

			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

			// Adding Tabs
			for (String tab_name : tabs) {
				actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));

			}

			// restore to navigation
			if (saved != null) {
				actionBar.setSelectedNavigationItem(saved.getInt(TAB_KEY_INDEX, 0));
			}

			// partograph activity
			if (SlidingActivity.isGraph) {
				actionBar.setSelectedNavigationItem(SlidingActivity.tabpos);
				SlidingActivity.isGraph = false;
			}

			// ViewProfile activity
			if (ViewProfile_Activity.isViewactivity) {
				actionBar.setSelectedNavigationItem(ViewProfile_Activity.viewtabpos);
				ViewProfile_Activity.isViewactivity = false;
			}

			// Apgar activity
			if (Activity_Apgar.isApgar) {
				// changed on 1Aug2016 by Arpitha
				actionBar.setSelectedNavigationItem(3);
				Activity_Apgar.isApgar = false;
			}

			// View Apgar
			if (Activity_View_Apgar.isApgarview) {
				// changed on 1Aug2016 by Arpitha
				actionBar.setSelectedNavigationItem(3);
				Activity_View_Apgar.isApgarview = false;// 02Nov2016 Arpitha

			}

			// StageOFLabor
			if (StageofLabor.isStagesOfLabor) {
				// changed on 1Aug2016 by Arpitha
				actionBar.setSelectedNavigationItem(3);
				StageofLabor.isStagesOfLabor = false;// 02Nov2016 Arpitha

			}

			if (View_Partograph.isViewParto) {
				actionBar.setSelectedNavigationItem(View_Partograph.comptabpos);
				View_Partograph.isViewParto = false;// 17Nov2016 Arpitha
			}

			// 22oct2016 Arpitha
			if (RegistrationActivity.isreg_activity) {
				actionBar.setSelectedNavigationItem(0);
				RegistrationActivity.isreg_activity = false;// 02Nov2016 Arpitha
			}

			if (isretrospective) {
				actionBar.setSelectedNavigationItem(4);
				isretrospective = false;// 02Nov2016 Arpitha
			}

			// 17Jan2017 Arpitha
			if (AdditionalDetails_Activity.additionalDetails) {
				actionBar.setSelectedNavigationItem(AdditionalDetails_Activity.addtabpos);
				AdditionalDetails_Activity.additionalDetails = false;
			}

			// 03Sep2017 Arpitha
			if (Comments_Activity.isComments) {
				actionBar.setSelectedNavigationItem(Comments_Activity.commentscount);
				Comments_Activity.isComments = false;
			}

			// 03Sep2017 Arpitha
			if (DeliveryInfo_Activity.isDel) {
				actionBar.setSelectedNavigationItem(DeliveryInfo_Activity.delscreen);
				DeliveryInfo_Activity.isDel = false;
			}

			// 03Sep2017 Arpitha
			if (LatentPhase_Activity.islatent) {
				actionBar.setSelectedNavigationItem(LatentPhase_Activity.latent);
				LatentPhase_Activity.islatent = false;
			}

			// 03Sep2017 Arpitha
			if (PostPartumCare_Activity.ispostpartum) {
				actionBar.setSelectedNavigationItem(PostPartumCare_Activity.postpartum);
				PostPartumCare_Activity.ispostpartum = false;
			}

			// 03Sep2017 Arpitha
			if (PostpartumCare_View_Activity.ispostview) {
				actionBar.setSelectedNavigationItem(PostpartumCare_View_Activity.postview);
				PostpartumCare_View_Activity.ispostview = false;
			}

			// 03Sep2017 Arpitha
			if (DischargedWomanList_Activity.isdisclist) {
				actionBar.setSelectedNavigationItem(DischargedWomanList_Activity.disclist);
				DischargedWomanList_Activity.isdisclist = false;
			}

			// 03Sep2017 Arpitha
			if (DiscargeDetails_Activity.isdisc) {
				actionBar.setSelectedNavigationItem(DiscargeDetails_Activity.discharge);
				DiscargeDetails_Activity.isdisc = false;
			}

			// 03Sep2017 Arpitha
			if (Summary_Activity.issummary) {
				actionBar.setSelectedNavigationItem(Summary_Activity.summary);
				Summary_Activity.issummary = false;
			}

			// 03Sep2017 Arpitha
			if (UseManual.isusermanual) {
				actionBar.setSelectedNavigationItem(UseManual.usermanual);
				UseManual.isusermanual = false;
			}
			if (Fragment_DIP.isreferral_updated) {
				actionBar.setSelectedNavigationItem(2);
				Fragment_DIP.isreferral_updated = false;// 02Nov2016 Arpitha
			}
			if (Fragment_DIP.isdelstatus) {
				actionBar.setSelectedNavigationItem(3);
				Fragment_DIP.isdelstatus = false;// 02Nov2016 Arpitha
			} // 22oct2016 Arpitha
			/**
			 * on swiping the viewpager make respective tab selected
			 */
			viewPager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int position) {

					try {
						// on changing the page
						// make respected tab selected
						Partograph_CommonClass.curr_tabpos = position;
						// actionBar.setSelectedNavigationItem(position);

						// Refresh the fragment
						fragment = (FragmentState) mAdapter.instantiateItem(viewPager, position);
						if (fragment != null) {
							fragment.fragmentVisible();
						}

						actionBar.setSelectedNavigationItem(Partograph_CommonClass.curr_tabpos);
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});

			actionBar.setSelectedNavigationItem(Partograph_CommonClass.curr_tabpos);

			calRetrieveColorCodedvalues();// 03May2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		KillAllActivitesAndGoToLogin.addToStack(this);
//		KillAllActivitesAndGoToLogin.activity_stack.add(this);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		Partograph_CommonClass.curr_tabpos = tab.getPosition();
		viewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			// 25Sep2016 Arpitha - addToTrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.logout));
				return true;

			case R.id.registration:
				if (!isExpired()) {
					if (Partograph_CommonClass.autodatetime(Activity_WomenView.this)) {
						displayConfirmationAlert("", "");// 16Oct2016 Arpitha

					}
				}
				return true;

			case R.id.sync:
				menuItem = item;
				menuItem.setActionView(R.layout.progressbar);
				menuItem.expandActionView();
				calSyncMtd();
				return true;

			case R.id.settings:
				if (Partograph_CommonClass.autodatetime(Activity_WomenView.this)) {
					Intent settings = new Intent(this, Settings_parto.class);
					startActivity(settings);
				}
				return true;

			case R.id.about:

				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;

			case R.id.info:
				Intent i = new Intent(getApplicationContext(), GraphInformation.class);
				startActivity(i);
				return true;

			// 13Oct2016 Arpitha
			case R.id.sms:
				display_messagedialog(Activity_WomenView.this, user_id);
				return true;

			// 13Oct2016 Arpitha
			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			// 10Jan2017 Arpitha
			case R.id.changepass:
				dispalyChangePasswordDialog();
				return true;

			// 09Dec2016 Arpitha
			case R.id.summary:

				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);

				return true;

			case R.id.usermanual:

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);
				return true;
			case R.id.search:
				Partograph_CommonClass.dialogSearch(Activity_WomenView.this);
				return true;

			case R.id.disch:
				Intent disch = new Intent(Activity_WomenView.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
																				// Arpitha
			 menu.findItem(R.id.sms).setVisible(false);
		} // 22oct2016 Arpitha

		menu.findItem(R.id.home).setVisible(false);// 24oct2016 Arpitha
		// 10May2017 Arpitha - v2.6
		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);// 10May2017 Arpitha -
															// v2.6

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void fragmentVisible() {
		// TODO Auto-generated method stub
		// 25Sep2016 Arpitha - addToTrace
		// AppContext.addToTrace(
		// new RuntimeException().getStackTrace()[0].getMethodName() + " - " +
		// this.getClass().getSimpleName());

	}

	// 16Oct2016 Arpitha
	// Display Confirmation to exit the screen
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(Activity_WomenView.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.reg_type) + "</font>"));// 14Feb2017
																			// Arpitha
		dialog.setContentView(R.layout.activity_reg_type);

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		regtype = 1;// 22oct2016 Arpitha
		regtypereason = "";// 02Nov2016 Arpitha
		retroreason = 0;// 21Nov2016 Arpitha

		ImageButton imgbtnyes = (ImageButton) dialog.findViewById(R.id.imgsend);
		ImageButton imgbtnno = (ImageButton) dialog.findViewById(R.id.imgcancel);
		RadioButton rdpredel = (RadioButton) dialog.findViewById(R.id.rdpredel);
		final RadioButton rdpostdel = (RadioButton) dialog.findViewById(R.id.rdpostdel);
		final EditText etreason = (EditText) dialog.findViewById(R.id.etreason);
		final TextView txtreason = (TextView) dialog.findViewById(R.id.txtreason);
		final TextView txtpartodisabled = (TextView) dialog.findViewById(R.id.txt1);
		final Spinner spnreason = (Spinner) dialog.findViewById(R.id.spnreason);// 21Nov2016
																				// Arpitha
		final TextView txtretrospectivereson = (TextView) dialog.findViewById(R.id.txtretrospectivereason);// 21Nov2016
																											// Arpitha

		etreason.setVisibility(View.GONE);
		txtreason.setVisibility(View.GONE);
		txtpartodisabled.setVisibility(View.GONE);
		spnreason.setVisibility(View.GONE);// 21Nov2016 Arpitha

		txtretrospectivereson.setVisibility(View.GONE);// 21Nov2016 Arpitha

		rdpredel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				regtype = 1;
				txtpartodisabled.setVisibility(View.GONE);
				txtretrospectivereson.setVisibility(View.GONE);// 21Nov2016
																// Arpitha
				spnreason.setVisibility(View.GONE);// 21Nov2016 Arpitha
				spnreason.setSelection(0);// 17Feb2017 Arpitha
				etreason.setVisibility(View.GONE);// 17Feb2017 Arpitha
				txtreason.setVisibility(View.GONE);// 17Feb2017 Arpitha

			}
		});

		rdpostdel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				regtype = 2;

				txtpartodisabled.setVisibility(View.VISIBLE);
				txtretrospectivereson.setVisibility(View.VISIBLE);// 21Nov2016
																	// Arpitha
				spnreason.setVisibility(View.VISIBLE);// 21Nov2016 Arpitha

			}
		});

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				regtypereason = etreason.getText().toString();

				retroreason = spnreason.getSelectedItemPosition();

				if (rdpostdel.isChecked() && spnreason.getSelectedItemPosition() == 0) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.plz_select_reason_for_retrospective_case),
							Toast.LENGTH_LONG).show();
				}

				else if (rdpostdel.isChecked() && etreason.getText().toString().trim().length() <= 0
						&& spnreason.getSelectedItemPosition() == 4) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_other_reason),
							Toast.LENGTH_LONG).show();
					etreason.requestFocus();// 10Nov2016 Arpitha

				} else {
					dialog.cancel();

					Intent registration = new Intent(Activity_WomenView.this, RegistrationActivity.class);
					startActivity(registration);

				}

			}

		});

		// 21Nov2016 Arpitha
		spnreason.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapter, View v, int position, long arg3) {
				// TODO Auto-generated method stub

				if (position == 4) {
					etreason.setVisibility(View.VISIBLE);
					txtreason.setVisibility(View.VISIBLE);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});// 21Nov2016 Arpitha

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		dialog.setCancelable(false);// 31Oct2016 Arpitha
		dialog.show();// 31Oct2016 Arpitha
	}

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert_exit(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.logout));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void displayConfirmationAlert_exit(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(Activity_WomenView.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.alert) + "</font>"));// 08Feb2017
																			// Arpitha
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		txtdialog1.setVisibility(View.GONE);
		txtdialog.setVisibility(View.GONE);
		txtdialog6.setText(exit_msg);
		imgbtnyes.setText(getResources().getString(R.string.yes));
		imgbtnno.setText(getResources().getString(R.string.no));

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.logout))) {
					Intent i = new Intent(Activity_WomenView.this, LoginActivity.class);
					startActivity(i);
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		dialog.setCancelable(false);

	}

	// 27Dec2016 Arpitha
	void dispalyChangePasswordDialog() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(Activity_WomenView.this);

			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			dialog.setContentView(R.layout.dialog_changepassword);
			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.change_password) + "</font>"));
			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.change_password) + "</font>"));

			int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = dialog.findViewById(dividerId);
			divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

			dialog.show();

			btnchangepass = (Button) dialog.findViewById(R.id.btnchangepass);
			etoldpass = (EditText) dialog.findViewById(R.id.etoldpass);
			etnewpass = (EditText) dialog.findViewById(R.id.etnewpass);
			etretypepass = (EditText) dialog.findViewById(R.id.etreenterpass);

			btnchangepass.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!ValidateAllFileds()) {

						dbh.db.beginTransaction();
						int transId = dbh.iCreateNewTrans(user.getUserId());

						try {
							dbh.updateNewPassword(etnewpass.getText().toString(), user.getUserId(), transId);
							commitTrans();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						etnewpass.setText("");
						etoldpass.setText("");
						etretypepass.setText("");
						dialog.cancel();
						Toast.makeText(getApplicationContext(),
								getResources().getString(R.string.new_pass_updated_successfully), Toast.LENGTH_LONG)
								.show();

					}

				}

			});

			dialog.setCancelable(false);// 03April2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	private void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();

		Toast.makeText(getApplicationContext(), getResources().getString(R.string.new_pass_updated_successfully),
				Toast.LENGTH_LONG).show();
		newUser = dbh.getUserProfile();
		Partograph_CommonClass.user = newUser;
		AsyncCallWS_ChangePassword task = new AsyncCallWS_ChangePassword();
		task.execute();
	}

	// 13April2017 Arpitha
	boolean ValidateAllFileds() {
		isErrorFound = false;

		if (etoldpass.getText().toString().trim().length() > 0) {

			if (!(etoldpass.getText().toString().equals(Partograph_CommonClass.user.getPwd()))) {

				etoldpass.setError(getResources().getString(R.string.entered_password_does_not_match_stored_password));
				// return false;
				isErrorFound = true;

			}
		} else {
			etoldpass.setError(getResources().getString(R.string.enter_old_password));
			isErrorFound = true;
		}

		if (etnewpass.getText().toString().trim().length() <= 0) {
			etnewpass.setError(getResources().getString(R.string.enter_new_password));
			isErrorFound = true;
		} else {
			if (etoldpass.getText().toString().equals(etnewpass.getText().toString())) {
				etnewpass.setError(getResources().getString(R.string.new_password_is_same_as_old_password));
				isErrorFound = true;
			}

			else if (etnewpass.getText().length() < 6) {
				etnewpass.setError(getResources().getString(R.string.new_password_must_contain_atleast_six_characters));
				isErrorFound = true;
			}
		}

		if (etretypepass.getText().toString().trim().length() <= 0) {
			etretypepass.setError(getResources().getString(R.string.re_enter_new_password));
			isErrorFound = true;
		} else {
			if (!(etnewpass.getText().toString().equals(etretypepass.getText().toString()))) {
				etretypepass.setError(getResources().getString(R.string.reentered_pass_does_not_match_new_password));
				isErrorFound = true;

			}
			// 03April2017 Arpitha
			if (etoldpass.getText().toString().equals(etretypepass.getText().toString())) {
				etretypepass.setError(getResources().getString(R.string.new_password_is_same_as_old_password));
				isErrorFound = true;
			} // 03April2017 Arpitha
		}
		return isErrorFound;

	}

	// 03May2017 Arpitha
	// Async cls to sync
	public class AsyncCallWS_ColorCodedValues extends AsyncTask<String, Void, String> {

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			if (!inetOn)
				System.out.println("Internet :" + getResources().getString(R.string.chk_internet));

			else {
				if (Partograph_CommonClass.movedToInputFolder && Partograph_CommonClass.responseAckCount <= 5) {
					if (!SyncFunctions.syncUptoDate) {
						AsyncCallWS_ColorCodedValues task = new AsyncCallWS_ColorCodedValues();
						task.execute();
					} else {
						AsyncCallWS_ColorCodedValues.this.cancel(true);

					}
				} else {
					AsyncCallWS_ColorCodedValues.this.cancel(true);

				}

			}

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}

		@Override
		protected String doInBackground(String... params) {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			try {
				inetOn = Partograph_CommonClass.isConnectionAvailable(getApplicationContext());
				if (inetOn)
					if (Partograph_CommonClass.sPojo != null) {
						serverResult = new SyncFunctions(getApplicationContext()).SyncFunctionColorCodedValues(
								Partograph_CommonClass.properties.getProperty("serverdbmaster"),
								Partograph_CommonClass.sPojo.getIpAddress());
					}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				return e.getLocalizedMessage();
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return serverResult;
		}

		@Override
		protected void onPreExecute() {
			System.out.println("BG PROCESS");
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Retrieve comments from tbl_comment from server
	private void calRetrieveColorCodedvalues() throws Exception {

		int interval = 1;

		commentsTimer = new CountDownTimer(Long.MAX_VALUE, interval * START_AFTER_SECONDS) {

			@Override
			public void onTick(long millisUntilFinished) {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				AsyncCallWS_ColorCodedValues task = new AsyncCallWS_ColorCodedValues();
				task.execute();
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub

			}
		}.start();

	}

	// 11Sep2017 Arpitha - method to check whether the the application has
	// expired or not
	boolean isExpired() throws Exception {
		boolean isExpired = false;
		// SettingsPojo spojo = dbh.getSettingsDetails(user_id);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date expire_date = null;
		if (LoginActivity.expiredate != null && LoginActivity.expiredate.trim().length() > 0)
			expire_date = format.parse(LoginActivity.decyptedEvalEndDate);

		Date date = format.parse(Partograph_CommonClass.getTodaysDate());
		if (expire_date != null && expire_date.before(date)) {
			isExpired = true;
			Partograph_CommonClass
					.displayAlertDialog(getResources().getString(R.string.application_evaluation_date_has_been_expired)
							+ " " + Partograph_CommonClass.getConvertedDateFormat(LoginActivity.decyptedEvalEndDate,
									Partograph_CommonClass.defdateformat),
							Activity_WomenView.this);
		}

		return isExpired;
	}

	// 13Oct2016 Arpitha
	public static void display_messagedialog(final Context context, final String userid) throws Exception {
		try {
			final Dialog sms_dialog = new Dialog(context);
			sms_dialog.setTitle(context.getResources().getString(R.string.send_sms));

			sms_dialog.setContentView(R.layout.sms_dialog);

			dbh = Partograph_DB.getInstance(context);

			sms_dialog.show();

			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
			etphn = (EditText) sms_dialog.findViewById(R.id.etphno);
			final EditText etmess = (EditText) sms_dialog.findViewById(R.id.etmess);
			TextView txtcontcats = (TextView) sms_dialog.findViewById(R.id.txtcontacts);

			txtcontcats.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//					context.startActivity(intent);
					context.startActivity(intent);
					return true;
				}
			});
			
			

			String p_no = "";
			ArrayList<String> values;
			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(3);
			p_no = "";
			for (int i = 0; i < values.size(); i++) {

				String s = values.get(i);
				p_no = p_no + values.get(i) + ",";
			}
			etphn.setText(p_no);

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						if (etphn.getText().toString().trim().length() <= 0) {
							Toast.makeText(context, context.getResources().getString(R.string.enter_valid_phn_no),
									Toast.LENGTH_LONG).show();
						} else if (etmess.getText().toString().trim().length() <= 0) {
							Toast.makeText(context, context.getResources().getString(R.string.enter_message),
									Toast.LENGTH_LONG).show();
						} else {
							sms_dialog.cancel();
							Partograph_CommonClass.InserttblMessageLog(etphn.getText().toString(),
									etmess.getText().toString(), userid);
							if (Partograph_CommonClass.isMessageLogsaved) {
								Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
										Toast.LENGTH_LONG).show();
								sendSMSFunction();

							}
						}

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			
			
			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
				}
			});
			sms_dialog.setCancelable(false);
		} catch (Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ context.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */// 13Oct2016 Arpitha
	private static void sendSMSFunction() throws Exception {
		new SendSMS();

	}
	
	
	//code 
	  @Override
	 public void onActivityResult(int reqCode, int resultCode, Intent data) {
	 super.onActivityResult(reqCode, resultCode, data);

	 switch (reqCode) {
	 case (PICK_CONTACT) :
	   if (resultCode == Activity.RESULT_OK) {

	     Uri contactData = data.getData();
	     Cursor c =  managedQuery(contactData, null, null, null, null);
	     if (c.moveToFirst()) {


	         String id =c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

	         String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

	           if (hasPhone.equalsIgnoreCase("1")) {
	          Cursor phones = getContentResolver().query( 
	                       ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, 
	                       ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id, 
	                       null, null);
	             phones.moveToFirst();
	              String cNumber = phones.getString(phones.getColumnIndex("data1"));
	             System.out.println("number is:"+cNumber);
	           }
	         String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));


	     }
	   }
	   break;
	 }
	  }

	

}
