package com.bc.partograph.sliding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

public class Fragment_Dilatation extends Fragment implements OnClickListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	GraphicalView mChartView = null;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	String strLastTime, strCurrentTime;
	Date dprev, dcurr;
	String disptime, dispdate, diff = null;
	int min;
	boolean isValidTime = false;
	// Changed on 27Mar2015
	String displaydateformat;
	String womenid;
	String userid;
	String lastdate;
	String lasttime;
	// updated on 7july2016 by Arpitha
	public static boolean isdilatation_notify = false;
	int lastdescentofheadval;

	String lastentrytime;
	int cervixvalue;
	int descent_of_head;
	int lastcervixval;

	// 31Aug2016 - bindu
	LinkedHashMap<Integer, Double> newDilValuesCervix;
	LinkedHashMap<Integer, Double> newDilValuesDescent;
	// 04Oct2016 Arpitha
	TextView txtdialogtitle;
	Women_Profile_Pojo woman;// 03Nov2016

	// public static String firstPartoTime;// 05May2017 Arpitha - v2.6 manti sid
	// // -0000235
	// public static String maxCervixVal;// 05May2017 Arpitha - v2.6 manti sid
	// -0000235

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_dilatation, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");
			womenid = woman.getWomenId();
			userid = woman.getUserId();

			initializeScreen(aq.id(R.id.rldilatation).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();
			setInputFiltersForEdittext();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.etdilatationtime).text(strcurrTime);
		aq.id(R.id.etdilatationdate).text(displaydateformat);

		aq.id(R.id.etdilatationdate).enabled(false);
		aq.id(R.id.etdilatationtime).enabled(false);

		aq.id(R.id.rldilatationchart).gone();

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluesdilatation);
		tbl.setVisibility(View.GONE);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha

		disableAddValues();

		obj_Type = getResources().getString(R.string.object3);
		obj_Id = dbh.getObjectID(obj_Type);

		loadListDataToDisplayValues();

		displayComments();
		updatetblComments(strwuserId, strWomenid, obj_Id);
		aq.id(R.id.imgbtnnotification).invisible();
		lastdate = dbh.lastvaldate(strWomenid, obj_Id);
		lasttime = dbh.lastvaltime(strWomenid, obj_Id, lastdate);
		// 08Oct2017 Arpitha - //08Oct2017 Arpitha -
		if (SlidingActivity.isdilatationtimevalid && !(SlidingActivity.arrVal.size() > 0)) {
			aq.id(R.id.imgbtnnotification).visible();
		} else
			aq.id(R.id.imgbtnnotification).invisible();

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha

		// 15Aug2017 Arpitha
		if (SlidingActivity.arrVal.size() > 0)
			aq.id(R.id.txtnodata)
					.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));
	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.dilcmt_cnt = 0;

		SlidingActivity.isMsgDil = false; // 15Sep2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());

		}
		if (arrComments.size() > 0) {
			Parcelable state = aq.id(R.id.listCommentsdilatation).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentsdilatation).visible();
			aq.id(R.id.txtCommentsdilatation).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_dilatation, arrComments);
			aq.id(R.id.listCommentsdilatation).adapter(adapter);

			// 18dec2015
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentsdilatation).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentsdilatation).getListView());
		} else {
			aq.id(R.id.listCommentsdilatation).gone();
			aq.id(R.id.txtCommentsdilatation).gone();
		}
	}

	// Add values layout visibility gone
	private void disableAddValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// 23Sep2016 Arpitha - make invisible when woman is referred

		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
		// ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		// arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2
				|| SlidingActivity.arrVal.size() > 0) {
			aq.id(R.id.lldilatationaddval).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {
			aq.id(R.id.lldilatationaddval).visible();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	// OnClick
	@Override
	public void onClick(View v) {

		try {
			// 25Sep2016 Arpitha - addToTrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (v.getId()) {
			case R.id.btnsavedilatation:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					Date lastregdate = null;
					String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
							getResources().getString(R.string.partograph));
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (strlastinserteddate != null) {
						lastregdate = format.parse(strlastinserteddate);
					}
					Date currentdatetime = new Date();
					if (lastregdate != null && currentdatetime.before(lastregdate) && (!LoginActivity.istesting)) {
						Partograph_CommonClass.showSettingsAlert(getActivity(),
								getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
					} else {// 27Sep2016 Arpitha

						strCurrentTime = todaysDate + "_" + aq.id(R.id.etdilatationtime).getText().toString();
						// Changed by Arpitha (made it as 60 before it was 239)
						isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 60);

						// 26jan2016
						if (listValuesItems.size() <= 0) {
							isValidTime = true;
						}

						if (isValidTime) {
							if (validateDilation()) {

								cervixvalue = Integer.parseInt(aq.id(R.id.etcervixval).getText().toString());
								descent_of_head = Integer.parseInt(aq.id(R.id.etdescentofheadval).getText().toString());

								// if (cervixvalue == 0 || cervixvalue > 10 ||
								// descent_of_head > 5) {
								displayConfirmationAlert("", "");

								// } else {
								//
								// displayConfirmationAlert(getResources().getString(R.string.do_you_want_to_save),
								// getResources().getString(R.string.save));

								// Add_value_pojo valpojo = new
								// Add_value_pojo();
								// valpojo.setCervix(aq.id(R.id.etcervixval).getText().toString());
								// valpojo.setStrTime(aq.id(R.id.etdilatationtime).getText().toString());
								// valpojo.setStrdate(todaysDate);
								// valpojo.setDesc_of_head(aq.id(R.id.etdescentofheadval).getText().toString());
								// valpojo.setObj_Id(obj_Id);
								// valpojo.setObj_Type(obj_Type); commented
								// on 01Dec2016 Arpitha

								/*
								 * dbh.db.beginTransaction(); int transId =
								 * dbh.iCreateNewTrans(strwuserId);
								 * 
								 * ArrayList<String> adata = new
								 * ArrayList<String>();
								 * adata.add(aq.id(R.id.etcervixval).getText().
								 * toString());
								 * adata.add(aq.id(R.id.etdescentofheadval).
								 * getText().toString()); // updated on 28feb16
								 * by Arpitha adata.add("");
								 * adata.add(aq.id(R.id.etdilationcomments).
								 * getText().toString());
								 * 
								 * ArrayList<String> prop_id = new
								 * ArrayList<String>(); ArrayList<String>
								 * prop_name = new ArrayList<String>();
								 * Property_pojo pdata = new Property_pojo();
								 * Cursor cursor = dbh.getPropertyIds(obj_Id);
								 * 
								 * if (cursor.getCount() > 0) {
								 * cursor.moveToFirst();
								 * 
								 * do {
								 * 
								 * prop_id.add(cursor.getString(0));
								 * prop_name.add(cursor.getString(1));
								 * 
								 * } while (cursor.moveToNext());
								 * 
								 * pdata.setObj_Id(obj_Id);
								 * pdata.setProp_id(prop_id);
								 * pdata.setProp_name(prop_name);
								 * pdata.setProp_value(adata);
								 * pdata.setStrdate(todaysDate);
								 * pdata.setStrTime(aq.id(R.id.etdilatationtime)
								 * .getText().toString());
								 * 
								 * // 05May2017 Arpitha - v2.6 - mantis id // -
								 * 0000240 and 0000238 if (listValuesItems !=
								 * null && listValuesItems.size() > 0) { int
								 * hours =
								 * Partograph_CommonClass.getHoursBetDates(
								 * strLastTime, todaysDate + "_" + strcurrTime);
								 * if ((Integer.parseInt(listValuesItems.get(
								 * listValuesItems.size() - 1)
								 * .getDesc_of_head()) == Integer.parseInt(
								 * aq.id(R.id.etdescentofheadval).getText().
								 * toString()) && hours >= 4) ||
								 * (Integer.parseInt(aq.id(R.id.etcervixval).
								 * getText() .toString()) == Integer.parseInt(
								 * listValuesItems.get(listValuesItems.size() -
								 * 1) .getCervix()) && hours >= 2))
								 * pdata.setIsDangerval1(1); else
								 * pdata.setIsDangerval1(0);// 05May2017 //
								 * Arpitha // - // v2.6 // - // mantis // id //
								 * - // 0000240 // and // 0000238 }
								 * 
								 * boolean isAdded = dbh.insertData(pdata,
								 * strWomenid, strwuserId, transId);
								 * 
								 * if (isAdded) {
								 * 
								 * dbh.iNewRecordTrans(strwuserId, transId,
								 * Partograph_DB.TBL_PROPERTYVALUES); //
								 * 17Oct2016 Arpitha if (listValuesItems.size()
								 * <= 0) { dbh.addtonotification(strwuserId,
								 * strWomenid, obj_Id); } else
								 * dbh.updatenotification(strWomenid, obj_Id);
								 * loadListDataToDisplayValues();
								 * displayValues(); commitTrans(); } else {
								 * rollbackTrans(); }
								 * 
								 * aq.id(R.id.etcervixval).text("");
								 * aq.id(R.id.etdescentofheadval).text(""); //
								 * updated 28feb16 by Arpitha
								 * aq.id(R.id.etdilationcomments).text(""); //
								 * aq.id(R.id.ethoursval).text("");
								 * 
								 * aq.id(R.id.etcervixval).getEditText().
								 * requestFocus();
								 */

								// }
								// }
							}
						} else {

							String newTime = Partograph_CommonClass.getnewtime(lastentrytime, 60);
							String newtime_12hrs = Partograph_CommonClass.gettimein12hrformat(newTime);
							aq.id(R.id.etcervixval).text("");
							aq.id(R.id.etdescentofheadval).text("");
							// updated 28feb16 by Arpitha
							aq.id(R.id.etdilationcomments).text("");

							displayAlertDialog(getResources().getString(R.string.fhr_duration) + " " + newtime_12hrs
									+ " " + getResources().getString(R.string.on));
						}
					}
				}
				break;

			case R.id.btnviewgraphdilatation: {

				loadListDataToDisplayValues();
				if (listValuesItems.size() > 0)
					showGraphDialog();
				else
					displayAlertDialog(getResources().getString(R.string.nodata));

			}
				break;

			case R.id.dilatationinfo: {
				displayDilatationinfo();
			}
				break;
			case R.id.imgbtnnotification: {
				notification(womenid);
			}

			}
		} catch (

		Exception e)

		{
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	/**
	 * validate dilatation - 12jan2016
	 * 
	 * @return
	 */
	private boolean validateDilation() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (aq.id(R.id.etcervixval).getEditText().getText().toString().trim().length() <= 0
				&& aq.id(R.id.etdescentofheadval).getEditText().getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.entercervixanddescentofheadval));

			aq.id(R.id.etcervixval).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etcervixval).getEditText().getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.entercervixval));

			aq.id(R.id.etcervixval).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etdescentofheadval).getEditText().getText().toString().trim().length() <= 0) {

			displayAlertDialog(getResources().getString(R.string.enterdescentofheadval));
			aq.id(R.id.etdescentofheadval).getEditText().requestFocus();
			return false;
		}

		// 31Aug2016 - bindu - to check dilatation value is >3 cm
		// 06Jun2017 Arpitha - dilatation has to start after 4 cm
		if (aq.id(R.id.etcervixval).getEditText().getText().toString().trim().length() > 0) {
			if (Integer.parseInt(aq.id(R.id.etcervixval).getText().toString()) < 4) {
				displayAlertDialog(getResources().getString(R.string.alertdilatationvalue));
				aq.id(R.id.etcervixval).getEditText().requestFocus();
				return false;
			}
		}

		if (lastcervixval > 0) {
			if (Integer.parseInt(aq.id(R.id.etcervixval).getText().toString()) < lastcervixval) {
				displayAlertDialog(getResources().getString(R.string.cervix_validation));
				aq.id(R.id.etcervixval).getEditText().requestFocus();
				return false;
			}
		}

		if (lastdescentofheadval > 0) {
			if (Integer.parseInt(aq.id(R.id.etdescentofheadval).getText().toString()) > lastdescentofheadval) {
				displayAlertDialog(getResources().getString(R.string.descent_head_validation));
				aq.id(R.id.etdescentofheadval).getEditText().requestFocus();
				return false;
			}
		}

		return true;
	}

	private void showGraphDialog() throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

			dialog.setContentView(R.layout.dialog_dilatation);

			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			dialog.getWindow().setBackgroundDrawableResource(R.color.white);

			TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(getResources().getString(R.string.dilatation));

			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			createChartDilatation(dialog);
			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void createChartDilatation(Dialog dialog) throws Exception {
		listValuesItems = dbh.getDilatationData(obj_Id, strWomenid, strwuserId, true);
		if (listValuesItems.size() > 0) {

			// 31Aug2016 - bindu
			getValuesForGraph();

			// Creating an XYSeries for values
			XYSeries cervixseries = new XYSeries(getResources().getString(R.string.cervix));
			XYSeries desc_headseries = new XYSeries(getResources().getString(R.string.desc_head));

			XYSeries alertline = new XYSeries(getResources().getString(R.string.alertline));
			XYSeries actionline = new XYSeries(getResources().getString(R.string.actionline));

			int[] x = { 0, 1, 2, 3, 4, 5, 6 };
			int[] alertval = { 4, 5, 6, 7, 8, 9, 10 };

			int[] xactionline = { 4, 5, 6, 7, 8, 9, 10 };
			int[] actionval = { 4, 5, 6, 7, 8, 9, 10 };

			double descofhead = 0;

			// 31Aug2016 - bindu
			for (Map.Entry<Integer, Double> cervixval : newDilValuesCervix.entrySet()) {
				cervixseries.add(cervixval.getKey(), cervixval.getValue());
			}
			// 31Aug2016 - bindu
			for (Map.Entry<Integer, Double> desval : newDilValuesDescent.entrySet()) {
				desc_headseries.add(desval.getKey(), desval.getValue());
			}

			for (int i = 0; i < x.length; i++) {
				alertline.add(x[i], alertval[i]);
			}

			for (int i = 0; i < xactionline.length; i++) {
				actionline.add(xactionline[i], actionval[i]);
			}
			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(cervixseries);
			dataset.addSeries(desc_headseries);
			dataset.addSeries(alertline);
			dataset.addSeries(actionline);

			// Creating XYSeriesRenderer to customize alertlineSeries
			XYSeriesRenderer alertRenderer = new XYSeriesRenderer();
			alertRenderer.setColor(getResources().getColor(R.color.amber));
			alertRenderer.setPointStyle(PointStyle.POINT);
			alertRenderer.setFillPoints(true);
			alertRenderer.setLineWidth(5);
			alertRenderer.setDisplayChartValues(false);

			// Creating XYSeriesRenderer to customize actionlineSeries
			XYSeriesRenderer actionRenderer = new XYSeriesRenderer();
			actionRenderer.setColor(getResources().getColor(R.color.red));
			actionRenderer.setPointStyle(PointStyle.POINT);
			actionRenderer.setFillPoints(true);
			actionRenderer.setLineWidth(5);
			actionRenderer.setDisplayChartValues(false);

			// Creating XYSeriesRenderer to customize pulseSeries
			XYSeriesRenderer cervixRenderer = new XYSeriesRenderer();
			cervixRenderer.setColor(getResources().getColor(R.color.springgreen));
			cervixRenderer.setPointStyle(PointStyle.TRIANGLE);
			cervixRenderer.setFillPoints(true);
			cervixRenderer.setLineWidth(3);
			cervixRenderer.setDisplayChartValues(true);
			cervixRenderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize bpsystolicSeries
			XYSeriesRenderer descheadRenderer = new XYSeriesRenderer();
			descheadRenderer.setColor(getResources().getColor(R.color.steel_blue));
			descheadRenderer.setPointStyle(PointStyle.CIRCLE);
			descheadRenderer.setFillPoints(true);
			descheadRenderer.setLineWidth(3);
			descheadRenderer.setDisplayChartValues(true);
			descheadRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			// multiRenderer.setChartTitle("Partograph");
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.cervixanddescentofhead));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 30, 60, 60, 30 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(15);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, false);
			multiRenderer.setYAxisMax(10);
			multiRenderer.setYAxisMin(0);
			multiRenderer.setYLabels(11);
			multiRenderer.setXAxisMin(1);
			multiRenderer.setXAxisMax(12);

			// 31Aug2016 - bindu
			for (int i = 0; i < 12; i++) {
				for (Map.Entry<Integer, Double> xaxisval : newDilValuesCervix.entrySet()) {
					// String val = "" + (xaxisval.getKey()+1);
					if (i == xaxisval.getKey()) {
						int xa = xaxisval.getKey() + 1;
						String val = "" + xa;
						multiRenderer.addXTextLabel(xaxisval.getKey(), val);
					} else
						multiRenderer.addXTextLabel((i), "" + (i + 1));
				}
			}

			multiRenderer.addSeriesRenderer(cervixRenderer);
			multiRenderer.addSeriesRenderer(descheadRenderer);
			multiRenderer.addSeriesRenderer(alertRenderer);
			multiRenderer.addSeriesRenderer(actionRenderer);

			mChartView = null;

			mChartView = ChartFactory.getLineChartView(getActivity(), dataset, multiRenderer);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			mChartView.setLayoutParams(params);

			RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.rldilatationchart);
			layout.addView(mChartView);

			if (mChartView != null) {
				mChartView.repaint();
			}
		}
	}

	// 31Aug2016 - bindu
	private void getValuesForGraph() throws ParseException {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		newDilValuesCervix = new LinkedHashMap<Integer, Double>();
		newDilValuesDescent = new LinkedHashMap<Integer, Double>();
		int hrs = 0;
		String str1, str2;
		if (listValuesItems != null && listValuesItems.size() > 0) {
			for (int i = 0; i < listValuesItems.size(); i++) {
				if (i == 0) {
					// hrs = 1;commented on 05May2017 Arpitha v2.6 for - mantis
					// id 0000236
					newDilValuesCervix.put(hrs, Double.parseDouble(listValuesItems.get(i).getCervix()));
					newDilValuesDescent.put(hrs, Double.parseDouble(listValuesItems.get(i).getDesc_of_head()));
					hrs = 0;
				} else {
					str1 = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
					str2 = listValuesItems.get(i - 1).getStrdate() + "_" + listValuesItems.get(i - 1).getStrTime();
					hrs = hrs + Partograph_CommonClass.getHoursBetDates(str1, str2);
					newDilValuesCervix.put(hrs, Double.parseDouble(listValuesItems.get(i).getCervix()));
					newDilValuesDescent.put(hrs, Double.parseDouble(listValuesItems.get(i).getDesc_of_head()));
				}
			}
		}

	}

	/** Method to rollback the transaction */
	private void rollbackTrans() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
		aq.id(R.id.imgbtnnotification).invisible();

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha

	}

	// Sync
	private void calSyncMtd() {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	public void doWork() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etdilatationtime).text(curTime);

						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	private void loadListDataToDisplayValues() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		listValuesItems = dbh.getDilatationData(obj_Id, strWomenid, strwuserId, false);
		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha

		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues();
		}
	}

	private void displayValues() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (listValuesItems.size() > 0) {

			isValidTime = true;

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);

			// 05May2017 Arpitha - v2.6 - mantis id - 0000240
			TextView txtcervixcolor = new TextView(getActivity());
			txtcervixcolor.setTextSize(18);
			txtcervixcolor.setPadding(10, 10, 10, 10);
			txtcervixcolor.setTextColor(getResources().getColor(R.color.black));
			txtcervixcolor.setTextSize(18);
			txtcervixcolor.setText(getResources().getString(R.string.color));
			txtcervixcolor.setGravity(Gravity.CENTER);
			trlabel.addView(txtcervixcolor);

			TextView textval2lbl = new TextView(getActivity());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval2lbl);

			// 05May2017 Arpitha - v2.6 - mantis id 0000240
			TextView txtcolor = new TextView(getActivity());
			txtcolor.setTextSize(18);
			txtcolor.setText(getResources().getString(R.string.color));
			txtcolor.setPadding(10, 10, 10, 10);
			txtcolor.setTextColor(getResources().getColor(R.color.black));
			txtcolor.setTextSize(18);
			txtcolor.setGravity(Gravity.LEFT);
			trlabel.addView(txtcolor);

			// updated on 28feb16 by Arpitha
			TextView txtcomments = new TextView(getActivity());
			txtcomments.setTextSize(18);
			txtcomments.setText(getResources().getString(R.string.comments));
			txtcomments.setPadding(10, 10, 10, 10);
			txtcomments.setTextColor(getResources().getColor(R.color.black));
			txtcomments.setTextSize(18);
			txtcomments.setGravity(Gravity.LEFT);
			trlabel.addView(txtcomments);

			TextView textval3lbl = new TextView(getActivity());
			textval3lbl.setTextSize(18);
			textval3lbl.setPadding(10, 10, 10, 10);
			textval3lbl.setTextColor(getResources().getColor(R.color.black));
			textval3lbl.setTextSize(18);
			textval3lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval3lbl);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				// txtdate.setText(listValuesItems.get(i).getStrdate());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				String val1 = "", val2 = "", val3 = "", val4 = "";

				val1 = listValuesItems.get(i).getCervix();

				val2 = listValuesItems.get(i).getDesc_of_head();
				val3 = listValuesItems.get(i).getHours();
				// updated on 28feb16 by Arpitha
				val4 = listValuesItems.get(i).getDilatationcomments();

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm");

				if (i > 0) {
					dprev = sdf.parse(dispdate + "_" + disptime);
					dcurr = sdf.parse(listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime());

					long difference = dprev.getTime() - dcurr.getTime();
					int days = (int) (difference / (1000 * 60 * 60 * 24));
					int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
					min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
					hours = (hours < 0 ? -hours : hours);

					String hh, mm;
					if (hours < 10)
						hh = "0" + hours;
					else
						hh = "" + hours;

					if (min < 10)
						mm = "0" + min;
					else
						mm = "" + min;

					diff = hh + ":" + mm;

				} else
					diff = "00:00";

				dispdate = listValuesItems.get(i).getStrdate();
				disptime = listValuesItems.get(i).getStrTime();

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
					lastdescentofheadval = Integer.parseInt(listValuesItems.get(i).getDesc_of_head());
					lastentrytime = listValuesItems.get(i).getStrTime();
					lastcervixval = Integer.parseInt(listValuesItems.get(i).getCervix());

					// firstPartoTime = listValuesItems.get(i).getStrdate() +
					// "_" + listValuesItems.get(i).getStrTime();// 05May2017
					// // Arpitha
					// // -
					// // v2.6
					// // manti
					// // sid
					// // -0000235

				}

				textval1lbl.setText(getResources().getString(R.string.cervix));
				textval2lbl.setText(getResources().getString(R.string.desc_head));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);
				tr.addView(txtvalue1);

				// 05May2017 Arpitha - v2.6 - mantis id - 0000238
				TextView txtcervixcolorvalue = new TextView(getActivity());
				txtcervixcolorvalue.setPadding(5, 5, 5, 5);
				txtcervixcolorvalue.setGravity(Gravity.CENTER);
				txtcervixcolorvalue.setWidth(20);
				txtcervixcolorvalue.setHeight(20);
				tr.addView(txtcervixcolorvalue);

				TextView txtvalue2 = new TextView(getActivity());
				txtvalue2.setText(val2);
				txtvalue2.setTextSize(18);
				txtvalue2.setPadding(10, 10, 10, 10);
				txtvalue2.setTextColor(getResources().getColor(R.color.black));
				txtvalue2.setTextSize(18);
				txtvalue2.setGravity(Gravity.CENTER);
				tr.addView(txtvalue2);

				// 05May2017 Arpitha - v2.6 - mantis id - 0000240
				TextView txtcolorvalue = new TextView(getActivity());
				txtcolorvalue.setPadding(5, 5, 5, 5);
				txtcolorvalue.setGravity(Gravity.CENTER);
				txtcolorvalue.setWidth(20);
				txtcolorvalue.setHeight(20);
				tr.addView(txtcolorvalue);

				// updated by Arpitha
				TextView txtcommentvalue = new TextView(getActivity());
				txtcommentvalue.setText(val4);
				txtcommentvalue.setTextSize(18);
				txtcommentvalue.setPadding(10, 10, 10, 10);
				txtcommentvalue.setTextColor(getResources().getColor(R.color.black));
				txtcommentvalue.setTextSize(18);
				txtcommentvalue.setGravity(Gravity.CENTER);
				tr.addView(txtcommentvalue);

				// 05May2017 Arpitha - v2.6 - mantis id - 0000239
				// if(i==0 && listValuesItems.size()>1)
				// {
				int descentofHeadPrevVal = 0;
				if (i < listValuesItems.size() - 1)
					descentofHeadPrevVal = Integer.parseInt(listValuesItems.get(i + 1).getDesc_of_head());
				int descentofHeadCurVal = 0;
				// String h = null;

				descentofHeadCurVal = Integer.parseInt(listValuesItems.get(i).getDesc_of_head());
				// h = listValuesItems.get(1).getDesc_of_head();

				// if (descentofHeadPrevVal ==2) {
				if ((descentofHeadPrevVal - descentofHeadCurVal == 1) && descentofHeadPrevVal == 2) {
					txtcolorvalue.setBackgroundColor(getResources().getColor(R.color.amber));

					// }
					// }
				} // 05May2017 Arpitha - v2.6 - mantis id - 0000239

				// 05May2017 Arpitha - v2.6 - mantis id - 0000240

				if (i < listValuesItems.size() - 1) {
					int hours = Partograph_CommonClass.getHoursBetDates(
							listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime(),
							listValuesItems.get(i + 1).getStrdate() + "_" + listValuesItems.get(i + 1).getStrTime());
					if (Integer.parseInt(listValuesItems.get(i).getDesc_of_head()) == Integer
							.parseInt(listValuesItems.get(i + 1).getDesc_of_head()) && (hours >= 4)) {
						txtcolorvalue.setBackgroundColor(getResources().getColor(R.color.red));
					}

				} // 05May2017 Arpitha - v2.6 - mantis id - 0000240

				// 05May2017 Arpitha - v2.6 - mantis id - 0000238
				if (i < listValuesItems.size() - 1 && Integer.parseInt(listValuesItems.get(i).getCervix()) > 4
						&& Integer.parseInt(listValuesItems.get(i).getCervix()) < 7) {
					int hours = Partograph_CommonClass.getHoursBetDates(
							listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime(),
							listValuesItems.get(i + 1).getStrdate() + "_" + listValuesItems.get(i + 1).getStrTime());
					if ((Integer.parseInt(listValuesItems.get(i).getCervix())
							- Integer.parseInt(listValuesItems.get(i + 1).getCervix()) <= 2) && hours >= 4) {
						txtcervixcolorvalue.setBackgroundColor(getResources().getColor(R.color.amber));

					}
				} // 05May2017 Arpitha - v2.6 - mantis id - 0000238
				/*
				 * }catch(Exception e) { e.printStackTrace(); }
				 */

				// 08May2017 Arpitha - v2.6
				int cervixPrevVal = 0;
				int hours = 0;// 11Aug2017 Arpitha
				if (i <= listValuesItems.size() - 2) {
					hours = Partograph_CommonClass.getHoursBetDates(
							listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime(),
							listValuesItems.get(i + 1).getStrdate() + "_" + listValuesItems.get(i + 1).getStrTime());
				} // 11Aug2017 Arpitha
				int cervixCurVal = Integer.parseInt(listValuesItems.get(i).getCervix());
				if (i <= listValuesItems.size() - 2)// 11Aug2017 Arpitha -
													// before it was < made it
													// as <=
					cervixPrevVal = Integer.parseInt(listValuesItems.get(i + 1).getCervix());
				if (cervixCurVal > 7 && cervixCurVal - cervixPrevVal <= 2 && hours >= 2) {
					txtcervixcolorvalue.setBackgroundColor(getResources().getColor(R.color.red));

				} // 08May2017 Arpitha - v2.6

				// 05May2017 Arpitha - v2.6 - mantis id - 0000238
				if (i < listValuesItems.size() - 1) {
					String cervixprevVal = listValuesItems.get(i + 1).getCervix();
					String cervixcurVal = null;
					// if (i > 0) {
					cervixcurVal = listValuesItems.get(i).getCervix();
					// }
					// try{
					if ((Integer.parseInt(cervixprevVal) == Integer.parseInt(cervixcurVal)
							&& (Partograph_CommonClass.getHoursBetDates(
									listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime(),
									listValuesItems.get(i + 1).getStrdate() + "_"
											+ listValuesItems.get(i + 1).getStrTime()) >= 2))) {
						txtcervixcolorvalue.setBackgroundColor(getResources().getColor(R.color.red));
					}

				} // 05May2017 Arpitha - v2.6 - mantis id - 0000238

				// 21oct2017 arpitha
				if (descentofHeadPrevVal == descentofHeadCurVal && (Partograph_CommonClass.getHoursBetDates(
						listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime(),
						listValuesItems.get(i + 1).getStrdate() + "_"
								+ listValuesItems.get(i + 1).getStrTime()) >= 2)) {
					txtcolorvalue.setBackgroundColor(getResources().getColor(R.color.red));
				}

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);

				// maxCervixVal = listValuesItems.get(0).getCervix();

			}
		}

	}

	public void displayDilatationinfo() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		final Dialog dialog = new Dialog(getActivity());
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.dilatation_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.dilatation));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		dialog.show();

	}

	public void notification(String womenid) throws Exception

	{
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.notification_dialog);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.dilatation));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		TextView txt = (TextView) dialog.findViewById(R.id.textView1);
		String convertedlastdate = Partograph_CommonClass.getConvertedDateFormat(lastdate, "dd-MM-yyyy");
		txt.setText(getResources().getString(R.string.enter_dilatation_last_entry) + " " + convertedlastdate + " "
				+ lasttime + " " + getResources().getString(R.string.was_at) + "\n"
				+ getResources().getString(R.string.note_enter_only_if_required));

		dialog.show();

	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		aq.id(R.id.etdilationcomments).getEditText()
				.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(200) });

	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		imgbtnyes.setText(getResources().getString(R.string.save));
		imgbtnno.setText(getResources().getString(R.string.recheck));

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);
		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		// 04Oct2016 ARpitha
		String alertmsg = "";
		String values = "";

		if (cervixvalue == 0 || cervixvalue > 10 || descent_of_head > 5)// 12May2017
																		// Arpitha
																		// -
																		// v2.6
		{// 12May2017 Arpitha - v2.6
			if (cervixvalue == 0 || cervixvalue > 10) {
				alertmsg = alertmsg + getResources().getString(R.string.cervix_range) + "\n";
				values = values + "" + cervixvalue + "\n";

			}

			if (descent_of_head == 0 || descent_of_head > 5) {

				alertmsg = alertmsg + getResources().getString(R.string.descent_of_head_range) + "\n";
				values = values + "" + descent_of_head + "\n";

			} // 12May2017 Arpitha - v2.6

			txtdialog6.setText(getResources().getString(R.string.bp_val));

			txtdialog.setText(alertmsg);
			txtdialog1.setText(values);
		} else// 12May2017 Arpitha - v2.6
			txtdialog.setText(getResources().getString(R.string.are_you_sure_want_to_save));// 12May2017
																							// Arpitha
																							// -
																							// v2.6

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("confirm")) {
					aq.id(R.id.etcervixval).getEditText().requestFocus();

				}

			}
		});

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				try {

					saveData();// 12May2017 Arpitha

					/*
					 * Add_value_pojo valpojo = new Add_value_pojo();
					 * valpojo.setCervix(aq.id(R.id.etcervixval).getText().
					 * toString());
					 * valpojo.setStrTime(aq.id(R.id.etdilatationtime).getText()
					 * .toString()); valpojo.setStrdate(todaysDate);
					 * valpojo.setDesc_of_head(aq.id(R.id.etdescentofheadval).
					 * getText().toString()); //
					 * valpojo.setHours(aq.id(R.id.ethoursval).getText() //
					 * .toString()); valpojo.setObj_Id(obj_Id);
					 * valpojo.setObj_Type(obj_Type);
					 * 
					 * dbh.db.beginTransaction(); int transId =
					 * dbh.iCreateNewTrans(strwuserId);
					 * 
					 * ArrayList<String> adata = new ArrayList<String>();
					 * adata.add(aq.id(R.id.etcervixval).getText().toString());
					 * adata.add(aq.id(R.id.etdescentofheadval).getText().
					 * toString()); // updated on 28feb16 by Arpitha
					 * adata.add("");
					 * adata.add(aq.id(R.id.etdilationcomments).getText().
					 * toString());
					 * 
					 * ArrayList<String> prop_id = new ArrayList<String>();
					 * ArrayList<String> prop_name = new ArrayList<String>();
					 * Property_pojo pdata = new Property_pojo(); Cursor cursor
					 * = dbh.getPropertyIds(obj_Id);
					 * 
					 * if (cursor.getCount() > 0) { cursor.moveToFirst();
					 * 
					 * do {
					 * 
					 * prop_id.add(cursor.getString(0));
					 * prop_name.add(cursor.getString(1));
					 * 
					 * } while (cursor.moveToNext());
					 * 
					 * pdata.setObj_Id(obj_Id); pdata.setProp_id(prop_id);
					 * pdata.setProp_name(prop_name);
					 * pdata.setProp_value(adata); pdata.setStrdate(todaysDate);
					 * pdata.setStrTime(aq.id(R.id.etdilatationtime).getText().
					 * toString());
					 * 
					 * // 05May2017 Arpitha - v2.6 - mantis id // - 0000240 and
					 * 0000238 int hours =
					 * Partograph_CommonClass.getHoursBetDates(strLastTime,
					 * strcurrTime); if ((Integer
					 * .parseInt(listValuesItems.get(listValuesItems.size() -
					 * 1).getDesc_of_head()) == Integer
					 * .parseInt(aq.id(R.id.etdescentofheadval).getText().
					 * toString()) && hours >= 4) ||
					 * (Integer.parseInt(aq.id(R.id.etcervixval).getText().
					 * toString()) == Integer
					 * .parseInt(listValuesItems.get(listValuesItems.size() -
					 * 1).getCervix()) && hours >= 2)) pdata.setIsDangerval1(1);
					 * else pdata.setIsDangerval1(0);// 05May2017 // Arpitha //
					 * - // v2.6 // - // mantis // id - // 0000240 and 0000238
					 * 
					 * 
					 * // 05May2017 Arpitha - v2.6 - mantis id - 0000240 if
					 * (Integer
					 * .parseInt(listValuesItems.get(listValuesItems.size() -
					 * 1).getDesc_of_head()) == Integer
					 * .parseInt(aq.id(R.id.etdescentofheadval).getText().
					 * toString())) pdata.setIsDangerval1(1); else
					 * pdata.setIsDangerval1(0);// 05May2017 Arpitha - v2.6 // -
					 * mantis id - 0000240
					 * 
					 * 
					 * 
					 * boolean isAdded = dbh.insertData(pdata, strWomenid,
					 * strwuserId, transId);
					 * 
					 * if (isAdded) {
					 * 
					 * dbh.iNewRecordTrans(strwuserId, transId,
					 * Partograph_DB.TBL_PROPERTYVALUES); // 17Oct2016 Arpitha
					 * if (listValuesItems.size() <= 0) {
					 * dbh.addtonotification(strwuserId, strWomenid, obj_Id); }
					 * else dbh.updatenotification(strWomenid, obj_Id);
					 * loadListDataToDisplayValues(); displayValues();
					 * commitTrans(); } else { rollbackTrans(); }
					 * 
					 * aq.id(R.id.etcervixval).text("");
					 * aq.id(R.id.etdescentofheadval).text(""); // updated
					 * 28feb16 by Arpitha
					 * aq.id(R.id.etdilationcomments).text("");
					 * 
					 * aq.id(R.id.etcervixval).getEditText().requestFocus();
					 * 
					 * }
					 */} catch (Exception e) {
					// TODO: handle exception
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});
	}

	void saveData() throws Exception {
		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(strwuserId);

		ArrayList<String> adata = new ArrayList<String>();
		adata.add(aq.id(R.id.etcervixval).getText().toString());
		adata.add(aq.id(R.id.etdescentofheadval).getText().toString());
		// updated on 28feb16 by Arpitha
		adata.add("");
		adata.add(aq.id(R.id.etdilationcomments).getText().toString());

		ArrayList<String> prop_id = new ArrayList<String>();
		ArrayList<String> prop_name = new ArrayList<String>();
		Property_pojo pdata = new Property_pojo();
		Cursor cursor = dbh.getPropertyIds(obj_Id);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {

				prop_id.add(cursor.getString(0));
				prop_name.add(cursor.getString(1));

			} while (cursor.moveToNext());

			pdata.setObj_Id(obj_Id);
			pdata.setProp_id(prop_id);
			pdata.setProp_name(prop_name);
			pdata.setProp_value(adata);
			pdata.setStrdate(todaysDate);
			pdata.setStrTime(aq.id(R.id.etdilatationtime).getText().toString());

			// 05May2017 Arpitha - v2.6 - mantis id
			// - 0000240 and 0000238
			if (listValuesItems != null && listValuesItems.size() > 0) {
				int hours = Partograph_CommonClass.getHoursBetDates(strLastTime, todaysDate + "_" + strcurrTime);
				/*
				 * if
				 * ((Integer.parseInt(listValuesItems.get(listValuesItems.size()
				 * - 1).getDesc_of_head()) == Integer
				 * .parseInt(aq.id(R.id.etdescentofheadval).getText().toString()
				 * ) && hours >= 4) ||
				 * (Integer.parseInt(aq.id(R.id.etcervixval).getText().toString(
				 * )) == Integer
				 * .parseInt(listValuesItems.get(listValuesItems.size() -
				 * 1).getCervix()) && hours >= 2) ||
				 * (((Integer.parseInt(aq.id(R.id.etcervixval).getText().
				 * toString())) -
				 * (Integer.parseInt(listValuesItems.get(listValuesItems.size()
				 * - 1).getCervix()))) <= 2 && hours >= 2 &&
				 * Integer.parseInt(aq.id(R.id.etcervixval).getText().toString()
				 * ) > 7))
				 */
				if (Integer.parseInt(listValuesItems.get(listValuesItems.size() - 1).getDesc_of_head()) == Integer
						.parseInt(aq.id(R.id.etdescentofheadval).getText().toString()) && hours >= 4)
					pdata.setDescentofHeadDanger(1);// 12Jun2017 Arpitha
				else
					pdata.setDescentofHeadDanger(0);// 05May2017
				// Arpitha
				// -
				// v2.6
				// -
				// mantis
				// id
				// -
				// 0000240
				// and
				// 0000238
				// 12Jun2017 Arpitha
				if ((Integer.parseInt(aq.id(R.id.etcervixval).getText().toString()) == Integer
						.parseInt(listValuesItems.get(listValuesItems.size() - 1).getCervix()) && hours >= 2)
						|| (((Integer.parseInt(aq.id(R.id.etcervixval).getText().toString()))
								- (Integer.parseInt(listValuesItems.get(listValuesItems.size() - 1).getCervix()))) <= 2
								&& hours >= 2 && Integer.parseInt(aq.id(R.id.etcervixval).getText().toString()) > 7)
						|| (Integer.parseInt(aq.id(R.id.etdescentofheadval).getText().toString()) == Integer
								.parseInt(listValuesItems.get(listValuesItems.size() - 1).getDesc_of_head())
								&& hours >= 2))
					pdata.setCervixDanger(1);
				else
					pdata.setCervixDanger(0);// 12Jun2017 Arpitha

			}

			boolean isAdded = dbh.insertData(pdata, strWomenid, strwuserId, transId);

			if (isAdded) {

				dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
				// 17Oct2016 Arpitha
				if (listValuesItems.size() <= 0) {
					dbh.addtonotification(strwuserId, strWomenid, obj_Id);
				} else
					dbh.updatenotification(strWomenid, obj_Id);
				loadListDataToDisplayValues();
				displayValues();
				commitTrans();
			} else {
				rollbackTrans();
			}

			aq.id(R.id.etcervixval).text("");
			aq.id(R.id.etdescentofheadval).text("");
			// updated 28feb16 by Arpitha
			aq.id(R.id.etdilationcomments).text("");
			// aq.id(R.id.ethoursval).text("");

			aq.id(R.id.etcervixval).getEditText().requestFocus();
		}
	}

}
