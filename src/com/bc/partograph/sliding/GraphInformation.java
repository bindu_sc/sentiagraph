package com.bc.partograph.sliding;

import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.usermanual.UseManual;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class GraphInformation extends Activity {

	TableLayout tblcolorcodedValues;// 08May2017 Arpitha - v2.6
	Partograph_DB dbh;
	String[] prop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.graph_info);

			// get the action bar
			ActionBar actionBar = getActionBar();

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);

			tblcolorcodedValues = (TableLayout) findViewById(R.id.tblcolorcodedvalues);// 08May2017
																						// Arpitha
																						// -
																						// v2.6

			dbh = Partograph_DB.getInstance(getApplicationContext());

			prop = new String[] { "1", "2.1", "3.1", "3.2", "4.1", "4.2", "7.1", "7.2", "7.3", "8.1", "11.1", "11.8",
					"12.1", "10" };

			dispalyValues();// 08May2017 Arpitha - v2.6

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** This method Calls the idle timeout *//*
												 * @Override public void
												 * onUserInteraction() {
												 * super.onUserInteraction();
												 * KillAllActivitesAndGoToLogin.
												 * delayedIdle(Integer.parseInt(
												 * Partograph_CommonClass.
												 * properties.getProperty(
												 * "idleTimeOut"))); }
												 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:
				Intent home = new Intent(this, Activity_WomenView.class);
				startActivity(home);
				return true;

			case R.id.about:
				Intent about = new Intent(this, About.class);
				startActivity(about);
				return true;
			// 10May2017 Arpitha - v2.6
			// case R.id.info:
			// Intent info = new Intent(getApplicationContext(),
			// GraphInformation.class);
			// startActivity(info);
			//
			// return true;

			case R.id.settings:

				Intent settings = new Intent(getApplicationContext(), Settings_parto.class);
				startActivity(settings);

				return true;

			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(GraphInformation.this,
						Partograph_CommonClass.user.getUserId());
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.summary:

				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);

				return true;
			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(GraphInformation.this);
				return true;

			case R.id.usermanual:

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);// 10May2017 Arpitha - v2.6
				return true;
			case R.id.disch:
				Intent disch = new Intent(GraphInformation.this, DischargedWomanList_Activity.class);
				startActivity(disch);
				return true;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// menu.findItem(R.id.help).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.delete).setVisible(false);

		// menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.info).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);
		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
			// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	// 17Nov2016 Arpitha
	@Override
	public void onBackPressed() {
		try {
			Intent i = new Intent(GraphInformation.this, Activity_WomenView.class);
			startActivity(i);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// 08May2017 Arpitha - v2.6
	void dispalyValues() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		try {

			tblcolorcodedValues.removeAllViews();

			TableRow trlabel = new TableRow(getApplicationContext());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView textparameter = new TextView(getApplicationContext());
			textparameter.setTextSize(18);
			textparameter.setPadding(10, 10, 10, 10);
			textparameter.setTextColor(getResources().getColor(R.color.black));
			textparameter.setTextSize(18);
			textparameter.setGravity(Gravity.LEFT);
			textparameter.setText(getResources().getString(R.string.parameters));

			trlabel.addView(textparameter);

			TextView textred = new TextView(getApplicationContext());
			textred.setTextSize(18);
			textred.setPadding(10, 10, 10, 10);
			textred.setTextColor(getResources().getColor(R.color.black));
			textred.setBackgroundColor(getResources().getColor(R.color.red));
			textred.setGravity(Gravity.CENTER);
			trlabel.addView(textred);

			TextView txt = new TextView(getApplicationContext());
			txt.setText("a");
			txt.setTextSize(2);
			txt.setPadding(10, 10, 10, 10);
			txt.setTextColor(getResources().getColor(R.color.white));
			trlabel.addView(txt);

			TextView textgreen = new TextView(getApplicationContext());
			textgreen.setTextSize(18);
			textgreen.setPadding(10, 10, 10, 10);
			textgreen.setTextColor(getResources().getColor(R.color.black));
			textgreen.setTextSize(18);
			textgreen.setGravity(Gravity.LEFT);
			textgreen.setBackgroundColor(getResources().getColor(R.color.green));
			trlabel.addView(textgreen);

			TextView txt1 = new TextView(getApplicationContext());
			txt1.setText("a");
			txt1.setTextSize(2);
			txt1.setPadding(10, 10, 10, 10);
			txt1.setTextColor(getResources().getColor(R.color.white));
			trlabel.addView(txt1);

			TextView textamber = new TextView(getApplicationContext());
			textamber.setTextSize(18);
			textamber.setPadding(10, 10, 10, 10);
			textamber.setTextColor(getResources().getColor(R.color.black));
			textamber.setTextSize(18);
			textamber.setGravity(Gravity.LEFT);
			textamber.setBackgroundColor(getResources().getColor(R.color.amber));
			trlabel.addView(textamber);

			for (int i = 0; i < prop.length; i++) {
				Cursor cur = dbh.getColorCodedValuesFromTable(prop[i]);
				cur.moveToFirst();

				TableRow trvalues = new TableRow(getApplicationContext());
				trvalues.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView textparanames = new TextView(getApplicationContext());
				textparanames.setTextSize(18);
				textparanames.setPadding(10, 10, 10, 10);
				textparanames.setTextColor(getResources().getColor(R.color.black));
				textparanames.setTextSize(18);
				textparanames.setMaxWidth(200);
				textparanames.setGravity(Gravity.LEFT);
				textparanames.setText(cur.getString(3));
				trvalues.addView(textparanames);

				TextView textredval = new TextView(getApplicationContext());
				textredval.setTextSize(18);
				textredval.setMaxWidth(200);
				textredval.setPadding(10, 10, 10, 10);
				textredval.setTextColor(getResources().getColor(R.color.black));
				textredval.setTextSize(18);
				String redVal = cur.getString(4);
				if (redVal.length() > 15)
					redVal = redVal.substring(0, (redVal.length() / 2) + 1) + "\n"
							+ redVal.substring((redVal.length() / 2) + 1);
				textredval.setText(redVal);
				textredval.setGravity(Gravity.LEFT);
				trvalues.addView(textredval);

				TextView txtval = new TextView(getApplicationContext());
				txtval.setText("a");
				txtval.setTextSize(2);
				txtval.setPadding(10, 10, 10, 10);
				txtval.setMaxWidth(200);
				txtval.setTextColor(getResources().getColor(R.color.white));
				trvalues.addView(txtval);

				TextView textgreenval = new TextView(getApplicationContext());
				textgreenval.setTextSize(18);
				textgreenval.setPadding(10, 10, 10, 10);
				textgreenval.setTextColor(getResources().getColor(R.color.black));
				textgreenval.setTextSize(18);
				textgreenval.setMaxWidth(200);
				textgreenval.setGravity(Gravity.LEFT);

				textgreenval.setText(cur.getString(6));
				trvalues.addView(textgreenval);

				TextView txtval1 = new TextView(getApplicationContext());
				txtval1.setText("a");
				txtval1.setTextSize(2);
				txtval1.setMaxWidth(200);
				txtval1.setPadding(10, 10, 10, 10);
				txtval1.setTextColor(getResources().getColor(R.color.white));
				trvalues.addView(txtval1);

				TextView textamberva = new TextView(getApplicationContext());
				textamberva.setTextSize(18);
				textamberva.setPadding(10, 10, 10, 10);
				textamberva.setTextColor(getResources().getColor(R.color.black));
				textamberva.setTextSize(18);
				textamberva.setMaxWidth(200);
				textamberva.setGravity(Gravity.LEFT);

				String val;
				val = cur.getString(5);
				if (val.length() > 15)
					val = cur.getString(5).substring(0, cur.getString(5).length() / 2) + "\n"
							+ cur.getString(5).substring(cur.getString(5).length() / 2);

				if (val.length() > 50)
					val = cur.getString(5).substring(0, (cur.getString(5).length() / 2) - 25) + "\n"
							+ cur.getString(5).substring((cur.getString(5).length() / 2) - 25,
									(cur.getString(5).length() / 2))
							+ "\n"
							+ cur.getString(5).substring(cur.getString(5).length() / 2,
									(cur.getString(5).length() / 2) + 20)
							+ "\n" + cur.getString(5).substring((cur.getString(5).length() / 2) + 20);

				textamberva.setText(val);
				trvalues.addView(textamberva);

				if (i == 0) {
					tblcolorcodedValues.addView(trlabel);
					View view1 = new View(getApplicationContext());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tblcolorcodedValues.addView(view1);
				}

				tblcolorcodedValues.addView(trvalues);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}
}
