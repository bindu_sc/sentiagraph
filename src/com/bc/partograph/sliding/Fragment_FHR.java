package com.bc.partograph.sliding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_FHR extends Fragment implements OnClickListener {

	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	/*
	 * ArrayList<Women_Profile_Pojo> rowItems; int pos = -1;
	 */
	public static ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl, tblComments;
	GraphicalView mChartView = null;
	Thread myThread;

	int obj_Id;
	String obj_Type;

	ArrayList<Add_value_pojo> arrComments;
	View rootView;
	// Changed on 27Mar2015
	String displaydateformat;
	public static String strLastTime, strCurrentTime;
	public static boolean isValidTime = false;
	Add_value_pojo valpojo;
	TextView txtdialogtitle;
	// updated by ARpitha
	String womenid;
	// boolean fhr;
	String userid;
	String lastdate;
	String lasttime;
	String lastentrytime;

	int fhrval;

	String newTime;
	String newtime_12hrs;
	Women_Profile_Pojo woman;// 03Nov2016

	String Red, Amber, Green;// 05May2017 Arpitha - v_2.6
	// 05May2017 Arpitha - v_2.6
	int fhramber1, fhramber2, fhramber3, fhramber4, fhrred2, fhrred1;

	public Fragment_FHR() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_fhr, container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			womenid = woman.getWomenId();
			userid = woman.getUserId();
			initializeScreen(aq.id(R.id.rlfhr).getView());

			initialView();

			myThread = null;
			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();

			setInputFiltersForEdittext();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	// OnClick
	@Override
	public void onClick(View v) {

		try {
			// 25Sep2016 Arpitha - addToTrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (v.getId()) {
			case R.id.btnsavefhr:

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					Date lastregdate = null;
					String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
							getResources().getString(R.string.partograph));
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (strlastinserteddate != null) {
						lastregdate = format.parse(strlastinserteddate);
					}
					Date currentdatetime = new Date();
					if (lastregdate != null && currentdatetime.before(lastregdate) && (!LoginActivity.istesting)) {
						Partograph_CommonClass.showSettingsAlert(getActivity(),
								getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
					} else {// 27Sep2016 Arpitha

						if (aq.id(R.id.etfhrval).getEditText().getText().toString().trim().length() > 0) {
							fhrval = Integer.parseInt(aq.id(R.id.etfhrval).getText().toString());

							strCurrentTime = todaysDate + "_" + aq.id(R.id.etfhrtime).getText().toString();

							isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

							// 26jan2016
							if (listValuesItems.size() <= 0) {
								isValidTime = true;
							}

							// updated on 20july2016 by Arpitha
							if (!isValidTime) {

								isValidTime = istimevalid();
							}

							if (isValidTime) {

								valpojo = new Add_value_pojo();
								valpojo.setStrVal(aq.id(R.id.etfhrval).getText().toString());
								valpojo.setFhrcomments(aq.id(R.id.etfhrcomments).getText().toString());
								valpojo.setStrTime(aq.id(R.id.etfhrtime).getText().toString());
								valpojo.setStrdate(todaysDate);
								valpojo.setObj_Id(obj_Id);
								valpojo.setObj_Type(obj_Type);

								// if (fhrval >= 180 || fhrval < 100)
								if (fhrval >= fhrred1 || fhrval < fhrred2)// 08May2017
																			// Arpitha
																			// -
																			// v2.6
									valpojo.setFhrDanger(1);// 12Jun2017 Arpitha
								// valpojo.setIsDanger(1);
								else
									valpojo.setFhrDanger(0);// 12Jun2017 Arpitha

								if (fhrval == 0) {
									displayAlertDialog(getResources().getString(R.string.invalidval_val));
									aq.id(R.id.etfhrval).getEditText().requestFocus();
								} else {
									// 05May2017 Arpitha - v2.6 mantis
									// id-0000233 - changed value from 180 to
									// 160
									if (fhrval >= 160 || fhrval < 100) {

										displayConfirmationAlert(getResources().getString(R.string.fhr_val_is) + fhrval
												+ "," + getResources().getString(R.string.bp_val), "confirmation");
										aq.id(R.id.etfhrval).getEditText().requestFocus();
									} else {

										dbh.db.beginTransaction();
										int transId = dbh.iCreateNewTrans(strwuserId);

										boolean isAdded = dbh.insertpropertyvalues(valpojo,
												Partograph_DB.TBL_PROPERTYVALUES, strWomenid, strwuserId, transId);

										if (isAdded) {
											dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
											dbh.updatenotification(strWomenid, obj_Id);// 18Oct2016
																						// Arpitha
											loadListDataToDisplayValues();
											displayValues(tbl);
											commitTrans();
										} else
											rollbackTrans();

										aq.id(R.id.etfhrval).text("");
										aq.id(R.id.etfhrcomments).text("");
									}
								}

							} else {

								// updated on 15july2016 by Arpitha
								if (lastentrytime != null) {
									newTime = Partograph_CommonClass.getnewtime(lastentrytime, 30);
									newtime_12hrs = Partograph_CommonClass.gettimein12hrformat(newTime);
									aq.id(R.id.etfhrval).text(""); // 11jan2016
									aq.id(R.id.etfhrcomments).text("");
									Partograph_CommonClass.displayAlertDialog(
											getResources().getString(R.string.fhr_duration) + " " + newtime_12hrs + " "
													+ getResources().getString(R.string.on),
											getActivity());
								}

							}
						} else {
							aq.id(R.id.etfhrval).getEditText().requestFocus(); // 11jan2016
							displayAlertDialog(getResources().getString(R.string.entr_val));

						}
					}
				}
				break;

			case R.id.btnviewgraphfhr: {

				loadListDataToDisplayValues();
				if (listValuesItems.size() > 0) {
					showGraphDialog();

				} else

					displayAlertDialog(getResources().getString(R.string.nodata));

			}
				break;
			case R.id.fhrinfo: {
				displayFhrinfo();
			}
				break;
			case R.id.imgbtnnotification: {
				notification(womenid);
			}

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void showGraphDialog() throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			dialog.setContentView(R.layout.dialog_fhr);
			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			dialog.getWindow().setBackgroundDrawableResource(R.color.white);

			txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(getResources().getString(R.string.fetal_heart_rate));

			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			createChart(dialog);
			// TableLayout tbl = (TableLayout)
			// dialog.findViewById(R.id.tblvaluesfhrdialog);
			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		aq.id(R.id.imgbtnnotification).invisible();
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());

		if (fhrval > 160 || fhrval < 100) {
			aq.id(R.id.txttimeconstraint).visible();
		}

		aq.id(R.id.txtnodata).gone();// 27Sep2016 Arpitha

		calSyncMtd();

		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();

	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etfhrtime).text(curTime);
						displayComments();

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {

					doWork();
					Thread.sleep(1000); // Pause of 1 Second

				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					Thread.currentThread().interrupt();
					myThread.stop();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					myThread.stop();
				}
			}
		}
	}

	private void loadListDataToDisplayValues() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// updated on 28feb16 by Arpitha
		listValuesItems = dbh.getFHRData_display(obj_Id, strWomenid, strwuserId);

		// listValuesItems = dbh.getDataToDisplay(strWomenid, strwuserId,
		// obj_Id);

		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 27Sep2016 Arpitha
		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues(tbl);
		}
	}

	private void displayValues(TableLayout tbl) throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			// TableRow trlabel = (TableRow) findViewById(R.id.tableRow1);
			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);

			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);

			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);

			trlabel.addView(textval1lbl);

			TextView textcolor = new TextView(getActivity());
			textcolor.setTextSize(18);
			textcolor.setPadding(10, 10, 10, 10);
			textcolor.setTextColor(getResources().getColor(R.color.black));
			textcolor.setTextSize(18);
			textcolor.setGravity(Gravity.CENTER);

			trlabel.addView(textcolor);

			TextView textcomment = new TextView(getActivity());
			textcomment.setTextSize(18);
			textcomment.setText(getResources().getString(R.string.comments));
			textcomment.setPadding(10, 10, 10, 10);
			textcomment.setTextColor(getResources().getColor(R.color.black));
			textcomment.setTextSize(18);
			textcomment.setGravity(Gravity.LEFT);

			trlabel.addView(textcomment);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));
			textcolor.setText("");

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);

				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);

				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
					lastentrytime = listValuesItems.get(i).getStrTime();
				}

				String val1 = "";

				val1 = listValuesItems.get(i).getStrVal();
				textval1lbl.setText(getResources().getString(R.string.txtvallbl));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);

				tr.addView(txtvalue1);

				textcolor.setText(getResources().getString(R.string.color));

				TextView txtColor = new TextView(getActivity());
				txtColor.setPadding(5, 5, 5, 5);
				txtColor.setGravity(Gravity.CENTER);
				txtColor.setWidth(20);
				txtColor.setHeight(20);
				//
				// if (listValuesItems.get(i).getStrVal() != null) {
				// int value =
				// Integer.parseInt(listValuesItems.get(i).getStrVal());
				// if (value >= 160 && value < 180 || (value >= 100 && value <
				// 110)) {
				// txtColor.setBackgroundColor(getResources().getColor(R.color.amber));
				// } else if (value >= 180 || value < 100) {
				// txtColor.setBackgroundColor(getResources().getColor(R.color.red));
				// } else
				// txtColor.setBackgroundColor(getResources().getColor(R.color.green));
				//
				// tr.addView(txtColor);
				// }

				// 05May2017 Arpitha - v_2.6
				if (listValuesItems.get(i).getStrVal() != null) {
					int value = Integer.parseInt(listValuesItems.get(i).getStrVal());
					if (value >= fhramber1 && value < fhramber2 || (value >= fhramber3 && value < fhramber4)) {
						txtColor.setBackgroundColor(getResources().getColor(R.color.amber));
						// } else if (value >= 180 || value < 100) {
					} else if (value >= fhrred1 || value < fhrred2) {
						txtColor.setBackgroundColor(getResources().getColor(R.color.red));
					} else
						txtColor.setBackgroundColor(getResources().getColor(R.color.green));

					tr.addView(txtColor);
				} // 05May2017 Arpitha - v_2.6

				// updated on 28feb16 by Arpitha
				TextView txtcommentvalue = new TextView(getActivity());
				txtcommentvalue.setText(listValuesItems.get(i).getFhrcomments());
				txtcommentvalue.setTextSize(18);
				txtcommentvalue.setPadding(10, 10, 10, 10);
				txtcommentvalue.setTextColor(getResources().getColor(R.color.black));
				txtcommentvalue.setTextSize(18);
				txtcommentvalue.setGravity(Gravity.CENTER);

				tr.addView(txtcommentvalue);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}

	}

	// Create the graph
	protected void createChart(Dialog dialog) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// listValuesItems = dbh.getFHRData1(obj_Id, strWomenid,
		// strwuserId);//commented on 12Oct2016 - Arpitha
		listValuesItems = dbh.getFHRGraphData(obj_Id, strWomenid, strwuserId);// on
																				// 12Oct2016
																				// -
																				// Arpitha
		if (listValuesItems.size() > 0) {
			String yLbl = getResources().getString(R.string.fhr);

			// Creating an XYSeries for values
			XYSeries valueseries = new XYSeries("Values");

			for (int i = 0; i < listValuesItems.size(); i++) {
				valueseries.add(i + 1, Double.parseDouble(listValuesItems.get(i).getStrVal()));

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(valueseries);

			// Creating XYSeriesRenderer to customize valueSeries
			XYSeriesRenderer valueRenderer = new XYSeriesRenderer();
			valueRenderer.setColor(Color.BLACK);
			valueRenderer.setPointStyle(PointStyle.CIRCLE);
			valueRenderer.setFillPoints(true);
			valueRenderer.setLineWidth(4);
			valueRenderer.setDisplayChartValues(true);
			valueRenderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);

			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(yLbl);
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 60, 80, 60, 60 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(25);
			multiRenderer.setPointSize(5);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, true);
			multiRenderer.setYAxisMax(200);

			// updated on 17Nov2015 - YAxisMin from 80 to 40
			multiRenderer.setYAxisMin(40);
			multiRenderer.setZoomButtonsVisible(true);

			multiRenderer.setGridColor(getResources().getColor(R.color.lightgray));
			multiRenderer.setYLabels(13);
			multiRenderer.setFitLegend(true);
			multiRenderer.setShowGrid(true);
			multiRenderer.setAntialiasing(true);
			multiRenderer.setInScroll(true);
			multiRenderer.setShowGridX(true);
			multiRenderer.setShowGridY(true);

			multiRenderer.setBarSpacing(.5);
			multiRenderer.setXLabelsAngle(45f);

			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);

			for (int i = 0; i < listValuesItems.size(); i++) {
				multiRenderer.addXTextLabel(i + 1, listValuesItems.get(i).getStrTime());
				multiRenderer.setYAxisMax(300);
				multiRenderer.setYLabels(15);
			}

			multiRenderer.addSeriesRenderer(valueRenderer);

			mChartView = null;
			mChartView = ChartFactory.getLineChartView(getActivity(), dataset, multiRenderer);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT, 400);
			mChartView.setLayoutParams(params);

			RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.rlfetalheartratechart);
			layout.setBackground(getResources().getDrawable(R.drawable.dprounded_corner_layout));
			layout.addView(mChartView);

			if (mChartView != null) {
				mChartView.repaint();
			}
		}
	}

	// Display Comments
	private void displayComments() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());

		}

		if (arrComments.size() > 0) {
			// Save ListView state
			Parcelable state = aq.id(R.id.listCommentsfhr).getListView().onSaveInstanceState();
			aq.id(R.id.listCommentsfhr).visible();
			aq.id(R.id.txtCommentsfhr).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_fhr, arrComments);
			aq.id(R.id.listCommentsfhr).adapter(adapter);

			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentsfhr).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentsfhr).getListView());
		} else {
			aq.id(R.id.listCommentsfhr).gone();
			aq.id(R.id.txtCommentsfhr).gone();
		}

	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.etfhrtime).text(strcurrTime);
		aq.id(R.id.etfhrdate).text(displaydateformat);

		aq.id(R.id.etfhrtime).enabled(false);
		aq.id(R.id.etfhrdate).enabled(false);

		aq.id(R.id.rlfetalheartratechart).gone();

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluesfhr);
		tbl.setVisibility(View.GONE);

		tblComments = (TableLayout) rootView.findViewById(R.id.tblcommentsfhr);
		tblComments.setVisibility(View.GONE);

		aq.id(R.id.txtnodata).gone();// 27Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha

		// Disable btnsave if the woman is delivered

		disableAddValues();

		aq.id(R.id.etfhrtime).text(Partograph_CommonClass.getCurrentTime());

		obj_Type = getResources().getString(R.string.object1);
		obj_Id = dbh.getObjectID(obj_Type);

		// 05May2017 Arpitha - v_2.6
		Cursor cur = dbh.getColorCodedValuesFromTable("1");
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			Red = cur.getString(4);
			Amber = cur.getString(5);
			Green = cur.getString(6);
		

		// 05May2017 Arpitha - v_2.6
		fhramber1 = Integer.parseInt(Amber.split("(or) | (and)")[0].split("(>=)|(<=)|(<)|(>)")[1].trim());
		fhramber2 = Integer.parseInt(Amber.split("(or) | (and)")[1].split("(>=)|(<=)|(<)|(>)")[1].trim());
		fhramber3 = Integer.parseInt(Amber.split("(or) | (and)")[2].split("(>=)|(<=)|(<)|(>)")[1].trim());
		fhramber4 = Integer.parseInt(Amber.split("(or) | (and)")[3].split("(>=)|(<=)|(<)|(>)")[1].trim());

		fhrred1 = Integer.parseInt(Red.split("(or) | (and)")[0].split("(>=)|(<=)|(<)|(>)")[1].trim());
		fhrred2 = Integer.parseInt(Red.split("(or) | (and)")[1].split("(>=)|(<=)|(<)|(>)")[1].trim());// 05May2017
																										// Arpitha
																										// -
																										// v_2.6
		
		} // 05May2017 Arpitha - v_2.6

		loadListDataToDisplayValues();
		displayValues(tbl);

		displayComments();

		if (woman.getDel_type() != 0) {
			aq.id(R.id.txttimeconstraint).gone();

		} else {
			istimevalid();
		}
		lastdate = dbh.lastvaldate(strWomenid, obj_Id);
		lasttime = dbh.lastvaltime(strWomenid, obj_Id, lastdate);

		updatetblComments(strwuserId, strWomenid, obj_Id);
		aq.id(R.id.imgbtnnotification).invisible();
		//08Oct2017 Arpitha - 
		if (SlidingActivity.isfhrtimevalid && !(SlidingActivity.arrVal.size()>0)) {
			aq.id(R.id.imgbtnnotification).visible();
		} else
			aq.id(R.id.imgbtnnotification).invisible();

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha
		
//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));

	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.fhrcmt_cnt = 0;
		SlidingActivity.isMsgFHR = false; // 4Jan2016
	}

	// Add values layout visibility gone
	private void disableAddValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		aq.id(R.id.txttimeconstraint).gone();
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// if (rowItems.get(pos).getDel_type() != 0 ||
		// (SlidingActivity.isReferred)) {
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
//		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
//		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2 || SlidingActivity.arrVal.size()>0) {
			aq.id(R.id.llfhraddval).gone();
			aq.id(R.id.txttimeconstraint).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {
			aq.id(R.id.llfhraddval).visible();
			// istimevalid();

		}
	}

	// Refresh the menu item - 21dec2015

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		imgbtnyes.setText(getResources().getString(R.string.save));
		imgbtnno.setText(getResources().getString(R.string.recheck));
		// String mess = exit_msg;

		txtdialog1.setText("" + fhrval);
		txtdialog.setText(getResources().getString(R.string.fhr_val_is));

		txtdialog2.setText(getResources().getString(R.string.bp_val));
		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase("confirm")) {
					aq.id(R.id.etfhrval).getEditText().requestFocus();

				}

			}
		});

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				try {
					dbh.db.beginTransaction();
					int transId = dbh.iCreateNewTrans(strwuserId);

					boolean isAdded = dbh.insertpropertyvalues(valpojo, Partograph_DB.TBL_PROPERTYVALUES, strWomenid,
							strwuserId, transId);

					if (isAdded) {
						dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
						dbh.updatenotification(strWomenid, obj_Id);// 18Oct2016
																	// Arpitha
						loadListDataToDisplayValues();
						displayValues(tbl);
						commitTrans();
					} else
						rollbackTrans();

					aq.id(R.id.etfhrval).text("");
					aq.id(R.id.etfhrcomments).text("");
				} catch (Exception e) {
					// TODO: handle exception
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});
	}

	// to dispaly information for FHR parameter
	public void displayFhrinfo() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.setTitle(getResources().getString(R.string.fetal_heart_rate));
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.fhr_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.fetal_heart_rate));

		TextView txtredval = (TextView) dialog.findViewById(R.id.greenval2);// 11may2017
																			// Arpitha
																			// -
																			// v2.6
		txtredval.setText(Red);// 11may2017 Arpitha- v2.6
		TextView txtgreenval = (TextView) dialog.findViewById(R.id.greenval4);// 11may2017
																				// Arpitha-
																				// v2.6
		txtgreenval.setText(Green);// 11may2017 Arpitha- v2.6
		TextView txtamberval = (TextView) dialog.findViewById(R.id.amberval9);// 11may2017
																				// Arpitha-
																				// v2.6
		txtamberval.setText(Amber.split("or")[0] + "\n" + Amber.split("or")[1]);// 11may2017
																				// Arpitha-
																				// v2.6

		TableRow tbl1 = (TableRow) dialog.findViewById(R.id.fhrduration);
		TableRow tbl2 = (TableRow) dialog.findViewById(R.id.fhrduration1);
		TableRow tbl3 = (TableRow) dialog.findViewById(R.id.fhrduration2);
		TableRow tbl4 = (TableRow) dialog.findViewById(R.id.fhrparameter);

		View view = dialog.findViewById(R.id.view1);
		view.setVisibility(View.VISIBLE);
		View view2 = dialog.findViewById(R.id.view4);
		view2.setVisibility(View.VISIBLE);

		tbl1.setVisibility(View.VISIBLE);
		tbl2.setVisibility(View.VISIBLE);
		tbl3.setVisibility(View.VISIBLE);
		tbl4.setVisibility(View.VISIBLE);

		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		dialog.show();

	}

	// to display notification
	public void notification(String womenid) throws Exception

	{
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		// 9Sep2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.notification_dialog);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.fetal_heart_rate));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		TextView txt = (TextView) dialog.findViewById(R.id.textView1);
		String convertedlastdate = Partograph_CommonClass.getConvertedDateFormat(lastdate, "dd-MM-yyyy");
		txt.setText(getResources().getString(R.string.fhr_last_entry) + " " + convertedlastdate + " " + lasttime + " "
				+ getResources().getString(R.string.was_at));
		dialog.show();

	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		aq.id(R.id.etfhrcomments).getEditText().setFilters(new InputFilter[] { filter , new InputFilter.LengthFilter(200)});

	}

	boolean istimevalid() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (listValuesItems != null && listValuesItems.size() > 0) {
			for (int i = 0; i < listValuesItems.size(); i++) {

				String str = listValuesItems.get(i).getStrVal();
				if ((Integer.parseInt(str)) < 100 || (Integer.parseInt(str)) > 160) {
					isValidTime = true;
					aq.id(R.id.txttimeconstraint).visible();
				}
			}
		} else {
			aq.id(R.id.txttimeconstraint).invisible();
		}
		return isValidTime;
	}

}
