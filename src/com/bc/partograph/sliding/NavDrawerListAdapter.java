package com.bc.partograph.sliding;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bluecrimson.partograph.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	int count = 0;

	public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.drawer_list_item, null);
		}

		// ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
		ImageView imgdanger = (ImageView) convertView.findViewById(R.id.imgdanger);

		// updated on 7july2016 by Arpitha
		ImageView imgnotify = (ImageView) convertView.findViewById(R.id.imgnotification);
		imgnotify.setVisibility(View.GONE);

		// imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
		txtTitle.setText(navDrawerItems.get(position).getTitle());

		int pos = SlidingActivity.partovaluemap.get(navDrawerItems.get(position).getTitle());// 02Nov2016

		try {
			getCount(pos);

			// displaying count
			// check whether it set visible or not
			if (navDrawerItems.get(position).getCounterVisibility()) {

				if (count > 0)
					txtCount.setText("" + count);
				// else
				// hide the counter view
				// txtCount.setVisibility(View.GONE);
			}

			if (SlidingActivity.dangerList != null && SlidingActivity.dangerList.size() > 0) {
				if (SlidingActivity.dangerList.contains(pos + 1))
					imgdanger.setVisibility(View.VISIBLE);
				else
					imgdanger.setVisibility(View.GONE);
			} else
				imgdanger.setVisibility(View.GONE);

			if (SlidingActivity.arrVal.size() <= 0) {//23Oct2017 Arpitha - nt to display icon for discharged woman
				// updated on 7july2016 by Arpitha
				if ((pos == 0 && SlidingActivity.isfhrtimevalid) || (pos == 2 && SlidingActivity.isdilatationtimevalid)
						|| (pos == 3 && SlidingActivity.iscontractiontimevalid)
						|| (pos == 6 && SlidingActivity.ispulsetimevalid)) {
					imgnotify.setVisibility(View.VISIBLE);
				}
			} else
				imgnotify.setVisibility(View.INVISIBLE);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return convertView;
	}

	private void getCount(int position) throws Exception {
		switch (position) {
		// fhr
		case 0:
			count = Partograph_CommonClass.fhrcmt_cnt;
			break;

		// afm
		case 1:
			count = Partograph_CommonClass.afmcmt_cnt;
			break;

		case 2:
			count = Partograph_CommonClass.dilcmt_cnt;
			break;

		case 3:
			count = Partograph_CommonClass.contcmt_cnt;
			break;

		case 4:
			count = Partograph_CommonClass.oxycmt_cnt;
			break;

		case 5:
			count = Partograph_CommonClass.drugscmt_cnt;
			break;

		case 6:
			count = Partograph_CommonClass.pbpcmt_cnt;
			break;

		case 7:
			count = Partograph_CommonClass.tempcmt_cnt;
			break;

		case 8:
			count = Partograph_CommonClass.utcmt_cnt;
			break;

		case 9:
			count = 0;
			break;

		// 29Dec2016 Arpitha
		case 10:
			count = 0;
			break;

		case 11:
			count = 0;
			break;

		case 12:
			count = 0;
			break;
		// 11May2017 Arpitha
		case 13:
			count = 0;
			break;
		default:
			break;
		}
	}

}
