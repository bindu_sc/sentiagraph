package com.bc.partograph.sliding;

import java.util.ArrayList;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Dialog;
import android.app.Fragment;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Fragment_AFM extends Fragment implements OnClickListener {
	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	// ArrayList<Add_value_pojo> afmData;
	ArrayList<Add_value_pojo> arrComments;
	String stramnioticfluidval, strmouldingval;
	// Changed on 27Mar2015
	String displaydateformat;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	// 04OCt2016 Arpitha
	TextView txtdialogtitle;
	Women_Profile_Pojo woman;// 03Nov2016
	String Red, Amber, Green;// 05May2017 Arpitha - v_2.6
	// ArrayList<DiscgargePojo> arrVal;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		try {// 30August2016 Arpitha
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			rootView = inflater.inflate(R.layout.fragment_afm, container, false);
			// updated on 19Nov2015

		} catch (Exception e)// 30August2016 Arpitha
		{
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());

			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			initializeScreen(aq.id(R.id.rlafm).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();
			setInputFiltersForEdittext();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	// OnClick
	@Override
	public void onClick(View v) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {
			case R.id.btnsaveafm:
				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					if (Partograph_CommonClass.CheckAutomaticDateTime(strwuserId,
							getResources().getString(R.string.partograph), getActivity())) {// 27Sep2016
																				// Arpitha
						if (stramnioticfluidval.trim().length() > 0 || strmouldingval.trim().length() > 0) {

							strCurrentTime = todaysDate + "_" + aq.id(R.id.etafmtime).getText().toString();

							isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

							if (isValidTime) {
								setAfmData();
							}
							// updated by Arpitha on 10May2016

							else {
								displayConfirmationAlert(getResources().getString(R.string.before_half_an_hour));
							}
						} else {
							Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_val),
									getActivity());
						}
					}
				}

				break;

			case R.id.afminfo: {
				displayAFMinfo();
			}
				break;

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	// /** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.etafmtime).text(strcurrTime);
		aq.id(R.id.etafmdate).text(displaydateformat);

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluesafm);
		tbl.setVisibility(View.GONE);

		obj_Type = getResources().getString(R.string.object2);
		obj_Id = dbh.getObjectID(obj_Type);

		// 05May2017 Arpitha - v_2.6
		Cursor cur = dbh.getColorCodedValuesFromTable("2.1");
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			Red = cur.getString(4);
			Amber = cur.getString(5);
			Green = cur.getString(6);
		} // 05May2017 Arpitha - v_2.6

		// arrVal = new ArrayList<DiscgargePojo>();
		// arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());

		loadListDataToDisplayValues();

		// Disable btnsave if the woman is delivered
		disableAddValues();

		displayComments();

		// Partograph_CommonClass.afmcmt_cnt = 0;
		updatetblComments(strwuserId, strWomenid, obj_Id);

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha

//		15Aug2017 Arpitha
		if(SlidingActivity.arrVal.size()>0)
		aq.id(R.id.txtnodata)
				.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));

	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.afmcmt_cnt = 0;
		SlidingActivity.isMsgAFM = false; // 04Jan2016
	}

	// Display Comments
	private void displayComments() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());
		}
		if (arrComments.size() > 0) {
			Parcelable state = aq.id(R.id.listCommentsafm).getListView().onSaveInstanceState();

			aq.id(R.id.txtCommentsafm).visible();
			aq.id(R.id.listCommentsafm).visible();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_afm, arrComments);
			aq.id(R.id.listCommentsafm).adapter(adapter);

			// 18dec2015
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentsafm).getListView().onRestoreInstanceState(state);

			Helper.getListViewSize(aq.id(R.id.listCommentsafm).getListView());

		} else {
			aq.id(R.id.listCommentsafm).gone();
			aq.id(R.id.txtCommentsafm).gone();
		}

	}

	// Add values layout visibility gone
	private void disableAddValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			// 23Sep2016 Arpitha - make invisible when woman is referred
			// 16Oct2016 Arpitha - don't allow to enter if if it retrospective
			// case

			if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2
					|| SlidingActivity.arrVal.size() > 0) {
				aq.id(R.id.llafmaddval).gone();
				aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

				if (listValuesItems.size() <= 0)// 31dec2016 Arpitha
				{
					aq.id(R.id.txtnodata).visible();
				} // 31dec2016 Arpitha

			} else {

				aq.id(R.id.llafmaddval).visible();
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public void doWork() throws Exception {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etafmtime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
				}
			}
		}
	}

	private void loadListDataToDisplayValues() throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		listValuesItems = dbh.getAFMData(obj_Id, strWomenid, strwuserId);

		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues();
		}

		tbl.setVisibility(View.VISIBLE);

	}

	// Display values
	private void displayValues() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);

			TextView textval2lbl = new TextView(getActivity());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval2lbl);

			// updated on 28feb16 by Arpitha

			TextView textcommnets = new TextView(getActivity());
			textcommnets.setTextSize(18);
			textcommnets.setText(getResources().getString(R.string.comments));
			textcommnets.setPadding(10, 10, 10, 10);
			textcommnets.setTextColor(getResources().getColor(R.color.black));
			textcommnets.setTextSize(18);
			textcommnets.setGravity(Gravity.LEFT);
			trlabel.addView(textcommnets);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				// txtdate.setText(listValuesItems.get(i).getStrdate());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
				}

				String val1 = "", val2 = "", val3 = "";

				val1 = listValuesItems.get(i).getAmnioticfluid();
				val2 = listValuesItems.get(i).getMoulding();

				// updated on 28feb16 by arpitha
				val3 = listValuesItems.get(i).getAfmcomments();

				if (val1.equals("0"))
					val1 = getResources().getString(R.string.c);
				else if (val1.equals("1"))
					val1 = getResources().getString(R.string.m);
				// 26Nov2015
				else if (val1.equals("2"))
					val1 = getResources().getString(R.string.i);
				else if (val1.equals("3"))
					val1 = getResources().getString(R.string.b);

				if (val2.equalsIgnoreCase("0"))
					val2 = getResources().getString(R.string.m1);
				else if (val2.equalsIgnoreCase("1"))
					val2 = getResources().getString(R.string.m2);
				else
					val2 = getResources().getString(R.string.m3);

				textval1lbl.setText(getResources().getString(R.string.amniotic_fluid));
				textval2lbl.setText(getResources().getString(R.string.moulding));

				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(18);
				txtvalue1.setPadding(10, 10, 10, 10);
				txtvalue1.setTextColor(getResources().getColor(R.color.black));
				txtvalue1.setTextSize(18);
				txtvalue1.setGravity(Gravity.CENTER);

				// 05May2017 Arpitha -v 2.6
				if (val1.equalsIgnoreCase(Green)) {
					txtvalue1.setTextColor(getResources().getColor(R.color.green));
				} else if (val1.equalsIgnoreCase(Red))
					txtvalue1.setTextColor(getResources().getColor(R.color.red));
				// 05May2017 Arpitha -v 2.6

				tr.addView(txtvalue1);

				TextView txtvalue2 = new TextView(getActivity());

				txtvalue2.setTextColor(getResources().getColor(R.color.black));

				txtvalue2.setText(val2);
				txtvalue2.setTextSize(18);
				txtvalue2.setPadding(10, 10, 10, 10);
				txtvalue2.setTextSize(18);
				txtvalue2.setGravity(Gravity.CENTER);
				tr.addView(txtvalue2);
				// updated on 28feb16 by Arpitha

				TextView txtcommentsval = new TextView(getActivity());
				txtcommentsval.setText(val3);
				txtcommentsval.setTextSize(18);
				txtcommentsval.setPadding(10, 10, 10, 10);
				txtcommentsval.setTextColor(getResources().getColor(R.color.black));
				txtcommentsval.setTextSize(18);
				txtcommentsval.setGravity(Gravity.CENTER);
				tr.addView(txtcommentsval);

				View view3 = new View(getActivity());
				view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
				view3.setBackgroundColor(Color.DKGRAY);
				tbl.addView(view3);

				if (i == 0) {
					tbl.addView(trlabel);
					View view1 = new View(getActivity());
					view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view1.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view1);
				}

				tbl.addView(tr);
			}
		}
	}

	/**
	 * This method invokes when Item clicked in Spinner
	 * 
	 * @throws Exception
	 */
	public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (adapter.getId()) {
			// facility
			case R.id.spnamnioticfluid:
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				stramnioticfluidval = "" + adapter.getSelectedItemPosition();

				break;

			case R.id.spnmoulding:
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());

				strmouldingval = "" + adapter.getSelectedItemPosition();

				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtval);
		final TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtval1);
		txtdialog1.setVisibility(View.GONE);
		txtdialog2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {
					setAfmData();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
				// }

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	public void displayAFMinfo() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());

		dialog.setTitle(getResources().getString(R.string.afm_abrreivation));// changed
																				// on
																				// 25Nov2016
																				// Arpitha
		// 04Oct2016 Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.fhr_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.afm));

		TextView txtredval = (TextView) dialog.findViewById(R.id.greenval2);// 11may2017
																			// Arpitha
																			// -
																			// v2.6
		txtredval.setText(Red);// 11may2017 Arpitha- v2.6
		TextView txtgreenval = (TextView) dialog.findViewById(R.id.greenval4);// 11may2017
																				// Arpitha-
																				// v2.6

		TextView txtamber = (TextView) dialog.findViewById(R.id.amberval4);
		txtamber.setVisibility(View.GONE);
		txtgreenval.setText(Green);// 11may2017 Arpitha- v2.6
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		dialog.show();

	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		aq.id(R.id.etafmcomments).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(200)});

	}

	// 25Nov2016 Arpitha
	void setAfmData() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			ArrayList<String> adata = new ArrayList<String>();

			adata.add(stramnioticfluidval);
			adata.add(strmouldingval);
			// updated on28feb16 by Arpitha
			adata.add(aq.id(R.id.etafmcomments).getText().toString());

			ArrayList<String> prop_id = new ArrayList<String>();
			ArrayList<String> prop_name = new ArrayList<String>();
			Property_pojo pdata = new Property_pojo();
			Cursor cursor = dbh.getPropertyIds(obj_Id);

			if (cursor.getCount() > 0) {
				cursor.moveToFirst();

				do {

					prop_id.add(cursor.getString(0));
					prop_name.add(cursor.getString(1));

				} while (cursor.moveToNext());

				pdata.setObj_Id(obj_Id);
				pdata.setProp_id(prop_id);
				pdata.setProp_name(prop_name);
				pdata.setProp_value(adata);
				pdata.setStrdate(todaysDate);
				pdata.setStrTime(aq.id(R.id.etafmtime).getText().toString());

				if (Integer.parseInt(stramnioticfluidval) == 1)
					pdata.setAmioticfluidDanger(1);// 12JUn2017 Arpitha
				// pdata.setIsDangerval1(1);
				else
					pdata.setAmioticfluidDanger(0);// 12JUn2017 Arpitha
				// pdata.setIsDangerval1(0);

				dbh.db.beginTransaction();
				int transId = dbh.iCreateNewTrans(strwuserId);

				boolean isAdded = dbh.insertData(pdata, strWomenid, strwuserId, transId);

				if (isAdded) {
					dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
					loadListDataToDisplayValues();
					commitTrans();
				} else {
					rollbackTrans();
				}

				// updated on 17Nov2015
				aq.id(R.id.spnamnioticfluid).setSelection(0);
				aq.id(R.id.spnmoulding).setSelection(0);
				// updated on 28feb16 byArpitha
				aq.id(R.id.etafmcomments).text("");

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();

		}

	}

}
