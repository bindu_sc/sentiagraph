package com.bc.partograph.sliding;

public class NavDrawerItem {
	
	private String title;
	private int icon;
	private String count = "0";
	// boolean to set visiblity of the counter
	private boolean isCounterVisible = false;
	private int cnt;
//  updated on 6july2016 by Arpitha	
	private int noti_icon;
	
	public NavDrawerItem(){}

	public NavDrawerItem(String title, int icon){
		this.title = title;
		this.icon = icon;
	}
	
	public NavDrawerItem(String title, int icon, boolean isCounterVisible, String count){
		this.title = title;
		this.icon = icon;
		this.isCounterVisible = isCounterVisible;
		this.count = count;
	}
	
	public NavDrawerItem(String title, int icon, boolean isCounterVisible, int count){
		this.title = title;
		this.icon = icon;
		this.isCounterVisible = isCounterVisible;
		this.cnt = count;
	}
	
	public NavDrawerItem(String title, boolean isCounterVisible, int count){
		this.title = title;
		this.isCounterVisible = isCounterVisible;
		this.cnt = count;
	}
//   update on 05/072016 by Arpitha	
	public NavDrawerItem(String title,int icon, boolean isCounterVisible, int count,int noti_icon ){
		this.title = title;
		this.isCounterVisible = isCounterVisible;
		this.cnt = count;
		this.noti_icon = noti_icon;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public int getIcon(){
		return this.icon;
	}
	
	public String getCount(){
		return this.count;
	}
	
	public boolean getCounterVisibility(){
		return this.isCounterVisible;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setIcon(int icon){
		this.icon = icon;
	}
	
	public void setCount(String count){
		this.count = count;
	}
	
	public void setCounterVisibility(boolean isCounterVisible){
		this.isCounterVisible = isCounterVisible;
	}
}
