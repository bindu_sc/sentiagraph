package com.bc.partograph.sliding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.Comments_Adapter;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

public class Fragment_Contraction extends Fragment implements OnClickListener {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	String todaysDate, strcurrTime, strWomenid, strwuserId;

	ArrayList<Add_value_pojo> listValuesItems;
	TableLayout tbl;
	GraphicalView mChartView = null;
	Thread myThread;
	int obj_Id;
	String obj_Type;
	ArrayList<Add_value_pojo> arrComments;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	boolean isGraphClicked = false;
	boolean isValidDuration = false;
	// Changed on 27Mar2015
	String displaydateformat;
	String womenid;
	String userid;
	String lastdate;
	String lasttime;
	// updated on 7july2016 by Arpitha
	public static boolean isnotify_contraction = false;

	String lastentrytime;
	// 04Oct2016 Arpitha
	TextView txtdialogtitle;
	Women_Profile_Pojo woman;// 03Nov2016

	String Red, Amber, Green;// 05May2017 Arpitha - v_2.6

	int hours = 0;// 05May2017 Arpitha - v2.6 mantis id 0000235

	ArrayList<Add_value_pojo> listValuesItemsDilatation;// 08May2017 Arpitha -
														// v2.6
	String maxCervixVal;// 08May2017 Arpitha - v2.6
	String firstPartoTime;// 08May2017 Arpitha - v2.6

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_contractions, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());

			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");
			womenid = woman.getWomenId();
			userid = woman.getUserId();
			initializeScreen(aq.id(R.id.rlcontractions).getView());

			initialView();

			myThread = null;

			Runnable myRunnableThread = new CountDownRunner();
			myThread = new Thread(myRunnableThread);
			myThread.start();
			setInputFiltersForEdittext();

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			switch (v.getId()) {

			case R.id.btnviewgraphcontract:

				if (listValuesItems.size() > 0) {
					isGraphClicked = true;
					showGraphDialog();
				} else
					displayAlertDialog(getResources().getString(R.string.nodata));

				break;

			case R.id.btnsavecontract:

				isGraphClicked = false;

				// 27Sep2016 Arpitha
				if (Partograph_CommonClass.autodatetime(getActivity())) {

					Date lastregdate = null;
					String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
							getResources().getString(R.string.partograph));
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (strlastinserteddate != null) {
						lastregdate = format.parse(strlastinserteddate);
					}
					Date currentdatetime = new Date();
					if (lastregdate != null && currentdatetime.before(lastregdate) && (!LoginActivity.istesting)) {
						Partograph_CommonClass.showSettingsAlert(getActivity(),
								getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
					} else {// 27Sep2016 Arpitha

						// changes 12jan2016
						strCurrentTime = todaysDate + "_" + aq.id(R.id.etcontracttime).getText().toString();
						isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);
						// 26jan2016
						if (listValuesItems.size() <= 0) {
							isValidTime = true;
						}

						if (isValidTime) {

							if (validateContraction()) {

								int contDur = 0;
								String duration = aq.id(R.id.etduration).getText().toString();
								if (!(duration.equals("") || (duration == null)))
									contDur = Integer.parseInt(duration);

								Add_value_pojo valpojo = new Add_value_pojo();
								valpojo.setContractions(aq.id(R.id.etcontractionval).getText().toString());
								// updated by Arpitha
								valpojo.setContactioncomments(aq.id(R.id.etcontractionscomments).getText().toString());

								valpojo.setDuration(contDur);
								valpojo.setStrTime(aq.id(R.id.etcontracttime).getText().toString());
								valpojo.setStrdate(todaysDate);
								valpojo.setObj_Id(obj_Id);
								valpojo.setObj_Type(obj_Type);

								if (Integer.parseInt(aq.id(R.id.etcontractionval).getText().toString()) > 5)
									valpojo.setContractionDanger(1);// 12Jun2017
																	// Arpitha
								// valpojo.setIsDanger(1);
								else
									valpojo.setContractionDanger(0);// 12Jun2017
																	// Arpitha
								// valpojo.setIsDanger(0);

								dbh.db.beginTransaction();
								int transId = dbh.iCreateNewTrans(strwuserId);

								boolean isAdded = dbh.insertContractiondata(valpojo, strWomenid, strwuserId, transId);

								if (isAdded) {

									dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
									dbh.updatenotification(strWomenid, obj_Id);// 18Oct2016
																				// Arpitha
									loadListDataToDisplayValues();
									displayValues();
									commitTrans();
								} else {
									rollbackTrans();
								}

								aq.id(R.id.etcontractionval).text("");
								aq.id(R.id.etduration).text("");
								// updated by Arpitha
								aq.id(R.id.etcontractionscomments).text("");

								aq.id(R.id.etcontractionval).getEditText().requestFocus();
							}

						} else {

							String newTime = Partograph_CommonClass.getnewtime(lastentrytime, 30);
							String newtime_12hrs = Partograph_CommonClass.gettimein12hrformat(newTime);

							aq.id(R.id.etcontractionval).text("");
							aq.id(R.id.etduration).text("");
							// updated by Arpitha
							aq.id(R.id.etcontractionscomments).text("");
							displayAlertDialog(getResources().getString(R.string.fhr_duration) + " " + newtime_12hrs
									+ " " + getResources().getString(R.string.on));

						}
					}
				}
				break;

			case R.id.contractioninfo: {
				displayContractioninfo();
			}
				break;
			case R.id.imgbtnnotification: {
				notification(womenid);
			}
				break;
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Method to rollback the transaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_SHORT).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		calSyncMtd();
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
		aq.id(R.id.imgbtnnotification).gone();

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha

	}

	// Sync
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	private void showGraphDialog() throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
			dialog.setContentView(R.layout.dialog_contractions);

			dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
			dialog.getWindow().setBackgroundDrawableResource(R.color.white);

			TextView txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
			txtdialogtitle.setText(getResources().getString(R.string.contraction));

			ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
			imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						dialog.cancel();
						isGraphClicked = false;
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			createContractionchart(dialog);

			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Contraction chart
	private void createContractionchart(Dialog dialog) throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		listValuesItems = dbh.getContractionData(obj_Id, strWomenid, strwuserId, isGraphClicked);

		if (listValuesItems.size() > 0) {
			// Creating an XYSeries for values
			XYSeries cont_L20sec = new XYSeries("");
			XYSeries cont_B20_40sec = new XYSeries("");
			XYSeries cont_M40sec = new XYSeries("");
			XYSeries cont_M60sec = new XYSeries("");

			// colorMap = new HashMap<Integer, Integer>();
			for (int i = 0; i < listValuesItems.size(); i++) {
				if (listValuesItems.get(i).getDuration() < 20)
					cont_L20sec.add(i + 1, Double.parseDouble(listValuesItems.get(i).getContractions()));

				if (listValuesItems.get(i).getDuration() >= 20 && listValuesItems.get(i).getDuration() <= 40) {
					cont_B20_40sec.add(i + 1, Double.parseDouble(listValuesItems.get(i).getContractions()));
				}

				if (listValuesItems.get(i).getDuration() > 40 && listValuesItems.get(i).getDuration() <= 60)
					cont_M40sec.add(i + 1, Double.parseDouble(listValuesItems.get(i).getContractions()));

				if (listValuesItems.get(i).getDuration() > 60)
					cont_M60sec.add(i + 1, Double.parseDouble(listValuesItems.get(i).getContractions()));

			}

			// Creating a dataset to hold each series
			XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			// Adding value Series to the dataset
			dataset.addSeries(cont_L20sec);
			dataset.addSeries(cont_B20_40sec);
			dataset.addSeries(cont_M40sec);
			dataset.addSeries(cont_M60sec);

			// Creating XYSeriesRenderer to customize contractions < 20 sec
			XYSeriesRenderer contraction_L20Renderer = new XYSeriesRenderer();

			contraction_L20Renderer.setColor(getResources().getColor(R.color.vividorange));
			contraction_L20Renderer.setFillPoints(true);
			contraction_L20Renderer.setLineWidth(2);
			contraction_L20Renderer.setDisplayChartValues(true);
			contraction_L20Renderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize contractions bet 20 and 40
			XYSeriesRenderer contraction_B20_40Renderer = new XYSeriesRenderer();
			contraction_B20_40Renderer.setColor(getResources().getColor(R.color.darkgreen));
			contraction_B20_40Renderer.setFillPoints(true);
			contraction_B20_40Renderer.setLineWidth(2);
			contraction_B20_40Renderer.setDisplayChartValues(true);
			contraction_B20_40Renderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize contractions > 40
			XYSeriesRenderer contraction_M40Renderer = new XYSeriesRenderer();
			contraction_M40Renderer.setColor(getResources().getColor(R.color.darkgreen));
			contraction_M40Renderer.setFillPoints(true);
			contraction_M40Renderer.setLineWidth((float) 10.5d);
			contraction_M40Renderer.setDisplayChartValues(true);
			contraction_M40Renderer.setChartValuesTextSize(20);

			// Creating XYSeriesRenderer to customize contractions > 40
			XYSeriesRenderer contraction_M60Renderer = new XYSeriesRenderer();
			contraction_M60Renderer.setColor(getResources().getColor(R.color.red));
			contraction_M60Renderer.setFillPoints(true);
			contraction_M60Renderer.setLineWidth((float) 10.5d);
			contraction_M60Renderer.setDisplayChartValues(true);
			contraction_M60Renderer.setChartValuesTextSize(20);

			// Creating a XYMultipleSeriesRenderer to customize the whole chart
			XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			multiRenderer.setXLabels(0);
			// multiRenderer.setChartTitle("Partograph");
			multiRenderer.setXTitle(getResources().getString(R.string.txttime));
			multiRenderer.setYTitle(getResources().getString(R.string.contraction));
			multiRenderer.setXLabelsColor(Color.BLACK);
			multiRenderer.setYLabelsColor(0, Color.BLACK);
			multiRenderer.setLabelsTextSize(20);
			multiRenderer.setLegendTextSize(20);
			multiRenderer.setLabelsColor(Color.BLACK);
			multiRenderer.setMargins(new int[] { 30, 60, 60, 30 });
			multiRenderer.setApplyBackgroundColor(true);
			multiRenderer.setBackgroundColor(Color.WHITE);
			multiRenderer.setMarginsColor(Color.WHITE);
			multiRenderer.setAxesColor(Color.BLACK);
			multiRenderer.setAxisTitleTextSize(15);
			multiRenderer.setPointSize(10);
			multiRenderer.setYLabelsAlign(Align.RIGHT);
			multiRenderer.setShowGrid(true);
			// Disable Pan on two axis
			multiRenderer.setPanEnabled(true, true);
			multiRenderer.setYAxisMax(10);
			multiRenderer.setYAxisMin(1);
			multiRenderer.setGridColor(getResources().getColor(R.color.lightgray));
			multiRenderer.setYLabels(10);
			multiRenderer.setFitLegend(true);
			multiRenderer.setShowGrid(true);
			// multiRenderer.setAntialiasing(true);
			// multiRenderer.setInScroll(true);
			multiRenderer.setShowGridX(true);
			multiRenderer.setShowGridY(true);

			multiRenderer.setXAxisMin(0);
			multiRenderer.setXAxisMax(12);
			multiRenderer.setZoomButtonsVisible(true);
			multiRenderer.setBarWidth(15);

			for (int i = 0; i < listValuesItems.size(); i++) {

				multiRenderer.addXTextLabel(i + 1,
						listValuesItems.get(i).getStrTime() + "\n" + listValuesItems.get(i).getDuration());
				multiRenderer.setYAxisMax(10);
				multiRenderer.setYLabels(10);
			}

			multiRenderer.addSeriesRenderer(contraction_L20Renderer);
			multiRenderer.addSeriesRenderer(contraction_B20_40Renderer);
			multiRenderer.addSeriesRenderer(contraction_M40Renderer);
			multiRenderer.addSeriesRenderer(contraction_M60Renderer);

			mChartView = null;

			String[] types = new String[] { BarChart.TYPE, BarChart.TYPE, BarChart.TYPE };

			mChartView = ChartFactory.getBarChartView(getActivity(), dataset, multiRenderer, Type.DEFAULT);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			mChartView.setLayoutParams(params);

			RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.rlcontractionchart);
			layout.addView(mChartView);

			if (mChartView != null) {
				mChartView.repaint();
			}
		}

	}

	// Initial View
	private void initialView() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();
		strcurrTime = Partograph_CommonClass.getCurrentTime();
		strWomenid = woman.getWomenId();
		strwuserId = woman.getUserId();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);

		aq.id(R.id.etcontracttime).text(strcurrTime);
		aq.id(R.id.etcontractdate).text(displaydateformat);

		aq.id(R.id.etcontractdate).enabled(false);
		aq.id(R.id.etcontracttime).enabled(false);

		tbl = (TableLayout) rootView.findViewById(R.id.tblvaluescontract);
		tbl.setVisibility(View.GONE);

		aq.id(R.id.txtnodata).gone();// 28Sep2016 Arpitha
		aq.id(R.id.ttxheading).gone();// 29Sep2016 Arpitha

		aq.id(R.id.rlcontractionchart).gone();
		disableAddvalues();

		obj_Type = getResources().getString(R.string.object4);
		obj_Id = dbh.getObjectID(obj_Type);

		// 05May2017 Arpitha - v_2.6
		Cursor cur = dbh.getColorCodedValuesFromTable("4.1");
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			Red = cur.getString(4);
			Amber = cur.getString(5);
			Green = cur.getString(6);
		} // 05May2017 Arpitha - v_2.6

		// 08May2017 Arpitha - v2.6
		listValuesItemsDilatation = dbh.getDilatationData(3, strWomenid, strwuserId, true);
		firstPartoTime = listValuesItemsDilatation.get(0).getStrdate() + "_"
				+ listValuesItemsDilatation.get(0).getStrTime();

		loadListDataToDisplayValues();

		displayComments();
		updatetblComments(strwuserId, strWomenid, obj_Id);
		aq.id(R.id.imgbtnnotification).invisible();
		lastdate = dbh.lastvaldate(strWomenid, obj_Id);
		lasttime = dbh.lastvaltime(strWomenid, obj_Id, lastdate);
		//08Oct2017 Arpitha - 
		if (SlidingActivity.iscontractiontimevalid && !(SlidingActivity.arrVal.size()>0)) {
			aq.id(R.id.imgbtnnotification).visible();
		} else
			aq.id(R.id.imgbtnnotification).invisible();

		// 19Nov2016 Arpitha
		if (woman.getregtype() == 2) {
			aq.id(R.id.txtpartodisabled).visible();
			aq.id(R.id.txtnodata).gone();
		} else {
			aq.id(R.id.txtpartodisabled).gone();
			// aq.id(R.id.txtnodata).visible();
		} // 19Nov2016 Arpitha

		// 15Aug2017 Arpitha
		if (SlidingActivity.arrVal.size() > 0)
			aq.id(R.id.txtnodata)
					.text(getResources().getString(R.string.cannot_enter_any_data_after_updating_discharge_details));

	}

	// Update tblcomments set isviewed=true
	private void updatetblComments(String userid, String womenid, int objid) throws Exception {

		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh.updatetblComments(userid, womenid, objid);
		Partograph_CommonClass.contcmt_cnt = 0;

		// 04jan2016
		SlidingActivity.isMsgCon = false;
	}

	// Display Comments
	private void displayComments() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Cursor cursor = dbh.getComments(strWomenid, strwuserId, obj_Id);

		arrComments = new ArrayList<Add_value_pojo>();
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				Add_value_pojo commentsData = new Add_value_pojo();
				commentsData.setComments(cursor.getString(0));
				commentsData.setStrdate(cursor.getString(1));
				commentsData.setStrTime(cursor.getString(2));

				arrComments.add(commentsData);
			} while (cursor.moveToNext());

		}

		if (arrComments.size() > 0) {
			aq.id(R.id.listCommentscontract).visible();
			aq.id(R.id.txtCommentscontract).visible();
			// Save ListView state
			Parcelable state = aq.id(R.id.listCommentscontract).getListView().onSaveInstanceState();
			Comments_Adapter adapter = new Comments_Adapter(getActivity(), R.layout.fragment_contractions, arrComments);
			aq.id(R.id.listCommentscontract).adapter(adapter);
			// Restore previous state (including selected item index and scroll
			// position)
			aq.id(R.id.listCommentscontract).getListView().onRestoreInstanceState(state);

			// 18dec2015
			Helper.getListViewSize(aq.id(R.id.listCommentscontract).getListView());
		} else {
			aq.id(R.id.listCommentscontract).gone();
			aq.id(R.id.txtCommentscontract).gone();

		}

	}

	// Add values layout visibility gone
	private void disableAddvalues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// 23Sep2016 Arpitha - make invisible when woman is referred
		// 16Oct2016 Arpitha - don't allow to enter if if it post delivery
		// ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		// arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (woman.getDel_type() != 0 || SlidingActivity.isReferred || woman.getregtype() == 2
				|| SlidingActivity.arrVal.size() > 0) {
			aq.id(R.id.llcontractionaddval).gone();
			aq.id(R.id.ttxheading).visible();// 29Sep2016 Arpitha

		} else {

			aq.id(R.id.llcontractionaddval).visible();
		}
	}

	public void doWork() {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.etcontracttime).text(curTime);
						displayComments();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(1000); // Pause of 1 Second
				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
				}
			}
		}
	}

	// Load data to display
	private void loadListDataToDisplayValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		listValuesItems = new ArrayList<Add_value_pojo>();

		listValuesItems = dbh.getContractionData(obj_Id, strWomenid, strwuserId, false);

		if (listValuesItems.size() <= 0) {
			tbl.setVisibility(View.GONE);
			strLastTime = todaysDate + "_" + "00:00";
			aq.id(R.id.txtnodata).visible();// 28Sep2016 Arpitha
		} else {
			tbl.setVisibility(View.VISIBLE);
			displayValues();
		}
	}

	private void displayValues() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (listValuesItems.size() > 0) {

			tbl.removeAllViews();

			// TableRow trlabel = (TableRow) findViewById(R.id.tableRow1);
			TableRow trlabel = new TableRow(getActivity());
			trlabel.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

			TextView texttimelbl = new TextView(getActivity());
			texttimelbl.setTextSize(18);
			texttimelbl.setPadding(10, 10, 10, 10);
			texttimelbl.setTextColor(getResources().getColor(R.color.black));
			texttimelbl.setTextSize(18);
			texttimelbl.setGravity(Gravity.CENTER);
			trlabel.addView(texttimelbl);

			TextView textdatelbl = new TextView(getActivity());
			textdatelbl.setTextSize(18);
			textdatelbl.setPadding(10, 10, 10, 10);
			textdatelbl.setTextColor(getResources().getColor(R.color.black));
			textdatelbl.setTextSize(18);
			textdatelbl.setGravity(Gravity.CENTER);
			trlabel.addView(textdatelbl);

			TextView textval1lbl = new TextView(getActivity());
			textval1lbl.setTextSize(18);
			textval1lbl.setPadding(10, 10, 10, 10);
			textval1lbl.setTextColor(getResources().getColor(R.color.black));
			textval1lbl.setTextSize(18);
			textval1lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval1lbl);

			TextView textval2lbl = new TextView(getActivity());
			textval2lbl.setTextSize(18);
			textval2lbl.setPadding(10, 10, 10, 10);
			textval2lbl.setTextColor(getResources().getColor(R.color.black));
			textval2lbl.setTextSize(18);
			textval2lbl.setGravity(Gravity.CENTER);
			trlabel.addView(textval2lbl);

			// updated by Arpitha
			TextView textcomments = new TextView(getActivity());
			textcomments.setTextSize(18);
			textcomments.setText(getResources().getString(R.string.comments));
			textcomments.setPadding(10, 10, 10, 10);
			textcomments.setTextColor(getResources().getColor(R.color.black));
			textcomments.setTextSize(18);
			textcomments.setGravity(Gravity.LEFT);
			trlabel.addView(textcomments);

			texttimelbl.setText(getResources().getString(R.string.txttime));
			textdatelbl.setText(getResources().getString(R.string.txtdate));

			for (int i = 0; i < listValuesItems.size(); i++) {
				TableRow tr = new TableRow(getActivity());
				tr.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 40));

				TextView txttime = new TextView(getActivity());
				txttime.setText(listValuesItems.get(i).getStrTime());
				txttime.setTextSize(18);
				txttime.setPadding(10, 10, 10, 10);
				txttime.setTextColor(getResources().getColor(R.color.black));
				txttime.setTextSize(18);
				txttime.setGravity(Gravity.CENTER);
				tr.addView(txttime);

				TextView txtdate = new TextView(getActivity());
				txtdate.setText(Partograph_CommonClass.getConvertedDateFormat(listValuesItems.get(i).getStrdate(),
						Partograph_CommonClass.defdateformat));
				txtdate.setTextSize(18);
				txtdate.setPadding(10, 10, 10, 10);
				txtdate.setTextColor(getResources().getColor(R.color.black));
				txtdate.setTextSize(18);
				txtdate.setGravity(Gravity.CENTER);
				tr.addView(txtdate);

				String val1 = "", val2 = "", val3 = "", val4 = "";

				textval1lbl.setText(getResources().getString(R.string.txtvallbl));

				val1 = listValuesItems.get(i).getContractions();
				val2 = String.valueOf(listValuesItems.get(i).getDuration());

				// updated on 28feb16 by Arpitha
				val4 = listValuesItems.get(i).getContactioncomments();

				textval1lbl.setText(getResources().getString(R.string.contraction));
				textval2lbl.setText(getResources().getString(R.string.duration));

				if (i == 0) {
					strLastTime = listValuesItems.get(i).getStrdate() + "_" + listValuesItems.get(i).getStrTime();
					lastentrytime = listValuesItems.get(i).getStrTime();
				}
				TextView txtvalue1 = new TextView(getActivity());
				txtvalue1.setText(val1);
				txtvalue1.setTextSize(20);
				txtvalue1.setPadding(10, 10, 10, 10);

				try {
					int contval = Integer.parseInt(val1);

					// String[] a = Red.split(">");

					if ((Red != null && Red.trim().length() > 0) && contval > Integer.parseInt(Red.split(">")[1]))// changed
						// on
						// 05May2017
						// Arpitha
						// -v2.6
						txtvalue1.setTextColor(getResources().getColor(R.color.red));
					else if ((Amber != null && Amber.trim().length() > 0)
							&& contval < Integer.parseInt(Amber.split("<")[1]))// changed
						// on

						// 05May2017
						// Arpitha
						// -v2.6 mantis id 0000234
						txtvalue1.setTextColor(getResources().getColor(R.color.amber));// changed
					// on
					// 05May2017
					// Arpitha
					// -v2.6 mantis id 0000234

					else
						txtvalue1.setTextColor(getResources().getColor(R.color.black));

					txtvalue1.setTextSize(18);
					txtvalue1.setGravity(Gravity.CENTER);
					tr.addView(txtvalue1);

					TextView txtvalue2 = new TextView(getActivity());
					txtvalue2.setText(val2);
					txtvalue2.setTextSize(18);
					txtvalue2.setPadding(10, 10, 10, 10);
					txtvalue2.setTextColor(getResources().getColor(R.color.black));
					txtvalue2.setTextSize(18);
					txtvalue2.setGravity(Gravity.CENTER);
					tr.addView(txtvalue2);

					// updated on 28feb16 by Arpitha
					TextView txtcommentsval = new TextView(getActivity());
					txtcommentsval.setText(val4);
					txtcommentsval.setTextSize(18);
					txtcommentsval.setPadding(10, 10, 10, 10);
					txtcommentsval.setTextColor(getResources().getColor(R.color.black));
					txtcommentsval.setTextSize(18);
					txtcommentsval.setGravity(Gravity.CENTER);
					tr.addView(txtcommentsval);

					View view3 = new View(getActivity());
					view3.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
					view3.setBackgroundColor(Color.DKGRAY);
					tbl.addView(view3);

					if (i == 0) {
						tbl.addView(trlabel);
						View view1 = new View(getActivity());
						view1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 1));
						view1.setBackgroundColor(Color.DKGRAY);
						tbl.addView(view1);
					}

					tbl.addView(tr);

					// 08May2017 Arpitha v2.6
					maxCervixVal = listValuesItemsDilatation.get(listValuesItemsDilatation.size() - 1).getCervix();
					String dilatationTime = listValuesItemsDilatation.get(listValuesItemsDilatation.size() - 1)
							.getStrdate() + "_"
							+ listValuesItemsDilatation.get(listValuesItemsDilatation.size() - 1).getStrTime();
					String contractionTime = listValuesItems.get(i).getStrdate() + "_"
							+ listValuesItems.get(i).getStrTime();

					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH:mm");
					Date d1 = format.parse(dilatationTime);
					Date d2 = format.parse(contractionTime);

					// 05may2017 Arpitha - v2.6 - 0000235
					if (firstPartoTime != null) {
						hours = Partograph_CommonClass.getHoursBetDates(firstPartoTime, contractionTime);
					}

					if ((listValuesItems.get(i).getDuration() < 20 && hours >= 4)
							|| (listValuesItems.get(i).getDuration() < 40 && Integer.parseInt(maxCervixVal) >= 6
									&& (d1.equals(d2) || d1.before(d2)))
							|| listValuesItems.get(i).getDuration() > 80) {
						txtvalue2.setTextColor(getResources().getColor(R.color.amber));
					} // 05may2017 Arpitha - v2.6 - 0000235

					/*
					 * // 22May2017 Arpitha - v2.6 if
					 * (Integer.parseInt(listValuesItems.get(i).getContractions(
					 * )) < Integer.parseInt(Amber.split("<")[1])) {
					 * txtvalue1.setTextColor(getResources().getColor(R.color.
					 * amber));
					 * 
					 * } // 22May2017 Arpitha - v2.6
					 */ } catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * validate dilatation - 12jan2016
	 * 
	 * @return
	 */
	private boolean validateContraction() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		if (aq.id(R.id.etcontractionval).getEditText().getText().toString().trim().length() <= 0
				&& aq.id(R.id.etduration).getEditText().getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.enter_val));
			aq.id(R.id.etcontractionval).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etcontractionval).getEditText().getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.enterfrequencyval));
			aq.id(R.id.etcontractionval).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etduration).getEditText().getText().toString().trim().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.enterdurationval));
			aq.id(R.id.etduration).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etduration).getEditText().getText().toString().trim().length() > 0) {

			if ((Integer.parseInt(aq.id(R.id.etduration).getEditText().getText().toString()) > 90)
					|| (Integer.parseInt(aq.id(R.id.etduration).getEditText().getText().toString())) < 10) {
				displayAlertDialog(getResources().getString(R.string.contract_validation));
				aq.id(R.id.etduration).getEditText().requestFocus();
				return false;
			}
		}
		if ((Integer.parseInt(aq.id(R.id.etcontractionval).getEditText().getText().toString())) == 0) {
			displayAlertDialog(getResources().getString(R.string.frequency0));
			return false;
		}

		return true;
	}

	public void displayContractioninfo() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		final Dialog dialog = new Dialog(getActivity());
		dialog.setTitle(getResources().getString(R.string.contr));
		// 04Oct2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.contraction_info);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.contr));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		dialog.show();

	}

	public void notification(String womenid) throws Exception

	{
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		final Dialog dialog = new Dialog(getActivity());
		dialog.setTitle(getResources().getString(R.string.contr));
		// 04Oct2016- Arpitha
		dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		dialog.setContentView(R.layout.notification_dialog);
		dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.dialog_title);
		txtdialogtitle = (TextView) dialog.findViewById(R.id.graph_title);
		txtdialogtitle.setText(getResources().getString(R.string.contr));
		ImageButton imgbtncanceldialog = (ImageButton) dialog.findViewById(R.id.imgbtncanceldialog);
		imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		TextView txt = (TextView) dialog.findViewById(R.id.textView1);
		String convertedlastdate = Partograph_CommonClass.getConvertedDateFormat(lastdate, "dd-MM-yyyy");
		txt.setText(getResources().getString(R.string.contr_last_entry) + " " + convertedlastdate + " " + lasttime + " "
				+ getResources().getString(R.string.was_at));

		dialog.show();

	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		aq.id(R.id.etcontractionscomments).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(200) });

	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

}
