package com.bc.partograph.sliding;

import java.io.File;

import org.achartengine.GraphicalView;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.login.LoginActivity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.lowagie.text.pdf.PdfPTable;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

public class Fragment_viewpartograph extends Fragment {

	View rootView;
	GraphicalView mChartViewDilatation = null;
	GraphicalView mChartViewFHR = null;
	GraphicalView mChartViewContraction = null;
	GraphicalView mChartViewPBP = null;
	public static int comptabpos = 0;
	public static boolean isViewParto;
	TableLayout tbllayoutfhr, tbllayoutdil, tbllayoutcontract, tbllayoutpbp, tbllayoutafm, tbllayoutoxytocin,
			tbllayoutdrugs, tbllayouttemp, tbllayouturinetest;
	AQuery aq;
	Women_Profile_Pojo woman;
	ImageView imgpdf;// 07Dec2016 Arpitha
	String path;// 27march2017 Arpitha
	File dir;// 27march2017 Arpitha
	File file;// 27march2017 Arpitha
	PdfPTable womanbasics;
	PdfPTable tableAFM;
	PdfPTable table_oxytocin;
	PdfPTable table_drug;
	PdfPTable table_temp;
	PdfPTable table_urine;
	Context context;// 16May2017 Arpitha - v2.6

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.activity_printparto, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {

			// 8jan2016 - hide keyboard
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");
			comptabpos = Partograph_CommonClass.curr_tabpos;
			isViewParto = true;
			aq = new AQuery(getActivity());
			context = getActivity();// 16May2017 Arpitha - v.26
			tbllayoutfhr = (TableLayout) getActivity().findViewById(R.id.tblfhr);
			tbllayoutdil = (TableLayout) getActivity().findViewById(R.id.tbldil);
			tbllayoutcontract = (TableLayout) getActivity().findViewById(R.id.tblcontractions);
			tbllayoutpbp = (TableLayout) getActivity().findViewById(R.id.tblpbp);
			tbllayoutafm = (TableLayout) getActivity().findViewById(R.id.tblafm);
			tbllayoutoxytocin = (TableLayout) getActivity().findViewById(R.id.tbloxytocin);
			tbllayoutdrugs = (TableLayout) getActivity().findViewById(R.id.tbldrugs);
			tbllayouttemp = (TableLayout) getActivity().findViewById(R.id.tbltemp);
			tbllayouturinetest = (TableLayout) getActivity().findViewById(R.id.tblut);
			// updated on 18july2016 by Arpitha
			if (woman != null) {

				String doa = "", toa = "", risk = "", dod = "", tod = "";

				aq.id(R.id.wname).text(woman.getWomen_name());
				aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
				if (woman.getregtype() != 2) {
					doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
							Partograph_CommonClass.defdateformat);
					toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
					aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
				} else {
					aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + woman.getDate_of_reg_entry());
				}
				aq.id(R.id.wgest)
						.text(getResources().getString(R.string.gest_age) + ":"
								+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
										: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016
				aq.id(R.id.wgravida)
						.text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida() + ", "
								+ getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
				woman.getRisk_category();
				if (woman.getRisk_category() == 1) {
					risk = "High";
				} else
					risk = "Low";
				// 31August2016 Arpitha - label changed
				aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

				if (woman.getDel_type() > 0) {
					String[] deltype = getResources().getStringArray(R.array.del_type);
					aq.id(R.id.wdelstatus).text(
							getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
					dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
							Partograph_CommonClass.defdateformat);
					tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
					// 31August2016 Arpitha
					aq.id(R.id.wdeldate).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

				}

			}

			// 16May2017 Arpitha - v2.6
			DisplayFHRGraph();
			DisplayAFMGraph();
			DisplayDilatationGraph();
			DisplayContrcationGraph();
			DisplayOxytocinGraph();
			DisplayDrugsGraph();
			DisplayPulseBPGraph();
			DisplayTempGraph();
			DisplayUrineTestGraph();// 16May2017 Arpitha - v2.6

			// 07Dec2016 Arpitha
			imgpdf = (ImageView) rootView.findViewById(R.id.imgpdf);

			imgpdf.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {
						path = AppContext.mainDir + "/PDF/PartographSheet";
						dir = new File(path);
						if (!dir.exists())
							dir.mkdirs();
						Log.d("PDFCreator", "PDF Path: " + path);
						file = new File(dir, woman.getWomen_name() + "_PartographSheet.pdf");
						Partograph_CommonClass.pdfAlertDialog(context, woman,"partograph",null,null);
						;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});// 07Dec2016 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// 16May2017 Arpitha - v2.6
	void DisplayFHRGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(1))) {
			tbllayoutfhr.setVisibility(View.VISIBLE);

			mChartViewFHR = Partograph_CommonClass.createChart(woman.getWomenId(), woman.getUserId(), context, 1);
			if (mChartViewFHR != null) {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT, 400);
				mChartViewFHR.setLayoutParams(params);
				tbllayoutfhr.setBackground(getResources().getDrawable(R.drawable.edit_text_style_blue));
				tbllayoutfhr.addView(mChartViewFHR);

				if (mChartViewFHR != null) {
					mChartViewFHR.repaint();
				}
			}
		} else
			tbllayoutfhr.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayDilatationGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(3))) {

			mChartViewDilatation = Partograph_CommonClass.createDilatationGraph(woman.getWomenId(), woman.getUserId(),
					context, 3);

			tbllayoutdil.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 450);
			mChartViewDilatation.setLayoutParams(params);

			tbllayoutdil.addView(mChartViewDilatation);

			if (mChartViewDilatation != null) {
				mChartViewDilatation.repaint();
			}
		} else
			tbllayoutdil.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayAFMGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(2))) {
			tbllayoutafm.setVisibility(View.VISIBLE);

			tbllayoutafm = Partograph_CommonClass.dispalyAFMData(context, 2, woman.getWomenId(), woman.getUserId(),
					tbllayoutafm);

		} else
			tbllayoutafm.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayContrcationGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(4))) {

			mChartViewContraction = Partograph_CommonClass.DisplayContraction(4, woman.getWomenId(), woman.getUserId(),
					context);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 450);
			mChartViewContraction.setLayoutParams(params);

			tbllayoutcontract.addView(mChartViewContraction);

			if (mChartViewContraction != null) {
				mChartViewContraction.repaint();
			}
		}
	}

	// 16May2017 Arpitha - v2.6
	void DisplayOxytocinGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(5))) {
			tbllayoutoxytocin.setVisibility(View.VISIBLE);
			tbllayoutoxytocin = Partograph_CommonClass.DisplayOxytocinData(context, 5, woman.getWomenId(),
					woman.getUserId(), tbllayoutoxytocin);
		} else
			tbllayoutoxytocin.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayDrugsGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(6))) {
			tbllayoutdrugs.setVisibility(View.VISIBLE);
			tbllayoutdrugs = Partograph_CommonClass.DisplayDrugsData(tbllayoutdrugs, 6, woman.getWomenId(),
					woman.getUserId(), context);
		} else
			tbllayoutdrugs.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayUrineTestGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(6))) {
			tbllayouturinetest.setVisibility(View.VISIBLE);
			tbllayouturinetest = Partograph_CommonClass.DisplayUrineTestData(context, 9, woman.getWomenId(),
					woman.getUserId(), tbllayouturinetest);
		} else
			tbllayouturinetest.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayTempGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(8))) {
			tbllayouttemp.setVisibility(View.VISIBLE);
			tbllayouttemp = Partograph_CommonClass.DisplayTemperatureData(context, 8, woman.getWomenId(),
					woman.getUserId(), tbllayouttemp);
		} else
			tbllayouttemp.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayPulseBPGraph() throws Exception {

		if (!(LoginActivity.objectid.contains(7))) {
			tbllayoutpbp.setVisibility(View.VISIBLE);
			mChartViewPBP = Partograph_CommonClass.DisplayPulseBPData(context, woman.getWomenId(), woman.getUserId(),
					7);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 400);
			mChartViewPBP.setLayoutParams(params);

			tbllayoutpbp.addView(mChartViewPBP);

			if (mChartViewPBP != null) {
				mChartViewPBP.repaint();
			}
		} else
			tbllayoutpbp.setVisibility(View.GONE);

	}

}