package com.bc.partograph.stagesoflabor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.dischargedetails.DiscgargePojo;
import com.bluecrimson.partograph.Property_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class FourthStageOfLabor extends Fragment implements OnClickListener {
	View rootView;

	static AQuery aq;
	Partograph_DB dbh;
	int obj_Id;
	static String todaysDate;
	String strWomenid;
	String strwuserId;
	int uterus, urinepassed, bleeding;
	String pulse, bpsystolic, bpdiastolic;
	String strLastTime, strCurrentTime;
	boolean isValidTime = false;
	Cursor cursor;
	// updated on 10july2016 by Arpitha
	// 26dec2015
	static int hour;
	static int minute;
	int year;
	int mon;
	int day;
	String selecteddate;
	static String strfourthstagedate;
	String strfourthstagetime;
	static final int TIME_DIALOG_ID = 999;

	int bpsysval = 0, bpdiaval = 0, pulval = 0;
	// 16Aug2016 - bindu
	Thread myThread;

	static Women_Profile_Pojo woman;// 03Nov2016

	static String strlastentry;// 22Nov2016 Arpitha

	// 13Feb2017 Arpitha
	int breastFeeding = 0;
	static String strbfTime;
	boolean brestfeedingDate = false;
	static String dateBreastFeeding;
	static boolean brestfeedingTime = false;
	String strbreastFeeding;
	String reason;
	String dateTime;
	boolean isBreastFeedingEntered = false;// 27Feb2017 Arpitha

	// String strlastentry;//17July2017 Arpitha - v2.6.1 bug fixing
	static String strpostpartumdatetime;// 20Sep2017 Arpitha
	static Date postpartumdatetime;// 20Sep2017 Arpitha

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_fourthstage, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			woman = (Women_Profile_Pojo) getActivity().getIntent().getSerializableExtra("woman");

			dbh = Partograph_DB.getInstance(getActivity());
			initializeScreen(aq.id(R.id.rlfourthstage).getView());

			// updated on 23Aug2016 by bindu
			getwomanbasicdata();

			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());

			initialView();

			// updated on 10july2016 by Arpitha
			// added datepicker to allow date change for 4th stage of Labor
			aq.id(R.id.etdateoffourthstage).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						brestfeedingDate = false;// 13Feb2017 Arpitha
						caldatepicker();
					}
					return true;
				}
			});

			aq.id(R.id.ettimeoffourthstage).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {

						brestfeedingTime = false;// 13Feb2017 Arpitha

						strfourthstagetime = aq.id(R.id.ettimeoffourthstage).getText().toString();

						if (strfourthstagetime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(strfourthstagetime);
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						showtimepicker();
					}
					return true;
				}
			});

			// 22Nov2016 Arpitha
			strlastentry = dbh.getlastentrytime(woman.getWomenId(), 0);

			// 13Feb2017 Arpitha

			dateBreastFeeding = Partograph_CommonClass.getTodaysDate();
			aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
					Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));

			aq.id(R.id.etdate).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {

							brestfeedingDate = true;// 13Feb2017 Arpitha
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			aq.id(R.id.ettime).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					brestfeedingTime = true;// 13Feb2017 Arpitha

					if (MotionEvent.ACTION_UP == event.getAction()) {
						strbfTime = aq.id(R.id.ettime).getText().toString();

						if (strbfTime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(strbfTime);
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						showtimepicker();
					}
					return true;
				}
			});

			setInputFiltersForEdittext();// 10April2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// updated on 23Aug2016 by bindu
	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wnamef).text(woman.getWomen_name());
			aq.id(R.id.wagef).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoaf).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {
				aq.id(R.id.wdoaf).text(getResources().getString(R.string.reg) + ": " + woman.getDate_of_reg_entry());
			}
			aq.id(R.id.wgestf)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));// 23Aug2016

			aq.id(R.id.wgravidaf).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);// 07Oct2016
				// Arpitha
			} else
				risk = getResources().getString(R.string.low);// 07Oct2016
			// Arpitha
			aq.id(R.id.wtriskf).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatusf)
						.text(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);
				tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				aq.id(R.id.wdeldatef).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

			}

		}
	}

	// Initial View
	private void initialView() throws Exception {
		obj_Id = 12;
		todaysDate = Partograph_CommonClass.getTodaysDate();
		if (woman != null) {
			strWomenid = woman.getWomenId();
			strwuserId = woman.getUserId();
		}

		aq.id(R.id.btnfourthstagenext).gone();

		String displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		aq.id(R.id.etdateoffourthstage).text(displaydateformat);

		aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());

		// update on 10july2016 by Arpitha
		aq.id(R.id.etdateoffourthstage).enabled(true);
		aq.id(R.id.ettimeoffourthstage).enabled(true);

		cursor = dbh.GetFourthStageDetails(strWomenid, strwuserId, 12);
		if (cursor.getCount() > 0 && cursor != null) {
			cursor.moveToLast();
			strLastTime = cursor.getString(0) + "_" + cursor.getString(1);
			cursor.moveToFirst();// 28April2017 Arpitha- 2.5 bug fixing
			do {
				if (strbreastFeeding == null || (strbreastFeeding != null && strbreastFeeding.equalsIgnoreCase("0"))) {
					strbreastFeeding = cursor.getString(9);// 13Feb2017 Arpitha
					reason = cursor.getString(10);// 13Feb2017 Arpitha
					dateTime = cursor.getString(11);// 13Feb2017 Arpitha
				}
			} while (cursor.moveToNext());
		} else
			strLastTime = todaysDate + "_" + "00:00";

		strfourthstagedate = todaysDate;// 05Jan2017 Arpitha

		// 13Feb2017 Arpitha

		if (strbreastFeeding != null && strbreastFeeding.equalsIgnoreCase("1")) {
			isBreastFeedingEntered = true;// 27Feb2017 Arpitha
			aq.id(R.id.rdbfno).checked(true);
			aq.id(R.id.rdbfyes).checked(false);
			aq.id(R.id.etreason).text(reason);
			aq.id(R.id.trreason).visible();
			aq.id(R.id.rdbfno).enabled(false);
			aq.id(R.id.rdbfyes).enabled(false);
			aq.id(R.id.etreason).enabled(false);
			aq.id(R.id.trdate).gone();
			aq.id(R.id.trtime).gone();
		} else if (strbreastFeeding != null && strbreastFeeding.equalsIgnoreCase("2")) {
			isBreastFeedingEntered = true;// 27Feb2017 Arpitha
			aq.id(R.id.rdbfyes).checked(true);
			aq.id(R.id.rdbfno).checked(false);
			aq.id(R.id.trdate).visible();
			aq.id(R.id.trtime).visible();
			String[] date = dateTime.split("\\s+");
			aq.id(R.id.etdate).text(date[0]);
			aq.id(R.id.ettime).text(date[1]);
			aq.id(R.id.rdbfno).enabled(false);
			aq.id(R.id.rdbfyes).enabled(false);
			aq.id(R.id.etdate).enabled(false);
			aq.id(R.id.ettime).enabled(false);
			aq.id(R.id.etdate).text(dateTime.substring(0, 10));// 12April2017
																// Arpitha
			aq.id(R.id.ettime).text(dateTime.substring(11));// 12April2017
															// Arpitha
		} else {
			isBreastFeedingEntered = false;// 27Feb2017 Arpitha
			aq.id(R.id.rdbfyes).checked(false);
			aq.id(R.id.rdbfno).checked(false);
			strbfTime = Partograph_CommonClass.getCurrentTime();// 13Feb2017
			aq.id(R.id.ettime).text(strbfTime);// 13Feb2017
		} // 13Feb2017 Arpitha
			// }

		// strbfTime = Partograph_CommonClass.getCurrentTime();// 13Feb2017
		// // Arpitha
		// aq.id(R.id.ettime).text(strbfTime);// 13Feb2017
		// Arpitha

		// 10Aug2017 Atpitha
		ArrayList<DiscgargePojo> arrVal = new ArrayList<DiscgargePojo>();
		arrVal = dbh.getDischargeData(woman.getUserId(), woman.getWomenId());
		if (arrVal.size() > 0) {
			aq.id(R.id.txtdisablefs).visible();
			// txtdisabled.setVisibility(View.VISIBLE);
			// aq.id(R.id.txtdisable).gone();
			aq.id(R.id.tbldetailsf).gone();
			aq.id(R.id.scfourthstage).gone();
			aq.id(R.id.btnsavefourthstage).enabled(false);
			aq.id(R.id.btnsavefourthstage).backgroundColor(getResources().getColor(R.color.black));

		} // 10Aug2017 Atpitha

		strpostpartumdatetime = dbh.getPostPartumDateTime(woman.getWomenId(), woman.getUserId());// 20Sep2017
		// Arpitha
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			}

			if (v.getClass() == RadioButton.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}
			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
			case R.id.rdcontracted:
				uterus = 0;
				break;
			case R.id.rdrelaxed:
				uterus = 1;
				break;
			case R.id.rdurinepassedyes:
				urinepassed = 0;
				break;
			case R.id.rdurinepassedno:
				urinepassed = 1;
				break;
			case R.id.rdnormal:
				bleeding = 0;
				break;
			case R.id.rdexcess:
				bleeding = 1;
				break;

			case R.id.btnsavefourthstage: {

				if (Partograph_CommonClass.autodatetime(getActivity())) {

					Date lastregdate = null;
					String strlastinserteddate = dbh.getlastmaxdate(strwuserId,
							getResources().getString(R.string.partograph));
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (strlastinserteddate != null) {
						lastregdate = format.parse(strlastinserteddate);
					}
					Date currentdatetime = new Date();
					if (lastregdate != null && currentdatetime.before(lastregdate)) {
						Partograph_CommonClass.showSettingsAlert(getActivity(),
								getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
					} else {// 27Sep2016 Arpitha

						strCurrentTime = todaysDate + "_" + Partograph_CommonClass.getCurrentTime();
						
						strCurrentTime  =strfourthstagedate+"_"+aq.id(R.id.ettimeoffourthstage).getText().toString();

						isValidTime = Partograph_CommonClass.getisValidTime(strLastTime, strCurrentTime, 30);

						if (isValidTime) {
							if (validatePulseandBP()) {

								if (aq.id(R.id.etpulsevalfourth).getText().toString().length() > 0) {
									pulval = Integer.parseInt(aq.id(R.id.etpulsevalfourth).getText().toString());
								}

								if (aq.id(R.id.etbpsystolicfourth).getText().toString().length() > 0
										&& aq.id(R.id.etbpdiastolicfourth).getText().toString().length() > 0) {
									bpsysval = Integer.parseInt(aq.id(R.id.etbpsystolicfourth).getText().toString());
									bpdiaval = Integer.parseInt(aq.id(R.id.etbpdiastolicfourth).getText().toString());
								}

								if (!((pulval > 120 || pulval < 50) || (bpsysval < 80 || bpsysval > 160)
										|| (bpdiaval < 50 || bpdiaval > 110))) {
									// // updated bindu - 25Aug2016 - check
									// confirmation of third stage date and time
									String msg = getResources().getString(R.string.fourthstagedatetimerecheck)
											+ aq.id(R.id.etdateoffourthstage).getText().toString() + " / "
											+ aq.id(R.id.ettimeoffourthstage).getText().toString()
											+ getResources().getString(R.string.confirmrecheck);
									ConfirmAlert(msg, getResources().getString(R.string.fourthstage));
								} else {
									displayConfirmationAlert("", getResources().getString(R.string.confirmation));
								}

							}
						} else {
							Toast.makeText(getActivity(), getResources().getString(R.string.pbpcontractvalidtime),
									Toast.LENGTH_LONG).show();
						}
					}
				}
			}
				break;
			case R.id.btnviewfourthstage: {
				Intent fourthstageview = new Intent(getActivity(), FourthStageOfLabor_View.class);
				fourthstageview.putExtra("woman", woman);
				startActivity(fourthstageview);
				break;
			}

			// 13Feb2017 Arpitha

			case R.id.rdbfyes:
				breastFeeding = 2;
				aq.id(R.id.trreason).gone();
				aq.id(R.id.trdate).visible();
				aq.id(R.id.trtime).visible();
				break;

			case R.id.rdbfno:
				breastFeeding = 1;
				aq.id(R.id.etreason).getEditText().requestFocus();
				aq.id(R.id.trreason).visible();
				aq.id(R.id.trdate).gone();
				aq.id(R.id.trtime).gone();
				break;

			default:
				break;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
		}
	}

	// Reset the values
	private void resetValues() throws Exception {
		aq.id(R.id.etcommentsfourthstage).text("");
		aq.id(R.id.etpulsevalfourth).text("");
		aq.id(R.id.etbpsystolicfourth).text("");
		aq.id(R.id.etbpdiastolicfourth).text("");
		aq.id(R.id.rdcontracted).checked(true);
		aq.id(R.id.rdurinepassedyes).checked(true);
		aq.id(R.id.rdnormal).checked(true);

		// 13Feb2017 Arpitha
		if (strbreastFeeding.equalsIgnoreCase("1")) {
			aq.id(R.id.rdbfno).checked(true);
			aq.id(R.id.rdbfyes).checked(false);
			aq.id(R.id.etreason).text(reason);
			aq.id(R.id.trreason).visible();
			aq.id(R.id.rdbfno).enabled(false);
			aq.id(R.id.rdbfyes).enabled(false);
			aq.id(R.id.etreason).enabled(false);
		} else if (strbreastFeeding.equalsIgnoreCase("2")) {
			aq.id(R.id.rdbfyes).checked(true);
			aq.id(R.id.rdbfno).checked(false);
			aq.id(R.id.trdate).visible();
			aq.id(R.id.etdate).text(dateTime);
			aq.id(R.id.rdbfno).enabled(false);
			aq.id(R.id.rdbfyes).enabled(false);
			aq.id(R.id.etdate).enabled(false);
		} else {
			aq.id(R.id.rdbfyes).checked(false);
			aq.id(R.id.rdbfno).checked(false);
		} // 13Feb2017 Arpitha

	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(), getResources().getString(R.string.failed), Toast.LENGTH_LONG).show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// updated on 19Nov2015
		Partograph_CommonClass.isRecordExistsForSync = true;
		Partograph_CommonClass.refreshActionBarMenu(getActivity());
		calSyncMtd();
		// 31Oct2016 Arpitha
		aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass
				.getConvertedDateFormat(Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
		aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());
		aq.id(R.id.etpulsevalfourth).text("");
		aq.id(R.id.etbpdiastolicfourth).text("");
		aq.id(R.id.etbpsystolicfourth).text("");
		aq.id(R.id.etcommentsfourthstage).text("");
		aq.id(R.id.rdcontracted).checked(true);
		aq.id(R.id.rdrelaxed).checked(false);
		aq.id(R.id.rduterineyes).checked(true);
		aq.id(R.id.rduterineno).checked(false);
		aq.id(R.id.rdnormal).checked(true);
		aq.id(R.id.rdexcess).checked(false);// 31Oct2016 Arpitha
		Toast.makeText(getActivity(), getResources().getString(R.string.success), Toast.LENGTH_LONG).show();
		Intent fourthstageview = new Intent(getActivity(), FourthStageOfLabor_View.class);
		fourthstageview.putExtra("woman", woman);

		startActivity(fourthstageview);

	}

	// Sync
	private void calSyncMtd() throws Exception {
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	/**
	 * validate Pulse and BP - 12jan2016
	 * 
	 * @return
	 */
	private boolean validatePulseandBP() throws Exception {
		if ((aq.id(R.id.etpulsevalfourth).getEditText().getText().toString().trim().length() <= 0)
				&& (aq.id(R.id.etbpsystolicfourth).getEditText().getText().toString().trim().length() <= 0
						&& aq.id(R.id.etbpdiastolicfourth).getEditText().getText().toString().trim().length() <= 0)) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enter_pulse_bpsys_bpdia_val),
					Toast.LENGTH_LONG).show();// 12April2017 Arpitha
			aq.id(R.id.etpulsevalfourth).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etpulsevalfourth).getEditText().getText().toString().trim().length() <= 0) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enterpulseval), Toast.LENGTH_LONG).show();
			aq.id(R.id.etpulsevalfourth).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etbpsystolicfourth).getEditText().getText().toString().trim().length() <= 0) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enterbpsystolicval), Toast.LENGTH_LONG)
					.show();
			aq.id(R.id.etbpsystolicfourth).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etbpdiastolicfourth).getEditText().getText().toString().trim().length() <= 0) {
			Toast.makeText(getActivity(), getResources().getString(R.string.enterbpdiastolicval), Toast.LENGTH_LONG)
					.show();
			aq.id(R.id.etbpdiastolicfourth).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.ettimeoffourthstage).getText().length() >= 1) {
			String date1, date2 = null;

			aq.id(R.id.ettimeoffourthstage).getEditText().requestFocus();
			if ((StageofLabor.isThirdStageDetails))// 13Feb2017 Arpitha
			{
				// if ((ThirdStageOfLabor.strthirdstagedate != null &&
				// ThirdStageOfLabor.strthirdstagetime != null)) {commented on
				// 12April2017 Arpitha
				if ((ThirdStageOfLabor.thidStageDate != null && ThirdStageOfLabor.thirdStageTime != null)) {
					if (ThirdStageOfLabor.duration != null && Integer.parseInt(ThirdStageOfLabor.duration) > 0) {
						String time = ThirdStageOfLabor.thirdStageTime;
						String[] a = time.split(":");

						// changed on 28july2016 by Arpitha
						int thirdstagetime = Integer.parseInt(a[1]);
						String thirdstage_time = a[0] + ":" + ("" + thirdstagetime);
						String date = Partograph_CommonClass.getConvertedDateFormat(ThirdStageOfLabor.thidStageDate,
								Partograph_CommonClass.defdateformat);

						date1 = ThirdStageOfLabor.thidStageDate + " " + ThirdStageOfLabor.thirdStageTime;// 17Nov2016
																											// Arpitha
						date1 = date + " " + thirdstage_time;
						date2 = strfourthstagedate + " " + aq.id(R.id.ettimeoffourthstage).getText().toString();
						if (!isTimevalid(date1, date2)) {
							displayAlertDialog(getResources().getString(R.string.before_thrdstage_dt));

							return false;
						}
					}
				}
			} else {
				displayAlertDialog(getResources().getString(R.string.enter_thirdstage_details));
				return false;
			}

			date2 = strfourthstagedate + " " + aq.id(R.id.ettimeoffourthstage).getText().toString();
			;// 05Jan2017 Arpitha
			if (strlastentry != null) {
				if ((!(isTimevalid(strlastentry, date2)))) {
					displayAlertDialog(getResources().getString(R.string.fourthstage_datetime_cannot_before_lastparto));
					return false;
				}
			} // 22Nov2016 Arpitha
		}

		// 13Feb2017 Arpitha
		if (aq.id(R.id.rdbfno).isChecked() && aq.id(R.id.etreason).getText().toString().trim().length() <= 0) {
			Toast.makeText(getActivity(), getResources().getString(R.string.pz_enter_reason_for_not_breast_feeding),
					Toast.LENGTH_LONG).show();
			return false;
		}

		return true;

	}

	// updated on 10july2016 by Arpitha

	private void caldatepicker() {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor)

					+ "'>" + getResources().getString(R.string.pickadate) + "</font>"));
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {
					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		try {
			
			// 20Sep2017 Arpitha
			if (strpostpartumdatetime != null && strpostpartumdatetime.trim().length() > 0) {
				postpartumdatetime = new SimpleDateFormat("yyyy-MM-dd").parse(strpostpartumdatetime);
			} // 20Sep2017 Arpitha

			if (brestfeedingDate)// 13Feb2017 Arpitha
			{
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
				Date selected = date.parse(selecteddate);
				String deldate = woman.getDel_Date();
				Date delDate = null;
				Date lastEntry = null;

				Date thirdstage = null;// 02March2017 Arpitha
				if (ThirdStageOfLabor.thidStageDate != null) {// 16Jan2017
																// Arpitha
					thirdstage = new SimpleDateFormat("yyyy-MM-dd").parse(ThirdStageOfLabor.thidStageDate);
				} // 02March2017 Arpitha

				if (deldate != null) {
					delDate = date.parse(deldate);
				}
				if (strlastentry != null) {
					lastEntry = date.parse(strlastentry);
				}
				
				
				
				if (selected.after(new Date())) {
					Toast.makeText(getActivity(),
							getResources().getString(R.string.date_cannot_be_greater_than_current_date),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
							Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
				} else if (delDate != null && selected.before(delDate)) {
					Toast.makeText(getActivity(),
							getResources().getString(R.string.breastfeeding_date_cannot_be_before_del_date),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
							Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));

				} else if (thirdstage != null && selected.before(thirdstage)) {

					String thirdstageDate = Partograph_CommonClass.getConvertedDateFormat(
							ThirdStageOfLabor.thidStageDate, Partograph_CommonClass.defdateformat);// 25April2017
																									// Arpitha

					Toast.makeText(getActivity(),
							getResources().getString(R.string.before_thrdstage) + " " + thirdstageDate,
							Toast.LENGTH_SHORT).show();
					aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strfourthstagedate = todaysDate;
				}

				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && selected.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getActivity(),
							getResources().getString(R.string.breastfeeding_datetime_cannot_be_before_postpartum_datetime)
									+ " - " + latdate ,
							Toast.LENGTH_SHORT).show();

					aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					//strfourthstagedate = todaysDate;

				} // 20Sep2017 Arpitha

				else if (lastEntry != null && selected.before(lastEntry)) {
					Toast.makeText(getActivity(),
							getResources().getString(R.string.breastfeeding_date_cannot_be_before_lastparto_date),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(
							Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
				} else {
					dateBreastFeeding = selecteddate;
					aq.id(R.id.etdate).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
				}
			}

			else// 13Feb2017 Arpitha
			{

				Date taken = new SimpleDateFormat("yyyy-MM-dd").parse(selecteddate);
				// Date regdate = new
				// SimpleDateFormat("yyyy-MM-dd").parse(woman.getDate_of_admission());
				// Date deldate = new
				// SimpleDateFormat("yyyy-MM-dd").parse(woman.getDel_Date());
				Date thirdstage = null;
				if (ThirdStageOfLabor.thidStageDate != null) {// 16Jan2017
																// Arpitha
					thirdstage = new SimpleDateFormat("yyyy-MM-dd").parse(ThirdStageOfLabor.thidStageDate);
				}
				// 22Nov2016 Arpitha
				Date lastentry = null;
				if (strlastentry != null) {
					lastentry = new SimpleDateFormat("yyyy-MM-dd").parse(strlastentry);
				} // 22Nov2016 Arpitha

				if (taken.after(new Date())) {
					Toast.makeText(getActivity(), getResources().getString(R.string.curr_date_validation),
							Toast.LENGTH_SHORT).show();
					aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strfourthstagedate = todaysDate;
				}

				else if (thirdstage != null && taken.before(thirdstage)) {

					String thirdstageDate = Partograph_CommonClass.getConvertedDateFormat(
							ThirdStageOfLabor.thidStageDate, Partograph_CommonClass.defdateformat);// 25April2017
																									// Arpitha

					Toast.makeText(getActivity(),
							getResources().getString(R.string.before_thrdstage) + " " + thirdstageDate,
							Toast.LENGTH_SHORT).show();
					aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strfourthstagedate = todaysDate;
				}
				// 22Nov2016 Arpitha
				else if (lastentry != null && taken.before(lastentry) && woman.getregtype() != 2) {

					String lastEntryDate = Partograph_CommonClass.getConvertedDateFormat(
							ThirdStageOfLabor.strlastentry.substring(0, 10), Partograph_CommonClass.defdateformat);// 25April2017
																													// Arpitha

					Toast.makeText(getActivity(),
							getResources().getString(R.string.fourthstage_datetime_cannot_before_lastparto) + " "
									+ lastEntryDate + " " + strlastentry.substring(11),
							Toast.LENGTH_SHORT).show();
					aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strfourthstagedate = todaysDate;
				} // 22Nov2016 Arpitha
				
				// 20Sep2017 Arpitha
				else if (postpartumdatetime != null && taken.before(postpartumdatetime)) {

					String latdate = Partograph_CommonClass.getConvertedDateFormat(strpostpartumdatetime.split("/")[0],
							Partograph_CommonClass.defdateformat);
					Toast.makeText(getActivity(),
							getResources().getString(R.string.fourthstage_datetime_cannot_be_before_postpartum_datetime)
									+ " - " + latdate ,
							Toast.LENGTH_SHORT).show();

					aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strfourthstagedate = todaysDate;

				} // 20Sep2017 Arpitha

				else {
					aq.id(R.id.etdateoffourthstage).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					strfourthstagedate = selecteddate;
				}

			} // 13Feb2017 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public void showtimepicker() {
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(this.getFragmentManager(), "timePicker");
	}

	public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
		public TimePickerFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

			try {

				if (brestfeedingTime) {

					Date selected;
					String strselectedtime;
					Date currenttime;
					Date lastentry = null;
					Date del = null;
					SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					currenttime = date.parse(
							Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime());
					strselectedtime = dateBreastFeeding + " " + new StringBuilder().append(padding_str(hourOfDay))
							.append(":").append(padding_str(minute));

					selected = date.parse(strselectedtime);

					if (woman.getDel_Date() != null) {

						del = date.parse(woman.getDel_Date() + " " + woman.getDel_Time());
					}

					// 17July2017 Arpitha - v2.6.1 bug fixing
					if (strlastentry != null) {
						lastentry = date.parse(strlastentry);
					} // 17July2017 Arpitha - v2.6.1 bug fixing

					if (selected.after(currenttime)) {
						Toast.makeText(getActivity(), getResources().getString(R.string.invalid_date),
								Toast.LENGTH_LONG).show();
						aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

					} else if (del != null && selected.before(del)) {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.breastfeeding_time_cannot_be_before_del_date),
								Toast.LENGTH_LONG).show();
						aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

					} else if (lastentry != null && selected.before(lastentry)) {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.breastfeeding_time_cannot_be_before_lastparto_date),
								Toast.LENGTH_LONG).show();
						aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

					}

					// 20Sep2017 Arpitha
					else if (postpartumdatetime != null && selected.before(postpartumdatetime)) {

						String latdate = Partograph_CommonClass.getConvertedDateFormat(
								strpostpartumdatetime.split("/")[0], Partograph_CommonClass.defdateformat);
						Toast.makeText(getActivity(),
								getResources()
										.getString(R.string.fourthstage_datetime_cannot_be_before_postpartum_datetime)
										+ " - " + latdate + " " + strpostpartumdatetime.split("/")[1],
								Toast.LENGTH_SHORT).show();

						aq.id(R.id.ettime).text(Partograph_CommonClass.getCurrentTime());

					} // 20Sep2017 Arpitha

					else {
						aq.id(R.id.ettime).text(new StringBuilder().append(padding_str(hourOfDay)).append(":")
								.append(padding_str(minute)));
						strbfTime = aq.id(R.id.ettime).getText().toString();// 12ZApril2017
																			// Arpitha
					}

				}

				else {

					// String thidSageDate =
					// Partograph_CommonClass.getConvertedDateFormat(ThirdStageOfLabor.thidStageDate,
					// "yyyy-MM-dd");
					String strthirdstagedate = ThirdStageOfLabor.thidStageDate + " " + ThirdStageOfLabor.thirdStageTime;// changed
																														// on
																														// 17Nov2016
																														// Arpitha
					Date thirdstagedate = null;
					if (strthirdstagedate != null) {// 16Jan2017 Arpitha
						thirdstagedate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strthirdstagedate);
					}
					String strcurrenttime = Partograph_CommonClass.getTodaysDate() + " "
							+ Partograph_CommonClass.getCurrentTime();
					Date currenttime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strcurrenttime);

					String strfourthstage;
					if (strfourthstagedate != null)// 10Nov2016 Arpitha
					{
						strfourthstage = strfourthstagedate + " " + new StringBuilder().append(padding_str(hourOfDay))
								.append(":").append(padding_str(minute));

					} else
						strfourthstage = todaysDate + " " + new StringBuilder().append(padding_str(hourOfDay))
								.append(":").append(padding_str(minute));// 10Nov2016
																			// Arpitha

					// 22Nov2016 Arpitha
					Date lastentry = null;
					if (strlastentry != null) {
						lastentry = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strlastentry);
					} // 22Nov2016 Arpitha

					Date fourthstage = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strfourthstage);

					String strdelDate = woman.getDel_Date() + " " + woman.getDel_Time();
					Date deldate = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(strdelDate);

					if (fourthstage.after(currenttime)) {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.date_cannot_be_greater_than_current_date),
								Toast.LENGTH_LONG).show();
						aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());
					} else if (deldate != null && fourthstage.before(deldate)) {// 12April2017
																				// Arpitha
						Toast.makeText(getActivity(),
								getResources().getString(R.string.fourth_stage_datetime_cannot_be_before_deldatetime)
										+ " " + strdelDate,
								Toast.LENGTH_LONG).show();// 12April2017 Arpil
						aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());

					} // 12April2017 Arpitha
					else if (fourthstage.before(thirdstagedate) && thirdstagedate != null) {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.before_thrdstage) + " "
										+ ThirdStageOfLabor.thidStageDate + " " + ThirdStageOfLabor.thirdStageTime,
								Toast.LENGTH_LONG).show();
						aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());

					} // 22Nov2016 Arpitha
					else if (lastentry != null && fourthstage.before(lastentry)) {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.fourthstage_datetime_cannot_before_lastparto) + " "
										+ strlastentry,
								Toast.LENGTH_SHORT).show();
						aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());
						strfourthstagedate = todaysDate;
					} // 22Nov2016 Arpitha

					// 20Sep2017 Arpitha
					else if (postpartumdatetime != null && fourthstage.before(postpartumdatetime)) {

						String latdate = Partograph_CommonClass.getConvertedDateFormat(
								strpostpartumdatetime.split("/")[0], Partograph_CommonClass.defdateformat);
						Toast.makeText(getActivity(),
								getResources()
										.getString(R.string.referral_datetime_cannot_be_before_postpartume_datetime)
										+ " - " + latdate + " " + strpostpartumdatetime.split("/")[1],
								Toast.LENGTH_SHORT).show();

						aq.id(R.id.ettimeoffourthstage).text(Partograph_CommonClass.getCurrentTime());
						strfourthstagedate = todaysDate;

					} // 20Sep2017 Arpitha

					else// 31Oct2016 Arpitha
						aq.id(R.id.ettimeoffourthstage).text(new StringBuilder().append(padding_str(hourOfDay))
								.append(":").append(padding_str(minute)));

				}

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	}

	private static String padding_str(int c) throws Exception {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	// updated on 13nov2015
	@SuppressLint("SimpleDateFormat")
	private boolean isTimevalid(String date1, String date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {
			dateofreg = sdf.parse(date1);
			dateofdelivery = sdf.parse(date2);
			Date d = sdf.parse(date1);
			Date s = sdf.parse(date2);

			if (s.before(d)) {
				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	void savafourthstagedata() throws Exception {

		ArrayList<String> fdata = new ArrayList<String>();
		fdata.add("" + uterus);
		fdata.add("" + urinepassed);
		fdata.add("" + bleeding);
		fdata.add("" + aq.id(R.id.etpulsevalfourth).getText().toString());
		fdata.add("" + aq.id(R.id.etbpsystolicfourth).getText().toString());
		fdata.add("" + aq.id(R.id.etbpdiastolicfourth).getText().toString());
		fdata.add(aq.id(R.id.etcommentsfourthstage).getText().toString());
		if (!isBreastFeedingEntered)// 27Feb2017 Arpitha
		{// 27Feb2017 Arpitha
			// 13Feb2017 Arpitha
			fdata.add("" + breastFeeding);

			fdata.add(aq.id(R.id.etreason).getText().toString());// 13Feb2017
																	// Arpitha

			fdata.add(dateBreastFeeding + " " + strbfTime);
		} // 27Feb2017 Arpitha

		ArrayList<String> prop_id = new ArrayList<String>();
		ArrayList<String> prop_name = new ArrayList<String>();
		Property_pojo pdata = new Property_pojo();
		Cursor cursor = dbh.getPropertyIds(obj_Id);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {

				prop_id.add(cursor.getString(0));
				prop_name.add(cursor.getString(1));

			} while (cursor.moveToNext());

			pdata.setObj_Id(obj_Id);
			pdata.setProp_id(prop_id);
			pdata.setProp_name(prop_name);
			pdata.setProp_value(fdata);
			// pdata.setStrdate(todaysDate);
			pdata.setStrdate(strfourthstagedate);
			pdata.setStrTime(aq.id(R.id.ettimeoffourthstage).getText().toString());

			// 05May2017 Arpitha - v2.6 mantis id-0000243
			if (uterus == 1)
				pdata.setIsDangerval1(1);
			else
				pdata.setIsDangerval1(0);// 05May2017 Arpitha - v2.6 mantis
											// id-0000243

			dbh.db.beginTransaction();
			int transId = dbh.iCreateNewTrans(strwuserId);

			boolean isAdded = dbh.insertData(pdata, strWomenid, strwuserId, transId);

			if (isAdded) {
				dbh.iNewRecordTrans(strwuserId, transId, Partograph_DB.TBL_PROPERTYVALUES);
				commitTrans();
			} else {
				rollbackTrans();
			}

			resetValues();
		}

	}

	// Display Confirmation 14/04/16
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.temp_alertdialog);

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		// 04Oct2016 ARpitha
		String alertmsg = "";
		String values = "";

		imgbtnyes.setText(getResources().getString(R.string.save));
		imgbtnno.setText(getResources().getString(R.string.recheck));

		if (pulval > 120 || pulval < 50) {
			alertmsg = alertmsg + getResources().getString(R.string.pul_val) + "\n";
			values = values + "" + pulval + "\n";

		}
		if (bpsysval < 80 || bpsysval > 160) {
			alertmsg = alertmsg + getResources().getString(R.string.bp_sys) + "\n";
			values = values + "" + bpsysval + "\n";

		}
		if (bpdiaval < 50 || bpdiaval >= 110) {
			alertmsg = alertmsg + getResources().getString(R.string.bp_dia) + "\n";
			values = values + "" + bpdiaval + "\n";

		}
		txtdialog6.setText(getResources().getString(R.string.bp_val));

		txtdialog.setText(alertmsg);
		txtdialog1.setText(values);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				if (classname.equalsIgnoreCase(getResources().getString(R.string.confirmation))) {
					try {
						// updated bindu - 25Aug2016 - check confirmation of
						// third stage date and time
						String msg = getResources().getString(R.string.fourthstagedatetimerecheck)
								+ aq.id(R.id.etdateoffourthstage).getText().toString() + " / "
								+ aq.id(R.id.ettimeoffourthstage).getText().toString()
								+ getResources().getString(R.string.confirmrecheck);
						ConfirmAlert(msg, getResources().getString(R.string.fourthstage));
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();

			}
		});
	}

	// 16Aug2016 - bindu
	public void doWork() throws Exception {

		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						String curTime = Partograph_CommonClass.getCurrentTime();
						aq.id(R.id.ettimeoffourthstage).text(curTime);

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
		}
	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {

					doWork();
					Thread.sleep(1000); // Pause of 1 Second

				} catch (InterruptedException e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					Thread.currentThread().interrupt();
					myThread.stop();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
					myThread.stop();
				}
			}
		}
	}

	/**
	 * updated bindu - 25Aug2016 Save Confirmation Alert
	 * 
	 * @param goToScreen
	 */
	private void ConfirmAlert(String msg, final String goToScreen) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(msg).setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.recheck), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						if (goToScreen.equalsIgnoreCase(getResources().getString(R.string.fourthstage))) {
							try {
								aq.id(R.id.etdateoffourthstage).getEditText().requestFocus();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						dialog.cancel();
					}
				}).setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							if (goToScreen.equalsIgnoreCase(getResources().getString(R.string.fourthstage))) {
								savafourthstagedata();
								dialog.cancel();
							} else
								dialog.cancel();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);

							e.printStackTrace();
						}

					}
				});
		AlertDialog alertDialog1 = alertDialogBuilder.create();
		alertDialog1.show();
	}

	// 10April2017 Arpitha
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		aq.id(R.id.etcommentsfourthstage).getEditText().setFilters(new InputFilter[] { filter });
		aq.id(R.id.etreason).getEditText().setFilters(new InputFilter[] { filter });

	}

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*.[]1234567890Â¶";
			String blockCharacterSet = "~#^|$%*!@/()-'\":;?{}=!$^';?Ã—Ã·<>{}â‚¬Â£Â¥â‚©%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\Â¥Â®Â¿ÃŒâ„¢â‚¹Â°^âˆšÏ€Ã·Ã—â–³Â¶Â£â€¢Â¢â‚¬â™¥â™¡â˜…â˜†â–²â–¼â†‘â†�â†“â†’Â¤â–³â™‚â™€â„ƒ||â–³Â©c/o||Â¿Â¡â„…â„¢Â®â‚¹Â°Â¢`â€¢âˆšÏ€Â¶âˆ†Â¢Â°âˆ†Â¶Ï€âˆšâ€¢`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

}
