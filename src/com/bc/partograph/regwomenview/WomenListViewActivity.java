/*package com.bc.partograph.regwomenview;


import java.util.Locale;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.FragmentState;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.registration.RegistrationActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.FourthStageOfLabor_View;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bc.partograph.stagesoflabor.ThirdStageOfLabor_View;
import com.bc.partograph.sync.SyncFunctions;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.apgar.Activity_View_Apgar;
import com.bluecrimson.apgar.Apgar_Pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

public class WomenListViewActivity extends FragmentActivity implements ActionBar.TabListener{
	
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private static final String TAB_KEY_INDEX = "tab_key";

	// Tab titles


	public static String[] tabs;
	FragmentState fragment = null;
	public static MenuItem menuItem;
	Partograph_DB dbh;
	UserPojo user;
	Cursor cursor;
	
//	04jan2016
	public static boolean[] checkSelected;	// store select/unselect information about the values in the list
//  16 may16 Arpitha
	
	public static boolean isreferral = false;
	
	@Override
	protected void onCreate(Bundle saved) {
		// TODO Auto-generated method stub
		super.onCreate(saved);
		
		
		
		try {
		setContentView(R.layout.activity_view);
		dbh = Partograph_DB.getInstance(getApplicationContext());
		user = Partograph_CommonClass.user;
		
		
		
		Locale locale = null;
		
		if(AppContext.prefs.getBoolean("isEnglish", false)){
			locale = new Locale("en");
			Locale.setDefault(locale);
		}else if(AppContext.prefs.getBoolean("isHindi", false)){
			locale = new Locale("hi");
			Locale.setDefault(locale);
		}
		
		Configuration config = new Configuration();
		config.locale = locale; 
		getResources().updateConfiguration(config, getResources().getDisplayMetrics());
		
		AppContext.setNamesAccordingToRes(getResources());
		
		tabs = new String[]{getResources().getString(R.string.today),getResources().getString(R.string.thisweek),getResources().getString(R.string.thismonth),getResources().getString(R.string.thisyear),getResources().getString(R.string.betweendates)};
		
		
		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setOffscreenPageLimit(1);
		actionBar = getActionBar();
		
		
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		
//		updated on 17Nov2015
		Partograph_CommonClass.isRecordExistsForSync = dbh.GetSyncCountRecords();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// Adding Tabs
					for (String tab_name : tabs) {
						actionBar.addTab(actionBar.newTab().setText(tab_name)
								.setTabListener(this));
					}
					
					
					
//					Load for today
				 cursor = dbh.getWomenDataByDate(1, Partograph_CommonClass.getTodaysDate(),
							user.getUserId(), getResources().getString(R.string.del_prog), 0, 0, "", "");
			if (cursor.getCount() > 0) {
				Partograph_CommonClass.strDelStatustoday = getResources().getString(R.string.del_prog);
				actionBar.setSelectedNavigationItem(0);
			} else {
				cursor = dbh.getWomenDataByDate(1,
						Partograph_CommonClass.getTodaysDate(),
						user.getUserId(),
						getResources().getString(R.string.delivered), 0, 0, "",
						"");
				if (cursor.getCount() > 0) {
					Partograph_CommonClass.strDelStatustoday = getResources().getString(R.string.delivered);
					actionBar.setSelectedNavigationItem(0);
				} else {
					Partograph_CommonClass.getCurrentWeek();
					cursor = dbh.getWomenDataByDate(2, "", user.getUserId(),
							getResources().getString(R.string.del_prog), 0, 0,
							"", "");
					if (cursor.getCount() > 0) {
						Partograph_CommonClass.strDelStatusweekly = getResources().getString(R.string.del_prog);
						actionBar.setSelectedNavigationItem(1);
					}
					else {
						cursor = dbh.getWomenDataByDate(2, "",
								user.getUserId(),
								getResources().getString(R.string.delivered),
								0, 0, "", "");
						if (cursor.getCount() > 0) {
							Partograph_CommonClass.strDelStatusweekly = getResources().getString(R.string.delivered);
							actionBar.setSelectedNavigationItem(1);
						}
						else {
							Partograph_CommonClass.getMonthYear(Partograph_CommonClass.getTodaysDate());
							cursor = dbh.getWomenDataByDate(3, "", user.getUserId(),
									getResources().getString(R.string.del_prog), Partograph_CommonClass.imon, Partograph_CommonClass.iyear, "fromdate", "todate");
							if(cursor.getCount()>0){
								Partograph_CommonClass.strDelStatusmonthly = getResources().getString(R.string.del_prog);
								actionBar.setSelectedNavigationItem(2);
							} else {
								cursor = dbh.getWomenDataByDate(3, "", user.getUserId(),
										getResources().getString(R.string.delivered), Partograph_CommonClass.imon, Partograph_CommonClass.iyear, "fromdate", "todate");
								if(cursor.getCount()>0){
									Partograph_CommonClass.strDelStatusmonthly = getResources().getString(R.string.delivered);
									actionBar.setSelectedNavigationItem(2);
								}
							}
						}
					}
				}
			}
					
					
					// restore to navigation
					if (saved != null) {
						actionBar.setSelectedNavigationItem(saved.getInt(
								TAB_KEY_INDEX, 0));
					}
					
//					partograph activity
					if(SlidingActivity.isGraph){
						actionBar.setSelectedNavigationItem(SlidingActivity.tabpos);
						SlidingActivity.isGraph = false;
					}
					
//					Registration activity
					if(RegistrationActivity.isRegistration){
						actionBar.setSelectedNavigationItem(RegistrationActivity.regtabpos);
						RegistrationActivity.isRegistration = false;
					}
					
//					ViewProfile activity
					if(ViewProfile_Activity.isViewactivity){
						actionBar.setSelectedNavigationItem(ViewProfile_Activity.viewtabpos);
						ViewProfile_Activity.isViewactivity = false;
					}
					

					
//					Apgar activity
					if(Activity_Apgar.isApgar){
						actionBar.setSelectedNavigationItem(Activity_Apgar.apgartabpos);
						Activity_Apgar.isApgar = false;
					}
					
//					View Apgar
					if(Activity_View_Apgar.isApgarview){
						actionBar.setSelectedNavigationItem(Activity_View_Apgar.apgarviewtabpos);
						Activity_View_Apgar.isApgarview = false;
					}
					
//					StageOFLabor
					if(StageofLabor.isStagesOfLabor){
						actionBar.setSelectedNavigationItem(StageofLabor.stagesoflaborpos);
						StageofLabor.isStagesOfLabor = false;
					}
					
//					View Third Stage of Labor
					if(ThirdStageOfLabor_View.isThirdStageOfLaborView){
						actionBar.setSelectedNavigationItem(ThirdStageOfLabor_View.viewthirdstagepos);
						ThirdStageOfLabor_View.isThirdStageOfLaborView = false;
					}
					
//					View Fourth Stage of Labor
					if(FourthStageOfLabor_View.isFourthStageOfLaborView){
						actionBar.setSelectedNavigationItem(FourthStageOfLabor_View.viewfourthstagepos);
						FourthStageOfLabor_View.isFourthStageOfLaborView = false;
					}
					
					*//**
					 * on swiping the viewpager make respective tab selected
					 * *//*
					viewPager.setOnPageChangeListener(new OnPageChangeListener() {

						@Override
						public void onPageSelected(int position) {

							try {
								// on changing the page
								// make respected tab selected
								Partograph_CommonClass.curr_tabpos = position;
								// actionBar.setSelectedNavigationItem(position);

								// Refresh the fragment
								fragment = (FragmentState) mAdapter.instantiateItem(
										viewPager, position);
								if (fragment != null) {
									fragment.fragmentVisible();
								}


								actionBar
										.setSelectedNavigationItem(Partograph_CommonClass.curr_tabpos);
							} catch (Exception e) {
								AppContext.addLog(
										new RuntimeException().getStackTrace()[0]
												.getMethodName()
												+ " - "
												+ this.getClass().getSimpleName(), e);
								e.printStackTrace();
							}
						}

						@Override
						public void onPageScrolled(int arg0, float arg1, int arg2) {
						}

						@Override
						public void onPageScrollStateChanged(int arg0) {
						}
					});

					actionBar
					.setSelectedNavigationItem(Partograph_CommonClass.curr_tabpos);
					
					
					KillAllActivitesAndGoToLogin.addToStack(this);
					KillAllActivitesAndGoToLogin.activity_stack.add(this);	
		}catch(Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}
	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		Partograph_CommonClass.curr_tabpos = tab.getPosition();
		viewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		try {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;

		case R.id.logout:
		Intent login = new Intent(this, LoginActivity.class);
			startActivity(login);
			return true;

		case R.id.registration:
			Intent registration = new Intent(this, RegistrationActivity.class);
			startActivity(registration);
			return true;
			
		case R.id.sync:
			 menuItem = item;
		     menuItem.setActionView(R.layout.progressbar);
		     menuItem.expandActionView();
			calSyncMtd();
			return true;

		case R.id.settings:
			Intent settings = new Intent(this, Settings_parto.class);
			startActivity(settings);
			return true;
			
		case R.id.about:
			Intent about = new Intent(this, About.class);
			startActivity(about);
			return true;
			
		}
		} catch(Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}
	
//	Sync
	private void calSyncMtd() throws Exception{
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		
		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}
	
	*//** This method Calls the idle timeout *//*
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(Partograph_CommonClass.isRecordExistsForSync)
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_red));
		else
			menu.findItem(R.id.sync).setIcon(getResources().getDrawable(R.drawable.ic_sync_green));
		return super.onPrepareOptionsMenu(menu);
	}
}
*/