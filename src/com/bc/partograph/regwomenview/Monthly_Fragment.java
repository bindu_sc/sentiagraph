/*package com.bc.partograph.regwomenview;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.FragmentState;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.comprehensiveview.View_Partograph;
import com.bc.partograph.regwomenlist.RecentCustomListAdapter;
import com.bc.partograph.regwomenview.Today_Fragment.CountDownRunner;
import com.bc.partograph.sliding.SlidingActivity;
import com.bc.partograph.stagesoflabor.StageofLabor;
import com.bluecrimson.apgar.Activity_Apgar;
import com.bluecrimson.partograph.ActionItem;
import com.bluecrimson.partograph.QuickAction;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.viewprofile.ViewProfile_Activity;

@SuppressLint("SimpleDateFormat") public class Monthly_Fragment extends Fragment implements FragmentState {

	View rootView;
	AQuery aq;
	Partograph_DB dbh;
	UserPojo user;
	int option;
	public static String delstatus_monthly, womenId, wUserId;
	ArrayList<Women_Profile_Pojo> rowItems;
	private int mSelectedRow = 0;
	QuickAction mQuickAction;
	public static final int ID_PROFILE = 1;
	public static final int ID_GRAPH = 2;
	public static final int ID_DELSTATUS = 3;
	public static final int ID_APGAR = 4;
	public static final int ID_COMPREHENSIVEVIEW = 5;
	String todaysDate;
	int mon, year, day;

	TextView txtdeltype, txtnoofchild, txtdelres1, txtchildwt1, txtchildsex1,
			txtdelres2, txtchildwt2, txtchildsex2, txtdelcomments, txtdeldate,
			txtdeltime, txtdeltime2, txtmothersdeathreason
			;
	TableRow trchilddet1, tr_childdetsex1, tr_childdet2, tr_childsex2,
			tr_secondchilddetails, tr_result2, tr_deltime2;
	EditText etbabywt1, etbabywt2, etdelcomments, etdeldate,
			etmothersdeathreason,tvmothersdeathreason;
	static EditText etdeltime;
	static EditText etdeltime2;
	Spinner spndel_type, spn_delres1, spn_delres2, spn_noofchild;
	RadioButton rdmale1, rdfemale1, rdmale2, rdfemale2;
	String no_of_child, selecteddate ;
	int delivery_type, delivery_result1, delivery_result2,
			babysex1 , babysex2 ;
	static boolean del_time1;
	static final int TIME_DIALOG_ID = 999;
	private static int hour;
	private static int minute;
	Button imgbtnok;
	int numofchildren;
//	CheckBox chkmotherdead;
	RadioGroup rdchildSexGrpbaby1, rdchildSexGrpbaby2;
	int year1, mon1, day1;
	RadioButton rd_delprogmonthly, rd_deliveredmonthly;
	public static String rddeltypemonthly;
	
	RadioButton rdmotheralive, rdmotherdead;
	String strDeldate;
//	updated on 13nov2015
	String strRegdate;
	
//	updated on 16Nov2015
	EditText etplaceofreferral, etreasonforreferral, etdateofreferral; 
	static EditText ettimeofreferral;
	Spinner spnreferralfacility;
	Button btnCancelRef, btnSaveRef;
	boolean isDateRefClicked = false;
	String strRefDate;
	public static final int ID_REFERRAL = 6;
	
	
//	updated on 23Nov2015
	static boolean isTimeRefClicked = false;
	int referralfacility;
	WomenReferral_pojo wrefpojo;
	ArrayList<WomenReferral_pojo> womenrefpojoItems;
	MultiSelectionSpinner mspndeltypereason;
	LinkedHashMap<String, Integer> deltypereasonMap;
	TableRow tr_deltypereason, tr_deltypeotherreasons;
	public static final int ID_STAGEOFLABOR = 7;
	EditText etdeltypeotherreasons;
	EditText txtdeltypereason, txtdeltypeotherreasons;
	
//	23dec2015
	public static final int ID_PRINTPARTO = 8;
	Thread myThread;
	
//	04jan2016
	EditText etdescriptionofreferral, etbpsystolicreferral ,
			etbpdiastolicreferral , etpulsereferral ;
			
	int conditionofmother=1, conditionofbaby=1;
	MultiSelectionSpinner mspnreasonforreferral;
	LinkedHashMap<String, Integer> reasonforreferralMap;
	RadioButton rd_motherconscious, rd_motherunconscious, rd_babyalive, rd_babydead, rdepiyes, rdepino, rdcatgut, rdvicryl;
	MultiSelectionSpinner mspntears;
	LinkedHashMap<String, Integer> tearsMap;
	TableRow trsuturematerial;
	int episiotomy = 1 , suturematerial = 0 ;
	TextView txtepisiotomy, txtsuturematerial;
	EditText ettears;
	TextView txtfirstchilddetails;
//	06jan2016
	boolean isDelUpdate	= false;
	
//	24jan2016
	TextView txtrefmode;
	
	// updated on 27Feb2016 by Arpitha
			EditText etheatrate,etspo2;
			TableRow trspo2,trheartrate;
			int spnpos = 0;
			ArrayList<WomenReferral_pojo> referralitems;
			int optionrefer=8;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_month, container, false);
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());

			aq = new AQuery(getActivity());

			dbh = Partograph_DB.getInstance(getActivity());
			user = Partograph_CommonClass.user;

			initializeScreen(aq.id(R.id.rlmonthly).getView());
			rd_delprogmonthly = (RadioButton) rootView
					.findViewById(R.id.rd_delprogmonthly);
			rd_deliveredmonthly = (RadioButton) rootView
					.findViewById(R.id.rd_deliveredmonthly);
			
			initialView();
			setspinnerdata();
			
			rd_delprogmonthly.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						delstatus_monthly = getResources().getString(
								R.string.del_prog);
						rddeltypemonthly = getResources().getString(
								R.string.del_prog);
						optionrefer=8;
						setspinnerdata();
						loadWomendata();
					} catch (Exception e) {
						AppContext.addLog(
								new RuntimeException().getStackTrace()[0]
										.getMethodName()
										+ " - "
										+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			rd_deliveredmonthly.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						delstatus_monthly = getResources().getString(
								R.string.delivered);
						rddeltypemonthly = getResources().getString(
								R.string.delivered);
						optionrefer =20;
						setspinnerdata();
						loadWomendata();
					} catch (Exception e) {
						AppContext.addLog(
								new RuntimeException().getStackTrace()[0]
										.getMethodName()
										+ " - "
										+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			aq.id(R.id.listwomenmonthly).getListView()
					.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapter, View v,
								int pos, long id) {
							try {

								mSelectedRow = pos; // set the selected row
								
								womenId = rowItems.get(mSelectedRow)
										.getWomenId();
								wUserId = rowItems.get(mSelectedRow)
										.getUserId(); 
								strRegdate = rowItems.get(mSelectedRow).getDate_of_admission() + " " + rowItems.get(mSelectedRow).getTime_of_admission();
								mQuickAction.show(v);
							} catch (Exception e) {
								AppContext.addLog(
										new RuntimeException().getStackTrace()[0]
												.getMethodName()
												+ " - "
												+ this.getClass()
														.getSimpleName(), e);
								e.printStackTrace();
							}

						}
					});

			mQuickAction = new QuickAction(getActivity());
			displayOptions();
			
//			26dec2015
			myThread = null;
			
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
		}

		KillAllActivitesAndGoToLogin.activity_stack.add(getActivity());
	}

	private void initialView() throws Exception {
		AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
				.getMethodName() + " - " + this.getClass().getSimpleName());
		option = 3;
		delstatus_monthly = getResources().getString(R.string.del_prog);
		todaysDate = Partograph_CommonClass.getTodaysDate();
		getMonthYear(todaysDate);
		
		if(Partograph_CommonClass.strDelStatusmonthly != null){
			delstatus_monthly = Partograph_CommonClass.strDelStatusmonthly;
			if(delstatus_monthly.equalsIgnoreCase(getResources().getString(R.string.del_prog)))
			{
				rd_delprogmonthly.setChecked(true);
			}
			else
			{
				rd_deliveredmonthly.setChecked(true);
			}
		}
		
		
		loadWomendata();
	}

	// Get the month , year
	private void getMonthYear(String todaysDate) throws Exception {
		AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
				.getMethodName() + " - " + this.getClass().getSimpleName());
		AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
				.getMethodName() + " - " + this.getClass().getSimpleName());
		Date today = Partograph_CommonClass.getDateS(todaysDate);
		day = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		mon = Integer.parseInt(new SimpleDateFormat("M").format(today));
		year = Integer.parseInt(new SimpleDateFormat("yyyy").format(today));

	}

	*//**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 *//*
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			return viewArrayList;
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}
		return result;
	}

	// Load women Data
	private void loadWomendata() throws Exception {
		AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
				.getMethodName() + " - " + this.getClass().getSimpleName());
		rowItems = new ArrayList<Women_Profile_Pojo>();
		Women_Profile_Pojo wdata;
		Cursor cur;

		if (rddeltypemonthly != null)
			delstatus_monthly = rddeltypemonthly;
		
		
			Cursor cursor = null;
			if(spnpos==2)
			{
				cursor = dbh.getWomenDataByDate(25, "", user.getUserId(),
						delstatus_monthly, mon, year, "fromdate", "todate");
			}
			else if(spnpos==1)
			{
				cursor = dbh.getWomenDataByDate(15, "", user.getUserId(), delstatus_monthly, mon, year, "fromdate", "todate");
			}else
			{

		 cursor = dbh.getWomenDataByDate(14, "", user.getUserId(),
				delstatus_monthly, mon, year, "fromdate", "todate");
			}
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();

			do {
				wdata = new Women_Profile_Pojo();
				wdata.setWomenId(cursor.getString(1));

				if (cursor.getType(2) > 0) {
					String b = (cursor.getString(2).length() > 1) ? cursor
							.getString(2) : null;
					byte[] decoded = (b != null) ? Base64.decode(b,
							Base64.DEFAULT) : null;
					wdata.setWomen_Image(decoded);
				}

				wdata.setWomen_name(cursor.getString(3));
				wdata.setDate_of_admission(cursor.getString(4));
				wdata.setTime_of_admission(cursor.getString(5));
				wdata.setAge(cursor.getInt(6));
				wdata.setAddress(cursor.getString(7));
				wdata.setPhone_No(cursor.getString(8));
				wdata.setDel_type(cursor.getInt(9));
				wdata.setDoc_name(cursor.getString(10));
				wdata.setNurse_name(cursor.getString(11));
				wdata.setW_attendant(cursor.getString(12));
				wdata.setGravida(cursor.getInt(13));
				wdata.setPara(cursor.getInt(14));
				wdata.setHosp_no(cursor.getString(15));
				wdata.setFacility(cursor.getString(16));
				wdata.setWomenId(cursor.getString(1));
				wdata.setSpecial_inst(cursor.getString(17));
				wdata.setUserId(cursor.getString(0));
				wdata.setComments(cursor.getString(19));
				// wdata.setCond_while_admn(cursor.getString(20));
				wdata.setRisk_category(cursor.getInt(20));
				wdata.setDel_Comments(cursor.getString(21));
				wdata.setDel_Time(cursor.getString(22));
				wdata.setDel_Date(cursor.getString(23));
				wdata.setDel_result1(cursor.getInt(24));
				wdata.setNo_of_child(cursor.getInt(25));
				wdata.setBabywt1(cursor.getInt(26));
				wdata.setBabywt2(cursor.getInt(27));
				wdata.setBabysex1(cursor.getInt(28));
				wdata.setBabysex2(cursor.getInt(29));
				wdata.setGestationage(cursor.getInt(30));
				wdata.setMothersdeath((cursor.getInt(32)) == 1 ? 1 : 0);
				wdata.setDel_result2(cursor.getInt(31));
				wdata.setAdmitted_with(cursor.getString(34));
				wdata.setMemb_pres_abs(cursor.getInt(35));
				wdata.setMothers_death_reason(cursor.getString(36));
				wdata.setState(cursor.getString(38));
				wdata.setDistrict(cursor.getString(39));
				wdata.setTaluk(cursor.getString(40));
				wdata.setFacility_name(cursor.getString(41));
				wdata.setGest_age_days(cursor.getInt(45));

//				updated on 23Nov2015
				wdata.setDel_time2(cursor.getString(33));
				
//				updated on 30Nov2015
				wdata.setThayicardnumber(cursor.getString(46));
				wdata.setDelTypeReason(cursor.getString(47));
				wdata.setDeltype_otherreasons(cursor.getString(48));
				
//				27dec2015
				wdata.setLmp(cursor.getString(49));
				wdata.setRiskoptions(cursor.getString(50));
				
//				04jan2016
				wdata.setTears(cursor.getString(51));
				wdata.setEpisiotomy(cursor.getInt(52));
				wdata.setSuturematerial(cursor.getInt(53));
				
//				19jan2016
				wdata.setBloodgroup(cursor.getInt(54));
				
//              updated by ARpitha				
				wdata.setEdd(cursor.getString(55));
				wdata.setHeight(cursor.getString(56));
				//wdata.setWeight(cursor.getInt(57));
				wdata.setOther(cursor.getString(58));
				
//              updated on 29May by Arpitha
				wdata.setHeight_unit(cursor.getInt(59));
				
				boolean isDanger = dbh.chkIsDanger(cursor.getString(1),
						user.getUserId());

				wdata.setDanger(isDanger);
				rowItems.add(wdata);
			} while (cursor.moveToNext());

			RecentCustomListAdapter adapter = new RecentCustomListAdapter(
					getActivity(), R.layout.activity_womenlist, rowItems, dbh);
			aq.id(R.id.listwomenmonthly).adapter(adapter);

			// cursor.close();
		}

		if (rowItems.size() <= 0) {
			aq.id(R.id.listwomenmonthly).gone();
		} else
			aq.id(R.id.listwomenmonthly).visible();
		
	//	}

		if (rddeltypemonthly != null) {
			if (rddeltypemonthly.equalsIgnoreCase(getResources().getString(
					R.string.del_prog))) {
				rd_delprogmonthly.setChecked(true);
				rd_deliveredmonthly.setChecked(false);
			} else {
				rd_deliveredmonthly.setChecked(true);
				rd_delprogmonthly.setChecked(false);
			}

		}
	}

	// Display options on click of list item
	private void displayOptions() {
		AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
				.getMethodName() + " - " + this.getClass().getSimpleName());

		try {

			ActionItem viewprofile_Item = new ActionItem(ID_PROFILE,
					getResources().getString(R.string.view_profile));
			ActionItem graph_Item = new ActionItem(ID_GRAPH, getResources()
					.getString(R.string.partograph));

			ActionItem deliverystatus_Item = new ActionItem(ID_DELSTATUS,
					getResources().getString(R.string.delivery_status));
			ActionItem apgarscore_Item = new ActionItem(ID_APGAR,
					getResources().getString(R.string.apgar_score));
		
			
//			updated on 16Nov2015
			ActionItem referredto_Item = new ActionItem(
					ID_REFERRAL, getResources().getString(R.string.referredto));
			
//			updated on 26Nov2015
			ActionItem stagesoflabor_Item = new ActionItem(
					ID_STAGEOFLABOR, getResources().getString(R.string.stagesoflabor));

//			updated on 23dec2015
			ActionItem print_partoitem = new ActionItem(
					ID_PRINTPARTO, getResources().getString(R.string.printparto));

//			updated on 23dec2015
			mQuickAction.addActionItem(print_partoitem);
			mQuickAction.addActionItem(graph_Item);
			mQuickAction.addActionItem(deliverystatus_Item);
			mQuickAction.addActionItem(apgarscore_Item);

			
//			updated on 26Nov2015
			mQuickAction.addActionItem(stagesoflabor_Item);
			
//          changed by Arpitha 26feb2016
			mQuickAction.addActionItem(referredto_Item);
			
			mQuickAction.addActionItem(viewprofile_Item);

			// setup the action item click listener
			mQuickAction
					.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
						@Override
						public void onItemClick(QuickAction quickAction,
								int pos, int actionId) {

							if (actionId == ID_PROFILE) { // Message item
															// selected

								Intent view = new Intent(getActivity(),
										ViewProfile_Activity.class);
								view.putExtra("rowitems", rowItems);
								view.putExtra("position", mSelectedRow);
								startActivity(view);

							}
							if (actionId == ID_GRAPH) {

								Intent graph = new Intent(getActivity(),
										SlidingActivity.class);
								graph.putExtra("rowitems", rowItems);
								graph.putExtra("position", mSelectedRow);
								startActivity(graph);

							}
							if (actionId == ID_DELSTATUS) {

								if (rowItems != null) {
//									05Jan2016
									try {
										showDialogToUpdateStatus();
									} catch (Exception e) {
										AppContext.addLog(
												new RuntimeException()
														.getStackTrace()[0]
														.getMethodName()
														+ " - "
														+ this.getClass()
																.getSimpleName(),
												e);
										e.printStackTrace();
									}
									
								}
							}

							if (actionId == ID_APGAR) {

								
								if (rowItems.get(mSelectedRow).getDel_type() != 0
										&& (rowItems.get(mSelectedRow)
												.getDel_result1() != 0)
										|| (rowItems.get(mSelectedRow)
												.getDel_result2() != 0)) {
									Intent viewapgar = new Intent(
											getActivity(), Activity_Apgar.class);
									viewapgar.putExtra("rowitems", rowItems);
									viewapgar
											.putExtra("position", mSelectedRow);
									startActivity(viewapgar);
								} else {
									Toast.makeText(
											getActivity(),
											getResources()
													.getString(
															R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}

							
							
//							updated on 16Nov2015
							if(actionId == ID_REFERRAL) {
								showDialogToUpdateReferral();
							}
							
//							updated on 26Nov2015
							if (actionId == ID_STAGEOFLABOR) {
								if (rowItems.get(mSelectedRow).getDel_type() != 0
										&& (rowItems.get(mSelectedRow)
												.getDel_result1() != 0 )
										|| (rowItems.get(mSelectedRow)
												.getDel_result2() != 0 )) {
								Intent stagesoflabor = new Intent(getActivity(),
										StageofLabor.class);
								stagesoflabor.putExtra("rowitems", rowItems);
								stagesoflabor.putExtra("position", mSelectedRow);
								startActivity(stagesoflabor);
								}  else {
									Toast.makeText(getActivity(),
											getResources().getString(R.string.msg_applicableafterdelivery),
											Toast.LENGTH_SHORT).show();
								}
							}
							
//							updated on 23Dec2015
							if(actionId == ID_PRINTPARTO) {
								Intent view_partograph = new Intent(getActivity(),
										View_Partograph.class);
								view_partograph.putExtra("rowitems", rowItems);
								view_partograph.putExtra("position", mSelectedRow);
								startActivity(view_partograph);
								
							} 
							
						}
					});

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	@Override
	public void fragmentVisible() {
		try {
			loadWomendata();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}

	}

	// Display Delivery report
	private void showDeliveryReport() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.setContentView(R.layout.delivery_report);
			dialog.setTitle(getResources().getString(R.string.delivery_report));

			AssignIdWidgets(dialog);

			setDeliveryreprtdata();

			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	// Assign Ids To Dialog report
	private void AssignIdWidgets(Dialog dialog) throws Exception {
		AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
				.getMethodName() + " - " + this.getClass().getSimpleName());
		txtchildsex1 = (TextView) dialog.findViewById(R.id.txtchildsex1val);
		txtchildsex2 = (TextView) dialog.findViewById(R.id.txtchildsex2val);
		txtchildwt1 = (TextView) dialog.findViewById(R.id.txtchildwt1val);
		txtchildwt2 = (TextView) dialog.findViewById(R.id.txtchildwt2val);
		txtdelcomments = (TextView) dialog.findViewById(R.id.txtdelcommval);
		txtdeldate = (TextView) dialog.findViewById(R.id.txtdeldateval);
		txtdelres1 = (TextView) dialog.findViewById(R.id.txtdelres1val);
		txtdelres2 = (TextView) dialog.findViewById(R.id.txtdelres2val);
		txtdeltime = (TextView) dialog.findViewById(R.id.txtdeltimeval);
		txtdeltype = (TextView) dialog.findViewById(R.id.txtstatusval);
		txtnoofchild = (TextView) dialog.findViewById(R.id.txtnoofchildval);
		txtdeltime2 = (TextView) dialog.findViewById(R.id.txtdeltimeval2);

		tr_childdet2 = (TableRow) dialog.findViewById(R.id.tr_childdet2);
		tr_childsex2 = (TableRow) dialog.findViewById(R.id.tr_childsex2);
		trchilddet1 = (TableRow) dialog.findViewById(R.id.tr_childdet1);
		tr_childdetsex1 = (TableRow) dialog.findViewById(R.id.tr_childdetsex1);
		tr_secondchilddetails = (TableRow) dialog
				.findViewById(R.id.tr_secondchilddetails);
		tr_result2 = (TableRow) dialog.findViewById(R.id.tr_result2);
		tr_deltime2 = (TableRow) dialog.findViewById(R.id.tr_deltime2);

		txtmothersdeathreason = (TextView) dialog
				.findViewById(R.id.txtmothersdeathreason);
		tvmothersdeathreason = (EditText) dialog
				.findViewById(R.id.etmothersdeathreason);
//		chkmotherdead = (CheckBox) dialog.findViewById(R.id.chkmotherdead);
		
		rdmotheralive = (RadioButton)dialog.findViewById(R.id.rdmotheralive);
		rdmotherdead = (RadioButton)dialog.findViewById(R.id.rdmotherdead);
		
//		02Dec2015
		tr_deltypereason = (TableRow)dialog.findViewById(R.id.trdeltypereason);
		tr_deltypeotherreasons = (TableRow)dialog.findViewById(R.id.trdeltypereasonothers);
		
		txtdeltypereason = (EditText) dialog.findViewById(R.id.txtdeltypereasonval);
		txtdeltypeotherreasons = (EditText) dialog.findViewById(R.id.txtdeltypeotherreasonval);
		
//		04jan2016
		ettears = (EditText)dialog.findViewById(R.id.ettears);
		txtepisiotomy = (TextView)dialog.findViewById(R.id.txtepisiotomyval);
		txtsuturematerial = (TextView)dialog.findViewById(R.id.txtsuturematerialval);
		trsuturematerial = (TableRow)dialog.findViewById(R.id.trsuturematerial);
		
		txtfirstchilddetails = (TextView)dialog.findViewById(R.id.txtfirstchilddetails);
		
//		24jan2016
		trsuturematerial.setVisibility(View.GONE);
	}

	// Show dialog to update delivery status
	private void showDialogToUpdateStatus() throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.setContentView(R.layout.edit_status_dialog);
			dialog.setTitle(getResources().getString(R.string.update_status));

			AssignIdsToWidgets(dialog);

			initialViewDialog();
			
//			04jan2016
			rdepiyes.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.VISIBLE);
					episiotomy = 1;
				}
			});
			
			rdepino.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					trsuturematerial.setVisibility(View.GONE);
					episiotomy = 0;
					
				}
			});
			
			rdcatgut.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					suturematerial = 1;
				}
			});
			
			rdvicryl.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					suturematerial = 2;
				}
			});


			// Num of children
			spn_noofchild
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> adapter,
								View view, int pos, long id) {
							no_of_child = (String) adapter.getSelectedItem();

							if (pos == 0) {
								trchilddet1.setVisibility(View.VISIBLE);
								tr_childdetsex1.setVisibility(View.VISIBLE);
								tr_secondchilddetails.setVisibility(View.GONE);
								tr_result2.setVisibility(View.GONE);
								tr_childdet2.setVisibility(View.GONE);
								tr_childsex2.setVisibility(View.GONE);
								tr_deltime2.setVisibility(View.GONE);
//								05Jan2016
								txtfirstchilddetails.setText(getResources().getString(R.string.child_details));

							} else {
								tr_childdet2.setVisibility(View.VISIBLE);
								tr_childsex2.setVisibility(View.VISIBLE);
								tr_secondchilddetails
										.setVisibility(View.VISIBLE);
								tr_result2.setVisibility(View.VISIBLE);
								trchilddet1.setVisibility(View.VISIBLE);
								tr_childdetsex1.setVisibility(View.VISIBLE);
								tr_deltime2.setVisibility(View.VISIBLE);
//								05Jan2016
								txtfirstchilddetails.setText(getResources().getString(R.string.first_child_details));

							}

						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {

						}
					});

			
			// Delivery type
			spndel_type.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1,
						int arg2, long arg3) {
//					delivery_type = (String) adapter.getSelectedItem();
					delivery_type = adapter.getSelectedItemPosition() + 1;
//					25Nov2015
					try {
						if(delivery_type >1) {
							tr_deltypereason.setVisibility(View.VISIBLE);
							tr_deltypeotherreasons.setVisibility(View.VISIBLE);
							setDelTypeReason(delivery_type);
						}else {
							tr_deltypereason.setVisibility(View.GONE);
						tr_deltypeotherreasons.setVisibility(View.GONE);
						}
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);

						e.printStackTrace();
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// Delivery result 1
			spn_delres1.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View view,
						int pos, long id) {
//					delivery_result1 = (String) adapter.getSelectedItem();
					delivery_result1 = adapter.getSelectedItemPosition() + 1;

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// Delivery result 2

			spn_delres2.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1,
						int arg2, long arg3) {
//					delivery_result2 = (String) adapter.getSelectedItem();
					delivery_result2 = adapter.getSelectedItemPosition() + 1;

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});
			
			rdmale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
//					babysex1 = getResources().getString(R.string.male);
					babysex1 = 0;
					
				}
			});

			rdfemale1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
//					babysex1 = getResources().getString(R.string.female);
					babysex1 = 1;
				}
			});

			rdmale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
//					babysex2 = getResources().getString(R.string.male);
					babysex2 = 0;
				}
			});

			rdfemale2.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
//					babysex2 = getResources().getString(R.string.female);
					babysex2 = 1;
				}
			});

			etdeldate.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							isDateRefClicked = false;
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(
									new RuntimeException().getStackTrace()[0]
											.getMethodName()
											+ " - "
											+ this.getClass().getSimpleName(),
									e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			etdeltime.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						del_time1 = true;
						// getActivity().showDialog(TIME_DIALOG_ID);

						showtimepicker();
					}
					return true;
				}
			});

			etdeltime2.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						del_time1 = false;
						
//						getActivity().showDialog(TIME_DIALOG_ID);
						
						showtimepicker();
					}
					return true;
				}
			});

			
			
			rdmotheralive.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					txtmothersdeathreason.setVisibility(View.GONE);
					 etmothersdeathreason.setVisibility(View.GONE);
				}
			});
			
			rdmotherdead.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					txtmothersdeathreason.setVisibility(View.VISIBLE);
					 etmothersdeathreason.setVisibility(View.VISIBLE);
					
					 etmothersdeathreason.requestFocus();
				}
			});

			final Calendar c = Calendar.getInstance();
			hour = c.get(Calendar.HOUR_OF_DAY);
			minute = c.get(Calendar.MINUTE);

			Button imgbtncancel = (Button) dialog.findViewById(R.id.btnclear);

			imgbtnok.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

//					String updateres = "";

					try {

						if (validateDelFields()) {
							
							Women_Profile_Pojo wpojo = new Women_Profile_Pojo();
							if(isDelUpdate) {
								int childwt1 = Integer.parseInt(etbabywt1.getText()
										.toString());

								
//								06Jan2016
								int childwt2 = 0;
								if(etbabywt2.getText()
										.toString().length()>0){
								childwt2 = Integer.parseInt(etbabywt2.getText()
										.toString());
								wpojo.setDel_Comments(etdelcomments.getText()
										.toString());
								wpojo.setBabywt1(childwt1);
								wpojo.setBabywt2(childwt2);
								wpojo.setBabysex1(babysex1);
								wpojo.setBabysex2(babysex2);
								int motherdead;
								
								
								if (rdmotherdead.isChecked())
									motherdead = 1;
								else
									motherdead = 0;

								wpojo.setMothersdeath(motherdead);
								wpojo.setMothers_death_reason(etmothersdeathreason.getText().toString());
								wpojo.setUserId(wUserId);
								wpojo.setWomenId(womenId);
								
								}
							} else {

							if (!(no_of_child.equals("") || (no_of_child == null)))
								numofchildren = Integer.parseInt(no_of_child);

							int childwt1 = Integer.parseInt(etbabywt1.getText()
									.toString());

							
//							06Jan2016
							int childwt2 = 0;
							if(etbabywt2.getText()
									.toString().length()>0){
							childwt2 = Integer.parseInt(etbabywt2.getText()
									.toString());
							}

							wpojo.setDel_type(delivery_type);
							wpojo.setDel_Comments(etdelcomments.getText()
									.toString());
							wpojo.setDel_result1(delivery_result1);
							wpojo.setDel_result2(delivery_result2);
							wpojo.setNo_of_child(numofchildren);
							wpojo.setBabywt1(childwt1);
							wpojo.setBabywt2(childwt2);
							wpojo.setBabysex1(babysex1);
							wpojo.setBabysex2(babysex2);

							int motherdead;
							

							if(rdmotherdead.isChecked())
								motherdead = 1;
							else
								motherdead = 0;
							
							wpojo.setMothersdeath(motherdead);
							wpojo.setDel_Time(etdeltime.getText().toString());
//		07Apr2015			wpojo.setDel_Date(etdeldate.getText().toString());
							wpojo.setDel_Date(strDeldate);
							
							if(numofchildren == 2)
								wpojo.setDel_time2(etdeltime2.getText().toString());
							else
								wpojo.setDel_time2("");
							
							wpojo.setMothers_death_reason(etmothersdeathreason
									.getText().toString());
							wpojo.setUserId(wUserId);
							wpojo.setWomenId(womenId);

							

//							02Dec2015
							String selIds = "";
							if(delivery_type >1) {
							List<String> selecteddeltypereason = mspndeltypereason.getSelectedStrings();
							if (selecteddeltypereason != null && selecteddeltypereason.size() > 0) {
								for (String str : selecteddeltypereason) {
									if (selecteddeltypereason.indexOf(str) != selecteddeltypereason.size() - 1)
										selIds = selIds + deltypereasonMap.get(str) + ",";
									else
										selIds = selIds + deltypereasonMap.get(str);
									}
								}
							}
							wpojo.setDelTypeReason(selIds);
							
							wpojo.setDeltype_otherreasons(""+etdeltypeotherreasons.getText().toString());
							
							
							
//							04jan2016
							String seltearsIds = "";
							List<String> selectedtears = mspntears.getSelectedStrings();
							if (selectedtears != null && selectedtears.size() > 0) {
								for (String str : selectedtears) {
									if (selectedtears.indexOf(str) != selectedtears.size() - 1)
										seltearsIds = seltearsIds + tearsMap.get(str) + ",";
									else
										seltearsIds = seltearsIds + tearsMap.get(str);
								}
							}
							
							wpojo.setTears(seltearsIds);
							wpojo.setEpisiotomy(episiotomy);
							wpojo.setSuturematerial(suturematerial);
							}
							
							SimpleDateFormat sdf = new SimpleDateFormat(
									"yyyyMMdd_HHmmss");
							String currentDateandTime = sdf.format(new Date());

							wpojo.setDatelastupdated(currentDateandTime);
							
							dbh.db.beginTransaction();
							String regSql = "";

							int transId = dbh.iCreateNewTrans(wUserId);

//							06Jan2061
							if(isDelUpdate)
								regSql = dbh.updateDelDetailsData(wpojo,transId);
							else
								regSql = dbh.updateDelData(wpojo, transId);
							
							if (regSql.length() > 0) {
								commitTrans();
								
//                         updated on 26Feb2016 by Arpitha	
								rd_deliveredmonthly.setChecked(true);
								delstatus_monthly = getResources().getString(
										R.string.delivered);
								rddeltypemonthly = getResources().getString(
										R.string.delivered);
								setspinnerdata();
							} else
								rollbackTrans();

							dialog.cancel();
						}
					} catch (Exception e) {
						AppContext.addLog(
								new RuntimeException().getStackTrace()[0]
										.getMethodName()
										+ " - "
										+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgbtncancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.cancel();
				}
			});
			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	// Set delivery report data
	private void setDeliveryreprtdata() throws Exception {

		txtchildwt1.setText("" + rowItems.get(mSelectedRow).getBabywt1());
		txtchildwt2.setText("" + rowItems.get(mSelectedRow).getBabywt2());



		txtdelcomments
				.setText(rowItems.get(mSelectedRow).getDel_Comments() == null ? " "
						: rowItems.get(mSelectedRow).getDel_Comments());

		
		
		txtdeldate
		.setText(rowItems.get(mSelectedRow).getDel_Date() == null ? "" : Partograph_CommonClass.getConvertedDateFormat(rowItems.get(mSelectedRow)
				.getDel_Date(), Partograph_CommonClass.defdateformat));
		txtdeltime
				.setText(rowItems.get(mSelectedRow).getDel_Time() == null ? Partograph_CommonClass
						.getCurrentTime() : rowItems.get(mSelectedRow)
						.getDel_Time());

		txtdeltime2
				.setText(rowItems.get(mSelectedRow).getDel_time2() == null ? Partograph_CommonClass
						.getCurrentTime() : rowItems.get(mSelectedRow)
						.getDel_time2());

//		txtdeltype.setText(rowItems.get(mSelectedRow).getDel_type());

		txtnoofchild.setText(String.valueOf(rowItems.get(mSelectedRow)
				.getNo_of_child()));

		int noofchild = rowItems.get(mSelectedRow).getNo_of_child();

		if (noofchild == 1) {
			trchilddet1.setVisibility(View.VISIBLE);
			tr_childdetsex1.setVisibility(View.VISIBLE);
			tr_secondchilddetails.setVisibility(View.GONE);
			tr_result2.setVisibility(View.GONE);
			tr_childdet2.setVisibility(View.GONE);
			tr_childsex2.setVisibility(View.GONE);
			tr_deltime2.setVisibility(View.GONE);
			
//			04jan2016
			txtfirstchilddetails.setText(getResources().getString(R.string.child_details));
		} else {
			tr_childdet2.setVisibility(View.VISIBLE);
			tr_childsex2.setVisibility(View.VISIBLE);
			tr_secondchilddetails.setVisibility(View.VISIBLE);
			tr_result2.setVisibility(View.VISIBLE);
			trchilddet1.setVisibility(View.VISIBLE);
			tr_childdetsex1.setVisibility(View.VISIBLE);
			tr_deltime2.setVisibility(View.VISIBLE);
//			04jan2016
			txtfirstchilddetails.setText(getResources().getString(R.string.first_child_details));
		}


		
		String[] delrestype = getResources().getStringArray(R.array.del_type);
		
		
		txtdeltype.setText(delrestype[rowItems.get(mSelectedRow).getDel_type()-1]);
		
		String[] delreslist = getResources().getStringArray(R.array.del_result);
		
		
		
		txtdelres1.setText(delreslist[rowItems.get(mSelectedRow).getDel_result1() - 1]);
		
		
		
		txtdelres2.setText(delreslist[rowItems.get(mSelectedRow).getDel_result2() - 1]);
		
		 babysex1 = rowItems.get(mSelectedRow).getBabysex1();
		
		
		if(babysex1==0)
			txtchildsex1.setText(getResources().getString(R.string.male));
		else
			txtchildsex1.setText(getResources().getString(R.string.female));
		
		 babysex2 = rowItems.get(mSelectedRow).getBabysex2();
		
		
		if(babysex2==0)
			txtchildsex2.setText(getResources().getString(R.string.male));
		else
			txtchildsex2.setText(getResources().getString(R.string.female));

		if (rowItems.get(mSelectedRow).isMothersdeath() == 1) {

//			chkmotherdead.setChecked(true);
			
			rdmotherdead.setChecked(true);
			txtmothersdeathreason.setVisibility(View.VISIBLE);
			tvmothersdeathreason.setVisibility(View.VISIBLE);
			tvmothersdeathreason.setText(rowItems.get(mSelectedRow)
					.getMothers_death_reason());
		} else {
//			chkmotherdead.setChecked(false);
			rdmotheralive.setChecked(true);
			txtmothersdeathreason.setVisibility(View.GONE);
			tvmothersdeathreason.setVisibility(View.GONE);
		}

//		chkmotherdead.setEnabled(false);
		
		rdmotheralive.setEnabled(false);
		rdmotherdead.setEnabled(false);
		tvmothersdeathreason.setEnabled(false);

//		02Dec2015
		if(rowItems.get(mSelectedRow).getDel_type() > 1) {
			String[] delTypeRes = rowItems.get(mSelectedRow).getDelTypeReason().split(",");
			String[] delTypeResArr = null;
			if(rowItems.get(mSelectedRow).getDel_type()==2)
				delTypeResArr = getResources().getStringArray(R.array.spnreasoncesarean);
			else if(rowItems.get(mSelectedRow).getDel_type()==3)
				delTypeResArr = getResources().getStringArray(R.array.spnreasoninstrumental);
			
			String delTypeResDisplay = "";
			if(delTypeResArr != null) {
				if(delTypeRes != null && delTypeRes.length > 0){
					for(String str : delTypeRes){
						if(str != null && str.length()>0)
							delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
					}
				}
			}
			
			txtdeltypereason.setText(delTypeResDisplay);
			txtdeltypeotherreasons.setText(rowItems.get(mSelectedRow).getDeltype_otherreasons());
			
		} 
		
//		04jan2016
		String[] tearsres = rowItems.get(mSelectedRow).getTears().split(",");
		String[] tearsResArr = null;
		
		tearsResArr = getResources().getStringArray(R.array.tears);
		
		String tearsResDisplay = "";
		if(tearsResArr != null) {
			if(tearsres != null && tearsres.length > 0){
				for(String str : tearsres){
					if(str != null && str.length()>0)
						tearsResDisplay = tearsResDisplay + tearsResArr[Integer.parseInt(str)] + "\n";
				}
			}
		}
		
		ettears.setText(tearsResDisplay);
		
		if(rowItems.get(mSelectedRow).getEpisiotomy()==1) {
			txtepisiotomy.setText(getResources().getString(R.string.yes));
			trsuturematerial.setVisibility(View.VISIBLE);
			if(rowItems.get(mSelectedRow).getSuturematerial()==1)
				txtsuturematerial.setText(getResources().getString(R.string.catgut));
			else
				txtsuturematerial.setText(getResources().getString(R.string.vicryl));
		}
		else {
			txtepisiotomy.setText(getResources().getString(R.string.no));
			trsuturematerial.setVisibility(View.GONE);
		}
	}

	// Assign Ids To Widgets
	private void AssignIdsToWidgets(Dialog dialog) throws Exception {
		etdelcomments = (EditText) dialog.findViewById(R.id.etdelcomments);
		etdeldate = (EditText) dialog.findViewById(R.id.etdeldate);
		etdeltime = (EditText) dialog.findViewById(R.id.etdeltime);
		etdeltime2 = (EditText) dialog.findViewById(R.id.etdeltime2);
		etbabywt1 = (EditText) dialog.findViewById(R.id.etchildwt1);
		etbabywt2 = (EditText) dialog.findViewById(R.id.etchildwt2);

		rdmale1 = (RadioButton) dialog.findViewById(R.id.rd_male1);
		rdfemale1 = (RadioButton) dialog.findViewById(R.id.rd_female1);
		rdmale2 = (RadioButton) dialog.findViewById(R.id.rd_male2);
		rdfemale2 = (RadioButton) dialog.findViewById(R.id.rd_female2);

		tr_childdet2 = (TableRow) dialog.findViewById(R.id.tr_childdet2);
		tr_childsex2 = (TableRow) dialog.findViewById(R.id.tr_childsex2);
		trchilddet1 = (TableRow) dialog.findViewById(R.id.tr_childdet1);
		tr_childdetsex1 = (TableRow) dialog.findViewById(R.id.tr_childdetsex1);
		tr_secondchilddetails = (TableRow) dialog
				.findViewById(R.id.tr_secondchilddetails);
		tr_result2 = (TableRow) dialog.findViewById(R.id.tr_result2);
		tr_deltime2 = (TableRow) dialog.findViewById(R.id.tr_deltime2);

		spn_delres1 = (Spinner) dialog.findViewById(R.id.spndelresult);
		spn_delres2 = (Spinner) dialog.findViewById(R.id.spndelresult2);
		spndel_type = (Spinner) dialog.findViewById(R.id.spndeltype);
		spn_noofchild = (Spinner) dialog.findViewById(R.id.spnnoofchild);
//		chkmotherdead = (CheckBox) dialog.findViewById(R.id.chkmotherdead);
		imgbtnok = (Button) dialog.findViewById(R.id.btnsave);

		rdchildSexGrpbaby1 = (RadioGroup) dialog.findViewById(R.id.rdsex1);
		rdchildSexGrpbaby2 = (RadioGroup) dialog.findViewById(R.id.rdsex2);

		etmothersdeathreason = (EditText) dialog
				.findViewById(R.id.etmothersdeathreason);
		txtmothersdeathreason = (TextView) dialog
				.findViewById(R.id.txtmothersdeathreason);

		rdmotheralive = (RadioButton)dialog.findViewById(R.id.rdmotheralive);
		rdmotherdead = (RadioButton)dialog.findViewById(R.id.rdmotherdead);
		
//		25Nov2015
		mspndeltypereason = (MultiSelectionSpinner)dialog.findViewById(R.id.mspndeltypereason);
		tr_deltypereason = (TableRow)dialog.findViewById(R.id.tr_deltypereason);
		tr_deltypeotherreasons = (TableRow)dialog.findViewById(R.id.tr_deltypeotherreasons);
		etdeltypeotherreasons = (EditText)dialog.findViewById(R.id.etdeltypeotherreasons);
		
//		04jan2016
			mspntears = (MultiSelectionSpinner)dialog.findViewById(R.id.mspntears);
			rdepiyes = (RadioButton) dialog.findViewById(R.id.rd_epiyes);
			rdepino = (RadioButton) dialog.findViewById(R.id.rd_epino);
			rdcatgut = (RadioButton) dialog.findViewById(R.id.rd_catgut);
			rdvicryl = (RadioButton) dialog.findViewById(R.id.rd_vicryl);
			trsuturematerial = (TableRow)dialog.findViewById(R.id.tr_suturematerial);
			
//			05jan2016
			txtfirstchilddetails = (TextView)dialog.findViewById(R.id.txtfirstchilddetails);
			txtdeltypereason = (EditText)dialog.findViewById(R.id.txtdeltypereasonval);
			ettears = (EditText)dialog.findViewById(R.id.ettears);
			
//			24jan2016
			trsuturematerial.setVisibility(View.GONE);
	}

	private void initialViewDialog() throws Exception {

//		05Jan2016
		if (rowItems.get(mSelectedRow)
				.getDel_type() != 0) {
			setDelReportData();
			isDelUpdate = true;
		} else {
			isDelUpdate = false;
			txtdeltypereason.setVisibility(View.GONE);
			txtmothersdeathreason.setVisibility(View.GONE);
			etmothersdeathreason.setVisibility(View.GONE);
			mspndeltypereason.setVisibility(View.VISIBLE);
			mspntears.setVisibility(View.VISIBLE);
			ettears.setVisibility(View.GONE);
			spndel_type.setEnabled(true);
			etdeltypeotherreasons.setEnabled(true);
			spn_noofchild.setEnabled(true);
			spn_delres1.setEnabled(true);
			spn_delres2.setEnabled(true);
			etdeltime.setEnabled(true);
			etdeltime2.setEnabled(true);
			etdeldate.setEnabled(true);
			rdepiyes.setEnabled(true);
			rdepino.setEnabled(true);
			rdcatgut.setEnabled(true);
			rdvicryl.setEnabled(true);
			
			etdeldate
			.setText(rowItems.get(mSelectedRow).getDel_Date() == null ? Partograph_CommonClass
					.getConvertedDateFormat(
							Partograph_CommonClass.getTodaysDate(),
							Partograph_CommonClass.defdateformat)
					: Partograph_CommonClass.getConvertedDateFormat(
							rowItems.get(mSelectedRow).getDel_Date(),
							Partograph_CommonClass.defdateformat));

	etdeltime
			.setText(rowItems.get(mSelectedRow).getDel_Time() == null ? Partograph_CommonClass
					.getCurrentTime() : rowItems.get(mSelectedRow)
					.getDel_Time());

	etdeltime2
			.setText(rowItems.get(mSelectedRow).getDel_time2() == null ? Partograph_CommonClass
					.getCurrentTime() : rowItems.get(mSelectedRow)
					.getDel_time2());
	
		todaysDate = Partograph_CommonClass.getTodaysDate();
		selecteddate = etdeldate.getText().toString();
		
		strDeldate = todaysDate;
		selecteddate = todaysDate;
		
		String delTime = etdeltime.getText().toString();

		Calendar calendar = Calendar.getInstance();

		Date d = null;
		d = dbh.getTime(delTime);
		calendar.setTime(d);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);

//		25Nov2015
//		setDelTypeReason();
		tr_deltypereason.setVisibility(View.GONE);
		tr_deltypeotherreasons.setVisibility(View.GONE);
		
//		04jan2016
		setTears();
		
//		05Jan2016
		txtfirstchilddetails.setText(getResources().getString(R.string.child_details));
		}
		
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			DatePickerDialog dialog = null;
			try {
				Calendar calendar = Calendar.getInstance();
				Date d = null;
				d = dbh.getDateS(selecteddate == null ? todaysDate
						: selecteddate);
				calendar.setTime(d);
				year1 = calendar.get(Calendar.YEAR);
				mon1 = calendar.get(Calendar.MONTH);
				day1 = calendar.get(Calendar.DAY_OF_MONTH);

				dialog = new DatePickerDialog(getActivity(), this, year1, mon1,
						day1);

	//			 remove calendar view 
				dialog.getDatePicker().setCalendarViewShown(false);

//				 Spinner View 
				dialog.getDatePicker().setSpinnersShown(true);

				dialog.setTitle(getResources().getString(R.string.pickadate));
			} catch (Exception e) {
				AppContext.addLog(
						new RuntimeException().getStackTrace()[0]
								.getMethodName()
								+ " - "
								+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {
					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(
							new RuntimeException().getStackTrace()[0]
									.getMethodName()
									+ " - "
									+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {

		selecteddate = String.valueOf(year) + "-"
				+ String.format("%02d", month) + "-"
				+ String.format("%02d", day);

		try {

			Date taken = new SimpleDateFormat("yyyy-MM-dd").parse(selecteddate);
			Date regdate = new SimpleDateFormat("yyyy-MM-dd").parse(rowItems
					.get(mSelectedRow).getDate_of_admission());
			if (taken.after(new Date())) {
				Toast.makeText(
						getActivity(),
						getResources().getString(R.string.curr_date_validation),
						Toast.LENGTH_SHORT).show();

				
				selecteddate = null; //updated on 19Nov2015
				if(isDateRefClicked) {
					etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));
					strRefDate = todaysDate;
				} else {
					etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));
					strDeldate = todaysDate;
				}
				
			} else if (taken.before(regdate)) {
				
				
selecteddate = null; //updated on 19Nov2015
				
				if(isDateRefClicked) {
					Toast.makeText(getActivity(),
							getResources().getString(R.string.refdate_cant_be_before_reg_date),
							Toast.LENGTH_SHORT).show();
					etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));
					strRefDate = todaysDate;
				} else {
				
					Toast.makeText(getActivity(),
							getResources().getString(R.string.date_cant_be_before_reg_date),
							Toast.LENGTH_SHORT).show();
					etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat));
					strDeldate = todaysDate;
				}
			}

			else {
			
				
				if(isDateRefClicked) {
					etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(selecteddate, Partograph_CommonClass.defdateformat));
					strRefDate = selecteddate;
				} else {				
				etdeldate.setText(selecteddate);
				etdeldate.setText(Partograph_CommonClass.getConvertedDateFormat(selecteddate, Partograph_CommonClass.defdateformat));
				strDeldate = selecteddate;
				}
			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

	protected Dialog onCreateDialog(int id) {

		try {
			switch (id) {
			case TIME_DIALOG_ID:
				// set time picker as current time
				return new TimePickerDialog(this.getActivity(),
						timePickerListener, hour, minute, false);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			hour = selectedHour;
			minute = selectedMinute;

			// set current time into edittext

			try {

				if (del_time1)
					etdeltime.setText(new StringBuilder()
							.append(padding_str(hour)).append(":")
							.append(padding_str(minute)));
				else
					etdeltime2.setText(new StringBuilder()
							.append(padding_str(hour)).append(":")
							.append(padding_str(minute)));
			} catch (Exception e) {
				AppContext.addLog(
						new RuntimeException().getStackTrace()[0]
								.getMethodName()
								+ " - "
								+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

			// updateTime(hour,minute);

		}
	};

	private static String padding_str(int c) throws Exception {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	// Validate Delivery Status Fields
	protected boolean validateDelFields() throws Exception {
//		06jan2016
		if(isDelUpdate) {
			
			if (etbabywt1.getText().length() <3) {
				displayAlertDialog(getResources().getString(R.string.child_wt));
				etbabywt1.requestFocus();
				return false;
			}
			
//          updated on 6/6/16			
			if (Integer.parseInt(etbabywt1.getText().toString())==0) {
				displayAlertDialog(getResources().getString(R.string.child_wt_val));
				etbabywt1.requestFocus();
				return false;
			}
			
			if (no_of_child != null) {
				if (Integer.parseInt(no_of_child) == 2) {
					if (etbabywt2.getText().length() < 3) {
						displayAlertDialog(getResources().getString(
								R.string.child_wt));
						etbabywt2.requestFocus();
						return false;
					}
//			           updated on 6/6/16			
						if (Integer.parseInt(etbabywt2.getText().toString())==0) {
							displayAlertDialog(getResources().getString(R.string.child_wt_val));
							etbabywt2.requestFocus();
							return false;
						}
				}
			}
			
			if(rdmotherdead.isChecked()) {
				if(etmothersdeathreason.getText().length() <= 0) {
					displayAlertDialog(getResources().getString(R.string.enter_mothersdeathreason));
					etmothersdeathreason.requestFocus();
					return false;
				}
			}
			
			
		} else {
			
//	           updated on 6/6/16			
				if (Integer.parseInt(etbabywt1.getText().toString())==0) {
					displayAlertDialog(getResources().getString(R.string.child_wt_val));
					etbabywt1.requestFocus();
					return false;
				}

		if (etdeldate.getText().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.ent_deldate));
			etdeldate.requestFocus();
			return false;
		}

		if (etdeltime.getText().length() <= 0) {
			displayAlertDialog(getResources().getString(R.string.ent_deltime));
			etdeltime.requestFocus();
			return false;
		}

		if (etbabywt1.getText().length() < 3) {
			displayAlertDialog(getResources().getString(R.string.child_wt));
			etbabywt1.requestFocus();
			return false;
		}
		
//		updated on 13nov2015 - validating delivery date and time with reg date
		if (etdeltime.getText().length() >= 1) {				
			String date1, date2	;							
			date1 = strRegdate;				
			String dtime = etdeltime.getText().toString();				
			date2 = strDeldate + " " + dtime;
			etdeltime.requestFocus();
			if(!isDeliveryTimevalid(date1, date2))
				return false;
		}

		

		if (no_of_child != null) {
			if (Integer.parseInt(no_of_child) == 2) {
				if (etbabywt2.getText().length() < 3) {
					displayAlertDialog(getResources().getString(
							R.string.child_wt));
					etbabywt2.requestFocus();
					return false;
				}
				
//		           updated on 6/6/16			
					if (Integer.parseInt(etbabywt2.getText().toString())==0) {
						displayAlertDialog(getResources().getString(R.string.child_wt_val));
						etbabywt2.requestFocus();
						return false;
					}
				
//				updated on 13nov2015 - validating delivery date and time with reg date
				if (etdeltime2.getText().length() >= 1) {				
					String date1, date2	;												
					date1 = strRegdate ;				
					String dtime = etdeltime2.getText().toString();				
					date2 = strDeldate + " " + dtime;
					 etdeltime2.requestFocus();
					 if(!isDeliveryTimevalid(date1, date2))
						 return false;
				}

				
			}
		}

		
		
		if(rdmotherdead.isChecked()) {
			if(etmothersdeathreason.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.enter_mothersdeathreason));
				etmothersdeathreason.requestFocus();
				return false;
			}
		}
		}
		return true;
	}

	// Alert dialog
	private void displayAlertDialog(String message) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		// set dialog message
		alertDialogBuilder.setMessage(message)

		.setPositiveButton(getResources().getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	*//** Method to rollback the trnsaction *//*
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		// dbh.db.close();
		Toast.makeText(getActivity(),
				getResources().getString(R.string.failed), Toast.LENGTH_SHORT)
				.show();
	}

	*//**
	 * Method to commit the trnsaction
	 * 
	 * @throws Exception
	 *//*
	private void commitTrans() throws Exception {

		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		// dbh.db.close();

		Toast.makeText(getActivity(),
				getResources().getString(R.string.success), Toast.LENGTH_SHORT)
				.show();
		
		loadWomendata();

	}

	public void showtimepicker() {
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(this.getFragmentManager(), "timePicker");
	}

	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {
		public TimePickerFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			

			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// time.setText("Selected Time is " + hourOfDay + ":" + minute);

			try {
				
//				updated on 23Nov2015
				if(isTimeRefClicked){
//					2dec2015
					ettimeofreferral.setText(new StringBuilder()
					.append(padding_str(hourOfDay)).append(":")
					.append(padding_str(minute)));
				}
				
				if (del_time1)
					

//					etdeltime.setText(hourOfDay + ":" + minute); // 2Dec2015
					etdeltime.setText(new StringBuilder()
					.append(padding_str(hourOfDay)).append(":")
					.append(padding_str(minute)));
				else
					etdeltime2.setText(new StringBuilder()
							.append(padding_str(hourOfDay)).append(":")
							.append(padding_str(minute)));
			} catch (Exception e) {
				AppContext.addLog(
						new RuntimeException().getStackTrace()[0]
								.getMethodName()
								+ " - "
								+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}
	}
//	updated on 13nov2015
	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date dateofreg, dateofdelivery;
		try {			
			dateofreg = sdf.parse(date1);			
			dateofdelivery = sdf.parse(date2);
			
		 if(dateofdelivery.before(dateofreg)) {
			 displayAlertDialog(getResources().getString(
						R.string.invaliddeltime));			
			 return false;
		 }	else
			 return true;
		
		}catch(Exception e) {
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
			return false;
		}				
	}
	
	// Show dialog to update delivery status
	private void showDialogToUpdateReferral() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());
			final Dialog dialog = new Dialog(getActivity());
			dialog.setContentView(R.layout.activity_reference);
			dialog.setTitle(getResources().getString(R.string.app_name) + "-" + getResources().getString(R.string.referredto));					
			
			AssignIdsToReferralWidgets(dialog);
			
			
			
			etdateofreferral.setText( Partograph_CommonClass.getConvertedDateFormat( 
					Partograph_CommonClass.getTodaysDate(),Partograph_CommonClass.defdateformat));
			
			initialViewReferral();
			
			btnCancelRef.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.cancel();
				}
			});
			
			etdateofreferral.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							isDateRefClicked = true;
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(
									new RuntimeException().getStackTrace()[0]
											.getMethodName()
											+ " - "
											+ this.getClass().getSimpleName(),
									e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});
			
			ettimeofreferral.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						isTimeRefClicked = true;
						showtimepicker();
					}
					return true;
				}
			});
			
		// Referred to facility
			spnreferralfacility.setOnItemSelectedListener(new OnItemSelectedListener() {
		
				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1,
						int arg2, long arg3) {
					referralfacility = adapter.getSelectedItemPosition();
				}
		
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
		
				}
			});
			
//			04jan2016
			rd_motherconscious.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					conditionofmother = 1;
				}
			});
			
			rd_motherunconscious.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					conditionofmother = 0;
				}
			});
			
			rd_babyalive.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					conditionofbaby = 1;
					aq.id(etspo2).visible();
					aq.id(etheatrate).visible();
					trspo2.setVisibility(View.VISIBLE);
					trheartrate.setVisibility(View.VISIBLE);
				}
			});
			
			rd_babydead.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					conditionofbaby = 0;
					aq.id(etspo2).gone();
					aq.id(etheatrate).gone();
					trspo2.setVisibility(View.GONE);
					trheartrate.setVisibility(View.GONE);
				}
			});
			
			btnSaveRef.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					try {
					String refdate = (strRefDate==null ? todaysDate : strRefDate) + " " + ettimeofreferral.getText().toString();
					
					if(validateReferralFields()) {
					
					if(Partograph_CommonClass.isTimeValid(strRegdate, refdate)) {
						wrefpojo = new WomenReferral_pojo();
						
						wrefpojo.setWomenname(rowItems.get(mSelectedRow).getWomen_name());
						wrefpojo.setWomenid(rowItems.get(mSelectedRow).getWomenId());
						wrefpojo.setUserid(user.getUserId());
						wrefpojo.setFacility(rowItems.get(mSelectedRow).getFacility());
						wrefpojo.setFacilityname(rowItems.get(mSelectedRow).getFacility_name());
						wrefpojo.setReferredtofacilityid(referralfacility);
						wrefpojo.setPlaceofreferral(etplaceofreferral.getText().toString());
						wrefpojo.setReasonforreferral(etreasonforreferral.getText().toString());
						
						
//					      updated on 27Feb2016 by Arpitha					 
						
		                wrefpojo.setHeartrate(etheatrate.getText().toString());
						wrefpojo.setSpo2(etspo2.getText().toString());
						
						String rdate = (strRefDate==null ? todaysDate : strRefDate);
						
						wrefpojo.setDateofreferral(rdate);
						wrefpojo.setTimeofreferral(ettimeofreferral.getText().toString());
						wrefpojo.setCreated_date(Partograph_CommonClass.getCurrentDateandTime());
						wrefpojo.setLastupdateddate(Partograph_CommonClass.getCurrentDateandTime());
						
//						04jan2016
						List<String> selectedriskoption = mspnreasonforreferral.getSelectedStrings();
						String selIds = "";
						if (selectedriskoption != null && selectedriskoption.size() > 0) {
							for (String str : selectedriskoption) {
								if (selectedriskoption.indexOf(str) != selectedriskoption.size() - 1)
									selIds = selIds + reasonforreferralMap.get(str) + ",";
								else
									selIds = selIds + reasonforreferralMap.get(str);
							}
						}
						 wrefpojo.setReasonforreferral(selIds);
						 String bp = etbpsystolicreferral.getText().toString() +"/"+etbpdiastolicreferral.getText().toString();
						 wrefpojo.setBp(bp);
						 wrefpojo.setPulse(etpulsereferral.getText().toString()+"");
						 wrefpojo.setConditionofmother(conditionofmother);
						 wrefpojo.setConditionofbaby(conditionofbaby);
						 wrefpojo.setDescriptionofreferral(etdescriptionofreferral.getText().toString());
		
						
						dbh.db.beginTransaction();
						int transId = dbh.iCreateNewTrans(user.getUserId());
						String updateRef = "";
						if(womenrefpojoItems != null && womenrefpojoItems.size() > 0) {
							updateRef = dbh.updateWomenReferralData(wrefpojo,transId);
							
							if (updateRef.length() > 0) {
								commitTrans();
							} else
								rollbackTrans();
							
						} else {
							boolean isAdded = dbh.addReferral(wrefpojo, transId);
	
							if (isAdded) {
								dbh.iNewRecordTrans(user.getUserId(), transId,
										Partograph_DB.TBL_REFERRAL);
								commitTrans();
								
							} else {
								rollbackTrans();
							}
						}	
						dialog.cancel();
					}			
					else
						Toast.makeText(getActivity(), getResources().getString(R.string.reftime_cant_be_before_reg_date), Toast.LENGTH_SHORT).show();
					}	
					} catch(Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			
			
			dialog.show();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName()
							+ " - " + this.getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

//Initial View of the referral Screen
	private void initialViewReferral() throws Exception {
		
//		4jan2016
			etreasonforreferral.setVisibility(View.GONE);
			setReasonForReferral();
			
		todaysDate = Partograph_CommonClass.getTodaysDate();
		ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());
		
//		14Jan2016
		String refTime = ettimeofreferral.getText().toString();
		Calendar calendar = Calendar.getInstance();
		Date d = null;
		d = dbh.getTime(refTime);
		calendar.setTime(d);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		
		womenrefpojoItems = new ArrayList<WomenReferral_pojo>();
		WomenReferral_pojo wrefdata;
		
		Cursor cur = dbh.getReferralDetails(wUserId, womenId);
		if(cur.getCount() > 0) {
			cur.moveToFirst();
			
			do {
				wrefdata = new WomenReferral_pojo();
				wrefdata.setReferredtofacilityid(cur.getInt(5));
				wrefdata.setPlaceofreferral(cur.getString(6));
				wrefdata.setReasonforreferral(cur.getString(7));
//				04jan2016
				wrefdata.setDescriptionofreferral(cur.getString(8));
				wrefdata.setConditionofmother(cur.getInt(9));
				wrefdata.setBp(cur.getString(10));
				wrefdata.setPulse(cur.getString(11));
				wrefdata.setConditionofbaby(cur.getInt(12));
				wrefdata.setDateofreferral(cur.getString(13));
				wrefdata.setTimeofreferral(cur.getString(14));
				
//              updated on 27Feb2016 by Arpitha				
				wrefdata.setHeartrate(cur.getString(18));
				wrefdata.setSpo2(cur.getString(19));
				womenrefpojoItems.add(wrefdata);
			} while (cur.moveToNext());
		}
		
		if(womenrefpojoItems != null && womenrefpojoItems.size() > 0 ) {
			for(int i=0; i<womenrefpojoItems.size();i++){
				spnreferralfacility.setSelection(womenrefpojoItems.get(i).getReferredtofacilityid());
				etplaceofreferral.setText(womenrefpojoItems.get(i).getPlaceofreferral());

//				04jan2016
				String[] reasonforreferral = womenrefpojoItems.get(i).getReasonforreferral().split(",");
				String[] reasonforrefArr = null;
					reasonforrefArr = getResources().getStringArray(R.array.reasonforreferral);
				
				String reasonforrefDisplay = "";
				if(reasonforrefArr != null) {
					if(reasonforreferral != null && reasonforreferral.length > 0){
						for(String str : reasonforreferral){
							if(str != null && str.length()>0)
								reasonforrefDisplay = reasonforrefDisplay + reasonforrefArr[Integer.parseInt(str)] + "\n";
						}
					} 
				}
				
				etreasonforreferral.setText(reasonforrefDisplay);
				etdateofreferral.setText(womenrefpojoItems.get(i).getDateofreferral());
				ettimeofreferral.setText(womenrefpojoItems.get(i).getTimeofreferral());
				
//              updated on 27feb2016 by Arpitha					

				etheatrate.setText(womenrefpojoItems.get(i).getHeartrate());
				etspo2.setText((womenrefpojoItems.get(i).getSpo2()));
				
				
				String[] str = womenrefpojoItems.get(i).getTimeofreferral().split(":");
				hour = Integer.parseInt(str[0]);
				minute = Integer.parseInt(str[1]);
				
//				04jan2016
				etdescriptionofreferral.setText(womenrefpojoItems.get(i).getDescriptionofreferral());
				
				String[] bp = womenrefpojoItems.get(i).getBp().split("/");
				String strbpsys = bp[0];
				String strbpdia = bp[1];
				etbpsystolicreferral.setText(strbpsys);
				etbpdiastolicreferral.setText(strbpdia);
				etpulsereferral.setText(""+womenrefpojoItems.get(i).getPulse());
				
				if(womenrefpojoItems.get(i).getConditionofmother()==1)
					rd_motherconscious.setChecked(true);
				else
					rd_motherunconscious.setChecked(true);
				
				if(womenrefpojoItems.get(i).getConditionofbaby()==1)
					rd_babyalive.setChecked(true);
				else
					rd_babydead.setChecked(true);
				
				mspnreasonforreferral.setVisibility(View.GONE);
				etreasonforreferral.setVisibility(View.VISIBLE);
				
			}
			disableReferredToFields();
		} else {
			spnreferralfacility.setEnabled(true);
			etplaceofreferral.setEnabled(true);
			etreasonforreferral.setEnabled(true);
			etdateofreferral.setEnabled(true);
			ettimeofreferral.setEnabled(true);
			btnSaveRef.setEnabled(true);
			btnSaveRef.setBackgroundColor(getResources().getColor(R.color.brown));
			
//          updated on 27Feb2016 by Arpitha				
			etheatrate.setEnabled(true);
			etspo2.setEnabled(true);
			
//			24jan2016
			txtrefmode.setText(getResources().getString(R.string.editmode));
		}
	}

//	Disable fields
		private void disableReferredToFields() throws Exception{
			spnreferralfacility.setEnabled(false);
			etplaceofreferral.setEnabled(false);
			etreasonforreferral.setEnabled(false);
			etdateofreferral.setEnabled(false);
			ettimeofreferral.setEnabled(false);
			btnSaveRef.setEnabled(false);
			btnSaveRef.setBackgroundColor(getResources().getColor(R.color.ashgray));
			
//			04jan2016
			etdescriptionofreferral.setEnabled(false);
			rd_motherconscious.setEnabled(false);
			rd_motherunconscious.setEnabled(false);
			rd_babyalive.setEnabled(false);
			rd_babydead.setEnabled(false);
			etpulsereferral.setEnabled(false);
			etbpdiastolicreferral.setEnabled(false);
			etbpsystolicreferral.setEnabled(false);
			
//          updated on 27Feb2016 by Arpitha				
			etheatrate.setEnabled(false);
			etspo2.setEnabled(false);
			
//			24jan2016
			txtrefmode.setText(getResources().getString(R.string.viewmode));
		}

	//				Assign Ids to Referral Widgets
	private void AssignIdsToReferralWidgets(Dialog dialog) throws Exception{
		etplaceofreferral = (EditText) dialog.findViewById(R.id.etplaceofreferral);
		etreasonforreferral = (EditText) dialog.findViewById(R.id.etreasonforreferral);
		etdateofreferral = (EditText) dialog.findViewById(R.id.etdateofreferrence);
		ettimeofreferral = (EditText) dialog.findViewById(R.id.ettimeofreferrence);
		spnreferralfacility = (Spinner)dialog.findViewById(R.id.spnreferredtofacility);
		btnCancelRef = (Button)dialog.findViewById(R.id.btncancelref);
		btnSaveRef = (Button)dialog.findViewById(R.id.btnsaveref);
		trspo2  = (TableRow) dialog.findViewById(R.id.trspo2);
		trheartrate  = (TableRow) dialog.findViewById(R.id.trheartrate);
		
//		04jan2016
		etdescriptionofreferral =  (EditText) dialog.findViewById(R.id.etdescforreferral);
		etbpsystolicreferral =  (EditText) dialog.findViewById(R.id.etbpsystolicreferral);
		etbpdiastolicreferral =  (EditText) dialog.findViewById(R.id.etbpdiastolicreferral);
		etpulsereferral =  (EditText) dialog.findViewById(R.id.etpulsereferral);
		rd_motherconscious =  (RadioButton) dialog.findViewById(R.id.rd_motherconscious);
		rd_motherunconscious =  (RadioButton) dialog.findViewById(R.id.rd_motherunconscious);
		rd_babyalive =  (RadioButton) dialog.findViewById(R.id.rd_babyalive);
		rd_babydead =  (RadioButton) dialog.findViewById(R.id.rd_babydead);
		mspnreasonforreferral = (MultiSelectionSpinner) dialog.findViewById(R.id.mspnreasonforreferral);

//		24jan2016
		txtrefmode = (TextView)dialog.findViewById(R.id.txtmode);
		
//      updated on 27Feb2016 by Arpitha			
		etheatrate = (EditText) dialog.findViewById(R.id.etheartrate);
		etspo2 = (EditText) dialog.findViewById(R.id.etspo2);
	}
	
	// Validate Referral mandatory fields
		private boolean validateReferralFields() throws Exception {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());

			if (etplaceofreferral.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.ent_placeofreferral));
				etplaceofreferral.requestFocus();
				return false;
			}

//			4jan2016
			if (etdescriptionofreferral.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.enterdescofreferral));
				etdescriptionofreferral.requestFocus();
				return false;
			}
			
//			12Jan2016
			if (etpulsereferral.getText().length() <= 0) {
				displayAlertDialog(getResources().getString(R.string.enterpulseval));
				etpulsereferral.requestFocus();
				return false;
			}	//12jan2016
			
//	      Changes made by Arpitha		
			if(etbpsystolicreferral.getText().length()<=0 && etbpdiastolicreferral.getText().length() <=0 )
			{
				displayAlertDialog("Please Enter BP value and Try Again!!!!");
				etbpsystolicreferral.requestFocus();
				return false;
			}
		
		
			if (etbpsystolicreferral.getText().length() > 0) {
					if(etbpdiastolicreferral.getText().length() <= 0){
					displayAlertDialog(getResources().getString(R.string.enterbpdiaval));
					etbpdiastolicreferral.requestFocus();
					return false;
				}
			}
			
			if (etbpdiastolicreferral.getText().length() > 0) {
					if(etbpsystolicreferral.getText().length() <= 0) {
					displayAlertDialog(getResources().getString(R.string.enterbpsysval));
					etbpsystolicreferral.requestFocus();
					return false;
				}
			}
			return true;
		}
		
//		Set Del Type Reason
		private void setDelTypeReason(int dtype) throws Exception{
			int i = 0;
			deltypereasonMap = new LinkedHashMap<String, Integer>();
			List<String> reasonStrArr = null ;
			if(dtype==2){
				 reasonStrArr = Arrays.asList(getResources().getStringArray(
						R.array.spnreasoncesarean));
			}
			else if(dtype==3){
			 reasonStrArr = Arrays.asList(getResources().getStringArray(
					R.array.spnreasoninstrumental));
			}
			
			if(reasonStrArr != null) {
				for (String str : reasonStrArr ) {
					deltypereasonMap.put(str, i);
					i++;
				}
				mspndeltypereason.setItems(reasonStrArr);
			}
			
		}
		
//		26dec2015
		class CountDownRunner implements Runnable {
			// @Override
			public void run() {
				while (!Thread.currentThread().isInterrupted()) {
					try {
						doWork();
						Thread.sleep(1000); // Pause of 1 Second
					} catch (InterruptedException e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);
						Thread.currentThread().interrupt();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);
					}
				}
			}
		}
		
		public void doWork() throws Exception {
			if(getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						loadWomendata();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName()+ " - " + this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			}
		}
		
//		Set options for reason for referral - 04jan0216
		protected void setReasonForReferral() throws Exception{
			int i = 0;
			reasonforreferralMap = new LinkedHashMap<String, Integer>();
			List<String> reasonStrArr = null ;
			
			 reasonStrArr = Arrays.asList(getResources().getStringArray(
					R.array.reasonforreferral));
			
			
			if(reasonStrArr != null) {
				for (String str : reasonStrArr ) {
					reasonforreferralMap.put(str, i);
					i++;
				}
				mspnreasonforreferral.setItems(reasonStrArr);
			}		
		}
		
//		Set options for tears - 04jan2016
		protected void setTears() throws Exception{
			int i = 0;
			tearsMap = new LinkedHashMap<String, Integer>();
			List<String> tearsStrArr = null ;
			
			 tearsStrArr = Arrays.asList(getResources().getStringArray(
					R.array.tears));
			
			
			if(tearsStrArr != null) {
				for (String str : tearsStrArr ) {
					tearsMap.put(str, i);
					i++;
				}
				mspntears.setItems(tearsStrArr);
			}		
		}
		
//		05Jan2016
		private void setDelReportData() throws Exception {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0]
					.getMethodName() + " - " + this.getClass().getSimpleName());
			
			
			mspndeltypereason.setVisibility(View.GONE);
			mspntears.setVisibility(View.GONE);
			ettears.setVisibility(View.VISIBLE);
			spndel_type.setEnabled(false);
			etdeltypeotherreasons.setEnabled(false);
			spn_noofchild.setEnabled(false);
			spn_delres1.setEnabled(false);
			spn_delres2.setEnabled(false);
			etdeltime.setEnabled(false);
			etdeltime2.setEnabled(false);
			etdeldate.setEnabled(false);
			rdepiyes.setEnabled(false);
			rdepino.setEnabled(false);
			rdcatgut.setEnabled(false);
			rdvicryl.setEnabled(false);
			
			
			etbabywt1.setText("" + rowItems.get(mSelectedRow).getBabywt1());
			etbabywt2.setText("" + rowItems.get(mSelectedRow).getBabywt2());

			babysex1 = rowItems.get(mSelectedRow).getBabysex1();
			babysex2 = rowItems.get(mSelectedRow).getBabysex2();

			if(babysex1==0)
				rdmale1.setChecked(true);
			else if(babysex1==1)
				rdfemale1.setChecked(true);
			
			if(rdmale1.isChecked())
				babysex1=0;
			else
				babysex1=1;

			if(babysex2==0)
				rdmale2.setChecked(true);
			else if(babysex2==1)
				rdfemale2.setChecked(true);
			
			if(rdmale2.isChecked())
				babysex2=0;
			else
				babysex2=1;

			etdelcomments
					.setText(rowItems.get(mSelectedRow).getDel_Comments() == null ? " "
							: rowItems.get(mSelectedRow).getDel_Comments());
			
			etdeldate
			.setText(rowItems.get(mSelectedRow).getDel_Date() == null ? "" : Partograph_CommonClass.getConvertedDateFormat(rowItems.get(mSelectedRow)
					.getDel_Date(), Partograph_CommonClass.defdateformat));

			etdeltime
			.setText(rowItems.get(mSelectedRow).getDel_Time() == null ? Partograph_CommonClass
					.getCurrentTime() : rowItems.get(mSelectedRow)
					.getDel_Time());
			
			
			etdeltime2
			.setText(rowItems.get(mSelectedRow).getDel_time2() == null ? Partograph_CommonClass
					.getCurrentTime() : rowItems.get(mSelectedRow)
					.getDel_time2());
							
								
					

			if (rowItems.get(mSelectedRow).isMothersdeath() == 1) {
				rdmotherdead.setChecked(true);
				etmothersdeathreason
				.setText(rowItems.get(mSelectedRow).getMothers_death_reason() == null ? "" : rowItems.get(mSelectedRow)
						.getMothers_death_reason());
				
				txtmothersdeathreason.setVisibility(View.VISIBLE);
			}
			else {
				rdmotheralive.setChecked(true);
				etmothersdeathreason.setVisibility(View.GONE);
				txtmothersdeathreason.setVisibility(View.GONE);
			}
			
			// Delivery type
			spndel_type.setSelection(rowItems.get(mSelectedRow)
					.getDel_type() - 1);


			
			// No of child
			ArrayList<String> listnoofchild = new ArrayList(
					Arrays.asList(getResources().getStringArray(
							R.array.num_of_children))); // array id of string
			// resource
			int noofchildpos = listnoofchild.indexOf(String.valueOf(rowItems.get(
					mSelectedRow).getNo_of_child()));
			spn_noofchild.setSelection(noofchildpos);
			
			if (noofchildpos == 0) {
				trchilddet1.setVisibility(View.VISIBLE);
				tr_childdetsex1.setVisibility(View.VISIBLE);
				tr_secondchilddetails.setVisibility(View.GONE);
				tr_result2.setVisibility(View.GONE);
				tr_childdet2.setVisibility(View.GONE);
				tr_childsex2.setVisibility(View.GONE);
				tr_deltime2.setVisibility(View.GONE);
				txtfirstchilddetails.setText(getResources().getString(R.string.child_details));

			} else {
				tr_childdet2.setVisibility(View.VISIBLE);
				tr_childsex2.setVisibility(View.VISIBLE);
				tr_secondchilddetails.setVisibility(View.VISIBLE);
				tr_result2.setVisibility(View.VISIBLE);
				trchilddet1.setVisibility(View.VISIBLE);
				tr_childdetsex1.setVisibility(View.VISIBLE);
				tr_deltime2.setVisibility(View.VISIBLE);
				
				txtfirstchilddetails.setText(getResources().getString(R.string.first_child_details));
			}

			// delivery result1
			ArrayList<String> listdelres1 = new ArrayList(
					Arrays.asList(getResources().getStringArray(R.array.del_result))); // array
																						// id
																						// of
																						// string
																						// resource
			int delres1pos = listdelres1.indexOf(rowItems.get(mSelectedRow)
					.getDel_result1() - 1);
			spn_delres1.setSelection(rowItems.get(mSelectedRow)
					.getDel_result1() - 1);

			// delivery result2

			ArrayList<String> listdelres2 = new ArrayList(
					Arrays.asList(getResources().getStringArray(R.array.del_result))); // array
																						// id
																						// of
																						// string
																						// resource
			int delres2pos = listdelres2.indexOf(rowItems.get(mSelectedRow)
					.getDel_result2() -1);
			spn_delres2.setSelection(rowItems.get(mSelectedRow)
					.getDel_result2() -1);
				
			if(rowItems.get(mSelectedRow).getDel_type() > 1) {
				String[] delTypeRes = rowItems.get(mSelectedRow).getDelTypeReason().split(",");
				String[] delTypeResArr = null;
				if(rowItems.get(mSelectedRow).getDel_type()==2)
					delTypeResArr = getResources().getStringArray(R.array.spnreasoncesarean);
				else if(rowItems.get(mSelectedRow).getDel_type()==3)
					delTypeResArr = getResources().getStringArray(R.array.spnreasoninstrumental);
				
				String delTypeResDisplay = "";
				if(delTypeResArr != null) {
					if(delTypeRes != null && delTypeRes.length > 0){
						for(String str : delTypeRes){
							if(str != null && str.length()>0)
								delTypeResDisplay = delTypeResDisplay + delTypeResArr[Integer.parseInt(str)] + "\n";
						}
					}
				}
				txtdeltypereason.setText(delTypeResDisplay);
				etdeltypeotherreasons.setText(rowItems.get(mSelectedRow).getDeltype_otherreasons());
			}
			
			String[] tearsres = rowItems.get(mSelectedRow).getTears().split(",");
			String[] tearsResArr = null;
			
			tearsResArr = getResources().getStringArray(R.array.tears);
			
			String tearsResDisplay = "";
			if(tearsResArr != null) {
				if(tearsres != null && tearsres.length > 0){
					for(String str : tearsres){
						if(str != null && str.length()>0)
							tearsResDisplay = tearsResDisplay + tearsResArr[Integer.parseInt(str)] + "\n";
					}
				}
			}
			
			ettears.setText(tearsResDisplay);
			
			if(rowItems.get(mSelectedRow).getEpisiotomy()==1) {
				
				rdepiyes.setChecked(true);
				trsuturematerial.setVisibility(View.VISIBLE);
				if(rowItems.get(mSelectedRow).getSuturematerial()==1)
					rdcatgut.setChecked(true);
				else
					rdvicryl.setChecked(true);
			}
			else {
				rdepino.setChecked(true);
				trsuturematerial.setVisibility(View.GONE);
			}
		}
		
		
		*//**
		 * This method invokes when Item clicked in Spinner
		 * 
		 * @throws Exception
		 *//*
		public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {
			try {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				switch (adapter.getId()) {

				
				case R.id.spnmonthly:
					
					spnpos  = adapter.getSelectedItemPosition();
					 if(spnpos==1)
					{
						loadWomendata();
					}

					else if(spnpos ==2) 
					{
						loadWomendata();
					}
					
					else
					{
						loadWomendata();
					}
						
					break;	

				}

			} catch (Exception e) {
				AppContext.addLog(
						new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
						e);
				e.printStackTrace();
			}
		}
		
		public void setspinnerdata()
		{
			List<String> spinnerArray =  new ArrayList<String>();
			if(rd_deliveredmonthly.isChecked())
			{
				spinnerArray.add(getResources().getString(R.string.deliveredltlist));
				spinnerArray.add(getResources().getString(R.string.deliveredgtlist));
				spinnerArray.add(getResources().getString(R.string.refer));
			}
			if(rd_delprogmonthly.isChecked())
			{
				spinnerArray.add(getResources().getString(R.string.dipwithin24));
				spinnerArray.add(getResources().getString(R.string.dipgt24));
				spinnerArray.add(getResources().getString(R.string.refer));
			}

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			    getActivity(), android.R.layout.simple_spinner_item, spinnerArray);

			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			Spinner sItems = (Spinner) rootView.findViewById(R.id.spnmonthly);
			sItems.setAdapter(adapter);
		}
}*/