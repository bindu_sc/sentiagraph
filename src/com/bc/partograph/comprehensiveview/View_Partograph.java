package com.bc.partograph.comprehensiveview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import org.achartengine.GraphicalView;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.Add_value_pojo;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class View_Partograph extends Activity {

	Partograph_DB dbh;
	String strWomenid, strwuserId;
	ArrayList<Add_value_pojo> listValuesItemsFHR;
	ArrayList<Add_value_pojo> listValuesItemsDilatation;
	ArrayList<Add_value_pojo> listValuesItemsContractions;
	ArrayList<Add_value_pojo> listValuesItemsPBP;
	GraphicalView mChartViewDilatation = null;
	GraphicalView mChartViewFHR = null;
	GraphicalView mChartViewContraction = null;
	GraphicalView mChartViewPBP = null;
	public static int comptabpos = 0;
	public static boolean isViewParto;
	TableLayout tbllayoutfhr, tbllayoutdil, tbllayoutcontract, tbllayoutpbp, tbllayoutafm, tbllayoutoxytocin,
			tbllayoutdrugs, tbllayouttemp, tbllayouturinetest;
	// 09jan2016
	TextView txtdata;
	LinearLayout llwinfo;
	// 23Jan2016
	ActionBar actionBar;

	// updated on 20june2016 by Arpitha
	PdfPTable table1;

	Document document;
	// private static Font redFont = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL,
	// harmony.java.awt.Color.RED);

	GraphicalView mChartView = null;
	// private CategorySeries mSeries = new CategorySeries("Category");

	// private DefaultRenderer mRenderer = new DefaultRenderer();
	HashMap<String, ArrayList<Women_Profile_Pojo>> categoryList;
	List<Double> maxSpentAmt;

	double Amount;
	int option;

	// private static Font smallBold = new Font(Font.TIMES_ROMAN, 6, Font.BOLD);

	private static Font small = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);

	HashMap<String, Double> bargraph_value = new LinkedHashMap<String, Double>();

	ArrayList<Women_Profile_Pojo> wpojo;

	// updated on 18july2016 by Arpitha
	AQuery aq;
	String doa;
	String toa;
	String risk;

	// 31Aug2016 - bindu
	LinkedHashMap<Integer, Double> newDilValuesCervix;
	LinkedHashMap<Integer, Double> newDilValuesDescent;

	Women_Profile_Pojo woman;// 03Nov2016
	PdfPTable tableAFM;
	PdfPTable table_oxytocin;
	PdfPTable table_drug;
	PdfPTable table_temp;
	PdfPTable table_urine;
	PdfPTable womanbasics;
	ImageView imgpdf;// 07Dec2016 Arpitha

	String path;
	File dir;
	TextView txtdialogtitle;

	File file;
	private static final int DISCOVER_DURATION = 300;// 24Jan2017 Arpitha
	private static final int REQUEST_BLU = 1;// 24Jan2017 Arpitha

	Context context;// 16May2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_printparto);
		try {

			// updated on 28july2016 by Arpitha
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			woman = (Women_Profile_Pojo) getIntent().getSerializableExtra("woman");
			dbh = Partograph_DB.getInstance(getApplicationContext());
			comptabpos = Partograph_CommonClass.curr_tabpos;
			isViewParto = true;

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			imgpdf = (ImageView) findViewById(R.id.imgpdf);// 07Dec2016 Arpitha

			context = View_Partograph.this;// 16May2017 Arpitha

			imgpdf.setVisibility(View.VISIBLE);

			imgpdf.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						path = AppContext.mainDir + "/PDF/PartographSheet";// 03April2017
																			// Arpitha

						dir = new File(path);
						if (!dir.exists())
							dir.mkdirs();

						Log.d("PDFCreator", "PDF Path: " + path);

						file = new File(dir, woman.getWomen_name() + "_PartographSheet.pdf");
						Partograph_CommonClass.pdfAlertDialog(context,  woman,"partograph",null,null);
						;
						// createPdfDocument(0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			actionBar = getActionBar();
			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setHomeButtonEnabled(true);
			actionBar.setTitle(woman.getWomen_name());

			strWomenid = woman.getWomenId();
			strwuserId = woman.getUserId();

			aq = new AQuery(this);

			// updated on 28july2016 by ARpitha
			getwomanbasicdata();

			tbllayoutfhr = (TableLayout) findViewById(R.id.tblfhr);
			tbllayoutdil = (TableLayout) findViewById(R.id.tbldil);
			tbllayoutcontract = (TableLayout) findViewById(R.id.tblcontractions);
			tbllayoutpbp = (TableLayout) findViewById(R.id.tblpbp);
			tbllayoutafm = (TableLayout) findViewById(R.id.tblafm);
			tbllayoutoxytocin = (TableLayout) findViewById(R.id.tbloxytocin);
			tbllayoutdrugs = (TableLayout) findViewById(R.id.tbldrugs);
			tbllayouttemp = (TableLayout) findViewById(R.id.tbltemp);
			tbllayouturinetest = (TableLayout) findViewById(R.id.tblut);

			// 09jan2016
			txtdata = (TextView) findViewById(R.id.txtdata);
			llwinfo = (LinearLayout) findViewById(R.id.llpartodata);

			/*
			 * // loadListDataFHR(); DisplayFHRGraph(); loadListDataAFM();
			 * DisplayDilatationGraph(); //loadListDataDilatation();
			 * loadListDataContraction(); loadListDataOxytocin();
			 * loadListDataDrugs(); loadListDataPulseBP(); loadListDataTemp();
			 * loadListDataUrineTest();
			 */

			// 16May2017 Arpitha - v2.6
			DisplayFHRGraph();
			DisplayAFMGraph();
			DisplayDilatationGraph();
			DisplayContrcationGraph();
			DisplayOxytocinGraph();
			DisplayDrugsGraph();
			DisplayPulseBPGraph();
			DisplayTempGraph();
			DisplayUrineTestGraph();// 16May2017 Arpitha - v2.6

			llwinfo.setVisibility(View.VISIBLE);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
		KillAllActivitesAndGoToLogin.addToStack(this);
	}

	// updated on 18july2016 by Arpitha
	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (woman != null) {

			String doa = "", toa = "", risk = "", dod = "", tod = "";

			aq.id(R.id.wname).text(woman.getWomen_name());
			aq.id(R.id.wage).text(woman.getAge() + getResources().getString(R.string.yrs));
			if (woman.getregtype() != 2) {
				doa = Partograph_CommonClass.getConvertedDateFormat(woman.getDate_of_admission(),
						Partograph_CommonClass.defdateformat);
				toa = Partograph_CommonClass.gettimein12hrformat(woman.getTime_of_admission());
				aq.id(R.id.wdoa).text(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
			} else {

				aq.id(R.id.wdoa).text(getResources().getString(R.string.doe) + ": " + woman.getDate_of_reg_entry());
			}

			aq.id(R.id.wgest)
					.text(getResources().getString(R.string.gest_age) + ":"
							+ (woman.getGestationage() == 0 ? getResources().getString(R.string.notknown)
									: woman.getGestationage() + getResources().getString(R.string.wks)));
			aq.id(R.id.wgravida).text(getResources().getString(R.string.gravida_short_label) + ":" + woman.getGravida()
					+ ", " + getResources().getString(R.string.para_short_label) + ":" + woman.getPara());
			woman.getRisk_category();
			if (woman.getRisk_category() == 1) {
				risk = getResources().getString(R.string.high);
			} else
				risk = getResources().getString(R.string.low);
			aq.id(R.id.wtrisk).text(getResources().getString(R.string.risk_short_label) + ":" + risk);

			if (woman.getDel_type() > 0) {
				String[] deltype = getResources().getStringArray(R.array.del_type);
				aq.id(R.id.wdelstatus)
						.text(getResources().getString(R.string.delstatus) + ":" + deltype[woman.getDel_type() - 1]);
				dod = Partograph_CommonClass.getConvertedDateFormat(woman.getDel_Date(),
						Partograph_CommonClass.defdateformat);
				tod = Partograph_CommonClass.gettimein12hrformat(woman.getDel_Time());
				aq.id(R.id.wdeldate).text(getResources().getString(R.string.del) + ": " + dod + "/" + tod);

			}

		}
	}

	/** This method Calls the idle timeout */
	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		KillAllActivitesAndGoToLogin
				.delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;

			case R.id.logout:
				Intent login = new Intent(this, LoginActivity.class);
				startActivity(login);
				return true;

			case R.id.home:
				Intent home = new Intent(this, Activity_WomenView.class);
				startActivity(home);
				return true;

			case R.id.about:
				Intent about = new Intent(this, About.class);
				startActivity(about);

				return true;

			// 10May2017 Arpitha - v2.6
			case R.id.info:
				Intent info = new Intent(this, GraphInformation.class);
				startActivity(info);
				return true;

			case R.id.settings:

				Intent settings = new Intent(this, Settings_parto.class);
				startActivity(settings);
				return true;

			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(View_Partograph.this, woman.getUserId());
				return true;

			case R.id.comments:
				Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
				startActivity(comments);
				return true;

			case R.id.summary:

				Intent summary = new Intent(getApplicationContext(), Summary_Activity.class);
				startActivity(summary);

				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(View_Partograph.this);

			case R.id.usermanual:

				Intent usermanual = new Intent(getApplicationContext(), UseManual.class);
				startActivity(usermanual);// 10May2017 Arpitha - v2.6

			case R.id.disch:
				Intent disch = new Intent(View_Partograph.this, DischargedWomanList_Activity.class);
				startActivity(disch);

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {// 22oct2016
			// Arpitha
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha

		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * public void createPdfDocument(int i) throws Exception { // TODO
	 * Auto-generated method stub
	 * 
	 * Document doc = new Document(PageSize.A4);
	 * 
	 * try { path = AppContext.mainDir + "/PDF";// changed on 03April2017
	 * Arpitha
	 * 
	 * dir = new File(path); if (!dir.exists()) dir.mkdirs();
	 * 
	 * Log.d("PDFCreator", "PDF Path: " + path);
	 * 
	 * file = new File(dir, woman.getWomen_name() + "_PartographSheet.pdf");
	 * FileOutputStream fOut = new FileOutputStream(file);
	 * 
	 * PdfWriter.getInstance(doc, fOut);
	 * 
	 * doc.setMarginMirroring(false); doc.setMargins(0, 0, 0, 0);
	 * 
	 * doc.open();
	 * 
	 * Paragraph p_heading = new
	 * Paragraph(getResources().getString(R.string.sentiagraph)); Font paraFont
	 * = new Font(Font.COURIER); p_heading.setAlignment(Paragraph.ALIGN_CENTER);
	 * p_heading.setFont(paraFont); doc.add(p_heading);
	 * 
	 * String strdatetime =
	 * Partograph_CommonClass.getConvertedDateFormat(Partograph_CommonClass.
	 * getTodaysDate(), Partograph_CommonClass.defdateformat) + " " +
	 * Partograph_CommonClass.getCurrentTime() + " "; Paragraph p1 = new
	 * Paragraph(strdatetime, small); p1.setAlignment(Paragraph.ALIGN_RIGHT);
	 * doc.add(p1);
	 * 
	 * womanBasicInfo();
	 * 
	 * doc.add(womanbasics);
	 * 
	 * 
	 * 
	 * if (!(LoginActivity.objectid.contains(1)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha Image img_fhr =
	 * Partograph_CommonClass.generateFHRGraph(context); doc.add(img_fhr); } if
	 * (!(LoginActivity.objectid.contains(2)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha tableAFM =
	 * Partograph_CommonClass.generateAFM(context); doc.add(tableAFM); }
	 * 
	 * if (!(LoginActivity.objectid.contains(3)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha Image bar_dilatation =
	 * Partograph_CommonClass.generateDilatationGraph();
	 * doc.add(bar_dilatation); }
	 * 
	 * if (!(LoginActivity.objectid.contains(4)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha Image imgcontraction =
	 * Partograph_CommonClass.generateContractionGraph();
	 * doc.add(imgcontraction); }
	 * 
	 * if (!(LoginActivity.objectid.contains(5)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha table_oxytocin=
	 * Partograph_CommonClass.generateOxytocinGraph(); doc.add(table_oxytocin);
	 * } // 27March2017 Arpitha
	 * 
	 * if (!(LoginActivity.objectid.contains(6)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha table_drug =
	 * Partograph_CommonClass.generateDrugsGraph(); doc.add(table_drug); } if
	 * (!(LoginActivity.objectid.contains(7)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha Image img_pulsepb =
	 * Partograph_CommonClass.generatePulseBPGraph(context);
	 * doc.add(img_pulsepb); }
	 * 
	 * if (!(LoginActivity.objectid.contains(8)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha table_temp =
	 * Partograph_CommonClass.generateTemperatureGraph(); doc.add(table_temp); }
	 * 
	 * if (!(LoginActivity.objectid.contains(9)))// 27March2017 Arpitha
	 * 
	 * {// 27March2017 Arpitha table_urine =
	 * Partograph_CommonClass.generateUrineTestGraph(); doc.add(table_urine); }
	 * 
	 * } catch (
	 * 
	 * DocumentException de)
	 * 
	 * { Log.e("PDFCreator", "DocumentException:" + de); } catch (
	 * 
	 * IOException e)
	 * 
	 * { Log.e("PDFCreator", "ioException:" + e); } finally
	 * 
	 * { doc.close(); }
	 * 
	 * }
	 */

	// 20oct2016 Arpitha
	@Override
	public void onBackPressed() {
		Intent i = new Intent(View_Partograph.this, Activity_WomenView.class);
		startActivity(i);
	}

	/*
	 * // 16Jan2017 Arpitha void displayAlertDialog() {
	 * 
	 * final Dialog dialog = new Dialog(View_Partograph.this);
	 * 
	 * dialog.setTitle(getResources().getString(R.string.afm_abrreivation));
	 * dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	 * dialog.setContentView(R.layout.dialog_action);
	 * dialog.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
	 * R.layout.dialog_title); txtdialogtitle = (TextView)
	 * dialog.findViewById(R.id.graph_title);
	 * txtdialogtitle.setText(getResources().getString(R.string.partograph));
	 * 
	 * ImageButton imgbtncanceldialog = (ImageButton)
	 * dialog.findViewById(R.id.imgbtncanceldialog);
	 * imgbtncanceldialog.setOnClickListener(new View.OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { try { dialog.cancel(); } catch
	 * (Exception e) { AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } } });
	 * 
	 * final RadioButton rdsavepdf = (RadioButton)
	 * dialog.findViewById(R.id.rdsave); final RadioButton rdviewpdf =
	 * (RadioButton) dialog.findViewById(R.id.rdview); Button btndone = (Button)
	 * dialog.findViewById(R.id.btndone);
	 * 
	 * final RadioButton rdshare = (RadioButton)
	 * dialog.findViewById(R.id.rdshare);
	 * 
	 * btndone.setOnClickListener(new OnClickListener() {
	 * 
	 * @Override public void onClick(View v) { // TODO Auto-generated method
	 * stub
	 * 
	 * if (rdsavepdf.isChecked()) { try { dialog.cancel(); createPdfDocument(0);
	 * Toast.makeText(getApplicationContext(),
	 * getResources().getString(R.string.pdf_file_is_saved_at_path) + " " +
	 * path, Toast.LENGTH_LONG).show(); } catch (Exception e) { // TODO
	 * Auto-generated catch block AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } } else if
	 * (rdviewpdf.isChecked())
	 * 
	 * { try { dialog.cancel(); createPdfDocument(0); // to view pdf file from
	 * appliction if (file.exists()) {
	 * 
	 * Uri pdfpath = Uri.fromFile(file); Intent intent = new
	 * Intent(Intent.ACTION_VIEW); intent.setDataAndType(pdfpath,
	 * "application/pdf"); intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	 * 
	 * try { startActivity(intent); } catch (ActivityNotFoundException e) {//
	 * 24Jan2017 // Arpitha // No application to view, ask to download one
	 * AlertDialog.Builder builder = new
	 * AlertDialog.Builder(View_Partograph.this); builder.setTitle(
	 * "No Application Found"); builder.setMessage(
	 * "Download one from Android Market?"); builder.setPositiveButton(
	 * "Yes, Please", new DialogInterface.OnClickListener() {
	 * 
	 * @Override public void onClick(DialogInterface dialog, int which) { Intent
	 * marketIntent = new Intent(Intent.ACTION_VIEW);
	 * marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
	 * startActivity(marketIntent); } }); builder.setNegativeButton("No, Thanks"
	 * , null); builder.create().show(); } } } catch (Exception e) { // TODO
	 * Auto-generated catch block AppContext.addLog(new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName(), e); e.printStackTrace(); } } //
	 * 24Jan2017
	 * 
	 * else if (rdshare.isChecked()) { SendviaBluetoth();
	 * 
	 * }
	 * 
	 * 
	 * else { Toast.makeText(getApplicationContext(),
	 * getResources().getString(R.string.please_select_one_option),
	 * Toast.LENGTH_LONG).show(); }
	 * 
	 * } }); dialog.show();
	 * 
	 * }
	 */

	// 24Jan2017 Arpitha
	public void SendviaBluetoth() {
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

		if (btAdapter == null) {
			Toast.makeText(getApplicationContext(), "Bluetooth is not supported", Toast.LENGTH_LONG).show();
		} else
			enableBlueethoth();
	}

	private void enableBlueethoth() {
		// TODO Auto-generated method stub

		Intent discoverintent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		discoverintent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, DISCOVER_DURATION);
		startActivityForResult(discoverintent, REQUEST_BLU);

	}

	@Override
	protected void onActivityResult(int requestcode, int resultcode, Intent data) {
		// TODO Auto-generated method stub
		if (resultcode == DISCOVER_DURATION && requestcode == REQUEST_BLU) {
			Intent send = new Intent();
			send.setAction(Intent.ACTION_SEND);
			send.setType("text/plains");
			File f = new File(Environment.getExternalStorageDirectory(), "blueethoth.txt");
			send.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));

			PackageManager pak = getPackageManager();
			List<ResolveInfo> list = pak.queryIntentActivities(send, 0);

			if (list.size() > 0) {
				String packagename = null;
				String className = null;
				boolean found = false;

				for (ResolveInfo info : list) {
					packagename = info.activityInfo.packageName;
					if (packagename.equals("com.android.bluetooth")) {
						className = info.activityInfo.name;
						found = true;
						break;
					}
				}
				if (!found) {
					Toast.makeText(getApplicationContext(), "Bluetooth haven not been found", Toast.LENGTH_LONG).show();
				} else {
					send.setClassName(packagename, className);
					startActivity(send);
				}
			} else {
				Toast.makeText(getApplicationContext(), "Bluetooth is Cancelled", Toast.LENGTH_LONG).show();
			}
		}

	}

	// 16May2017 Arpitha - v2.6
	void DisplayFHRGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(1))) {
			tbllayoutfhr.setVisibility(View.VISIBLE);

			mChartViewFHR = Partograph_CommonClass.createChart(woman.getWomenId(), woman.getUserId(), context, 1);
			if (mChartViewFHR != null) {
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
						RelativeLayout.LayoutParams.MATCH_PARENT, 400);
				mChartViewFHR.setLayoutParams(params);

				// tbllayoutfhr.setVisibility(View.VISIBLE);
				tbllayoutfhr.setBackground(getResources().getDrawable(R.drawable.edit_text_style_blue));
				tbllayoutfhr.addView(mChartViewFHR);

				if (mChartViewFHR != null) {
					mChartViewFHR.repaint();
				}
			}
		} else
			tbllayoutfhr.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayDilatationGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(3))) {

			mChartViewDilatation = Partograph_CommonClass.createDilatationGraph(woman.getWomenId(), woman.getUserId(),
					context, 3);

			tbllayoutdil.setVisibility(View.VISIBLE);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 450);
			mChartViewDilatation.setLayoutParams(params);

			tbllayoutdil.addView(mChartViewDilatation);

			if (mChartViewDilatation != null) {
				mChartViewDilatation.repaint();
			}
		} else
			tbllayoutdil.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayAFMGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(2))) {
			tbllayoutafm.setVisibility(View.VISIBLE);

			tbllayoutafm = Partograph_CommonClass.dispalyAFMData(context, 2, woman.getWomenId(), woman.getUserId(),
					tbllayoutafm);

		} else
			tbllayoutafm.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayContrcationGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(4))) {

			mChartViewContraction = Partograph_CommonClass.DisplayContraction(4, woman.getWomenId(), woman.getUserId(),
					context);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 450);
			mChartViewContraction.setLayoutParams(params);

			tbllayoutcontract.addView(mChartViewContraction);

			if (mChartViewContraction != null) {
				mChartViewContraction.repaint();
			}
		}
	}

	// 16May2017 Arpitha - v2.6
	void DisplayOxytocinGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(5))) {
			tbllayoutoxytocin.setVisibility(View.VISIBLE);
			tbllayoutoxytocin = Partograph_CommonClass.DisplayOxytocinData(context, 5, woman.getWomenId(),
					woman.getUserId(), tbllayoutoxytocin);
		} else
			tbllayoutoxytocin.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayDrugsGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(6))) {
			tbllayoutdrugs.setVisibility(View.VISIBLE);
			tbllayoutdrugs = Partograph_CommonClass.DisplayDrugsData(tbllayoutdrugs, 6, woman.getWomenId(),
					woman.getUserId(), context);
		} else
			tbllayoutdrugs.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayUrineTestGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(6))) {
			tbllayouturinetest.setVisibility(View.VISIBLE);
			tbllayouturinetest = Partograph_CommonClass.DisplayUrineTestData(context, 9, woman.getWomenId(),
					woman.getUserId(), tbllayouturinetest);
		} else
			tbllayouturinetest.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayTempGraph() throws Exception {
		if (!(LoginActivity.objectid.contains(8))) {
			tbllayouttemp.setVisibility(View.VISIBLE);
			tbllayouttemp = Partograph_CommonClass.DisplayTemperatureData(context, 8, woman.getWomenId(),
					woman.getUserId(), tbllayouttemp);
		} else
			tbllayouttemp.setVisibility(View.GONE);

	}

	// 16May2017 Arpitha - v2.6
	void DisplayPulseBPGraph() throws Exception {

		if (!(LoginActivity.objectid.contains(7))) {
			tbllayoutpbp.setVisibility(View.VISIBLE);
			mChartViewPBP = Partograph_CommonClass.DisplayPulseBPData(context, woman.getWomenId(), woman.getUserId(),
					7);

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT, 400);
			mChartViewPBP.setLayoutParams(params);

			tbllayoutpbp.addView(mChartViewPBP);

			if (mChartViewPBP != null) {
				mChartViewPBP.repaint();
			}
		} else
			tbllayoutpbp.setVisibility(View.GONE);

	}

}
