package com.bc.partograph.regwomenlist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.MessageLogPojo;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_CommonClass.AsyncCallWS;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.SendSMS;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RecentCustomListAdapterDelivered extends ArrayAdapter<Women_Profile_Pojo> implements AnimationListener {
	Context context;
	ArrayList<Women_Profile_Pojo> data;
	Partograph_DB dbh;
	public static LinkedHashMap<String, Integer> riskoptionsMap;
	EditText etmesss;
	String risk_observed;
	int option = 2;
	ArrayList<String> values;
	String p_no = "";
	// updated 09Oct2016 -Arpitha
	boolean isMessageLogsaved = false;
	Cursor curref;
	// 20Oct2016 Arpitha
	ArrayList<WomenReferral_pojo> rval;
	WomenReferral_pojo wpojo;
	// 21Oct2016 Arpitha
	ImageView img1, img2, img3, img4, img5, img6, img7;// 01nov2016 Arpitha img7
	ArrayList<Women_Profile_Pojo> mStringFilterListDel;
	// ValueFilter valueFilter;
	// 1Nov2016 Arpitha
	String wname;
	int age;
	String regdate;
	int gest;
	String gravida;
	String risk;
	String delstatus = "";
	String deldeldate = "";
	int pos;
	private static byte[] image1 = null;// 08Nov2016
	ArrayList<WomenReferral_pojo> arrval = new ArrayList<WomenReferral_pojo>();// 15Nov2016
																				// Arpitha
	boolean isRefrred = false;// 15Nov2016 Arpitha

	ValueFilter valueFilter;

	public RecentCustomListAdapterDelivered(Context context, int textViewResourceId, ArrayList<Women_Profile_Pojo> data,
			Partograph_DB dbh) {
		super(context, textViewResourceId, data);
		this.context = context;
		this.dbh = dbh;
		this.data = data;
		mStringFilterListDel = data;

	}

	private class WomenListItem {
		TextView txtWomennamedel;
		TextView txtstatusdel;
		TextView txtdate_of_admdel;
		ImageView imgwphotodel;
		ImageView imgdangerSigndel;
		ImageView imghighriskdel;
		TextView txtreferreddel;
		TextView txtreferredplacedel;
		TextView txtreferreddatedel;
		TextView txtadddatedel;
		ImageView imgmsgdel;
		TextView txtcommentcountdel;
		TextView txtpostdel;// 16Oct2016 Arpitha
		ImageView imgsms;// 09Oct2016 Arpitha
		TextView txtbreech;// 07May2017 Arpitha - v2.6

	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// 21oct2016 Arpitha
	@Override
	public int getCount() {
		return (data != null && data.size() > 0) ? data.size() : 0;
	}

	@Override
	public Women_Profile_Pojo getItem(int position) {
		return data.get(position);
	}

	// Get main view
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			WomenListItem holder = null;
			final Women_Profile_Pojo rowItem = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.delivered_adapter, null);

				holder = new WomenListItem();
				holder.txtWomennamedel = (TextView) convertView.findViewById(R.id.txtwnamedel);
				holder.txtstatusdel = (TextView) convertView.findViewById(R.id.txtstatusdel);
				holder.txtdate_of_admdel = (TextView) convertView.findViewById(R.id.txtDateOfAdmdel);
				holder.imgwphotodel = (ImageView) convertView.findViewById(R.id.imgwphotodel);
				holder.imgdangerSigndel = (ImageView) convertView.findViewById(R.id.imgdangersigndel);
				holder.imghighriskdel = (ImageView) convertView.findViewById(R.id.imgriskdel);
				holder.txtreferreddel = (TextView) convertView.findViewById(R.id.txtreferreddel);
				holder.txtreferredplacedel = (TextView) convertView.findViewById(R.id.txtplaceofreferraldel);
				holder.txtreferreddatedel = (TextView) convertView.findViewById(R.id.txtdateofreferraldel);
				holder.txtadddatedel = (TextView) convertView.findViewById(R.id.txtadddatedel);
				holder.txtcommentcountdel = (TextView) convertView.findViewById(R.id.txtcommentcountdel);
				holder.imgmsgdel = (ImageView) convertView.findViewById(R.id.imgmsgdel);
				holder.txtpostdel = (TextView) convertView.findViewById(R.id.txtpostdel);// 16Oct2016
																							// Arpitha
				holder.imgsms = (ImageView) convertView.findViewById(R.id.imgsmsdel);// 09Oct2016
				// Arpitha
				holder.txtbreech = (TextView) convertView.findViewById(R.id.txtbreechdel);// 07May2017
																							// Arpitha
																							// -
																							// v2.6

				convertView.setTag(holder);
			} else
				holder = (WomenListItem) convertView.getTag();

			if (rowItem.getregtype() == 2) {// 15Nov2016 Arpitha
				holder.txtpostdel.setVisibility(View.VISIBLE);// 16Oct2016
																// Arpitha
			} // 16Oct2016 Arpitha

			final String womenid = rowItem.getWomenId();

			String[] deltype = context.getResources().getStringArray(R.array.del_type);
			holder.txtWomennamedel.setText(rowItem.getWomen_name() == null ? " " : rowItem.getWomen_name());

			holder.txtstatusdel.setText(deltype[rowItem.getDel_type() - 1]);

			if (rowItem.getregtype() != 2) {
				holder.txtdate_of_admdel
						.setText(rowItem.getDate_of_admission() == null ? " "
								: context.getResources().getString(R.string.regdate) + " : "
										+ Partograph_CommonClass.getConvertedDateFormat(rowItem.getDate_of_admission(),
												Partograph_CommonClass.defdateformat)
										+ " / " + rowItem.getTime_of_admission());
			} else {
				holder.txtdate_of_admdel
						.setText(
								rowItem.getDate_of_reg_entry() == null ? " "
										: context.getResources().getString(R.string.doe) + " : "
												+ Partograph_CommonClass.getConvertedDateFormat(
														rowItem.getDate_of_reg_entry(),
														Partograph_CommonClass.defdateformat));
			}

			holder.txtadddatedel.setText(rowItem.getDel_Date() == null ? " "
					: context.getResources().getString(R.string.actualdd) + " : " + Partograph_CommonClass
							.getConvertedDateFormat(rowItem.getDel_Date(), Partograph_CommonClass.defdateformat) + " / "
							+ rowItem.getDel_Time());

			// 08Nov2016
			image1 = rowItem.getWomen_Image();
			if (image1 != null)
				new DownloadImageTask().execute(holder);

			int comment_count = dbh.getComentsCount(rowItem.getWomenId(), Partograph_CommonClass.user.getUserId());
			if (comment_count > 0) {
				holder.imgmsgdel.setVisibility(View.VISIBLE);
				holder.txtcommentcountdel.setVisibility(View.VISIBLE);
				holder.txtcommentcountdel.setText("" + comment_count);
			}

			if (rowItem.getRisk_category() == 1) {
				holder.imghighriskdel.setImageResource(R.drawable.ic_hr);

				holder.imghighriskdel.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						try {

							if (MotionEvent.ACTION_UP == event.getAction()) {
								risk_observed = rowItem.getComments();

								if (rowItem.getRiskoptions().length() > 0
										|| (rowItem.getComments() != null && rowItem.getComments().length() > 0)) {
									Partograph_CommonClass.displayHighRiskReasons(rowItem.getRiskoptions(),
											risk_observed, context);
								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return true;
					}
				});

			}
			// else
			// holder.imghighriskdel.setImageResource(0);

			if (rowItem.isDanger()) {
				holder.imgdangerSigndel.setVisibility(View.VISIBLE);
				holder.imgdangerSigndel.setImageResource(R.drawable.ic_compl);

				holder.imgdangerSigndel.setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {

						try {

							ArrayList<String> arr;

							arr = dbh.getobjectid_danger(rowItem.getWomenId());

							if (MotionEvent.ACTION_UP == event.getAction()) {

								Partograph_CommonClass.displayDangervalues(arr, context);

							}
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}

						return true;
					}
				});
			}

			if (Activity_WomenView.referredWomen.contains(rowItem.getWomenId()))
				isRefrred = true;
			else
				isRefrred = false;

			if (isRefrred)// 15Nov2016 Arpitha
			{

				rval = new ArrayList<WomenReferral_pojo>();// 20Oct2016 Arpitha

				curref = dbh.getReferralDetails(Partograph_CommonClass.user.getUserId(), rowItem.getWomenId());
				if (curref != null && curref.getCount() > 0) {
					curref.moveToFirst();
					do {
						// isRefrred = true;
						holder.txtreferreddel.setText(context.getResources().getString(R.string.referred));

						// 18Aug2016-bindu - ref date in def date format
						holder.txtreferreddatedel
								.setText(curref.getString(13) == null ? " "
										: context.getResources().getString(R.string.refdt) + " : "
												+ Partograph_CommonClass.getConvertedDateFormat(curref.getString(13),
														Partograph_CommonClass.defdateformat)
												+ " / " + curref.getString(14));

						holder.txtreferredplacedel
								.setText(context.getResources().getString(R.string.refto) + curref.getString(6));

						holder.txtreferredplacedel.setVisibility(View.VISIBLE);
						holder.txtreferreddatedel.setVisibility(View.VISIBLE);
						holder.txtreferreddel.setVisibility(View.VISIBLE);

						wpojo = new WomenReferral_pojo();// 20Oct2016 Arpitha
						wpojo.setWomenid(curref.getString(1));
						wpojo.setWomenname(curref.getString(2));
						wpojo.setReasonforreferral(curref.getString(7));
						wpojo.setDescriptionofreferral(curref.getString(8));
						// arrpos = 0;
						// arrpos++;
						arrval.add(wpojo);

						rval.add(wpojo);
					} while (curref.moveToNext());

				}
				// else// 15Nov2016 Arpitha
				// isRefrred = false;// 15Nov2016 Arpitha

				// //18Oct2016 ARpitha

				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					holder.imgsms.setVisibility(View.VISIBLE);// 09Oct2016
																// Arpitha

					holder.imgsms.setOnTouchListener(new OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {

							try {

								if (MotionEvent.ACTION_UP == event.getAction()) {
									displayConfirmationAlert(context.getResources().getString(R.string.send_mess),
											womenid, position);

								}
							} catch (NotFoundException e) {
								AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName(), e);
								e.printStackTrace();
							} catch (Exception e) {
								AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName(), e);
								e.printStackTrace();
							}
							return true;
						}
					});
				}
			}

			// 21oct2016 Arpitha
			holder.imgwphotodel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						pos = position;// 01Nov2016 Arpitha
						Partograph_CommonClass.displaySummaryDialog(data, pos, context);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			// 07May2017 Arpitha - v2.6
			if (rowItem.getNormaltype() == 2)
				holder.txtbreech.setVisibility(View.VISIBLE);
			else
				holder.txtbreech.setVisibility(View.GONE);// 07May2017 Arpitha -
															// v2.6

		}

		catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

	@Override
	public void onAnimationEnd(Animation animation) {

	}

	@Override
	public void onAnimationRepeat(Animation animation) {

	}

	@Override
	public void onAnimationStart(Animation animation) {

	}

	// Set options for risk options if high risk - 25dec2015
	protected void setRiskOptions() throws Exception {
		int i = 0;
		riskoptionsMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;

		reasonStrArr = Arrays.asList(context.getResources().getStringArray(R.array.riskoptions));

		if (reasonStrArr != null) {
			for (String str : reasonStrArr) {
				riskoptionsMap.put(str, i);
				i++;
			}
		}
	}

	// 18Oct2016 Arpitha
	// Display Confirmation to exit the screen - 09Oct2016 Arpitha
	private boolean displayConfirmationAlert(String exit_msg, final String classname, final int position)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(context);
		dialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
				+ context.getResources().getString(R.string.sms_alert) + "</font>"));
		dialog.setContentView(R.layout.alertdialog);

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtdialog);
		TextView txt1 = (TextView) dialog.findViewById(R.id.txtval);
		TextView txt2 = (TextView) dialog.findViewById(R.id.txtval1);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);

		txtdialog.setText(exit_msg);
		txt1.setVisibility(View.GONE);
		txt2.setVisibility(View.GONE);

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog.cancel();

				try {
					display_messagedialog(classname, position);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {

					dialog.cancel();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		});
		return true;
	}

	// 18Oct2016 Arpitha
	public void display_messagedialog(final String womenid, final int position) throws Exception {
		try {
			final Dialog sms_dialog = new Dialog(context);
			sms_dialog.setTitle(Html.fromHtml("<font color='" + context.getResources().getColor(R.color.appcolor) + "'>"
					+ context.getResources().getString(R.string.send_sms) + "</font>"));// 08Feb2017
																						// Arpitha

			sms_dialog.setContentView(R.layout.alertdialog_sms);

			// Dialog d = alertDialog.show();
			int dividerId = sms_dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
			View divider = sms_dialog.findViewById(dividerId);
			divider.setBackgroundColor(context.getResources().getColor(R.color.appcolor));// 08Feb2017

			sms_dialog.show();

			// final TextView txtdialog = (TextView)
			// sms_dialog.findViewById(R.id.txtdialog);
			TextView txt1 = (TextView) sms_dialog.findViewById(R.id.txtval);
			TextView txt2 = (TextView) sms_dialog.findViewById(R.id.txtval1);
			// Button imgbtnyes = (Button)
			// sms_dialog.findViewById(R.id.imgbtnyes);
			final EditText etphno = (EditText) sms_dialog.findViewById(R.id.etphnno);

			etmesss = (EditText) sms_dialog.findViewById(R.id.etmess);
			EditText etopt = (EditText) sms_dialog.findViewById(R.id.etreasonoptions);
			TextView txtreason = (TextView) sms_dialog.findViewById(R.id.txtval6);
			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);

			// String RefResDisplay = "";
			String womanname = "";
			String desc = "";
			String reasonforref = null;
			final WomenReferral_pojo wpojo_sms;

			p_no = "";
			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);
			for (int i = 0; i < values.size(); i++) {

				// String s = values.get(i);
				p_no = p_no + values.get(i) + ",";
			}

			etphno.setText(p_no);

			Cursor c = dbh.getReferralDetails(Partograph_CommonClass.user.getUserId(), womenid);

			if (c != null && c.getCount() > 0) {
				c.moveToFirst();

				womanname = c.getString(2);
				desc = c.getString(8);
				reasonforref = c.getString(7);
			}

			txt1.setText(womanname);
			txt2.setText(desc);
			String selectedriskoption = reasonforref;// 20oct2016
			// Arpitha

			ArrayList<String> op = new ArrayList<String>();
			String admResDisplay = "";

			String[] admres = selectedriskoption.split(",");
			String[] admResArr = null;

			admResArr = context.getResources().getStringArray(R.array.reasonforreferralvalues);

			if (admResArr != null && admres != null) {// 09Nov2016 Arpitha -
				// checking null for
				// admres
				if (admres != null && admres.length > 0) {
					for (String str : admres) {
						if (str != null && str.length() > 0 && (!str.equalsIgnoreCase("null")))// 09Nov2016
							// Arpitha
							// -
							// checking
							// null
							// for
							// str
							admResDisplay = admResDisplay + admResArr[Integer.parseInt(str)] + "\n";

					}
				}
			}

			if (admResDisplay.length() > 0) {
				etopt.setText(admResDisplay);
			} else {
				etopt.setVisibility(View.GONE);
				txtreason.setVisibility(View.GONE);
			}

			wpojo_sms = new WomenReferral_pojo();
			wpojo_sms.setWomenname(womanname);
			wpojo_sms.setDescriptionofreferral(desc);
			wpojo_sms.setReasonforreferral(reasonforref);

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {

							if (values != null && values.size() > 0) {
								InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(), womenid,
										wpojo_sms);
								if (isMessageLogsaved) {
									Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
											Toast.LENGTH_LONG).show();
									sendSMSFunction();
									sms_dialog.cancel();
								}
							} else {
								if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() >= 10) {
									InserttblMessageLog(etphno.getText().toString(), etmesss.getText().toString(),
											womenid, wpojo_sms);
									if (isMessageLogsaved) {
										Toast.makeText(context, context.getResources().getString(R.string.sending_sms),
												Toast.LENGTH_LONG).show();
										sendSMSFunction();
									}
									sms_dialog.cancel();

								} else if (etphno.getText().toString().length() <= 0) {
									Toast.makeText(context, context.getResources().getString(R.string.entr_phno),
											Toast.LENGTH_LONG).show();
								} else if (etphno.getText().toString().length() > 0
										&& etphno.getText().toString().length() < 10) {
									Toast.makeText(context,
											context.getResources().getString(R.string.enter_valid_phn_no),
											Toast.LENGTH_LONG).show();
								}
							}

						} else
							Toast.makeText(context, context.getResources().getString(R.string.no_sim),
									Toast.LENGTH_LONG).show();

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
				}
			});
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Insert to record to Message Log
	private void InserttblMessageLog(String phoneno, String comments, String womanid, WomenReferral_pojo data)
			throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String message = "";

		if (data != null) {
			// c.moveToFirst();
			String RefResDisplay = "";
			String womanname = data.getWomenname();
			String desc = data.getDescriptionofreferral();
			String reasonforref = data.getReasonforreferral();
			if (reasonforref != null && reasonforref.length() > 0) {

				String[] admres = reasonforref.split(",");
				String[] admResArr = null;

				admResArr = context.getResources().getStringArray(R.array.reasonforreferralvaluessms);

				if (admResArr != null) {
					if (admres != null && admres.length > 0) {
						for (String str : admres) {
							if (str != null && str.length() > 0)
								RefResDisplay = RefResDisplay + admResArr[Integer.parseInt(str)] + ",";

						}
					}
				}
			}

			if (comments.length() > 0)
				comments = " Cmnts: " + comments; // 11Sep2016 - bindu chk comm
													// length

			message = "ePartograph Ref :- " + womanname + ", " + " Reason: " + RefResDisplay + " Desc: " + desc
					+ comments;

			ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

			if (phoneno.length() > 0) {
				String[] phn = phoneno.split("\\,");
				for (int i = 0; i < phn.length; i++) {
					String num = phn[i];
					if (num != null && num.length() > 0) {
						MessageLogPojo mlp = new MessageLogPojo(Partograph_CommonClass.user.getUserId(), womanid, num,
								desc, RefResDisplay, message, 1, 0);
						mlpArr.add(mlp);
					}
				}
			}

			if (mlpArr != null) {
				for (MessageLogPojo mlpp : mlpArr) {
					dbh.db.beginTransaction();
					int transId = dbh.iCreateNewTrans(Partograph_CommonClass.user.getUserId());// 18Oct2016
					// Arpitha
					boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 18Oct2016
																				// Arpitha
					if (isinserted) {
						isMessageLogsaved = true;
						dbh.iNewRecordTrans(Partograph_CommonClass.user.getUserId(), transId,
								Partograph_DB.TBL_MESSAGELOG);// 18Oct2016
						// Arpitha
						commitTrans();
					} else
						throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
				}
			}
		}
	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */
	private void sendSMSFunction() throws Exception {
		new SendSMS();
	}

	private void commitTrans() throws Exception {
		// TODO Auto-generated method stub
		dbh.db.setTransactionSuccessful();
		dbh.db.endTransaction();
		calSyncMtd();
		Toast.makeText(context, context.getResources().getString(R.string.success), Toast.LENGTH_SHORT).show();
	}

	// Sync - 18Oct2016 Arpitha
	private void calSyncMtd() throws Exception {
		// 25Sep2016 Arpitha - addToTrace
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + context.getClass().getSimpleName());
		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;

		AsyncCallWS task = new AsyncCallWS();
		task.execute();
	}

	// 08Nov2016
	public class DownloadImageTask extends AsyncTask<WomenListItem, Void, Bitmap> {

		ImageView imageView = null;

		protected Bitmap doInBackground(WomenListItem... item) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.imageView = (ImageView) item[0].imgwphotodel;
			return getBitmapDownloaded();
		}

		protected void onPostExecute(Bitmap result) {
			if (result != null)
				imageView.setImageBitmap(result);
		}

		/** This function downloads the image and returns the Bitmap **/
		private Bitmap getBitmapDownloaded() {
			Bitmap btmp = null;
			if (image1 != null) {
				btmp = Bitmap.createScaledBitmap(BitmapFactory.decodeByteArray(image1, 0, image1.length), 64, 64,
						false);
			}
			return btmp;
		}
	}

	private class ValueFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<Women_Profile_Pojo> filterList = new ArrayList<Women_Profile_Pojo>();
				for (int i = 0; i < mStringFilterListDel.size(); i++) {
					if (mStringFilterListDel.get(i).getWomen_name().toUpperCase()
							.contains(constraint.toString().toUpperCase()) || mStringFilterListDel.get(i).getPhone_No()
							.contains(constraint.toString().toUpperCase())) {

						filterList.add(mStringFilterListDel.get(i));
					}

				}
				results.count = filterList.size();
				results.values = filterList;
			} else {
				results.count = mStringFilterListDel.size();
				results.values = mStringFilterListDel;
			}
			return results;

		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			data = (ArrayList<Women_Profile_Pojo>) results.values;
			notifyDataSetChanged();
		}

	}

	@Override
	public Filter getFilter() {
		if (valueFilter == null) {
			valueFilter = new ValueFilter();
		}
		return valueFilter;
	}

}
