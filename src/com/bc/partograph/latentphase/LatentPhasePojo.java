package com.bc.partograph.latentphase;

public class LatentPhasePojo {

	String userId;
	String womanId;
	int pulse;
	int bpsystolic;
	int bpdiastolic;
	String pv;
	int fhs;
	int contractions;
//	String duration;

	String datetime;
	String advice;
	String bp;
	String womanName;

	public String getWomanName() {
		return womanName;
	}

	public void setWomanName(String womanName) {
		this.womanName = womanName;
	}

	public String getBp() {
		return bp;
	}

	public void setBp(String bp) {
		this.bp = bp;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWomanId() {
		return womanId;
	}

	public void setWomanId(String womanId) {
		this.womanId = womanId;
	}

	public int getPulse() {
		return pulse;
	}

	public void setPulse(int pulse) {
		this.pulse = pulse;
	}

	public int getBpsystolic() {
		return bpsystolic;
	}

	public void setBpsystolic(int bpsystolic) {
		this.bpsystolic = bpsystolic;
	}

	public int getBpdiastolic() {
		return bpdiastolic;
	}

	public void setBpdiastolic(int bpdiastolic) {
		this.bpdiastolic = bpdiastolic;
	}

	public String getPv() {
		return pv;
	}

	public void setPv(String pv) {
		this.pv = pv;
	}

	public int getFhs() {
		return fhs;
	}

	public void setFhs(int fhs) {
		this.fhs = fhs;
	}

	public int getContractions() {
		return contractions;
	}

	public void setContractions(int contractions) {
		this.contractions = contractions;
	}

	

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

}
