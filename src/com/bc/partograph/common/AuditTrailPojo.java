package com.bc.partograph.common;

public class AuditTrailPojo {
	
	private String user_Id;
	private String woman_Id;
	private String table_Name;
	private String col_Name;
	private String old_value;
	private String new_value;
	private String dateChanged;
	private int transId;
	
	public AuditTrailPojo( String userId, String womanId, String tableName, String columnName, String dateChanged, 
			String old_Value, String new_Value, int transId){
		this.user_Id = userId;
		this.woman_Id = womanId;
		this.table_Name = tableName;
		this.col_Name = columnName;
		this.dateChanged = dateChanged;
		this.old_value = old_Value;
		this.new_value = new_Value;
		this.transId = transId;
	}
	
	

	public String getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(String user_Id) {
		this.user_Id = user_Id;
	}
	public String getWoman_Id() {
		return woman_Id;
	}
	public void setWoman_Id(String woman_Id) {
		this.woman_Id = woman_Id;
	}
	public String getTable_Name() {
		return table_Name;
	}
	public void setTable_Name(String table_Name) {
		this.table_Name = table_Name;
	}
	public String getCol_Name() {
		return col_Name;
	}
	public void setCol_Name(String col_Name) {
		this.col_Name = col_Name;
	}
	public String getOld_value() {
		return old_value;
	}
	public void setOld_value(String old_value) {
		this.old_value = old_value;
	}
	public String getNew_value() {
		return new_value;
	}
	public void setNew_value(String new_value) {
		this.new_value = new_value;
	}
	public String getDateChanged() {
		return dateChanged;
	}
	public void setDateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
	}
	public int getTransId() {
		return transId;
	}
	public void setTransId(int transId) {
		this.transId = transId;
	}
	
	

}
