package com.bc.partograph.common;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

public class KillAllActivitesAndGoToLogin {
	
	public static ArrayList<Activity> activity_stack = new ArrayList<Activity>();
	public static Context context;
	
	static Handler _idleHandler = new Handler();
	static Runnable _idleRunnable = new Runnable() {
	    @Override
	    public void run() {
	        if(context != null){
	        	killAll();
	        	android.os.Process.killProcess(android.os.Process.myPid());
	        }
	    }
	};
	
	public static void delayedIdle(int delayMinutes) {
	    _idleHandler.removeCallbacks(_idleRunnable);
	    _idleHandler.postDelayed(_idleRunnable, (delayMinutes * 1000 * 60));
	}

//	Add activities to the stack
	public static void addToStack(Activity activity){
		try{
			activity_stack.add(activity);
			context = activity;
			delayedIdle(Integer.parseInt(Partograph_CommonClass.properties.getProperty("idleTimeOut")));
		}catch(Exception e){
			AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - " + new RuntimeException().getClass().getSimpleName(), e);
			e.printStackTrace();
		}
	}

//	Kill all the activities
	public static void killAll(){
		if(activity_stack != null){
			try {
				for(Activity act : KillAllActivitesAndGoToLogin.activity_stack){
					act.finish();
				}
				if(AppContext.printlog != null)
					AppContext.printlog.close();
				if(AppContext.logFile != null)
					AppContext.logFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//		will clear the arraylist of activity
		if(activity_stack != null || activity_stack.size() > 0)
			activity_stack.clear();
	}
}
