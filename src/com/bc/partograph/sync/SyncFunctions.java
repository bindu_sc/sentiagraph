package com.bc.partograph.sync;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import android.util.Base64;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bluecrimson.partograph.UserPojo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Log;

public class SyncFunctions {

	public static boolean syncUptoDate;

	String DATABASE_NAME;
	String displayText;
	Partograph_DB dbh;
	ArrayList<ContentValues> arrComments;

	String Messagecnt = null, RequestId = null, xStatus = null, FileStatus = null, DBStatus = null;

	Context Cx = null;
	ArrayList<String> settingsUpdateStr = null;
	ArrayList<String> userUpdateStr = null;
	public static String strSqlupdateuser, strSqlupdateSettings;
	public static ArrayList<String> strUserupdate = null;
	public static ArrayList<String> strSettingsupdate = null;
	public static boolean blnUserisValid = false;

	String displayTextJava;// 28Sep2016 Arpitha

	UserPojo userSync;// 03April2017 Arpitha

	String strSqlUpdateColorCodedValues;// 12May2017 Arpitha - v2.6
	ArrayList<String> strColorcoded;// 12May2017 Arpitha - v2.6

	// 03April2017 Arpitha
	public static boolean serValidUser = false, serTabLost = false, invalidDb = false, readyToDownload = false;
	public static String userIdReturned = "", returnFilename = "", returnedInfo = "";

	public SyncFunctions(Context context) {
		Cx = context;
	}

	public String getMacAddress() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		WifiManager wimanager = (WifiManager) Cx.getSystemService(Context.WIFI_SERVICE);
		String macAddress = wimanager.getConnectionInfo().getMacAddress();
		if (macAddress == null) {
			macAddress = "";
		} else {
			macAddress = macAddress.replace(":", "");
			macAddress = macAddress.substring(macAddress.length() - 4);
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return macAddress;
	}

	public void display222(XMLObject mObject) {
		if (mObject != null) {

			List<XMLObject> mXmlObjects = mObject.getChilds();

			if (mXmlObjects != null && mXmlObjects.size() > 0) {
				for (XMLObject xmlObject : mXmlObjects) {

					int varSize = 0;

					if (xmlObject.getChilds() != null)
						varSize = xmlObject.getChilds().size();

					if (xmlObject != null && varSize > 0) {
						display222(xmlObject);
					}

					if ((xmlObject.getName() != null) && (xmlObject.getValue() != null)) {
						Log.d("KEY:" + xmlObject.getName() + " :: Value=", xmlObject.getValue());

						String xKey = xmlObject.getName();
						String xValue = xmlObject.getValue();

						if (xKey.equals("Messagecnt")) {
							Messagecnt = xValue;
						} else if (xKey.equals("RequestId")) {
							RequestId = xValue;
							RequestId = RequestId.replace("'", "");

							// 30Nov2015
							// RequestId = RequestId.substring(0, 16);
						} else if (xKey.equals("Status")) {
							xStatus = xValue;
						} else if (xKey.equals("FileStatus")) {
							FileStatus = xValue;
						} else if (xKey.equals("DBStatus"))
							DBStatus = xValue;

						// 03April2017 Arpitha
						else if (xKey.equals("isvalidate")) {
							serValidUser = Boolean.parseBoolean(xValue);
						} /*
							 * else if(xKey.equals("UserIdreturned")) {
							 * userIdReturned = xValue.trim(); }
							 */else if (xKey.equals("Losttab")) {
							serTabLost = Boolean.parseBoolean(xValue);
						} else if (xKey.equals("InvalidDb")) {
							invalidDb = Boolean.parseBoolean(xValue);
						} else if (xKey.equals("ReadyToDownload")) {
							readyToDownload = Boolean.parseBoolean(xValue);
						} else if (xKey.equals("ReturnFileTag")) {
							returnFilename = xValue.trim();
						} else if (xKey.equals("InfoTag")) {
							returnedInfo = xValue.trim();
						}
					}
				}
			}
		}
	}

	public String SyncFunction(String xUserId, String dbName, String xIPAddress) throws Exception {
		String SendMessage = null;
		DATABASE_NAME = dbName;

		String SendMessageJava = null;// 28Sep2016 Arpitha
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		dbh = Partograph_DB.getInstance(Cx);
		try {
			String xLastTranPendingXML = dbh.callResponseXML();
			System.out.println("xLastTranPendingXML " + xLastTranPendingXML);

			if (xLastTranPendingXML.equals("")) { // Normal Transaction

				// updated on 30Nov2015
				String androidId = getAndroidId(KillAllActivitesAndGoToLogin.context);
				SendMessage = dbh.callXMLExport(androidId, xUserId);
				if (SendMessage.equals("")) {
					syncUptoDate = true;

					// add 04feb
					Partograph_CommonClass.movedToInputFolder = true;
					Partograph_CommonClass.responseAckCount = 0; //
					displayText = "SYNC UPTODATE. NO RECORDS FOUND TO SYNC";
					System.out.println("MessageForwarder " + displayText);

					// updated on 17Nov2015
					Partograph_CommonClass.isRecordExistsForSync = false;
				} else if (SendMessage.equals("ERR")) {
					System.out.println("Error.......2");
					AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName());
					return "";
				} else {

					SendMessageJava = SendMessage;// 28Sep2016 Arpitha
					SendMessageJava = new jEncrypt().jEncryptStr_java(SendMessageJava);// 28Sep2016
																						// Arpitha
//					SendMessageJava = SendMessageJava.replaceAll("\\s+", "");// 28Sep2016
																				// Arpitha
//					writeTxtFile(SendMessageJava);// 28Sep2016 Arpitha

					SendMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + SendMessage;

					SendMessage = new jEncrypt().jEncryptStr(SendMessage);
					SendMessage = SendMessage.replaceAll("\\s+", "");
					writeTxtFile(SendMessage);
					displayText = WebService.invokeHelloWorldWS(SendMessage, "MessageForwarder", xIPAddress);

					// displayTextJava =
					// WebServiceJava.invokeHelloWorldWS(SendMessageJava,
					// "forward", xIPAddress, false);// 28Sep2016
					// Arpitha

					System.out.println("MessageForwarder " + displayText);
					updateResponse();

				}
			} else {
				// Get the reponse of LastPending
				SendMessage = xLastTranPendingXML;
				// SendMessageJava = xLastTranPendingXML;//28Sep2016 Arpitha
				SendMessage = new jEncrypt().jEncryptStr(SendMessage);
				// SendMessageJava = new
				// jEncrypt().jEncryptStr_java(SendMessageJava);//28Sep2016
				// Arpitha
				writeTxtFile(SendMessage);
				// writeTxtFile(SendMessageJava);//28Sep2016 Arpitha
				displayText = WebService.invokeHelloWorldWS(SendMessage, "GetDataFromServer", xIPAddress);

				// displayTextJava =
				// WebServiceJava.invokeHelloWorldWS(SendMessage, "getData",
				// xIPAddress, false);displayText =
				// WebServiceJava.invokeHelloWorldWS(SendMessage, "getData",
				// xIPAddress, false);//28Sep2016 Arpitha

				System.out.println("GetDataFromServer  " + displayText);
				updateResponse();
			}

			// 23Feb2017 Arpitha
			userSync = new UserPojo();
			userSync = dbh.getUserProfile();
			SyncFunctionUpdateUserDetails(userSync.getUserId(), userSync.getLastWomennumber(),
					userSync.getLastTransNumber(), userSync.getLastRequestNumber(), userSync.getPwd());

		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			throw e;
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return displayText;
	}

	public void updateResponse() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			// Toast.makeText(Cx, "Server Not Responding",
			// Toast.LENGTH_SHORT).show();
			Partograph_CommonClass.movedToInputFolder = false;
		} else {
			try {
				InputStream is = new ByteArrayInputStream(displayText.getBytes());
				XMLObject mObject = XmlParser.parseXml(is);
				if (mObject != null) {
					Messagecnt = null;
					RequestId = null;
					xStatus = null;
					FileStatus = null;
					DBStatus = null;
					display222(mObject);

					System.out.println("recived Rid: " + RequestId);

					// iCommanDB = new EjananiDBCommon(Cx, DATABASE_NAME);
					if (xStatus.equals("Failure")) {
						// Serious Issue... To be decided What needs to be done
						dbh.UpdateTransStatus(RequestId, "R");
						Partograph_CommonClass.movedToInputFolder = false;
					} else if (DBStatus.equals("TransactionRollback")) {
						// Update Database as Request Rolled Back
						dbh.UpdateTransStatus(RequestId, "R"); // Rejected by
																// Server
						Partograph_CommonClass.movedToInputFolder = false;
					} else if (DBStatus.equals("TransactionCommit")) {
						// Update Database as Request Committed
						dbh.UpdateTransStatus(RequestId, "C"); // Committed by
																// Server
						Partograph_CommonClass.movedToInputFolder = true;
						Partograph_CommonClass.responseAckCount = 0;

						// updated on 19Nov2015
						Partograph_CommonClass.isRecordExistsForSync = false;
					} else if (FileStatus.equals("SaveToFolder")) {
						// ACK. Try not to update second time...(In-Progress)
						dbh.UpdateTransStatus(RequestId, "A");
						Partograph_CommonClass.movedToInputFolder = true;
						Partograph_CommonClass.responseAckCount++;
					}
				}
			} catch (IOException e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
	}

	// This method is what actually pings the host
	@SuppressWarnings("unused")
	private boolean pingHost(String pingAddress) throws Exception {
		Process p1 = null; // Create a process object, which will be used to
							// perform the ping
		int returnVal = 0; // Set ping return status to 0, which automatically
							// declares it as fail

		try {
			// Since Android is Unix base, we can perform a unix ping command.
			// This will return 0 if the ping was unsuccessful, or 1 if the ping
			// returned true
			p1 = java.lang.Runtime.getRuntime().exec("ping " + pingAddress);
		} catch (IOException e) {
			throw e;
		}

		try {
			returnVal = p1.waitFor();
		} catch (InterruptedException e) {
			throw e;
		}

		// return true or false, depending on the status of ping
		if (returnVal == 0)
			return true;
		else
			return false;
	}

	public void writeTxtFile(String sg) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		// SDcard is available
		File f = new File(Environment.getExternalStorageDirectory() + "/req_Enc_file.xml");
		try {
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			System.out.println("\nFile is created...................................");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// method to get comments
	public String SyncFunctionComments(String wUserId, String dbName, String ipAdd) {
		DATABASE_NAME = dbName;
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh = Partograph_DB.getInstance(Cx);

		try {

			String lastModifieddate = dbh.getCommentMaxdate(wUserId);
			if (lastModifieddate == null)
				lastModifieddate = "";
			displayText = WebService.invokeHelloWorldWS(wUserId, dbName, "GetCommentsData", lastModifieddate, ipAdd,
					Cx);

			// lastModifieddate =	qqq
			// lastModifieddate.replaceAll("\\s+","%20");//28Sep2016 Arpitha

			/*
			 * displayTextJava = WebServiceJava.invokeHelloWorldWS(wUserId,
			 * dbName, "getCommentsData", ipAdd,Cx, lastModifieddate
			 * );//28Sep2016 Arpitha
			 */

			System.out.println("GetCommentsData  " + displayText);
			updateToTblComments();
		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();

		}
		return displayText;
	}

	// Update to tbl_comments
	private void updateToTblComments() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			Partograph_CommonClass.responseAckCount++;

		} else {
			displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
			updateComments(displayText, dbh);
			syncUptoDate = true;
			Partograph_CommonClass.movedToInputFolder = true;
			Partograph_CommonClass.responseAckCount = 0;
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Update tbl_Comments Details
	private void updateComments(String data, Partograph_DB dbh) throws Exception {
		writeTxtFile(displayText, "RespFile");

		displayCommentsObject(data);

		if (arrComments != null && arrComments.size() > 0) {
			for (ContentValues values : arrComments) {

				dbh.insertComments(values);

			}
		}
	}

	private void displayCommentsObject(String data) throws ParserConfigurationException, SAXException, IOException {

		InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory;

		dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("Table");

		arrComments = new ArrayList<ContentValues>();

		if (nList.getLength() > 0) {

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				ContentValues values = new ContentValues();

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					values.put("user_Id", getTagValue(eElement, "user_id"));
					values.put("women_Id", getTagValue(eElement, "women_id"));
					values.put("object_Id", getTagValue(eElement, "object_id"));
					values.put("comments", getTagValue(eElement, "comments"));
					values.put("date_of_entry", getTagValue(eElement, "date_of_entry"));
					values.put("time_of_entry", getTagValue(eElement, "time_of_entry"));
					values.put("dateinserted", getTagValue(eElement, "date_inserted"));
					values.put("isviewed", 0);
					values.put("child_id", getTagValue(eElement, "child_id"));
					arrComments.add(values);

				}
			}
		}

	}

	public String getTagValue(Element eElement, String tagName) {
		if (eElement.getElementsByTagName(tagName).item(0) != null)
			return eElement.getElementsByTagName(tagName).item(0).getTextContent();
		else
			return "";
	}

	public void writeTxtFile(String sg, String filename) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		// SDcard is available
		File f = new File(AppContext.mainDir + "/" + filename + ".xml");
		try {
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			System.out.println("\nFile is created...................................");

		} catch (IOException e) {
			e.printStackTrace();
		}
		// }
	}

	// method to get comments
	public String SyncFunctionUserDetails(String wUserId, String dbName, String ipAdd) {
		DATABASE_NAME = dbName;
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh = Partograph_DB.getInstance(Cx);

		try {

			displayText = WebService.invokeHelloWorldWSuser(wUserId, dbName, "GetUserData", ipAdd, Cx);
			System.out.println("GetUserData  " + displayText);
			updateToTblUser(wUserId);
		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();

		}
		return displayText;
	}

	// Update to tbluser
	private void updateToTblUser(String userid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			Partograph_CommonClass.responseAckCount++;

		} else {
			displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
			updateUser(displayText, dbh, userid);
			syncUptoDate = true;
			Partograph_CommonClass.movedToInputFolder = true;
			Partograph_CommonClass.responseAckCount = 0;
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Update to tbluser
	private void updateUser(String data, Partograph_DB dbh, String userid) throws Exception {
		writeTxtFile(displayText, "RespFile");

		displayUserObject(data);

		if (arrComments != null && arrComments.size() > 0) {
			for (ContentValues values : arrComments) {

				dbh.updateUserData(values, userid);

			}
		}
	}

	// get data tbluser
	private void displayUserObject(String data) throws ParserConfigurationException, SAXException, IOException {

		InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory;

		dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("Table");

		arrComments = new ArrayList<ContentValues>();

		if (nList.getLength() > 0) {

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				ContentValues values = new ContentValues();

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					values.put("user_id", getTagValue(eElement, "user_Id"));
					values.put("state", getTagValue(eElement, "state"));
					values.put("district", getTagValue(eElement, "district"));
					values.put("taluk", getTagValue(eElement, "taluk"));
					values.put("facility", getTagValue(eElement, "facility"));
					values.put("facility_name", getTagValue(eElement, "facility_name"));
					arrComments.add(values);

				}
			}
		}

	}

	// Get Settings data from server
	// method to get comments
	public String SyncFunctionSettings(String wUserId, String dbName, String ipAdd) {
		DATABASE_NAME = dbName;
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh = Partograph_DB.getInstance(Cx);

		try {

			String lastModifieddate = dbh.getSettingsLastmodifieddate(wUserId);
			if (lastModifieddate == null)
				lastModifieddate = "";
			displayText = WebService.invokeHelloWorldWS(wUserId, dbName, "GetUserandSettingdetails", lastModifieddate,
					ipAdd, Cx);
			/*
			 * displayTextJava =
			 * WebServiceJava.invokeGetUserandSettingdetails(wUserId, dbName,
			 * "getUserandSettingdetails", lastModifieddate, ipAdd,
			 * Cx);//28Sep2016 Arpitha
			 */

			System.out.println("GetUserandSettingdetails  " + displayText);
			updateToTblSettings();
		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();

		}
		return displayText;
	}

	// Update to tbl_settings
	private void updateToTblSettings() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			Partograph_CommonClass.responseAckCount++;

		} else {
			displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
			updateSettingsDetails(displayText, dbh);
			syncUptoDate = true;
			Partograph_CommonClass.movedToInputFolder = true;
			Partograph_CommonClass.responseAckCount = 0;
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	private void updateSettingsDetails(String data, Partograph_DB dbh) throws Exception {
		writeTxtFile(displayText, "RespFile");

		displaySettingsObject(data);

		if (userUpdateStr.size() > 0) {
			for (String str : userUpdateStr) {
				AppContext.getDb().execSQL(str);
				System.out.println("Str " + str);
			}
		}

		if (settingsUpdateStr.size() > 0) {
			for (String str : settingsUpdateStr) {
				AppContext.getDb().execSQL(str);
				System.out.println("Str " + str);
			}
		}
	}

	private void displaySettingsObject(String data)
			throws UnsupportedEncodingException, ParserConfigurationException, SAXException, IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory;

		dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("Table");

		arrComments = new ArrayList<ContentValues>();

		settingsUpdateStr = new ArrayList<String>();
		userUpdateStr = new ArrayList<String>();

		if (nList.getLength() > 0) {

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					if (eElement.getElementsByTagName("state").item(0) != null) {

						String utemp = " Update tbl_user Set ";

						utemp = utemp + " state = '" + getTagValue(eElement, "state") + "', ";
						utemp = utemp + " district = '" + getTagValue(eElement, "district") + "', ";
						utemp = utemp + " taluk = '" + getTagValue(eElement, "taluk") + "', ";
						utemp = utemp + " facility = '" + getTagValue(eElement, "facility") + "', ";
						utemp = utemp + " facility_name = '" + getTagValue(eElement, "facility_name") + "', ";

						if (eElement.getElementsByTagName("modifieddate").item(0) != null) {
							utemp = utemp + " lastmodifieddate = '" + getTagValue(eElement, "modifieddate") + "' ";
						}

						utemp = utemp + " Where user_Id = '" + getTagValue(eElement, "user_Id") + "' ";
						utemp = utemp + "; ";

						userUpdateStr.add(utemp);

					}

					else {
						String stemp = " Update tbl_settings Set ";

						if (eElement.getElementsByTagName("ipAddress").item(0) != null) {
							stemp = stemp + " ipAddress = '" + getTagValue(eElement, "ipAddress") + "', ";
							stemp = stemp + " serverdb = '" + getTagValue(eElement, "serverdb") + "', ";
						}

						if (eElement.getElementsByTagName("lastmodifieddate").item(0) != null) {
							stemp = stemp + " lastmodifieddate = '" + getTagValue(eElement, "lastmodifieddate") + "' ";
						}

						// 25oct2016 Arpitha
						if (eElement.getElementsByTagName("customfield1").item(0) != null) {
							stemp = stemp + ", " + " customfield1 = '" + getTagValue(eElement, "customfield1") + "' ";
						} // 25oct2016 Arpitha

						if (eElement.getElementsByTagName("customfield2").item(0) != null) {
							stemp = stemp + ", " + " customfield2 = '" + getTagValue(eElement, "customfield2") + "' ";
						} // 25oct2016 Arpitha
						
						//14Sep2017 Arpitha
						if (eElement.getElementsByTagName("user_Id").item(0) != null) {
							stemp = stemp + ", " + " user_Id = '" + getTagValue(eElement, "user_Id") + "' ";
						} //14Sep2017 Arpitha
						
						//14Sep2017 Arpitha
						if (eElement.getElementsByTagName("evalEndDate").item(0) != null) {
							byte[] evalDate = getTagValue(eElement, "evalEndDate").getBytes("UTF-8");
							String SendMessage = Base64.encodeToString(evalDate, Base64.DEFAULT);
							stemp = stemp + ", " + " evalEndDate = '" + SendMessage + "' ";
						} //14Sep2017 Arpitha
						

						stemp = stemp + " Where emailid = '" + getTagValue(eElement, "emailid") + "' ";
						stemp = stemp + "; ";

						settingsUpdateStr.add(stemp);
					}
				}
			}
		}
	}

	// Get User and Setting Details at First time login is valid true
	public String SyncFuncGetUserandSettingDetailsatlogin(String userid, String pwd, String ipAdd) {
		DATABASE_NAME = ipAdd;
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh = Partograph_DB.getInstance(Cx);

		try {

			displayText = WebService.invokeHelloWorldWS(userid, pwd, "validUserandGetDataForUser",
					Partograph_CommonClass.properties.getProperty("ipAddress"), Cx, "", 0, 0, 0);// changed
																									// on
																									// 03April2017
																									// Arpitha

			// displayTextJava = WebServiceJava.invokeHelloWorldWS(userid, pwd,
			// "getvalidUserandGetDataForUser",
			// Partograph_CommonClass.properties.getProperty("ipAddress"), Cx,
			// "");//28Sep2016 Arpitha

			preparedataforuser(userid);

			updateResponse(dbh, displayText);// 03April2017 Arpitha

		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();

		}
		return displayText;
	}

	// Prepare data for user to insert records into tbluser and tblsettings
	private void preparedataforuser(String userid) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			Partograph_CommonClass.responseAckCount++;

		} else {
			displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;

			prepareSqlinsertdataforuserandsetting(displayText, dbh);

			if (blnUserisValid) {
				syncUptoDate = true;
				Partograph_CommonClass.movedToInputFolder = true;
				Partograph_CommonClass.responseAckCount = 0;
			} else {
				syncUptoDate = false;
				Partograph_CommonClass.movedToInputFolder = false;
				Partograph_CommonClass.responseAckCount++;
			}
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Prepare Sql
	private void prepareSqlinsertdataforuserandsetting(String data, Partograph_DB dbh) throws Exception {
		displayUserandSettingsData(data);
	}

	private void displayUserandSettingsData(String data) throws Exception {
		InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		String struservalid = doc.getElementsByTagName("isvalidate").item(0).getTextContent();
		blnUserisValid = Boolean.parseBoolean(struservalid);

		if (blnUserisValid) {
			NodeList nList = doc.getElementsByTagName("Table");

			strUserupdate = new ArrayList<String>();
			strSettingsupdate = new ArrayList<String>();

			if (nList.getLength() > 0) {

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;

						if (eElement.getElementsByTagName("state").item(0) != null) {

							strSqlupdateuser = " Update tbl_user Set ";

							strSqlupdateuser = strSqlupdateuser + " state = '" + getTagValue(eElement, "state") + "', ";
							strSqlupdateuser = strSqlupdateuser + " district = '" + getTagValue(eElement, "district")
									+ "', ";
							strSqlupdateuser = strSqlupdateuser + " taluk = '" + getTagValue(eElement, "taluk") + "', ";
							strSqlupdateuser = strSqlupdateuser + " facility = '" + getTagValue(eElement, "facility")
									+ "', ";
							strSqlupdateuser = strSqlupdateuser + " facility_name = '"
									+ getTagValue(eElement, "facility_name") + "', ";

							if (eElement.getElementsByTagName("modifieddate").item(0) != null) {
								strSqlupdateuser = strSqlupdateuser + " lastmodifieddate = '"
										+ getTagValue(eElement, "modifieddate") + "' ";
							}

							strSqlupdateuser = strSqlupdateuser + " Where user_Id = '"
									+ getTagValue(eElement, "user_Id") + "' ";
							strSqlupdateuser = strSqlupdateuser + "; ";

							strUserupdate.add(strSqlupdateuser);

						} else {

							strSqlupdateSettings = " Update tbl_settings Set ";
							if (eElement.getElementsByTagName("ipAddress").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + " ipAddress = '"
										+ getTagValue(eElement, "ipAddress") + "', ";
								strSqlupdateSettings = strSqlupdateSettings + " serverdb = '"
										+ getTagValue(eElement, "serverdb") + "', ";
							}

							if (eElement.getElementsByTagName("lastmodifieddate").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + " lastmodifieddate = '"
										+ getTagValue(eElement, "lastmodifieddate") + "' ";
							}

							// 25oct2016 Arpitha
							if (eElement.getElementsByTagName("customfield1").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + ", " + " customfield1 = '"
										+ getTagValue(eElement, "customfield1") + "' ";
							} // 25oct2016 Arpitha

							if (eElement.getElementsByTagName("customfield2").item(0) != null) {
								strSqlupdateSettings = strSqlupdateSettings + ", " + " customfield2 = '"
										+ getTagValue(eElement, "customfield2") + "' ";
							} // 25oct2016 Arpitha

							strSqlupdateSettings = strSqlupdateSettings + " Where user_Id = '"
									+ getTagValue(eElement, "user_Id") + "' ";
							strSqlupdateSettings = strSqlupdateSettings + "; ";

							strSettingsupdate.add(strSqlupdateSettings);
						}

					}
				}
			}
		}
	}

	public String getAndroidId(Context context) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		String android_id = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return android_id;
	}

	/*
	 * // 10Jan2017 Arpitha - ChangePassword method : to update the new password
	 * public String SyncFunctionChangePassword(String wUserId, String password,
	 * String dbName, String ipAdd) { DATABASE_NAME = dbName;
	 * AppContext.addToTrace( new
	 * RuntimeException().getStackTrace()[0].getMethodName() + " - " +
	 * this.getClass().getSimpleName()); dbh = Partograph_DB.getInstance(Cx);
	 * 
	 * try {
	 * 
	 * displayText = WebService.invokeHelloWorldWS(wUserId, dbName,
	 * "ChangePassword", password, ipAdd, Cx);
	 * 
	 * System.out.println("ChangePassword  " + displayText); } catch (Exception
	 * e) { syncUptoDate = false; Partograph_CommonClass.movedToInputFolder =
	 * false; e.printStackTrace();
	 * 
	 * } return displayText; }
	 */

	// 03April2017 Arpitha
	// 23Feb2017 Arpitha - ChangePassword method : to update the new password
	public String SyncFunctionUpdateUserDetails(String user_id, int lastWomanNumber, int lastTransNumber,
			int lastRequestNumber, String Pswd) {
		DATABASE_NAME = Partograph_CommonClass.properties.getProperty("serverdbmaster");
		String ipAdd = Partograph_CommonClass.properties.getProperty("ipAddress");
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh = Partograph_DB.getInstance(Cx);

		try {

			displayText = WebService.invokeHelloWorldWS(user_id, Pswd, "updateUserDetails", ipAdd, Cx, "",
					lastWomanNumber, lastTransNumber, lastRequestNumber);

			System.out.println("updateUserDetails  " + displayText);
		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();

		}
		return displayText;
	}

	// 03April2017 Arpitha
	public void updateResponse(Partograph_DB dbh, String displayText) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			// Toast.makeText(Cx, "Server Not Responding",
			// Toast.LENGTH_LONG).show();
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()
					+ " ** Error in Sync Response ** " + " - " + this.getClass().getSimpleName());
			Partograph_CommonClass.movedToInputFolder = false;
		} else {
			try {
				// displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
				// displayText;
				InputStream is = new ByteArrayInputStream(displayText.getBytes("UTF-8"));
				XMLObject mObject = XmlParser.parseXml(is);
				if (mObject != null) {
					Messagecnt = "";
					RequestId = "";
					xStatus = "";
					FileStatus = "";
					DBStatus = "";
					display222(mObject);

					System.out.println("recived Rid: " + RequestId);

					// iCommanDB = new EjananiDBCommon(Cx, DATABASE_NAME);
					if (xStatus.equals("Failure")) {
						// Serious Issue... To be decided What needs to be done
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()
								+ " ** Sync Response: Failure ** " + " - " + this.getClass().getSimpleName());
						dbh.UpdateTransStatus(RequestId, "R");
						Partograph_CommonClass.movedToInputFolder = false;
					} else if (DBStatus.equals("TransactionRollback")) {
						// Update Database as Request Rolled Back
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()
								+ " ** Sync Response: Transc Rollback ** " + " - " + this.getClass().getSimpleName());
						dbh.UpdateTransStatus(RequestId, "R"); // Rejected by
																// Server
						Partograph_CommonClass.movedToInputFolder = false;
					} else if (DBStatus.equals("TransactionCommit")) {
						// Update Database as Request Committed
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()
								+ " ** Sync Response: Transc Commit ** " + " - " + this.getClass().getSimpleName());
						dbh.UpdateTransStatus(RequestId, "C"); // Committed by
																// Server
						Partograph_CommonClass.movedToInputFolder = true;
						Partograph_CommonClass.responseAckCount = 0;
					} else if (FileStatus.equals("SaveToFolder")) {
						// ACK. Try not to update second time...(In-Progress)
						AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()
								+ " ** Sync Response: Success ** " + " - " + this.getClass().getSimpleName());
						dbh.UpdateTransStatus(RequestId, "A");
						Partograph_CommonClass.movedToInputFolder = true;
						Partograph_CommonClass.responseAckCount++;
					}
				}
			} catch (IOException e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
	}

	// 03May2017 Arpitha
	// method to get color coded values
	public String SyncFunctionColorCodedValues(String dbName, String ipAdd) {
		DATABASE_NAME = dbName;
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		dbh = Partograph_DB.getInstance(Cx);

		try {

			String lastModifieddate = dbh.getColoCodedMaxdate();
			
			if (lastModifieddate == null)
				lastModifieddate = "";
			//lastModifieddate = "2016-05-26%2012:51:37";
			displayText = WebService.invokeHelloWorldWS("", dbName, "GetColorCodedValues", lastModifieddate, ipAdd, Cx);

			System.out.println("GetColorCodedValues  " + displayText);
			updateToTblColorCodedValues();
		} catch (Exception e) {
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			e.printStackTrace();

		}
		return displayText;
	}

	// 03May2017 Arpitha
	// Update to tbl_colorcodedvalues
	private void updateToTblColorCodedValues() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (displayText == "Error") {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			syncUptoDate = false;
			Partograph_CommonClass.movedToInputFolder = false;
			Partograph_CommonClass.responseAckCount++;

		} else {
			displayText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + displayText;
			updateColorCodeValues(displayText, dbh);
			syncUptoDate = true;
			Partograph_CommonClass.movedToInputFolder = true;
			Partograph_CommonClass.responseAckCount = 0;
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
		}
	}

	// Update tbl_Colorcodevalues Details - 03May2017 Arpitha
	private void updateColorCodeValues(String data, Partograph_DB dbh) throws Exception {
		writeTxtFile(displayText, "RespFile");

		displayColorCodedObject(data);

		if (arrComments != null && arrComments.size() > 0) {
			for (ContentValues values : arrComments) {

				dbh.insertColorCodedValues(values);

			}
		}
		// 12May2017 Arpitha - v2.6
		if (strColorcoded != null && strColorcoded.size() > 0) {
			for (String str : strColorcoded) {
				AppContext.getDb().execSQL(str);
			}

		} // 12May2017 Arpitha - v2.6

	}

	// 03May2017 Arpitha
	private void displayColorCodedObject(String data) throws ParserConfigurationException, SAXException, IOException {

		InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));

		DocumentBuilderFactory dbFactory;

		dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("Table");

		arrComments = new ArrayList<ContentValues>();

		strColorcoded = new ArrayList<String>();// 12May2017 Arpitha - v2.6

		if (nList.getLength() > 0) {

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				ContentValues values = new ContentValues();

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					ArrayList<String> propid;// 12may2017 Arpitha - v2.6

					propid = dbh.getAllIds();// 12may2017 Arpitha - v2.6

					if (!(propid.contains(getTagValue(eElement, "Id"))))// 12may2017
																		// Arpitha
																		// -
																		// v2.6
					{
						values.put("Id", getTagValue(eElement, "Id"));
						values.put("object_Id", getTagValue(eElement, "object_Id"));
						values.put("Prop_Id", getTagValue(eElement, "Prop_Id"));
						values.put("prop_Name", getTagValue(eElement, "prop_name"));
						values.put("redValues", getTagValue(eElement, "redValues"));
						values.put("amberValues", getTagValue(eElement, "amberValues"));
						values.put("greenValues", getTagValue(eElement, "greenValues"));
						values.put("suggestedBy", getTagValue(eElement, "suggestedBy"));
						values.put("isDefault", getTagValue(eElement, "isDefault"));
						values.put("institution_id", getTagValue(eElement, "institution_id"));
						values.put("servercreateddate", getTagValue(eElement, "createddate"));
						values.put("lastmodifieddate", getTagValue(eElement, "lastmodifieddate"));
						arrComments.add(values);
					} else// 12may2017 Arpitha - v2.6
					{

						strSqlUpdateColorCodedValues = " Update tbl_ColorCodedValues Set ";
						if (eElement.getElementsByTagName("object_Id").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + " object_Id = '"
									+ getTagValue(eElement, "object_Id") + "', ";
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + " Prop_Id = '"
									+ getTagValue(eElement, "Prop_Id") + "', ";
						}

						if (eElement.getElementsByTagName("prop_name").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + " prop_Name = '"
									+ getTagValue(eElement, "prop_name") + "' ";
						}

						// 25oct2016 Arpitha
						if (eElement.getElementsByTagName("redValues").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " redValues = '"
									+ getTagValue(eElement, "redValues") + "' ";
						} // 25oct2016 Arpitha

						if (eElement.getElementsByTagName("amberValues").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " amberValues = '"
									+ getTagValue(eElement, "amberValues") + "' ";
						} // 25oct2016 Arpitha

						// 21March2017 Arpitha
						if (eElement.getElementsByTagName("greenValues").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " greenValues = '"
									+ getTagValue(eElement, "greenValues") + "' ";
						} // 21March2017 Arpitha

						if (eElement.getElementsByTagName("suggestedBy").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " suggestedBy = '"
									+ getTagValue(eElement, "suggestedBy") + "' ";
						}

						if (eElement.getElementsByTagName("isDefault").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " isDefault = '"
									+ getTagValue(eElement, "isDefault") + "' ";
						}

						if (eElement.getElementsByTagName("institution_id").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " institution_id = '"
									+ getTagValue(eElement, "institution_id") + "' ";
						}

						if (eElement.getElementsByTagName("createddate").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", "
									+ " servercreateddate = '" + getTagValue(eElement, "createddate") + "' ";
						}

						if (eElement.getElementsByTagName("lastmodifieddate").item(0) != null) {
							strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + ", " + " lastmodifieddate = '"
									+ getTagValue(eElement, "lastmodifieddate") + "' ";
						}

						strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + " Where Id = '"
								+ getTagValue(eElement, "Id") + "' ";
						strSqlUpdateColorCodedValues = strSqlUpdateColorCodedValues + "; ";

						strColorcoded.add(strSqlUpdateColorCodedValues);

					} // 12may2017 Arpitha - v2.6

				}
			}
		}

	}

}