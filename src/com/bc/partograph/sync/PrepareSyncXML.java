package com.bc.partograph.sync;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.SyncStateContract.Constants;
import android.util.Base64;
import android.util.Log;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;

@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
public class PrepareSyncXML {
	SQLiteDatabase db;
	String xmlString = null;

	// private final SQLiteDatabase db;
	private SyncXmlBuilder xmlBuilder;
	Context context;

	public PrepareSyncXML(final SQLiteDatabase db, Context context) {
		this.db = db;
		this.context = context;

	}

	// Prepares XML for PendingStatus
	public String WriteResponseXML() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String SQLStr = null;
		Cursor c1;
		String reqId;

		try {

			// updated 13Nov2015
			SQLStr = "SELECT " + Partograph_DB.C_REQUESTID + " FROM " + Partograph_DB.TBL_SYNCMASTER + " WHERE "
					+ Partograph_DB.C_REQUESTSTATUS + " = 'A' or " + Partograph_DB.C_REQUESTSTATUS + " = 'S'";// or
																												// "+Partograph_DB.C_REQUESTSTATUS
																												// +"
																												// =
																												// 'N'";

			c1 = db.rawQuery(SQLStr, new String[0]);

			if (c1.moveToFirst()) {
				reqId = c1.getString(0);
				c1.close();

				// get the status of last request
				prepareResponseXML(reqId);
				// set current requestid to global variable // updated on
				// 13Nov2015
				Partograph_CommonClass.requestId = reqId;
				xmlString = xmlBuilder.end();
				writeTxtFile(xmlString);
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				return xmlString;
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			throw e;
		}

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		return "";
	}

	public String WriteSyncXML(String macAddress, String UserId) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		String SQLStr = null;
		String xFirstDate = null;
		Cursor c1;
		String reqId;

		try {

			/*SQLStr = "SELECT " + Partograph_DB.C_TRANSDATE + "," + "Count(*) , Date(substr(" + Partograph_DB.C_TRANSDATE
					+ ", 7) || '-' || substr(" + Partograph_DB.C_TRANSDATE + ",4,2) || '-' || substr("
					+ Partograph_DB.C_TRANSDATE + ",1,2)) as tdate FROM " + Partograph_DB.TBL_TRANSHEADER + " WHERE "
					+ Partograph_DB.C_TRANSSTATUS + " = 'N' Group by tdate having tdate <= Date('now') order by tdate";*/
			SQLStr = "SELECT " + Partograph_DB.C_TRANSDATE + "," + "Count(*) , Date(substr(" + Partograph_DB.C_TRANSDATE
					+ ", 7) || '-' || substr(" + Partograph_DB.C_TRANSDATE + ",4,2) || '-' || substr("
					+ Partograph_DB.C_TRANSDATE + ",1,2)) as tdate FROM " + Partograph_DB.TBL_TRANSHEADER + " WHERE "
					+ Partograph_DB.C_TRANSSTATUS + " = 'N' ";

			c1 = db.rawQuery(SQLStr, new String[0]);
			if (c1.moveToFirst()) {
				xFirstDate = c1.getString(0); // Get the First Date
				c1.close();

				int LastRequestNumber = getLastRequestNumber(UserId);
				reqId = macAddress + UserId + String.format("%05d", LastRequestNumber + 1); // Create
																							// a
																							// new
																							// RequestId

				// set current requestid to global variable // updated on
				// 13Nov2015
				Partograph_CommonClass.requestId = reqId;

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String xTodaysDate = sdf.format(new Date());

				// Insert Into tblSyncM
				SQLStr = "INSERT INTO " + Partograph_DB.TBL_SYNCMASTER + " Values ('" + UserId + "','" + reqId + "','"
						+ xTodaysDate + "', '" + xTodaysDate + "', 'N')";
				db.execSQL(SQLStr);

				// Update tblUsers
				SQLStr = "UPDATE " + Partograph_DB.TBL_USER + " SET " + Partograph_DB.LAST_REQUESTNUM + " = "
						+ Partograph_DB.LAST_REQUESTNUM + " + 1 WHERE " + Partograph_DB.USER_ID + " = '" + UserId + "'";
				db.execSQL(SQLStr);
			} else {
				c1.close();
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				return "";
			}

			// Get all records from Trans Header - Not SENT
			SQLStr = "SELECT " + Partograph_DB.TRANS_ID + " FROM " + Partograph_DB.TBL_TRANSHEADER + " WHERE "
					+ Partograph_DB.C_TRANSSTATUS + " = 'N' AND " + Partograph_DB.C_TRANSDATE + "= '" + xFirstDate
					+ "'";

			c1 = db.rawQuery(SQLStr, new String[0]);

			if (c1.moveToFirst()) {
				xmlBuilder = new SyncXmlBuilder();
				xmlBuilder.openRow("Request");

			
				xmlBuilder.addColumn("ApplicationId", "partograph");// 29Sep2016
																	// Arpitha

				xmlBuilder.addColumn("RequestId", reqId);
				xmlBuilder.addColumn("DatabaseId", Partograph_CommonClass.sPojo.getServerDb());
				// Loop thru the records in Trans Header
				do {
					String xTranId = c1.getString(0);
					// Get all records from Trans Detail
					String sqlTransDetail = "SELECT " + Partograph_DB.C_ACTION + "," + Partograph_DB.C_TABLENAME + ","
							+ Partograph_DB.C_SQLSTATEMENT + " FROM " + Partograph_DB.TBL_TRANSDETAIL + " WHERE "
							+ Partograph_DB.TRANS_ID + " = '" + xTranId + "'";
					Cursor c2 = db.rawQuery(sqlTransDetail, new String[0]);
					if (c2.moveToFirst()) {
						do {
							String xAction = c2.getString(0); // Action
							String xTableName = c2.getString(1); // Table Name
							if (xAction.equals("I")) {
								String xVAL = prepareXMLforInsert(xTableName, xTranId);
								if (xVAL == null) {
									AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName()
											+ " - " + this.getClass().getSimpleName());
									return "ERR";
								}
							} else if (xAction.equals("U")) {
								String xUpdateSQL = c2.getString(2);
								prepareXMLforUpdate(xTableName, xTranId, xUpdateSQL);
							}
						} while (c2.moveToNext());
					}
				} while (c1.moveToNext());
			}

			xmlBuilder.openRow("/Request");
			xmlString = xmlBuilder.end();

			System.out.println("SEE THIS" + xmlString);
			Log.i(Constants._COUNT, "exporting database complete");

			String SQLStr1 = "UPDATE " + Partograph_DB.TBL_TRANSHEADER + " SET " + Partograph_DB.C_REQUESTID + "= '"
					+ reqId + "'" + " WHERE " + Partograph_DB.C_TRANSSTATUS + " ='N' AND " + Partograph_DB.C_TRANSDATE
					+ "= '" + xFirstDate + "'";
			db.execSQL(SQLStr1);

			System.out.println("send Rid: " + reqId);

			writeTxtFile(xmlString);

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			return xmlString;
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			throw e;
		}
	}

	@SuppressLint("DefaultLocale")
	private String prepareXMLforInsert(String tableName, final String transId) throws Exception {
		String returnValue = "";
		String str = "";

		AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
				+ this.getClass().getSimpleName() + " tblName: " + tableName + " transId: " + transId);

		tableName = tableName.toLowerCase();

		String inputSQL = "SELECT * FROM " + tableName + " WHERE " + Partograph_DB.TRANS_ID + "='" + transId + "'";
		try {
			Cursor c = db.rawQuery(inputSQL, new String[0]);
			if (c.moveToFirst()) {
				int cols = c.getColumnCount();

				xmlBuilder.openRow(tableName + "nodes");
				do {

					xmlBuilder.openRow(tableName + "node");
					xmlBuilder.addColumn("tablename", tableName);
					xmlBuilder.addColumn("action", "I");

					for (int i = 0; i < cols; i++) {
						if ((tableName.toLowerCase().equals(Partograph_DB.TBL_REGISTEREDWOMEN)) && (i == 2)) {
							System.out.println("col - blob" + c.getColumnName(i));
							System.out.println("col - Length" + c.getType(i));
							if (c.getType(i) > 0) { // Null = 0

								@SuppressWarnings("unused")
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								byte[] b = c.getBlob(2);
								if (b.length > 1) {
									String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

									str = encodedImage;
								}
							}
							xmlBuilder.addColumn(c.getColumnName(i).toLowerCase(), str);
						} else {

							String mStr = c.getString(i);
							xmlBuilder.addColumn(c.getColumnName(i).toLowerCase(), mStr);
						}
					}
					xmlBuilder.openRow("/" + tableName + "node"); // Close
				} while (c.moveToNext());
				xmlBuilder.openRow("/" + tableName + "nodes"); // Close
			}
			c.close();
		} catch (Exception e) {
			throw e;
		}
		return returnValue;
	}

	private String prepareXMLforUpdate(String tableName, final String transId, final String updateSQL)
			throws Exception {
		try {
			tableName = tableName.toLowerCase();

			xmlBuilder.openRow(tableName + "nodes");
			xmlBuilder.openRow(tableName + "node");

			xmlBuilder.addColumn("tablename", tableName);
			xmlBuilder.addColumn("action", "U");

			xmlBuilder.addColumn("SQLSTATEMENT", updateSQL);

			xmlBuilder.openRow("/" + tableName + "node"); // Close
			xmlBuilder.openRow("/" + tableName + "nodes"); // Close

			xmlString = xmlBuilder.end();
			return xmlString;
		} catch (Exception e) {
			throw e;
		}
	}

	private void prepareResponseXML(final String reqId) throws IOException {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		xmlBuilder = new SyncXmlBuilder();
		xmlBuilder.openRow("Request");
		xmlBuilder.addColumn("ApplicationId", "partograph");// 28Sep2016
		xmlBuilder.addColumn("RequestId", reqId);
		xmlBuilder.addColumn("DatabaseId", Partograph_CommonClass.sPojo.getServerDb());
		xmlBuilder.openRow("/Request");
		xmlString = xmlBuilder.end();
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
	}

	/**
	 * XmlBuilder is used to write XML tags (open and close, and a few
	 * attributes) to a StringBuilder. Here we have nothing to do with IO or
	 * SQL, just a fancy StringBuilder.
	 * 
	 * @author ccollins
	 *
	 */
	@SuppressWarnings("unused")
	static class SyncXmlBuilder {

		// private static final String SOAP_ENVELOPE = " <soap :Envelope
		// xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		// xmlns:xsd="http://www.w3.org/2001/XMLSchema"
		// xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">";

		private static final String OPEN_XML_STANZA = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		private static final String CLOSE_WITH_TICK = ">";
		private static final String DB_OPEN = "<database name='";
		private static final String DB_CLOSE = "</database>";
		private static final String TABLE_OPEN = "<table name='";
		private static final String TABLE_CLOSE = "</table>";
		private static final String ROW_OPEN = "<row>";
		private static final String ROW_CLOSE = "</row>";
		private static final String COL_OPEN = "<col name='";
		private static final String COL_CLOSE = "</col>";

		private static final String OPEN_WITH_TICK = "<";

		private final StringBuilder sb;

		public SyncXmlBuilder() throws IOException {
			sb = new StringBuilder();
		}

		public String end() throws IOException {
			return sb.toString();
		}

		void openRow() {
			sb.append(SyncXmlBuilder.ROW_OPEN);
		}

		void openRow(String xVal) {
			sb.append(SyncXmlBuilder.OPEN_WITH_TICK + xVal + SyncXmlBuilder.CLOSE_WITH_TICK);
		}

		void closeRow() {
			sb.append(SyncXmlBuilder.ROW_CLOSE);
		}

		void addColumn(final String name, final String val) throws IOException {

			if (val == null) {
			} else {
				sb.append(SyncXmlBuilder.OPEN_WITH_TICK + name + SyncXmlBuilder.CLOSE_WITH_TICK + val
						+ SyncXmlBuilder.OPEN_WITH_TICK + "/" + name + SyncXmlBuilder.CLOSE_WITH_TICK);
			}

		}
	}

	public int getLastRequestNumber(String userId) throws Exception {
		int LastRequestNumber = 0;

		String sqlStr = "SELECT " + Partograph_DB.LAST_REQUESTNUM + " FROM " + Partograph_DB.TBL_USER + " WHERE "
				+ Partograph_DB.USER_ID + " = '" + userId + "'";
		Cursor c = db.rawQuery(sqlStr, null);
		if (c != null) {
			if (c.moveToFirst()) {
				LastRequestNumber = c.getInt(0);
			}
		}
		c.close();
		return LastRequestNumber;
	}

	public void writeTxtFile(String sg) throws Exception {
		InputStream inputStream = new ByteArrayInputStream(sg.getBytes());
		@SuppressWarnings("unused")
		String state = Environment.getExternalStorageState();

		// SDcard is available
		File f = new File(Environment.getExternalStorageDirectory() + "/resp_file.xml");
		try {
			f.createNewFile();
			OutputStream out = new FileOutputStream(f);
			byte buf[] = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();
			inputStream.close();
			System.out.println("\nFile is created...................................");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}