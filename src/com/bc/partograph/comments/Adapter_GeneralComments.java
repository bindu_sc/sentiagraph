//13Oct2016 Arpitha - to dispaly general comments on a list view
package com.bc.partograph.comments;

import java.util.ArrayList;

import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bluecrimson.partograph.Comments_Pojo;
import com.bluecrimson.partograph.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class Adapter_GeneralComments extends ArrayAdapter<Comments_Pojo> {
	Context context;
	Comments_Pojo data;

	// constructor
	public Adapter_GeneralComments(Context context, int textViewResourceId, ArrayList<Comments_Pojo> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
	}

	private class CommentsListItem {
		TextView txtComments;
		TextView txtdate_of_com;
		TextView txttime_of_com;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	// Get main view
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			CommentsListItem holder = null;
			Comments_Pojo arrval = getItem(position);

			LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.activity_comment, null);

				holder = new CommentsListItem();
				holder.txtComments = (TextView) convertView.findViewById(R.id.txtComment);
				holder.txtdate_of_com = (TextView) convertView.findViewById(R.id.txtdateComments);
				holder.txttime_of_com = (TextView) convertView.findViewById(R.id.txttimecomments);

				convertView.setTag(holder);
			} else
				holder = (CommentsListItem) convertView.getTag();

			holder.txtComments.setText(arrval.getComment() == null ? " " : arrval.getComment());

			holder.txtdate_of_com.setText(arrval.getGivenby() == null ? " " : arrval.getGivenby());

			String date = arrval.getDate_of_insertion() == null ? " "
					: Partograph_CommonClass.getConvertedDateFormat(arrval.getDate_of_insertion(),
							Partograph_CommonClass.defdateformat);
			String time = arrval.getTime_of_insertion() == null ? " " : arrval.getTime_of_insertion();
			String datetime = date + "/" + time;

			holder.txttime_of_com.setText(datetime);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		return convertView;
	}

}
