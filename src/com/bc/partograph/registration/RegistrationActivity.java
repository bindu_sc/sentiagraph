﻿package com.bc.partograph.registration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import com.androidquery.AQuery;
import com.bc.partograph.comments.Comments_Activity;
import com.bc.partograph.common.AppContext;
import com.bc.partograph.common.KillAllActivitesAndGoToLogin;
import com.bc.partograph.common.MessageLogPojo;
import com.bc.partograph.common.MultiSelectionSpinner;
import com.bc.partograph.common.Partograph_CommonClass;
import com.bc.partograph.common.Partograph_DB;
import com.bc.partograph.common.SendSMS;
import com.bc.partograph.login.LoginActivity;
import com.bc.partograph.referralinfo.ReferralInfo_Activity;
import com.bc.partograph.settings.About;
import com.bc.partograph.settings.Settings_parto;
import com.bc.partograph.sliding.GraphInformation;
import com.bc.partograph.sync.SyncFunctions;
import com.bc.partograph.womenview.Activity_WomenView;
import com.bc.partograph.womenview.Fragment_DIP;
import com.bluecrimson.additionalDetails.Summary_Activity;
import com.bluecrimson.dischargedetails.DischargedWomanList_Activity;
import com.bluecrimson.partograph.R;
import com.bluecrimson.partograph.UserPojo;
import com.bluecrimson.partograph.WomenReferral_pojo;
import com.bluecrimson.partograph.Women_Profile_Pojo;
import com.bluecrimson.usermanual.UseManual;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class RegistrationActivity extends FragmentActivity implements OnClickListener, OnFocusChangeListener {
	static AQuery aq;
	String path = null;
	ByteArrayOutputStream baos;
	File imgfileBeforeResult;
	Uri outputFileUri;
	private static final int IMAGE_CAPTURE = 0;
	Bitmap bitmap;
	Partograph_DB dbh;
	Women_Profile_Pojo wpojo;
	int womanAge, gest_age, gest_age_days;
	static String todaysDate;
	String selecteddate;
	int year, mon, day;
	UserPojo user;
	static final int TIME_DIALOG_ID = 999;
	private static int hour;
	private static int minute;
	boolean inetOn = false;
	String serverResult = null;
	boolean[] itemsChecked;
	int risk, memb_pre_abs;
	public static int regtabpos = 0;
	String displaydateformat;// Changed on 27Mar2015
	String admitted_with;
	int igravida, ipara, igest_age_days;
	MultiSelectionSpinner mspnriskoptions;// 25Dec2015
	public static LinkedHashMap<String, Integer> riskoptionsMap;
	// TableRow trriskoptions;
	MultiSelectionSpinner mspnadmittedwith;// 9Jan0216
	LinkedHashMap<String, Integer> admittedwithMap;
	int bldgrp = 0;// 19jan2016
	// updated by Arpitha on 27feb2016
	boolean islmp = false;
	String iweight;
	String strlmpdate, stredddate;
	int height_numeric = 0;
	int height_decimal = 0;
	RadioButton rd_feet;
	RadioButton rd_cm, rd_htdontknow;
	int height_unit;
	ImageView imgphoto;
	public static boolean isreg_activity;
	// updated on 14july2016 by Arpitha
	RadioButton rd_highrisk;
	List<String> reasonStrArr = null;
	CheckBox chkgest;// updated on 20july2016 by Arpitha
	int option = 1;
	ArrayList<String> values;
	String womenId;
	int para;
	// updated on25july2016 by Arpitha
	ArrayList<String> risk_observed;
	String robserved = "";
	String edd;
	boolean isage = false;
	boolean isheight = false;
	boolean isweight = false;
	RadioButton rd_lowrisk;
	// updated on3August2016 by Arpitha
	boolean ishighrisk = false;
	// updated 23Aug2016 - bindu
	boolean isMessageLogsaved = false;
	// updated 25Aug2016 - bindu
	String messagetobesent;
	// 28Sep2016 Arpitha
	int age = 0;
	double height = 0;
	double weight = 0;
	String height_feet;
	String feet_height;
	Double d;
	// String doa;
	// 01Oct2016 Arpitha
	Dialog referral_dialog;
	EditText etplaceofreferral, etreasonforreferral, etdateofreferral;
	Button btnCancelRef, btnSaveRef;
	boolean isDateRefClicked = false;
	static EditText ettimeofreferral;
	static boolean isTimeRefClicked = false;
	Spinner spnreferralfacility;
	int referralfacility;
	RadioButton rd_motherconscious, rd_motherunconscious, rd_babyalive, rd_babydead, rdepiyes, rdepino, rdcatgut,
			rdvicryl;
	int conditionofmother = 1, conditionofbaby = 1;
	EditText etheatrate, etspo2;
	LinkedHashMap<String, Integer> reasonforreferralMap;
	MultiSelectionSpinner mspnreasonforreferral;
	TextView txtrefmode;
	TextView txtwname, txtwage, txtwdoa, txtwtoa, txtwgest, txtwgravida, txtwrisk;
	EditText etdescriptionofreferral, etbpsystolicreferral, etbpdiastolicreferral, etpulsereferral;
	TableRow trspo2, trheartrate;
	int bpsysval = 0, bpdiaval = 0, pulval = 0;
	int heartrate;
	int spo2;
	String refdate;
	static String strRefDate;
	// 03Oct2016 Arpitha
	WomenReferral_pojo wrefpojo;
	// 04Oct2016 Arpitha
	String reftime;
	ArrayList<WomenReferral_pojo> womenrefpojoItems;
	public static boolean isDelinfoadd = false;// 18Oct2016 Arpitha
	int baby1weight;// 18Oct2016 Arpitha
	int baby2weight;// 18Oct2016 Arpitha
	int duration = 0;// 18Oct2016 Arpitha
	String no_of_child = "";
	int numofchildren;
	int episiotomy = 0, suturematerial = 0;
	RadioButton rdmotheralive, rdmotherdead;
	MultiSelectionSpinner mspndeltypereason;
	MultiSelectionSpinner mspntears;
	LinkedHashMap<String, Integer> tearsMap;
	LinkedHashMap<String, Integer> deltypereasonMap;
	int delivery_type, delivery_result1, delivery_result2;
	int babysex1, babysex2;
	ImageView imgwt1_warning, imgwt2_warning;
	EditText txtdeltypereason, txtdeltypeotherreasons;
	RadioButton rdmale1, rdfemale1, rdmale2, rdfemale2;
	static String strlastentrytime;
	static String strDeldate;
	String strRegdate;
	String delTime;
	static boolean isDelTime1;
	boolean isDeldate;
	boolean isedd = false;// 22oct2016 Arpitha
//	TextView txtheadingref;// 16Nov2016 Arpitha
	int normaltype = 1;// 20NOv2016 Arpitha
	RadioButton rdvertext, rdbreech;// 20NOv2016 Arpitha
	RadioButton rd_memb_pre;
	RadioButton rd_memb_abs;
	RadioGroup rgnormaltype;// 27Feb2017 Arpitha
	LinearLayout llbaby;// 20March2017 Arpitha
	RadioGroup rgheight;// 25April2017 Arpitha

	RadioGroup rgbabypresentation;// 05may2017 Arpitha- v2.6 mantis id 0000231
	RadioButton rd_vertex_pres, rd_breech_pres, rd_others_pres;// 05may2017
																// Arpitha- v2.6
																// mantis id
																// 0000231
	int presentation = 0;// 05may2017 Arpitha- v2.6 mantis id 0000231

	static Context context;// 22Jun2017 Arpitha
	ImageView imgpdf;//13Oct2017 Arpitha

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_newviewprofile);

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			// 05Nov2016 Arpitha
			Locale locale = null;

			if (AppContext.prefs.getBoolean("isEnglish", false)) {
				locale = new Locale("en");
				Locale.setDefault(locale);
			} else if (AppContext.prefs.getBoolean("isHindi", false)) {
				locale = new Locale("hi");
				Locale.setDefault(locale);
			}

			context = RegistrationActivity.this;// 22Jun2017 Arpitha

			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

			AppContext.setNamesAccordingToRes(getResources());// 05Nov2016
																// Arpitha

			// get the action bar
			ActionBar actionBar = getActionBar();

			// Enabling Back navigation on Action Bar icon
			actionBar.setDisplayHomeAsUpEnabled(true);

			aq = new AQuery(this);

			dbh = Partograph_DB.getInstance(getApplicationContext());

			regtabpos = Partograph_CommonClass.curr_tabpos;

			user = dbh.getUserProfile();
			Partograph_CommonClass.user = user;

			initializeScreen(aq.id(R.id.rlregistration1).getView());

			Fragment_DIP.istears = false; // 26Sep2016 Arpitha

			mspnriskoptions = (MultiSelectionSpinner) findViewById(R.id.mspnriskoptions);

			aq.id(R.id.tr_riskoptions).gone();// 26April2017 Arpitha

			// 9jan2016
			mspnadmittedwith = (MultiSelectionSpinner) findViewById(R.id.mspnadmittedwith);
			aq.id(R.id.etadmittedwith).gone();

			// 18Oct2016 Arpitha
			if (Activity_WomenView.regtype == 2) {
				rdmotheralive = (RadioButton) findViewById(R.id.rdmotheralive);
				rdmotherdead = (RadioButton) findViewById(R.id.rdmotherdead);
				mspndeltypereason = (MultiSelectionSpinner) findViewById(R.id.mspndeltypereason);
				mspntears = (MultiSelectionSpinner) findViewById(R.id.mspntears);

				txtdeltypereason = (EditText) findViewById(R.id.txtdeltypereasonval);
				txtdeltypeotherreasons = (EditText) findViewById(R.id.etdeltypeotherreasons);
				imgwt1_warning = (ImageView) findViewById(R.id.warweight1);
				imgwt2_warning = (ImageView) findViewById(R.id.warweight2);

				rdmale1 = (RadioButton) findViewById(R.id.rd_male1);
				rdfemale1 = (RadioButton) findViewById(R.id.rd_female1);
				rdmale2 = (RadioButton) findViewById(R.id.rd_male2);
				rdfemale2 = (RadioButton) findViewById(R.id.rd_female2);
				rdepiyes = (RadioButton) findViewById(R.id.rd_epiyes);
				rdepino = (RadioButton) findViewById(R.id.rd_epino);
				rdcatgut = (RadioButton) findViewById(R.id.rd_catgut);
				rdvicryl = (RadioButton) findViewById(R.id.rd_vicryl);

				rdbreech = (RadioButton) findViewById(R.id.rd_breech);// 20Nov2016
				// Arpitha
				rdvertext = (RadioButton) findViewById(R.id.rd_vertex);// 20Nov2016
				// Arpitha
			} // 18Oct2016 Arpitha

			initialView();

			// Arpitha - change listener for edittext
			aq.id(R.id.etage).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etheight).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etweight).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etpara).getEditText().addTextChangedListener(watcher);// 16july2016
			aq.id(R.id.etgestationage).getEditText().addTextChangedListener(watcher);// 20july2106
			aq.id(R.id.etgestationagedays).getEditText().addTextChangedListener(watcher);// 24july2016
			aq.id(R.id.etgravida).getEditText().addTextChangedListener(watcher);
			aq.id(R.id.etchildwt1).getEditText().addTextChangedListener(watcher);// 19Oct2016
			aq.id(R.id.etchildwt2).getEditText().addTextChangedListener(watcher);// 19Oct2016
																					// Arpitha

			imgphoto = (ImageView) findViewById(R.id.imgphoto);

			// updated on 20july2016 by Arpitha
			chkgest = (CheckBox) findViewById(R.id.chk);

			// 27dec2015
			aq.id(R.id.etlmp).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							v.performClick();// 26April2017 Arpitha
							// updated on 27Feb2016 by arpitha
							islmp = true;
							isDateRefClicked = false;// 04Oct2016 Arpitha
							isDeldate = false;// 19Oct2016 Arpitha
							isedd = false;// 22oct2016 Arpitha
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			// updated on 27Feb2016 by Arpitha
			aq.id(R.id.etedd).getEditText().setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							v.performClick();// 26April2017 Arpitha
							// changed by Arpitha
							islmp = false;
							isDateRefClicked = false;// 04Oct2016 Arpitha
							isDeldate = false;// 19Oct2016 Arpitha
							isedd = true;// 22oct2016 Arpitha
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			rd_highrisk = (RadioButton) findViewById(R.id.rd_hishrisk);
			rd_lowrisk = (RadioButton) findViewById(R.id.rd_lowrisk);
			rd_memb_pre = (RadioButton) findViewById(R.id.rd_mempresent);
			rd_memb_abs = (RadioButton) findViewById(R.id.rd_memabsent);
			rd_feet = (RadioButton) findViewById(R.id.rd_feet);
			rd_cm = (RadioButton) findViewById(R.id.rd_cm);
			// 25Aug2016-bindu
			rd_htdontknow = (RadioButton) findViewById(R.id.rd_htdontknow);

			// 05may2017 Arpitha- v2.6 mantis id 0000231
			rd_vertex_pres = (RadioButton) findViewById(R.id.rd_vertexpresentation);
			rd_breech_pres = (RadioButton) findViewById(R.id.rd_breechpresentation);
			// rd_others_pres = (RadioButton)
			// findViewById(R.id.rd_otherpresentation);
			// 05may2017 Arpitha- v2.6 mantis id 0000231

			risk = 0;

			memb_pre_abs = 0;

			height_unit = 0;

			rd_highrisk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						risk = 1;

						ishighrisk = true;// 3August2016 by Arpitha
						aq.id(R.id.tr_riskoptions).visible();// 26April17
																// ARpitha
						mspnriskoptions.setVisibility(View.VISIBLE);// 24Sep2016-ARpitha
						setRiskOptions();
						aq.id(R.id.etcomments).getEditText().requestFocus();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});

			rd_lowrisk.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					risk = 0;
					aq.id(R.id.tr_riskoptions).gone();// 26April2017 Arpitha
					mspnriskoptions.setSelected(false);// 24julyu2016 by Arpitha
					mspnriskoptions.setVisibility(View.GONE);// 24Sep2016-ARpitha

				}
			});

			rd_memb_pre.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					memb_pre_abs = 1;
				}
			});

			rd_memb_abs.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					memb_pre_abs = 0;
				}
			});

			rd_feet.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					height_unit = 1;
					aq.id(R.id.tr_height).visible();
					aq.id(R.id.tr_heightcm).gone();
					// updated on 24julyu2016 by Arpitha
					aq.id(R.id.etheight).text("");
					// 24Sep2016 -Arpitha
					aq.id(R.id.spnHeightDecimal).getSpinner().setVisibility(View.VISIBLE);
					aq.id(R.id.spnHeightNumeric).getSpinner().setVisibility(View.VISIBLE);
					aq.id(R.id.etHeightDot).getTextView().setVisibility(View.VISIBLE);

					aq.id(R.id.warheight_ft).getImageView().setVisibility(View.INVISIBLE);
					aq.id(R.id.txtheight).getTextView().setVisibility(View.VISIBLE);
					aq.id(R.id.etheight).getEditText().setVisibility(View.GONE);
					aq.id(R.id.warheight).getImageView().setVisibility(View.GONE);

					aq.id(R.id.spnHeightNumeric).setSelection(5);// 07Oct2016
																	// Arpitha
					aq.id(R.id.spnHeightDecimal).setSelection(0);// 07Oct2016
																	// Arpitha

				}
			});
			rd_cm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					height_unit = 2;
					aq.id(R.id.tr_heightcm).visible();
					aq.id(R.id.tr_height).gone();
					// updated on 24julyu2016 by Arpitha
					aq.id(R.id.spnHeightNumeric).setSelection(5);
					aq.id(R.id.spnHeightDecimal).setSelection(0);
					// 24Sep2016 -Arpitha
					aq.id(R.id.spnHeightDecimal).getSpinner().setVisibility(View.GONE);
					aq.id(R.id.spnHeightNumeric).getSpinner().setVisibility(View.GONE);
					aq.id(R.id.etHeightDot).getTextView().setVisibility(View.GONE);

					aq.id(R.id.warheight_ft).getImageView().setVisibility(View.GONE);
					aq.id(R.id.etheight).getEditText().setVisibility(View.VISIBLE);
					aq.id(R.id.warheight).getImageView().setVisibility(View.INVISIBLE);

					aq.id(R.id.etheight).getEditText().requestFocus();// 09Nov2016
																		// Arpitha

				}
			});

			// updated 25Aug2016 - bindu
			rd_htdontknow.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					height_unit = 3;
					aq.id(R.id.tr_heightcm).gone();
					aq.id(R.id.tr_height).gone();
					aq.id(R.id.etheight).text("");
					// 01Sep2016 -Arpitha
					aq.id(R.id.spnHeightDecimal).getSpinner().setVisibility(View.GONE);
					aq.id(R.id.spnHeightNumeric).getSpinner().setVisibility(View.GONE);
					aq.id(R.id.etHeightDot).getTextView().setVisibility(View.GONE);

					aq.id(R.id.warheight_ft).getImageView().setVisibility(View.GONE);
					aq.id(R.id.txtheight).getTextView().setVisibility(View.GONE);
					aq.id(R.id.etheight).getEditText().setVisibility(View.GONE);
					aq.id(R.id.warheight).getImageView().setVisibility(View.GONE);
				}
			});

			imgphoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						selectImage();
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			// 18Oct2016 Arpitha

			if (Activity_WomenView.regtype == 2)// 22oct2016 Arpitha
			{
				aq.id(R.id.etdeldate).getEditText().setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							islmp = false; // 25Jan2016

							// 11Sep2016 - bindu
							// islmpdate = true;
							try {
								v.performClick();// 26April2017 Arpitha
								isDeldate = true;// 19Oct2016 Arpitha
								isDateRefClicked = false;// 22oct2016 Arpitha
								isedd = false;// 22oct2016 Arpitha
								caldatepicker();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						return true;
					}
				});

				aq.id(R.id.etdeltime).getEditText().setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							v.performClick();// 26April2017 Arpitha
							isDelTime1 = true;
							delTime = aq.id(R.id.etdeltime).getText().toString();

							if (delTime != null) {
								isTimeRefClicked = false;// 19May2017 Arpitha -
															// 2.6
								Calendar calendar = Calendar.getInstance();
								Date d = null;
								d = dbh.getTime(delTime);
								calendar.setTime(d);
								hour = calendar.get(Calendar.HOUR_OF_DAY);
								minute = calendar.get(Calendar.MINUTE);
							}
							// showDialog(TIME_DIALOG_ID);
							showtimepicker();
						}
						return true;
					}
				});

				aq.id(R.id.etdeltime2).getEditText().setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (MotionEvent.ACTION_UP == event.getAction()) {
							v.performClick();// 26April2017 Arpitha
							isDelTime1 = false;
							delTime = aq.id(R.id.etdeltime2).getText().toString();

							if (delTime != null) {
								isTimeRefClicked = false;// 19May2017 Arpitha -
															// v2.6
								Calendar calendar = Calendar.getInstance();
								Date d = null;
								d = dbh.getTime(delTime);
								calendar.setTime(d);
								hour = calendar.get(Calendar.HOUR_OF_DAY);
								minute = calendar.get(Calendar.MINUTE);
							}
							// showDialog(TIME_DIALOG_ID);
							showtimepicker();// 26April2017 Arpitha
						}
						return true;
					}
				});

				rdmotheralive.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						aq.id(R.id.txtmothersdeathreason).gone();
						aq.id(R.id.etmothersdeathreason).gone();

						// Bindu - Aug082016
						aq.id(R.id.etmothersdeathreason).text("");
					}
				});

				rdmotherdead.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						aq.id(R.id.txtmothersdeathreason).visible();
						aq.id(R.id.etmothersdeathreason).visible();

						aq.id(R.id.etmothersdeathreason).getEditText().requestFocus();
					}
				});

				// 05jan2016
				rdepiyes.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// trsuturematerial.setVisibility(View.VISIBLE);
						aq.id(R.id.tr_suturematerial).visible();// 26April2017
																// Arpitha
						episiotomy = 1;

						rdcatgut.setVisibility(View.VISIBLE);// 24Sep2016
																// -Arpitha
						rdvicryl.setVisibility(View.VISIBLE);// 24Sept2016
																// -Arpitha
					}
				});

				rdepino.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// trsuturematerial.setVisibility(View.GONE);
						aq.id(R.id.tr_suturematerial).gone();// 26April2017
																// Arpitha
						episiotomy = 0;

						rdcatgut.setVisibility(View.GONE);// 24Sep2016 -Arpitha
						rdvicryl.setVisibility(View.GONE);// 24Sept2016 -Arpitha

					}
				});

				rdcatgut.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						suturematerial = 1;
					}
				});

				rdvicryl.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						suturematerial = 2;
					}
				});

				// 29Sep2016 ARpitha
				mspndeltypereason.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						// v.performClick();// 26April2017 arpitha
						Fragment_DIP.istears = false;// 29Sep2016 ARpitha
						return false;
					}
				});
				// 29Sep2016 ARpitha
				mspntears.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						// v.performClick();// 26April207 Arpitha
						Fragment_DIP.istears = true;// 29Sep2016 ARpitha
						return false;
					}
				});// 18Oct2016 Arpitha

				// 20Nov2016 Arpitha
				rdbreech.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (rd_vertex_pres.isChecked())
							try {
								displayAlertDialog1(
										getResources().getString(R.string.you_have_selected) + " "
												+ getResources().getString(R.string.breech) + " "
												+ getResources().getString(R.string.during_reg) + " "
												+ getResources().getString(R.string.are_you_sure_want_to_change),
										"breech");
							} catch (NotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						/*
						 * Toast.makeText(getApplicationContext(),
						 * getResources().getString(R.string.you_have_selected)
						 * + " " + getResources().getString(R.string.vertex) +
						 * " " + getResources().getString(R.string.
						 * in_woman_basic_details), Toast.LENGTH_LONG).show();//
						 * 12May2017 // Arpitha
						 *
						 */ else
							normaltype = 2;

					}
				});

				rdvertext.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (rd_breech_pres.isChecked())
							try {
								displayAlertDialog1(
										getResources().getString(R.string.you_have_selected) + " "
												+ getResources().getString(R.string.vertex) + " "
												+ getResources().getString(R.string.during_reg) + " "
												+ getResources().getString(R.string.are_you_sure_want_to_change),
										"vertex");
							} catch (NotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						else
							normaltype = 1;

					}
				});// 20Nov2016 Arpitha
			} // 22oct2016 Arpitha

			Fragment_DIP.istears = false;// 06Nov2016 Arpitha

			values = new ArrayList<String>();
			values = Partograph_CommonClass.getphonenumber(option);

			// updated on 19August2016 by Arpitha
			strlastentrytime = dbh.getlastentrytime(womenId, 0);// 18Oct2016
																// Arpitha

			setInputFiltersForEdittext();

			rgnormaltype = (RadioGroup) findViewById(R.id.rdnormaltype);// 27Feb2017

			rgheight = (RadioGroup) findViewById(R.id.rdheight);// 25April2017
																// Arpitha

			// 05may2017 Arpitha- v2.6 mantis id 0000231
			rgbabypresentation = (RadioGroup) findViewById(R.id.rdbabypresentation);
			rd_vertex_pres.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					presentation = 1;

				}
			});

			rd_breech_pres.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					presentation = 2;

				}
			});// 05may2017 Arpitha- v2.6 mantis id 0000231

			aq.id(R.id.etgestationage).getEditText().setOnFocusChangeListener(this);// 12Oct2017
																					// Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

		// KillAllActivitesAndGoToLogin.activity_stack.add(this);
		KillAllActivitesAndGoToLogin.addToStack(this);

	}

	// Set options for risk options if high risk - 25dec2015
	protected void setRiskOptions() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// int i = 0;
		// updated on 04/07/2016 by Arpitha
		int i = 0;
		riskoptionsMap = new LinkedHashMap<String, Integer>();
		// 06jul2016 - assign diff array to get the position.
		List<String> reasonStrArrValues = null;
		reasonStrArrValues = Arrays.asList(getResources().getStringArray(R.array.riskoptionsvalues));

		// to get the values(pos) for the options
		if (reasonStrArrValues != null) {
			for (String str : reasonStrArrValues) {
				riskoptionsMap.put(str, i);
				i++;
			}
		}

		// 05May2017 Arpitha - v2.6
		if (aq.id(R.id.etgravida).getText().toString().trim().length() > 0
				&& Integer.parseInt(aq.id(R.id.etgravida).getText().toString()) > 1)
			// to display the options
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.riskoptions));
		else// 05May2017 Arpitha - v2.6
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.riskoptionsnotprimi));

		if (reasonStrArr != null) {
			mspnriskoptions.setItems(reasonStrArr);
		}

	}

	// change made on 01Apr2015
	// setInput Filters
	private void setInputFiltersForEdittext() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		// 08Sep2016 - bindu - set max char length
		aq.id(R.id.etwname).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etcomments).getEditText()
				.setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(400) });
		aq.id(R.id.etattendant).getEditText()
				.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etdocname).getEditText().setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etnursename).getEditText()
				.setFilters(new InputFilter[] { filter, new InputFilter.LengthFilter(45) });
		// 10April2017 Arpitha
		aq.id(R.id.etother).getEditText().setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(45) });
		aq.id(R.id.etspecialinstructions).getEditText()
				.setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(200) });
		aq.id(R.id.etextracoments).getEditText()
				.setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(200) });
		aq.id(R.id.etaddress).getEditText().setFilters(new InputFilter[] { filter1, new InputFilter.LengthFilter(60) });
	}

	// Initial view
	private void initialView() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		todaysDate = Partograph_CommonClass.getTodaysDate();

		displaydateformat = Partograph_CommonClass.getConvertedDateFormat(todaysDate,
				Partograph_CommonClass.defdateformat);
		// doa = todaysDate;// 10nOv2016 Arpitha
		aq.id(R.id.etdoa).text(displaydateformat);

		aq.id(R.id.ettoa).text(Partograph_CommonClass.getCurrentTime());

		final Calendar c = Calendar.getInstance();
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);

		aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.gamboge));// 26April2017
																								// Arpitha
																								// -
																								// changed
																								// getcolor(int)
																								// to
																								// R.color.color
		aq.id(R.id.scradditionalinfo).gone();
		aq.id(R.id.scrdeliverystatus).gone();

		aq.id(R.id.btndelstatus).enabled(false);
		aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.lightgray));// 26April2017
																								// Arpitha
																								// -
																								// changed
																								// getcolor(int)
																								// to
																								// R.color.color

		aq.id(R.id.etdoa).enabled(false);
		aq.id(R.id.ettoa).enabled(false);

		aq.id(R.id.etstate).text(user.getState());
		aq.id(R.id.etdistrict).text(user.getDistrict());
		aq.id(R.id.ettaluk).text(user.getTaluk());
		aq.id(R.id.etfacility).text(user.getFacility());
		aq.id(R.id.etfacilityname).text(user.getFacility_name());

		aq.id(R.id.etstate).enabled(false);
		aq.id(R.id.etdistrict).enabled(false);
		aq.id(R.id.ettaluk).enabled(false);
		aq.id(R.id.etfacility).enabled(false);
		aq.id(R.id.etfacilityname).enabled(false);

		// 09-01-2015
		getMonthYear(todaysDate);

		// 26-01-2015
		aq.id(R.id.chkdelinfo).gone();
		aq.id(R.id.txtretrospective).gone();// 01nov2016 Arpitha

		aq.id(R.id.etriskoptions).gone();

		// 09jan2016
		setAdmittedWith();
		// 24Sep2016 -Arpitha
		aq.id(R.id.spnHeightDecimal).getSpinner().setVisibility(View.GONE);
		aq.id(R.id.spnHeightNumeric).getSpinner().setVisibility(View.GONE);
		aq.id(R.id.etHeightDot).getTextView().setVisibility(View.GONE);

		aq.id(R.id.warheight_ft).getImageView().setVisibility(View.GONE);
		aq.id(R.id.etheight).getEditText().setVisibility(View.GONE);
		aq.id(R.id.warheight).getImageView().setVisibility(View.GONE);
		mspnriskoptions.setVisibility(View.GONE);

		aq.id(R.id.tr_heightcm).gone();
		aq.id(R.id.tr_height).gone();

		aq.id(R.id.warage).invisible();
		aq.id(R.id.imgweight).invisible();

		// updated on 25july2016 by Arpitha
		risk_observed = new ArrayList<String>();

		// 19OCt2016 Arpitha
		if (Activity_WomenView.regtype == 2) {
			aq.id(R.id.etdeltime).text(Partograph_CommonClass.getCurrentTime());
			delTime = aq.id(R.id.etdeltime).getText().toString();

			strDeldate = todaysDate;
			if (delTime != null) {
				Calendar calendar = Calendar.getInstance();
				Date d = null;
				d = dbh.getTime(delTime);
				calendar.setTime(d);
				hour = calendar.get(Calendar.HOUR_OF_DAY);
				minute = calendar.get(Calendar.MINUTE);
			}
			setTears();
			mspndeltypereason.setVisibility(View.GONE);
			aq.id(R.id.etdeltypeotherreasons).getEditText().setVisibility(View.GONE);
			aq.id(R.id.spndelresult2).getSpinner().setVisibility(View.GONE);
			aq.id(R.id.etchildwt2).getEditText().setVisibility(View.GONE);
			imgwt2_warning.setVisibility(View.GONE);
			rdfemale2.setVisibility(View.GONE);
			rdmale2.setVisibility(View.GONE);
			aq.id(R.id.etdeltime2).getEditText().setVisibility(View.GONE);
			aq.id(R.id.delothrrason).getImageView().setVisibility(View.GONE);

			aq.id(R.id.tr_deltypereason).gone();// 26April2017 Arpitha
			mspndeltypereason.setVisibility(View.GONE);
			aq.id(R.id.tr_deltypereasonc).gone();// 26April2017 Arpitha

			txtdeltypereason.setVisibility(View.GONE);

			aq.id(R.id.tr_deltypeotherreasons).gone();// 26April2017 Arpitha

			txtdeltypeotherreasons.setVisibility(View.GONE);

			aq.id(R.id.delothrrason).gone();
			aq.id(R.id.wardaltypereason).gone();
			aq.id(R.id.wardeltype).gone();
			aq.id(R.id.delothrrason).gone();
			aq.id(R.id.warweight1).gone();
			aq.id(R.id.ettears).gone();
			aq.id(R.id.ettears).gone();

			aq.id(R.id.tr_suturematerial).gone();// 26April2017 Arpitha

			rdcatgut.setVisibility(View.GONE);
			rdvicryl.setVisibility(View.GONE);
			aq.id(R.id.txtmothersdeathreason).gone();
			aq.id(R.id.etmothersdeathreason).gone();

			aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(
					Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
			aq.id(R.id.etdeltime2).text(Partograph_CommonClass.getCurrentTime());
			rdepino.setChecked(true);

			// 21Nov2016 Arpitha
			aq.id(R.id.tr_riskoptions).gone();// 26April2017 Arpitha

			aq.id(R.id.tr_condwhileadm).gone();
			mspnriskoptions.setVisibility(View.GONE);
			aq.id(R.id.rdrisk).gone();
			aq.id(R.id.etriskoptions).gone();
			aq.id(R.id.tr_comments).gone();
			aq.id(R.id.etcomments).gone();
			aq.id(R.id.etother).gone();
			aq.id(R.id.tr_other).gone();
			aq.id(R.id.etadmittedwith).gone();
			aq.id(R.id.tr_admittedwith).gone();
			aq.id(R.id.tr_doa).gone();
			aq.id(R.id.tr_timeofadm).gone();
			aq.id(R.id.txt_heightin).invisible();
			aq.id(R.id.ettoa).gone();
			aq.id(R.id.etdoa).gone();
			aq.id(R.id.imgadmittedwith).gone();
			aq.id(R.id.imgadmittedwithother).gone();
			aq.id(R.id.imgconditionwhileadmission).gone();
			aq.id(R.id.imgriskoptions).gone();
			aq.id(R.id.imgotherrisk).gone();
			mspnadmittedwith.setVisibility(View.GONE);
			aq.id(R.id.tr_retrospectivereason).visible();
			aq.id(R.id.txtretrospectivereason).visible();

			aq.id(R.id.ettxtretroreason).visible();
			aq.id(R.id.txtretroreason).visible();// 26Nov2016 Arpitha
			aq.id(R.id.imgreason).visible();

			if (Activity_WomenView.retroreason == 4) {
				aq.id(R.id.tr_retrospectivereasonother).visible();
				aq.id(R.id.ettxtretroreasonother).visible();
				aq.id(R.id.imgotherreason).visible();
				aq.id(R.id.ettxtretroreasonother).text(Activity_WomenView.regtypereason);

				aq.id(R.id.txtretroreasonother).visible();// 26Nov2016 Arpitha
				aq.id(R.id.txtmandretreason).visible();// 15May2017 Arpitha -
														// v2.6

			}
			String[] retroreason = getResources().getStringArray(R.array.retrospective_reason);
			aq.id(R.id.ettxtretroreason).text(retroreason[Activity_WomenView.retroreason]);
			// 21Nov2016 Arpitha

			// if (Activity_WomenView.regtype == 2) {
			aq.id(R.id.chkdelinfo).visible();
			aq.id(R.id.chkdelinfo).enabled(true);
			aq.id(R.id.txtretrospective).visible();// 01nov2016 Arpitha
			aq.id(R.id.txtretrospective).text(getResources().getString(R.string.retrospective_case));// 02nov2016
			// Arpitha

		}
		// 10Nov2016 Arpitha
		aq.id(R.id.etdoe).enabled(false);
		aq.id(R.id.etdoe).text(displaydateformat + "/" + Partograph_CommonClass.getCurrentTime());

		ReferralInfo_Activity.isdip = false;// 14Nov2016 Arpitha

	}

	// Get the month , year
	private void getMonthYear(String todaysDate) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		Date today = Partograph_CommonClass.getDateS(todaysDate);
		day = Integer.parseInt(new SimpleDateFormat("dd", java.util.Locale.getDefault()).format(today));
		mon = Integer.parseInt(new SimpleDateFormat("M", java.util.Locale.getDefault()).format(today));
		year = Integer.parseInt(new SimpleDateFormat("yyyy", java.util.Locale.getDefault()).format(today));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_womenlist, menu);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				// NavUtils.navigateUpFromSameTask(this);

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.main));

				return true;
			case R.id.home:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.main));
				return true;

			case R.id.logout:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.login));
				return true;
			// 10May2017 Arpitha - v2.6
			case R.id.info:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.info));

				return true;

			case R.id.about:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.about));

				return true;

			case R.id.settings:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.settings));

				return true;

			case R.id.sms:
				Partograph_CommonClass.display_messagedialog(RegistrationActivity.this, user.getUserId());
				return true;

			case R.id.comments:
				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.comments));
				return true;

			case R.id.summary:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.summary));

				return true;

			case R.id.changepass:
				Partograph_CommonClass.dispalyChangePasswordDialog(RegistrationActivity.this);
				return true;

			case R.id.usermanual:

				displayConfirmationAlert(getResources().getString(R.string.exit_msg),
						getResources().getString(R.string.user_manual));
				return true;// 10May2017 Arpitha - v2.6

			case R.id.disch:
				Intent disch = new Intent(RegistrationActivity.this, DischargedWomanList_Activity.class);
				startActivity(disch);

			}
		} catch (NotFoundException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * This is a Recursive method that traverse through the group and subgroup
	 * of view Sets text, assigns clickListners
	 * 
	 * @throws Exception
	 */
	private ArrayList<View> initializeScreen(View v) throws Exception {
		if (!(v instanceof ViewGroup)) {
			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);

			// Imageview - assign ID
			if (v.getClass() == ImageView.class || v.getClass() == ImageButton.class) {
				// aq.id(v.getId()).getImageView().setOnClickListener(this);
			}

			// Edittext
			if (v.getClass() == EditText.class) {
				aq.id(v.getId()).getEditText().setOnClickListener(this);
			}

			// Button
			if (v.getClass() == Button.class) {
				aq.id(v.getId()).getButton().setOnClickListener(this);
			}

			// 19Oct2016 Arpitha
			if (v.getClass() == CheckBox.class) {
				aq.id(v.getId()).getCheckBox().setOnClickListener(this);
			} // 19Oct2016 Arpitha

			return viewArrayList;
		}
		if (v instanceof Spinner) {
			aq.id(v.getId()).itemSelected(this, "onSpinnerClicked");
		}

		if (v.getClass() == TableRow.class) {
			// aq.id(v.getId()).gett
		}

		ArrayList<View> result = new ArrayList<View>();

		ViewGroup viewGroup = (ViewGroup) v;
		for (int i = 0; i < viewGroup.getChildCount(); i++) {

			View child = viewGroup.getChildAt(i);

			ArrayList<View> viewArrayList = new ArrayList<View>();
			viewArrayList.add(v);
			viewArrayList.addAll(initializeScreen(child));

			result.addAll(viewArrayList);
		}
		return result;
	}

	@Override
	public void onClick(View v) {

		try {
			switch (v.getId()) {

			case R.id.btnwomendetails:
				aq.id(R.id.scradditionalinfo).gone();
				aq.id(R.id.scrdeliverystatus).gone();
				aq.id(R.id.scrwomendetails).visible();

				aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.gamboge));
				aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.brown));
				// 18Oct2016 Arpitha
				if (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked()) {
					aq.id(R.id.btndelstatus).enabled(true);
					aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);
				} // 18Oct2016 Arpitha

				break;
			case R.id.btnother:
				try {
					aq.id(R.id.scradditionalinfo).visible();
					aq.id(R.id.scrdeliverystatus).gone();
					aq.id(R.id.scrwomendetails).gone();
					aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.gamboge));

					// 18Oct2016 Arpitha
					if (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked()) {
						aq.id(R.id.btndelstatus).enabled(true);
						aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);
					} // 18Oct2016 Arpitha
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

				break;

			// 18Oct2016 Arpitha
			case R.id.btndelstatus:
				try {
					ReferralInfo_Activity.isdip = false;
					// Fragment_DIP.istears = true;// 28Sep2016 Arpitha
					aq.id(R.id.scradditionalinfo).gone();
					aq.id(R.id.scrdeliverystatus).visible();
					aq.id(R.id.scrwomendetails).gone();

					aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.brown));
					aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.gamboge));
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
				break;// 18Oct2016 Arpitha

			// 18Oct2016 Arpitha
			case R.id.chkdelinfo:
				if (((CheckBox) v).isChecked()) {
					isDelinfoadd = true;
					aq.id(R.id.btndelstatus).enabled(true);
					aq.id(R.id.btndelstatus).background(R.drawable.btn_selector);
				} else {
					isDelinfoadd = false;
					aq.id(R.id.btndelstatus).enabled(false);
					aq.id(R.id.btndelstatus).backgroundColor(getResources().getColor(R.color.lightgray));
					aq.id(R.id.scrdeliverystatus).gone();// 11May2017 Arpitha -
															// v2.6
					aq.id(R.id.scrwomendetails).visible();// 11May2017 Arpitha -
															// v2.6
					aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.gamboge));// 11May2017
																											// Arpitha
																											// -
																											// v2.6
				}

				break;// 18Oct2016 Arpitha

			case R.id.btnregister:
				try {
					saveRegDetails();
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}

				break;

			case R.id.btnclear:
				displayConfirmationAlert(getResources().getString(R.string.clear_msg),
						getResources().getString(R.string.clear));// 07Oct2016
																	// Arpitha -
																	// strng val
																	// from
																	// strings.xml

				break;

			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Save reg details
	private void saveRegDetails() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (!(no_of_child.equals("") || (no_of_child == null)))
			numofchildren = Integer.parseInt(no_of_child);

		if (validateRegFields()) {

			age = 0;
			height = 0;
			d = 0.0;
			weight = 0;
			baby1weight = 0;// 18Oct2016 Arpitha
			baby2weight = 0;// 18Oct2016 Arpitha

			if (aq.id(R.id.etage).getText().length() > 0) {
				age = Integer.parseInt(aq.id(R.id.etage).getText().toString());
			}
			if (aq.id(R.id.etheight).getText().length() > 0 && rd_cm.isChecked()) {
				height = Double.parseDouble(aq.id(R.id.etheight).getText().toString());
			}
			if (aq.id(R.id.etweight).getText().length() > 0) {
				weight = Double.parseDouble(aq.id(R.id.etweight).getText().toString());
			}
			int height_numeric = Integer.parseInt((String) aq.id(R.id.spnHeightNumeric).getSelectedItem());
			int height_decimal = Integer.parseInt((String) aq.id(R.id.spnHeightDecimal).getSelectedItem());
			feet_height = height_numeric + "." + height_decimal;
			if (height_decimal < 10) {
				height_feet = "" + height_numeric + ".0" + height_decimal;
			} else {
				height_feet = "" + height_numeric + "." + height_decimal;
			}
			d = Double.parseDouble(height_feet);

			// 18Oct2016 Arpitha
			if (isDelinfoadd && Activity_WomenView.regtype == 2 && aq.id(R.id.chkdelinfo).isChecked()) {
				if (aq.id(R.id.etchildwt1).getText().toString().trim().length() > 0) {
					baby1weight = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());
				}
				if (Integer.parseInt(no_of_child) == 2) {
					baby2weight = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());
				}

				String del_datetime = strDeldate + "_" + aq.id(R.id.etdeltime).getText().toString();

				if (strlastentrytime != null) {
					strlastentrytime = strlastentrytime.replace(" ", "_");
					duration = Partograph_CommonClass.getHoursBetDates(del_datetime, strlastentrytime);
				}

			}
			if (isDelinfoadd && Activity_WomenView.regtype == 2 && aq.id(R.id.chkdelinfo).isChecked()) {

				if ((age > 0 && (age < 18 || age > 40)) || (height > 0 && (height > 176 || height < 146))
						|| (weight > 0 && (weight <= 35 || weight > 80)) || (d > 0 && (d > 5.08 || d < 4.08))
						|| (baby1weight > 0 && (baby1weight < 2500 || baby1weight > 4000))
						|| (baby2weight > 0 && (baby2weight < 2500 || baby2weight > 4000))) {
					displayConfirmationAlert("", getResources().getString(R.string.reg));
				} else
					savedata();

			} else {
				// 18Oct2016 Arpitha

				if ((age > 0 && (age < 18 || age > 40)) || (height > 0 && (height > 176 || height < 146))
						|| (weight > 0 && (weight <= 35 || weight > 80)) || (d > 0 && (d > 5.08 || d < 4.08))) {
					displayConfirmationAlert("", getResources().getString(R.string.reg));
				} else
					savedata();
			}

		}

	}

	// Sync
	private void calSyncMtd() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		SyncFunctions.syncUptoDate = false;
		Partograph_CommonClass.responseAckCount = 0;
		Partograph_CommonClass.AsyncCallWS task = new Partograph_CommonClass.AsyncCallWS();
		task.execute();
	}

	/** Method to rollback the trnsaction */
	private void rollbackTrans() throws Exception {
		dbh.db.endTransaction();
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.regfailed), Toast.LENGTH_SHORT)
				.show();
	}

	/** Method to commit the trnsaction */
	private void commitTrans(String classname) throws Exception {

		if (classname.equalsIgnoreCase(getResources().getString(R.string.referral))) {
			Fragment_DIP.isreferral_updated = true;// 22oct2016 Arpitha
			isreg_activity = false;// 22oct2016 Arpitha
			Fragment_DIP.isdelstatus = false;// 22oct2016 Arpitha
			Intent i = new Intent(RegistrationActivity.this, Activity_WomenView.class);
			startActivity(i);

			dbh.db.setTransactionSuccessful();
			dbh.db.endTransaction();
		} else {
			Fragment_DIP.isreferral_updated = false;// 22oct2016 Arpitha
			if (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked()) {
				Fragment_DIP.isdelstatus = true;
			} else {
				if (Activity_WomenView.regtype != 2) {
					isreg_activity = true;// 22oct2016 Arpitha
				} else {
					Activity_WomenView.isretrospective = true;
					// 24oct2016
					// Arpitha
				}
			}

			dbh.db.setTransactionSuccessful();
			dbh.db.endTransaction();

			Toast.makeText(getApplicationContext(), getResources().getString(R.string.regsuccess), Toast.LENGTH_LONG)
					.show();

			calSyncMtd();

			if (Activity_WomenView.regtype != 2) {
				if (AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY) {
					displayConfirmationAlert(getResources().getString(R.string.confirm_sms),
							getResources().getString(R.string.sms));
				} else {

					int options = mspnriskoptions.getSelectedStrings().size();
					String risks = mspnriskoptions.getSelectedItemsAsString();
					if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
							&& (options > 0
									&& ((risks.contains(getResources().getString(R.string.other_risk_option)) && options>1) || ishighrisk) ))
							&& Activity_WomenView.regtype != 2)// 22oct2016
					// Arpitha

					
					/*if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
							&& (ishighrisk && options > 1
									))
							&& Activity_WomenView.regtype != 2)// 22oct2016
					// Arpitha
*/					
					{
						displayAlertDialog(
								getResources().getString(R.string.risk_high_would_you_like_to_refer)
										+ mspnriskoptions.getSelectedStrings()
										+ getResources().getString(R.string.would_you_like_to_refer),
								getResources().getString(R.string.isrefer));
					} else {
						Intent viewWomenList = new Intent(RegistrationActivity.this, Activity_WomenView.class);
						startActivity(viewWomenList);
						finish();

					}

				}
			} else {
				Intent viewWomenList = new Intent(RegistrationActivity.this, Activity_WomenView.class);
				startActivity(viewWomenList);
				finish();
			}

		}
	}

	// Options to set Image
	private void selectImage() throws Exception {
		final CharSequence[] options = { getResources().getString(R.string.take_photo),
				getResources().getString(R.string.remove_photo) };

		AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);

		builder.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.profile_photo) + "</font>"));
		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals(getResources().getString(R.string.take_photo))) {
					CapturingImage();
					dialog.dismiss();
				} else if (options[item].equals(getResources().getString(R.string.remove_photo))) {
					aq.id(R.id.imgphoto).getImageView().setImageResource(R.drawable.women);
					bitmap = null;
					dialog.dismiss();
				}
			}
		});

		Dialog d = builder.show();
		int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = d.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		d.show();
	}

	/** This method invokes camera intent */
	@SuppressLint("SimpleDateFormat")
	protected void CapturingImage() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		try {
			// intent to start device camera
			Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
			Date xDate = new Date();
			String lasmod = new SimpleDateFormat("dd-hh-mm-ss").format(xDate);
			String fileName = "profile_img" + lasmod;
			File imageDir = new File(AppContext.imgDirRef);
			if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
				if (imageDir != null) {
					if (!imageDir.mkdirs()) {
						if (!imageDir.exists()) {
							Log.d("CameraSample", "failed to create directory");
						} else {
							Log.d("CameraSample", "created directory");
						}
					}
				}
			} else {
				Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
			}
			path = imageDir + "/" + fileName + ".jpg";
			imgfileBeforeResult = new File(path);
			outputFileUri = Uri.fromFile(imgfileBeforeResult);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
			startActivityForResult(intent, IMAGE_CAPTURE);
		} catch (NullPointerException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** This method get the result from Camera Intent, gets the image */
	@SuppressLint("SimpleDateFormat")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if (requestCode == IMAGE_CAPTURE) {
				if (resultCode == RESULT_OK) {
					baos = new ByteArrayOutputStream();

					bitmap = BitmapFactory.decodeFile(path);
					Bitmap resizedBmp = Bitmap.createScaledBitmap(bitmap, 230, 210, true);
					resizedBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
					aq.id(R.id.imgphoto).getImageView().setImageBitmap(resizedBmp);

				} else if (resultCode == RESULT_CANCELED) {
					deleteImages();
				}
			}
		} catch (NullPointerException e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	/** Delete the temporarily stored Images */
	private void deleteImages() throws Exception {
		if (imgfileBeforeResult != null)
			imgfileBeforeResult.delete();
	}

	// Validate mandatory fields
	private boolean validateRegFields() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		aq.id(R.id.btnwomendetails).visible();
		aq.id(R.id.scradditionalinfo).gone();
		aq.id(R.id.scrdeliverystatus).gone();
		aq.id(R.id.scrwomendetails).visible();

		aq.id(R.id.btnwomendetails).backgroundColor(getResources().getColor(R.color.gamboge));
		aq.id(R.id.btnother).backgroundColor(getResources().getColor(R.color.brown));

		if (aq.id(R.id.etwname).getEditText().getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.pls_ent_wname), context);
			aq.id(R.id.etwname).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etage).getText().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.pls_ent_age), context);
			aq.id(R.id.etage).getEditText().requestFocus();

			return false;
		}
		// changed on 1/7/16 by Arpitha
		if (Integer.parseInt(aq.id(R.id.etage).getText().toString()) == 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.invalid_age), context);// 07Oct2016
			// Arpitha
			// -
			// string
			// value
			// from
			// strings.xml
			// updated on 3August2016 by Arpitha
			aq.id(R.id.etage).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etgestationage).getText().toString().trim().length() <= 0 && (!chkgest.isChecked())
				&& aq.id(R.id.etgestationagedays).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.pls_ent_gestationage), context);
			aq.id(R.id.etgestationage).getEditText().requestFocus();
			return false;
		}

		if ((aq.id(R.id.etgestationage).getText().toString().length()) > 0) {
			if (Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) < 27
					|| Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) > 44) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.gestationvalid), context);
				aq.id(R.id.etgestationage).getEditText().requestFocus();
				return false;
			}
		}

		if (aq.id(R.id.etgestationagedays).getText().length() > 0) {
			if (Integer.parseInt(aq.id(R.id.etgestationagedays).getText().toString()) > 6) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.gestationvaliddays),
						context);
				aq.id(R.id.etgestationagedays).getEditText().requestFocus();
				return false;
			}

		}

		// updated on 16july2016 by Arpitha
		if (aq.id(R.id.etgravida).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.entr_gravida), context);
			aq.id(R.id.etgravida).getEditText().requestFocus();
			return false;
		}

		// updated on 16july2016 by Arpitha
		if (aq.id(R.id.etpara).getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_para), context);
			aq.id(R.id.etpara).getEditText().requestFocus();
			return false;
		}

		// updated on 24july2016 by Arpitha
		if (Integer.parseInt(aq.id(R.id.etgravida).getText().toString()) == 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.gravida_invalid), context);
			aq.id(R.id.etgravida).getEditText().requestFocus();
			return false;
		}

		// updated on 16jul2016 by Arpitha
		if (aq.id(R.id.spnbloodgroup).getSelectedItemPosition() == 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.select_bloodgroup), context);
			aq.id(R.id.etweight).getEditText().requestFocus();
			return false;
		}

		// updated on 2/3/16 by Arpitha

		if (aq.id(R.id.etthayicardno).getText().toString().length() > 0) {
			if (aq.id(R.id.etthayicardno).getText().toString().length() < 7) {
				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.thayicardno_mustcontain_sevendigits), context);
				aq.id(R.id.etthayicardno).getEditText().requestFocus();
				return false;
			}
			// updated on 13july2016 by Arpitha
			if (Long.parseLong((aq.id(R.id.etthayicardno).getText().toString())) == 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.invalid_thayicard),
						context);
				aq.id(R.id.etthayicardno).getEditText().requestFocus();
				return false;
			}
		}

		if (rd_highrisk.isChecked()) {
			if (mspnriskoptions.getSelectedIndicies().size() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.high_risk_val), context);
				aq.id(R.id.etcomments).getEditText().requestFocus();
				return false;

			}
		}

		// changed on 21Nov2016 Arpitha - Activity_WomenView.regtype==2
		if (mspnriskoptions != null && Activity_WomenView.regtype != 2) {

			if ((mspnriskoptions.getSelectedStrings() != null)) {
				if ((mspnriskoptions.getSelectedStrings().size() > 0)) {
					List<String> pos = mspnriskoptions.getSelectedStrings();

					if (pos.contains(getResources().getString(R.string.otherrisk))
							&& aq.id(R.id.etcomments).getText().toString().trim().length() <= 0) {
						Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.other_option_val),
								context);
						aq.id(R.id.etcomments).getEditText().requestFocus();
						return false;
					}
				}
			}
		}

		// updated on 3August2016 by Arpitha
		if (aq.id(R.id.etgestationagedays).getText().length() > 0
				&& aq.id(R.id.etgestationage).getText().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.gestationvalid), context);
			aq.id(R.id.etgestationage).getEditText().requestFocus();
			return false;
		}

		// updated 25Aug2016 - bindu - height mandatory
		// changed on 21Nv2016 Arpitha
		if (height_unit == 0 && Activity_WomenView.regtype != 2) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.plsenterheight), context);
			aq.id(R.id.etpara).getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etheight).getText().length() <= 0 && rd_cm.isChecked()
				|| (rd_cm.isChecked() && aq.id(R.id.etheight).getText().length() > 0
						&& Double.parseDouble(aq.id(R.id.etheight).getText().toString()) == 0)) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.plsenterheight), context);
			aq.id(R.id.etheight).getEditText().requestFocus();
			return false;
		}
		// 08Oct2017 Arpitha
		if (rd_feet.isChecked() && aq.id(R.id.spnHeightNumeric).getSelectedItemPosition() == 0
				&& aq.id(R.id.spnHeightDecimal).getSelectedItemPosition() == 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.plsenterheight), context);
			return false;
		} // 08Oct2017 Arpitha

		// 18Oct2016 Arpitha
		if (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked()) {

			if (aq.id(R.id.etchildwt1).getText().length() < 4) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.child_wt), context);
				aq.id(R.id.etchildwt1).getEditText().requestFocus();
				return false;
			}

			// updated on 6/6/16 by Arpitha
			if (Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString()) == 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.child_wt_val), context);
				aq.id(R.id.etchildwt1).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etdeldate).getText().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.ent_deldate), context);
				aq.id(R.id.etdeldate).getEditText().requestFocus();
				return false;
			}

			if (aq.id(R.id.etdeltime).getText().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.ent_deltime), context);
				aq.id(R.id.etdeltime).getEditText().requestFocus();
				return false;
			}

			// 21Nov2016 Arpitha
			int days;
			if (strlmpdate != null) {
				days = Partograph_CommonClass.getDaysBetweenDates(strDeldate, strlmpdate);
				if (days < 196) {
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.deldate_cannot_be_before_sevenmonths_from_lmp), context);
					return false;
				} else if (days > 301) {
					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.deldate_cannot_be_after_ninemonths_from_lmp), context);
					return false;
				}
			} // 21Nov2016 Arpitha

			if (numofchildren == 2) {
				if (aq.id(R.id.etchildwt2).getText().length() < 4) {
					Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.child_wt), context);
					aq.id(R.id.etchildwt2).getEditText().requestFocus();
					return false;
				}

			}

			if (aq.id(R.id.rdmotherdead).isChecked()) {
				if (aq.id(R.id.etmothersdeathreason).getText().toString().trim().length() <= 0) {
					Partograph_CommonClass
							.displayAlertDialog(getResources().getString(R.string.enter_mothersdeathreason), context);
					aq.id(R.id.etmothersdeathreason).getEditText().requestFocus();
					return false;
				}
			}
		} // 18Oct2016 Arpitha

		return true;
	}

	// Alert dialog
	private void displayAlertDialog(String message, final String classname) throws Exception {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();

						if (classname.equalsIgnoreCase(getResources().getString(R.string.issmssend))) {
							try {
								InserttblMessageLog("");
								if (isMessageLogsaved)
									sendSMSFunction();
							} catch (Exception e) {
								AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
										+ this.getClass().getSimpleName(), e);
								e.printStackTrace();
							} // 23Aug2016 - created by bindu
						}

						// 26Sep2016 Arpitha
						if (classname.equalsIgnoreCase(getResources().getString(R.string.isrefer))) {

							showDialogToUpdateReferral();
							// ReferralInfo_Activity.isdip = false;// 09Nov2016
							// Arpitha

						}

						if (classname.equalsIgnoreCase("breech"))// 18May2017
						// v2.6
						{
							normaltype = 2;

						} else if (classname.equalsIgnoreCase("vertex")) {
							normaltype = 1;

						}

					}
				});
		if (classname.equalsIgnoreCase(getResources().getString(R.string.issmssend))
				|| classname.equalsIgnoreCase(getResources().getString(R.string.isrefer))
				|| classname.equalsIgnoreCase("breech") || classname.equalsIgnoreCase("vertex")) {
			alertDialogBuilder.setMessage(message).setNegativeButton(getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							dialog.cancel();

							// 26Sep2016 Arpitha
							int options = 0;
							if (classname.equalsIgnoreCase(getResources().getString(R.string.issmssend))) {
								String risks = null;
								try {
									risks = mspnriskoptions.getSelectedItemsAsString();
								} catch (Exception e2) {
									// TODO Auto-generated catch block
									e2.printStackTrace();
								}
								try {
									options = mspnriskoptions.getSelectedStrings().size();
									// String risks
									// =mspnriskoptions.getSelectedItemsAsString();
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								/*if ((aq.id(R.id.etfacility).getText().toString()
										.equalsIgnoreCase(
												"PHC")
										&& risk == 1
										&& (ishighrisk || options > 1
												|| (!(risks.contains(
														getResources().getString(R.string.other_risk_option))))))
										&& Activity_WomenView.regtype != 2)// 22oct2016
								// Arpitha
								 * 
*/
								if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
										&& (options > 0
												&& ((risks.contains(getResources().getString(R.string.other_risk_option)) && options>1) || ishighrisk) ))
										&& Activity_WomenView.regtype != 2)// 22oct2016
								// Arpitha
								{
									try {

										displayAlertDialog(
												getResources().getString(R.string.risk_high_would_you_like_to_refer)
														+ mspnriskoptions.getSelectedStrings()
														+ getResources().getString(R.string.would_you_like_to_refer),
												getResources().getString(R.string.isrefer));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									Intent viewWomenList = new Intent(RegistrationActivity.this,
											Activity_WomenView.class);
									startActivity(viewWomenList);

									finish();
								}
							} else {
								dialog.cancel();
								Intent viewWomenList = new Intent(RegistrationActivity.this, Activity_WomenView.class);
								startActivity(viewWomenList);

								finish();
							}

							if (classname.equalsIgnoreCase("breech")) {
								normaltype = 1;
								rdvertext.setChecked(true);
								rdbreech.setChecked(false);
							} else if (classname.equalsIgnoreCase("vertex")) {
								normaltype = 2;
								rdvertext.setChecked(false);
								rdbreech.setChecked(true);
							}

						}
					});
		}

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
		alertDialog.setCancelable(false);
	}

	private void caldatepicker() throws Exception {
		DialogFragment newFragment = new SelectDateFragment();
		newFragment.show(this.getSupportFragmentManager(), "DatePicker");
	}

	@SuppressLint("ValidFragment")
	private class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar calendar = Calendar.getInstance();

			Date d = null;
			d = dbh.getDateS(selecteddate == null ? todaysDate : selecteddate);
			calendar.setTime(d);
			year = calendar.get(Calendar.YEAR);
			mon = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);

			DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, mon, day);

			/* remove calendar view */
			dialog.getDatePicker().setCalendarViewShown(false);

			/* Spinner View */
			dialog.getDatePicker().setSpinnersShown(true);

			dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
					+ getResources().getString(R.string.pickadate) + "</font>"));

			return dialog;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			if (view.isShown()) {
				try {

					populateSetDate(yy, mm + 1, dd);
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
			}
		}
	}

	public void populateSetDate(int year, int month, int day) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		// changed on 09 mar 2015
		selecteddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);

		// String lmpdate;

		try {

			// 04OCt2016 ARpitha
			if (isDateRefClicked) {
				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());
				Date selected = date.parse(selecteddate);
				Date reg = date.parse(todaysDate);

				if (selected.after(new Date())) {
					// displayAlertDialog(getResources().getString(R.string.invalid_date),
					// "");
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_date),
							Toast.LENGTH_LONG).show();
					etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));

				} else if (selected.before(reg)) {
					// displayAlertDialog(getResources().getString(R.string.refdate_cant_be_before_reg_date),
					// "");
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.refdate_cant_be_before_reg_date), Toast.LENGTH_LONG)
							.show();
					etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
				} else
					etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));

			}

			if (islmp) {

				Date taken = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault()).parse(selecteddate);
				if (taken.after(new Date())) {

					displayAlertDialog(getResources().getString(R.string.invalid_date), "");
					aq.id(R.id.etlmp).text(" ");
					aq.id(R.id.etedd).text("");// 23Nov2016 Arpitha
				} else {

					aq.id(R.id.etlmp).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));

					if (validateLmpADDDateSet(todaysDate, selecteddate)) {
						String gestation = Partograph_CommonClass.getNumberOfWeeksDays(selecteddate,
								Partograph_CommonClass.getTodaysDate());// 08Nov2016
						String[] gest = gestation.split(",");// 08Nov2016
						aq.id(R.id.etgestationage).text(gest[0]);// 08Nov2016
						aq.id(R.id.etgestationagedays).text(gest[1]);// 08Nov2016

						strlmpdate = selecteddate; // 11Sep2016 - bindu - set
													// lmp only if valid
						populateEDD(strlmpdate);// 10Nov2016 Arpitha
					}

				}

			}

			// updated by Arpitha
			if (isedd)// else changed on 10Nov2016 Arpitha
			{
				// String lmpdate = aq.id(R.id.etlmp).getText().toString();
				// strlmpdate =
				// Partograph_CommonClass.getConvertedDateFormat(lmpdate ,
				// "yyyy-MM-dd");// 30April2017
				// Arpitha
				if (strlmpdate != null && strlmpdate.trim().length() > 0) {// 25April2017
																			// Arpitha
					if (validateEDDADDDateSet(strlmpdate, selecteddate)) {
						edd = Partograph_CommonClass.getConvertedDateFormat(selecteddate,
								Partograph_CommonClass.defdateformat);
						aq.id(R.id.etedd).text(edd);

						stredddate = selecteddate;
					}
				} else {
					aq.id(R.id.etedd).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					stredddate = selecteddate;
				}
			}

			// 19Oct2016 Arpitha
			if (isDeldate) {
				// Date lmp = null;
				int days = 0;

				Date taken = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault()).parse(selecteddate);

				if (strlmpdate != null) {

					days = Partograph_CommonClass.getDaysBetweenDates(strlmpdate, selecteddate);
				}

				if (taken.after(new Date())) {

					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.date_cannot_be_greater_than_current_date), context);
					aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strDeldate = todaysDate;
				} else if (strlmpdate != null && days < 196) {

					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.deldate_cannot_be_before_sevenmonths_from_lmp), context);
					aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strDeldate = todaysDate;
				} else if ((strlmpdate != null && days > 301)) {

					Partograph_CommonClass.displayAlertDialog(
							getResources().getString(R.string.deldate_cannot_be_after_ninemonths_from_lmp), context);
					aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(todaysDate,
							Partograph_CommonClass.defdateformat));
					strDeldate = todaysDate;

				} else {
					aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(selecteddate,
							Partograph_CommonClass.defdateformat));
					strDeldate = selecteddate;
				}
			} // 19Oct2016 Arpitha

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// To avoid special characters in Input type
	public static InputFilter filter1 = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*.[]1234567890¶";
			String blockCharacterSet = "~#^|$%*!@()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\¥®¿Ì™₹°^√π÷×△¶£•¢€♥♡★☆▲▼↑←↓→¤△♂♀℃||△©co||¿¡℅™®₹°¢`•√π¶∆¢°∆¶π√•`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	// To avoid special characters in Input type
	public static InputFilter filter = new InputFilter() {
		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			// String blockCharacterSet =
			// "~#^|$%*!@/()-'\":;?{}=!$^';?×÷<>{}€£¥₩%&+*.[]1234567890¶";
			String blockCharacterSet = "0123456789~#^|$%*!@/()-'\":;,?{}=!$^';?×÷<>{}€£¥₩%&+*[]~#^|$%&*!`@_=?;]}{[()\"\\¥®¿Ì™₹°^√π÷×△¶£•¢€♥♡★☆▲▼↑←↓→¤△♂♀℃||△©c/o||¿¡℅™®₹°¢`•√π¶∆¢°∆¶π√•`";
			if (source != null && blockCharacterSet.contains(("" + source))) {
				return "";
			}
			return null;
		}
	};

	/**
	 * Validate Lmp
	 * 
	 * @param sdate
	 * @param tdate
	 */
	private boolean validateLmpADDDateSet(String tdate, String sdate) throws Exception {
		int days = Partograph_CommonClass.getDaysBetweenDates(tdate, sdate);
		if (days < 196) {

			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.msg010), context);
			aq.id(R.id.etlmp).text("").getEditText().requestFocus();
			aq.id(R.id.etedd).text("");// 19Nov2016 Arpitha
			aq.id(R.id.etgestationage).text("");// 27Feb2017 Arpitha
			aq.id(R.id.etgestationagedays).text("");// 27Feb2017 Arpitha
			return false;
		}
		if (days > 301) {

			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.msg011), context);
			aq.id(R.id.etlmp).text("").getEditText().requestFocus();
			aq.id(R.id.etedd).text("");// 19Nov2016 Arpitha
			aq.id(R.id.etgestationage).text("");// 27Feb2017 Arpitha
			aq.id(R.id.etgestationagedays).text("");// 27Feb2017 Arpitha
			return false;
		}

		return true;
	}

	// updated on 23May2016 by Arpitha
	private boolean validateEDDADDDateSet(String tdate, String sdate) throws Exception {

		int days = Partograph_CommonClass.getDaysBetweenDates(sdate, tdate);
		// 19July2017 Arpitha - 2.6.1 bug fixing
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date lmpDate = null;
		Date edddate = null;
		if (tdate != null & tdate.length() > 0)
			lmpDate = format.parse(tdate);

		if (sdate != null & sdate.length() > 0)
			edddate = format.parse(sdate);

		if (edddate.before(lmpDate)) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.edd_cannnot_be_lmp), context);
			return false;
		} // 19July2017 Arpitha - 2.6.1 bug fixing

		if (days < 212) {

			Partograph_CommonClass.displayAlertDialog(
					getResources().getString(R.string.edd_cannot_be_sevenmonths_before_lmp), context);
			aq.id(R.id.etedd).text("").getEditText().requestFocus();
			return false;
		}
		String today = Partograph_CommonClass.getTodaysDate("dd-MM-yyyy");
		int daysbt = Partograph_CommonClass.getDaysBetweenDates(sdate, today);
		if (aq.id(R.id.etlmp).getText().toString().trim().length() <= 0 && (daysbt > 0 && daysbt > 180)) {

			Partograph_CommonClass.displayAlertDialog(
					getResources().getString(R.string.edd_cannot_be_after_sixmonths_from_doa), context);
			aq.id(R.id.etedd).text("").getEditText().requestFocus();
			return false;
		}

		if (aq.id(R.id.etlmp).getText().toString().trim().length() <= 0 && daysbt < -180)

		{
			Partograph_CommonClass.displayAlertDialog(
					getResources().getString(R.string.edd_cannot_be_after_sixmonths_from_doa), context);
			aq.id(R.id.etedd).text("").getEditText().requestFocus();
			return false;
		}

		// 18May2017 Arpitha - v2.6
		if (aq.id(R.id.etlmp).getText().toString().trim().length() >= 0) {
			int no_of_days_lmp_edd = Partograph_CommonClass.getDaysBetweenDates(tdate, sdate);
			if ((no_of_days_lmp_edd > 300 || no_of_days_lmp_edd < -300)) {
				/*
				 * Toast.makeText(getApplicationContext(),
				 * getResources().getString(R.string.
				 * edd_cannot_be_after_sixmonths_from_doa), Toast.LENGTH_LONG)
				 * .show();
				 */
				Partograph_CommonClass.displayAlertDialog(
						getResources().getString(R.string.edd_cannot_be_after_sixmonths_from_doa), context);
				aq.id(R.id.etedd).text("").getEditText().requestFocus();
				return false;
			}
		} // 18May2017 Arpitha - v2.6

		return true;
	}

	// Set options for admitted with - 9jan2016
	protected void setAdmittedWith() throws Exception {
		int i = 0;
		admittedwithMap = new LinkedHashMap<String, Integer>();
		List<String> admittedwithStrArr = null;

		admittedwithStrArr = Arrays.asList(getResources().getStringArray(R.array.admittedarraylist));

		if (admittedwithStrArr != null) {
			for (String str : admittedwithStrArr) {
				admittedwithMap.put(str, i);
				i++;
			}
			mspnadmittedwith.setItems(admittedwithStrArr);
		}
	}

	/**
	 * This method invokes when Item clicked in Spinner
	 * 
	 * @throws Exception
	 */
	public void onSpinnerClicked(AdapterView<?> adapter, View v, int position, long arg3) throws Exception {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			switch (adapter.getId()) {
			// facility
			case R.id.spnbloodgroup:
				bldgrp = adapter.getSelectedItemPosition();

				break;

			case R.id.spnHeightNumeric:
				height_numeric = adapter.getSelectedItemPosition();
				checkHeightValue();

				break;
			case R.id.spnHeightDecimal:
				height_decimal = adapter.getSelectedItemPosition();
				checkHeightValue();

				break;

			// }

			// 18Oct2016 Arpitha
			// num of children
			case R.id.spnnoofchild: {
				no_of_child = (String) adapter.getSelectedItem();

				if (position == 0) {

					aq.id(R.id.tr_childdet2).visible();// 26April2017 Arpitha

					aq.id(R.id.tr_childdetsex1).visible();// 26April2017 Arpitha

					aq.id(R.id.tr_secondchilddetails).gone();// 26April2017
																// Arpitha

					aq.id(R.id.tr_result2).gone();// 26April207 Arpitha

					aq.id(R.id.tr_childdet2).gone();// 26April207 Arpitha

					aq.id(R.id.tr_childsex2).gone();// 26April207 Arpitha

					aq.id(R.id.tr_deltime2).gone();// 26April207 Arpitha

					aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.child_details));

					if (rdmale1.isChecked())
						babysex1 = 0;
					else
						babysex1 = 1;

					// 24Sep2016 Arpitha
					aq.id(R.id.etdeltime2).getEditText().setVisibility(View.GONE);
					rdfemale2.setVisibility(View.GONE);
					rdmale2.setVisibility(View.GONE);
					aq.id(R.id.etchildwt2).getEditText().setVisibility(View.GONE);
					imgwt2_warning.setVisibility(View.GONE);
					aq.id(R.id.spndelresult2).getSpinner().setVisibility(View.GONE);
					aq.id(R.id.imgdeltime2).getImageView().setVisibility(View.GONE);
					aq.id(R.id.imgdelresult).getImageView().setVisibility(View.GONE);
					aq.id(R.id.imgsex2).getImageView().setVisibility(View.GONE);// 24Sep2016
																				// Arpitha

				} else {
					aq.id(R.id.tr_childdet2).visible();// Arpitha 26April2017

					aq.id(R.id.tr_childsex2).visible();// 26April207 Arpitha

					aq.id(R.id.tr_secondchilddetails).visible();// 26April2017
																// Arpitha

					aq.id(R.id.tr_result2).visible();// 26April207 Arpitha

					aq.id(R.id.tr_childdet2).visible();// 26April2017 Arpitha

					aq.id(R.id.tr_childdetsex1).visible();// 26April2017 Arpitha

					aq.id(R.id.tr_deltime2).visible();// 26April207 Arpitha

					aq.id(R.id.txtfirstchilddetails).text(getResources().getString(R.string.first_child_details));

					if (rdmale2.isChecked())
						babysex2 = 0;
					else
						babysex2 = 1;

					// 24Sep2016 Arpitha
					aq.id(R.id.etdeltime2).getEditText().setVisibility(View.VISIBLE);
					rdfemale2.setVisibility(View.VISIBLE);
					rdmale2.setVisibility(View.VISIBLE);
					aq.id(R.id.etchildwt2).getEditText().setVisibility(View.VISIBLE);
					imgwt2_warning.setVisibility(View.INVISIBLE);// changed
																	// visible
																	// to
																	// invisible
																	// -
																	// 01Oct2016
																	// Arpitha
					aq.id(R.id.spndelresult2).getSpinner().setVisibility(View.VISIBLE);
					aq.id(R.id.imgdeltime2).getImageView().setVisibility(View.INVISIBLE);
					aq.id(R.id.imgdelresult).getImageView().setVisibility(View.INVISIBLE);
					aq.id(R.id.imgsex2).getImageView().setVisibility(View.INVISIBLE);// 24Sep2016
																						// Arpitha

				}
			}
				break;

			// Del result1
			case R.id.spndelresult: {

				delivery_result1 = adapter.getSelectedItemPosition() + 1;

			}
				break;

			// del result2
			case R.id.spndelresult2: {
				delivery_result2 = adapter.getSelectedItemPosition() + 1;
			}
				break;

			// del type
			case R.id.spndeltype: {
				delivery_type = adapter.getSelectedItemPosition() + 1;
				// 04Dec2015
				try {
					if (delivery_type > 1) {

						aq.id(R.id.tr_deltypereason).visible();// 26April2017
																// Arpitha

						aq.id(R.id.tr_deltypeotherreasons).visible();// 26April2017
																		// Arpitha

						setDelTypeReason(delivery_type);
						// 24Sep2016 -Arpitha
						mspndeltypereason.setVisibility(View.VISIBLE);
						aq.id(R.id.etdeltypeotherreasons).getEditText().setVisibility(View.VISIBLE);
						aq.id(R.id.wardeltype).getImageView().setVisibility(View.INVISIBLE);
						aq.id(R.id.delothrrason).getImageView().setVisibility(View.INVISIBLE);// 24Sep2016
																								// -Arpitha

						// 20Nov2016 Arpitha
						rdbreech.setVisibility(View.GONE);
						rdvertext.setVisibility(View.GONE);
						rdbreech.setChecked(false);
						rdvertext.setChecked(false);
						rgnormaltype.clearCheck();// 26April2017 Arpitha
						normaltype = 0;// 20Nov2016 Arpitha
						aq.id(R.id.txtnormaltype).gone();// 27Nov2016 Arpitha

						// 11May2017 Arpitha -v2.6 mantis id-0000231
						if (presentation == 2) {
							if (delivery_type == 2) {
								mspndeltypereason.setSelection(5);
							}
						} // 11May2017 Arpitha -v2.6 mantis id-0000231

					} else {

						aq.id(R.id.tr_deltypereason).gone();// 26April2017
															// Arpitha

						aq.id(R.id.tr_deltypeotherreasons).gone();// 26April2017
																	// Arpitha

						// 24Sep2016 -Arpitha
						mspndeltypereason.setVisibility(View.GONE);
						aq.id(R.id.etdeltypeotherreasons).getEditText().setVisibility(View.GONE);
						aq.id(R.id.wardeltype).getImageView().setVisibility(View.GONE);
						aq.id(R.id.delothrrason).getImageView().setVisibility(View.GONE);// 24Sep2016
																							// -Arpitha

						rdbreech.setVisibility(View.VISIBLE);// 20Nov2016
						// Arpitha
						rdvertext.setVisibility(View.VISIBLE);// 20Nov2016
						// Arpitha
						rdvertext.setChecked(true);// 20Nov2016 Arpitha
						normaltype = 1;
						aq.id(R.id.txtnormaltype).visible();// 27Nov2016 Arpitha

						if (presentation != 2)// 05May2017 Arpitha
												// -v2.6 mantis
												// id-0000231
						{
							rdvertext.setChecked(true);// 20Nov2016 Arpitha
							rdbreech.setChecked(false);
							normaltype = 1;
						} else// 05May2017 Arpitha -v2.6 mantis id-0000231
						{
							rdvertext.setChecked(false);
							rdbreech.setChecked(true);
							normaltype = 2;
						} // 05May2017 Arpitha -v2.6 mantis id-0000231

					}
				} catch (Exception e) {
					AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
							+ this.getClass().getSimpleName(), e);
					e.printStackTrace();
				}
				break;
			} // 18Oct2016 Arpitha

			}

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	private void checkHeightValue() throws Exception {
		// TODO Auto-generated method stub
		String feet;
		if (height_decimal < 10) {
			feet = "" + height_numeric + ".0" + height_decimal;
		} else {
			feet = "" + height_numeric + "." + height_decimal;
		}

		Double f = Double.parseDouble(feet);
		if (rd_feet.isChecked()) {
			if (rd_feet.isChecked() && (f < 4.08 || f > 5.08)) {
				aq.id(R.id.warheight_ft).visible();
				aq.id(R.id.etage).getEditText().requestFocus();
				isheight = true;

			} else {
				aq.id(R.id.warheight_ft).invisible();
				isheight = false;

			}
			if (Activity_WomenView.regtype != 2)
				setrisk();// 08nov2016 Arpitha

		}

	}

	protected void smscontentforwoman() throws Exception {
		String gest = "";
		if (aq.id(R.id.etgestationage).getText().toString().trim().length() <= 0) {
			gest = getResources().getString(R.string.sms_gest_not_known);// 07Oct2016
																			// Arpitha
																			// -
																			// string
																			// value
																			// from
																			// strings.xml
		} else {
			gest = aq.id(R.id.etgestationage).getText().toString() + "w, ";
		}

		String w_risk = "", riskreasons = "";
		if (risk == 1) {
			w_risk = getResources().getString(R.string.high);// 07Oct2016
																// Arpitha -
																// string value
																// from
																// strings.xml
			riskreasons = getResources().getString(R.string.risks) + " - " + mspnriskoptions.getSelectedStrings();// 07Oct2016
																													// Arpitha
																													// -
																													// string
																													// value
																													// from
																													// strings.xml
			if (aq.id(R.id.etcomments).getText().toString().length() > 0)
				riskreasons = riskreasons + ", Other risk - " + aq.id(R.id.etcomments).getText().toString();
		} else if (risk == 0) {
			w_risk = "Low";
			if (aq.id(R.id.etcomments).getText().toString().length() > 0) {
				riskreasons = " , Risk/s :" + aq.id(R.id.etcomments).getText().toString();
			}
		}

		messagetobesent = "ePartograph Registration :- FN:" + aq.id(R.id.etfacilityname).getText().toString() + ", "
				+ "WName: " + aq.id(R.id.etwname).getText().toString() + ", " + " Age: "
				+ aq.id(R.id.etage).getText().toString() + "Yrs, " + " Gest: " + gest + " R: " + w_risk + riskreasons;

	}

	// Insert to record to Message Log
	private void InserttblMessageLog(String phoneno) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		String gest = "";
		if (aq.id(R.id.etgestationage).getText().toString().trim().length() <= 0) {
			gest = "Gest:Not Known, ";
		} else {
			gest = aq.id(R.id.etgestationage).getText().toString() + "wks, ";
		}

		String w_risk = "", riskreasons = "";
		if (risk == 1) {
			w_risk = "High";
			riskreasons = " , Risk/s: " + " - " + mspnriskoptions.getSelectedStrings();
			if (aq.id(R.id.etcomments).getText().toString().length() > 0)
				riskreasons = riskreasons + ", Others - " + aq.id(R.id.etcomments).getText().toString();
		} else if (risk == 0) {
			w_risk = "Low";
			if (aq.id(R.id.etcomments).getText().toString().length() > 0) {
				riskreasons = " , Risk/s :" + aq.id(R.id.etcomments).getText().toString();
			}
		}

		String message = "ePartograph Reg :- FN:" + aq.id(R.id.etfacilityname).getText().toString() + ", "
				+ aq.id(R.id.etwname).getText().toString() + ", " + aq.id(R.id.etage).getText().toString() + "Yrs, "
				+ gest + " R: " + w_risk + riskreasons;

		ArrayList<MessageLogPojo> mlpArr = new ArrayList<MessageLogPojo>();

		String phoneStr = null;
		if (values != null && values.size() > 0) {
			for (int i = 0; i < values.size(); i++) {
				phoneStr = values.get(i);
				if (phoneStr != null && phoneStr.length() > 0) {
					MessageLogPojo mlp = new MessageLogPojo(user.getUserId(), womenId, phoneStr, w_risk, riskreasons,
							message, 1, 0);
					mlpArr.add(mlp);
				}
			}
		} else if (phoneno.length() > 0) {
			String[] phn = phoneno.split("\\,");
			for (int i = 0; i < phn.length; i++) {
				String num = phn[i];
				if (num != null && num.length() > 0) {
					MessageLogPojo mlp = new MessageLogPojo(user.getUserId(), womenId, num, w_risk, riskreasons,
							message, 1, 0);
					mlpArr.add(mlp);
				}
			}

		}

		if (mlpArr != null) {
			for (MessageLogPojo mlpp : mlpArr) {
				int transId = dbh.iCreateNewTrans(user.getUserId());// 15Oct2016
																	// Arpitha
				boolean isinserted = dbh.insertToMessageLog(mlpp, transId);// 15Oct2016
																			// Arpitha
				if (isinserted) {
					isMessageLogsaved = true;
					dbh.iNewRecordTrans(user.getUserId(), transId, Partograph_DB.TBL_MESSAGELOG);// 15Oct2016
																									// Arpitha
				} else
					throw new Exception("InserttblMessageLog(): Failed to Insert Message to tblMessageLog ");
			}
		}
	}

	/**
	 * This method invokes after successfull save of record Sends SMS to the
	 * Specified Number
	 */
	private void sendSMSFunction() throws Exception {
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.sending_sms), Toast.LENGTH_SHORT)
				.show();

		// 26Sep2016 Arpitha
		int options = mspnriskoptions.getSelectedStrings().size();
		String risks = mspnriskoptions.getSelectedItemsAsString();
		/*if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
				&& (ishighrisk || options > 1
						|| (!(risks.contains(getResources().getString(R.string.other_risk_option))))))
				&& Activity_WomenView.regtype != 2)// 22oct2016
		// Arpitha
*/
		
		if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
				&& (options > 0
						&& ((risks.contains(getResources().getString(R.string.other_risk_option)) && options>1) || ishighrisk) ))
				&& Activity_WomenView.regtype != 2)// 22oct2016
		// Arpitha
		{
			try {
				displayAlertDialog(
						getResources().getString(R.string.risk_high_would_you_like_to_refer)
								+ mspnriskoptions.getSelectedStrings()
								+ getResources().getString(R.string.would_you_like_to_refer),
						getResources().getString(R.string.isrefer));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Intent viewWomenList = new Intent(RegistrationActivity.this, Activity_WomenView.class);
			startActivity(viewWomenList);

			finish();
		}

		new SendSMS();
	}

	void savedata() {
		try {

			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());

			wpojo = new Women_Profile_Pojo();

			// 27Sep2016 Arpitha
			if (Partograph_CommonClass.autodatetime(RegistrationActivity.this)) {

				Date lastregdate = null;
				String strlastregdate = dbh.getlastmaxdate(user.getUserId(),
						getResources().getString(R.string.registration));
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss", java.util.Locale.getDefault());
				if (strlastregdate != null) {
					lastregdate = format.parse(strlastregdate);
				}
				Date regdatetime = new Date();
				if (lastregdate != null && regdatetime.before(lastregdate)) {
					Partograph_CommonClass.showSettingsAlert(RegistrationActivity.this,
							getResources().getString(R.string.plz_enable_automatic_date_and_set_current_date));
				} else {// 27Sep2016 Arpitha

					String age = aq.id(R.id.etage).getText().toString();
					if (!(age.equals("") || (age == null)))
						womanAge = Integer.parseInt(age);

					String gestationage = aq.id(R.id.etgestationage).getText().toString();

					if (!(gestationage.equals("") || (gestationage == null)))
						gest_age = Integer.parseInt(gestationage);
					wpojo.setGestationage(gest_age);
					// }

					String gestationagedays = aq.id(R.id.etgestationagedays).getText().toString();
					if (!(gestationagedays.equals("") || (gestationagedays == null)))
						gest_age_days = Integer.parseInt(gestationagedays);

					wpojo.setAddress(aq.id(R.id.etaddress).getText().toString());
					wpojo.setAge(womanAge);
					wpojo.setGest_age_days(gest_age_days);
					//
					String gravida = aq.id(R.id.etgravida).getText().toString();
					if (!(gravida.equals("") || (gravida == null)))
						igravida = Integer.parseInt(gravida);

					String para = aq.id(R.id.etpara).getText().toString();
					if (!(para.equals("") || (para == null)))
						ipara = Integer.parseInt(para);

					// wpojo.setDate_of_admission(todaysDate); 10Nov2016 Arpitha
					// wpojo.setDate_of_admission(todaysDate);21Nov2016 Arpitha
					wpojo.setDoc_name(aq.id(R.id.etdocname).getText().toString());
					wpojo.setGravida(igravida);
					wpojo.setHosp_no(aq.id(R.id.ethospno).getText().toString());
					wpojo.setNurse_name(aq.id(R.id.etnursename).getText().toString());
					wpojo.setPara(ipara);
					wpojo.setPhone_No(aq.id(R.id.etphone).getText().toString());
					// wpojo.setTime_of_admission(aq.id(R.id.ettoa).getText().toString());21Nov2016
					// Arpitha
					wpojo.setUserId(user.getUserId());
					wpojo.setW_attendant(aq.id(R.id.etattendant).getText().toString());
					wpojo.setWomen_Image(baos == null ? null : baos.toByteArray());
					wpojo.setWomen_name(aq.id(R.id.etwname).getText().toString());
					wpojo.setSpecial_inst(aq.id(R.id.etspecialinstructions).getText().toString());
					// wpojo.setComments(aq.id(R.id.etcomments).getText().toString());

					// wpojo.setRisk_category(risk);21Nov2016 Arpitha
					wpojo.setMemb_pres_abs(memb_pre_abs);

					// 02-01-2015
					wpojo.setState(aq.id(R.id.etstate).getText().toString());
					wpojo.setDistrict(aq.id(R.id.etdistrict).getText().toString());
					wpojo.setTaluk(aq.id(R.id.ettaluk).getText().toString());
					wpojo.setFacility(aq.id(R.id.etfacility).getText().toString());
					wpojo.setFacility_name(aq.id(R.id.etfacilityname).getText().toString());

					// updated on 30Nov2015
					wpojo.setThayicardnumber("" + aq.id(R.id.etthayicardno).getText().toString());

					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", java.util.Locale.getDefault());// 26APril2017
																													// Arpitha
					String currentDateandTime = sdf.format(new Date());

					wpojo.setDateinserted(currentDateandTime);

					getMonthYear(Partograph_CommonClass.getTodaysDate()); // 23jan2016

					wpojo.setWmonth(mon);
					wpojo.setWyear(year);

					womenId = dbh.getWomenId(user.getUserId());
					wpojo.setWomenId(womenId);

					if (strlmpdate != null && strlmpdate.trim().length() > 0) {// 25April2017
																				// Arpitha

						strlmpdate = Partograph_CommonClass.getConvertedDateFormat(strlmpdate, "yyyy-MM-dd");
						wpojo.setLmp(strlmpdate);
					}

					// updated on 27Feb2016 by Arpitha - 11Aug2017 Arpitha
					// stredddate.length() > 0
					if (stredddate != null && stredddate.trim().length() > 0) {
						stredddate = Partograph_CommonClass.getConvertedDateFormat(stredddate, "yyyy-MM-dd");
						wpojo.setEdd(stredddate);
					}

					// updated on 20MAy16
					wpojo.setHeight_unit(height_unit);

					// String height = null;
					if (height_unit == 1) {
						String heightval = "" + (height_numeric + "." + height_decimal);
						wpojo.setHeight(heightval);

					} else {
						wpojo.setHeight(aq.id(R.id.etheight).getText().toString());

					}

					String weight = "";
					weight = aq.id(R.id.etweight).getText().toString();
					if (!(weight.equals("") || (weight == null)))
						iweight = weight;
					wpojo.setW_weight(iweight);

					// 19jan2016
					wpojo.setBloodgroup(bldgrp);

					wpojo.setExtracomments(aq.id(R.id.etextracoments).getText().toString());// 13Oct2016
																							// Arpitha

					wpojo.setregtype(Activity_WomenView.regtype);// 16Oct2016
																	// Arpitha

					// 18Oct2016 Arpitha
					if (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked()) {

						wpojo.setDel_Comments(aq.id(R.id.etdelcomments).getText().toString());
						wpojo.setDel_Time(aq.id(R.id.etdeltime).getText().toString());
						wpojo.setDel_time2(aq.id(R.id.etdeltime2).getText().toString());
						wpojo.setDel_Date(strDeldate); // changed 07Apr2015

						int motherdead;

						if (aq.id(R.id.rdmotheralive).isChecked())
							motherdead = 0;
						else
							motherdead = 1;

						wpojo.setMothersdeath(motherdead);

						int babywt1 = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());
						int babywt2 = 0;
						if (aq.id(R.id.etchildwt2).getText().toString().trim().length() > 0) {
							babywt2 = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());
						}

						wpojo.setBabywt1(babywt1);
						wpojo.setBabywt2(babywt2);
						wpojo.setBabysex1(babysex1);
						wpojo.setBabysex2(babysex2);
						wpojo.setDel_result1(delivery_result1);
						wpojo.setDel_result2(delivery_result2);
						wpojo.setDel_type(delivery_type);
						wpojo.setNo_of_child(numofchildren);
						wpojo.setMothers_death_reason(aq.id(R.id.etmothersdeathreason).getText().toString());

						// 04Dec2015
						String selIds_deltype = "";
						if (delivery_type > 1) {
							List<String> selecteddeltypereason = mspndeltypereason.getSelectedStrings();

							if (selecteddeltypereason != null && selecteddeltypereason.size() > 0) {
								for (String str : selecteddeltypereason) {
									if (selecteddeltypereason.indexOf(str) != selecteddeltypereason.size() - 1)
										selIds_deltype = selIds_deltype + deltypereasonMap.get(str) + ",";
									else
										selIds_deltype = selIds_deltype + deltypereasonMap.get(str);
								}
							}
						}
						wpojo.setDelTypeReason(selIds_deltype);

						wpojo.setDeltype_otherreasons("" + aq.id(R.id.etdeltypeotherreasons).getText().toString());

						// 05jan2016
						String seltearsIds = "";
						List<String> selectedtears = mspntears.getSelectedStrings();
						if (selectedtears != null && selectedtears.size() > 0) {
							for (String str : selectedtears) {
								if (selectedtears.indexOf(str) != selectedtears.size() - 1)
									seltearsIds = seltearsIds + tearsMap.get(str) + ",";
								else
									seltearsIds = seltearsIds + tearsMap.get(str);
							}
						}

						wpojo.setTears(seltearsIds);
						wpojo.setEpisiotomy(episiotomy);
						wpojo.setSuturematerial(suturematerial);

						wpojo.setNormaltype(normaltype);// 20Nov2016 Arpitha

					} // 18Oct2016 Arpitha
					else// 05May2017 Arpitha - v2.6 mantis id-0000231
						wpojo.setNormaltype(presentation);// 05May2017 Arpitha -
															// v2.6 mantis
															// id-0000231

					wpojo.setDate_of_reg_entry(todaysDate + "/" + Partograph_CommonClass.getCurrentTime());// 10Nov2016
																											// Arpitha

					// 21Nov2016 Arpitha
					if (Activity_WomenView.regtype == 2) {
						wpojo.setRisk_category(99);
						// wpojo.setDate_of_admission("");
						// wpojo.setTime_of_admission("");
						wpojo.setRegtypeOtherReason(Activity_WomenView.regtypereason);
						wpojo.setRegtypereason("" + Activity_WomenView.retroreason);// 18Oct2016
						// Arpitha
					} else {
						wpojo.setDate_of_admission(todaysDate);
						wpojo.setTime_of_admission(aq.id(R.id.ettoa).getText().toString());
						wpojo.setRisk_category(risk);

						String selIds = "";
						if (risk == 1) {
							List<String> selectedriskoption = mspnriskoptions.getSelectedStrings();

							if (selectedriskoption != null && selectedriskoption.size() > 0) {
								for (String str : selectedriskoption) {
									if (selectedriskoption.indexOf(str) != selectedriskoption.size() - 1)
										selIds = selIds + riskoptionsMap.get(str) + ",";
									else
										selIds = selIds + riskoptionsMap.get(str);
								}
							}
						}
						wpojo.setRiskoptions(selIds);
						// 09jan2016
						String seladmittedwithIds = "";
						List<String> selectedadmittedwith = mspnadmittedwith.getSelectedStrings();

						if (selectedadmittedwith != null && selectedadmittedwith.size() > 0) {
							for (String str : selectedadmittedwith) {
								if (selectedadmittedwith.indexOf(str) != selectedadmittedwith.size() - 1)
									seladmittedwithIds = seladmittedwithIds + admittedwithMap.get(str) + ",";
								else
									seladmittedwithIds = seladmittedwithIds + admittedwithMap.get(str);
							}
						}
						wpojo.setAdmitted_with(seladmittedwithIds); // 09jan2016

						wpojo.setOther(aq.id(R.id.etother).getText().toString());
						wpojo.setComments(aq.id(R.id.etcomments).getText().toString());

					}

					dbh.db.beginTransaction();
					int transId = dbh.iCreateNewTrans(user.getUserId());
					boolean isAdded;
					if (Activity_WomenView.regtype == 2 || (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked())) {
						isAdded = dbh.addRetrospectiveUser(wpojo, transId);
					} else
						isAdded = dbh.addUser(wpojo, transId);

					if (isAdded) {
						dbh.iNewRecordTrans(wpojo.getUserId(), transId, Partograph_DB.TBL_REGISTEREDWOMEN);

						String upSql = dbh.UpdateLastWomenNumber(wpojo.getUserId(), transId); // Update
																								// LastWomenNumber
																								// in
																								// tblUsers
						if (upSql.length() > 0) {

							commitTrans("");
						} else
							rollbackTrans();

					} else {
						rollbackTrans();
					}
				}
			}
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();

		}

	}

	TextWatcher watcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			try {
				AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName());
				if (s == aq.id(R.id.etage).getEditable()) {
					if (aq.id(R.id.etage).getText().toString().length() > 0) {
						if (Integer.parseInt(aq.id(R.id.etage).getText().toString()) < 18
								|| Integer.parseInt(aq.id(R.id.etage).getText().toString()) > 40) {
							aq.id(R.id.warage).visible();

							isage = true;
						} else {
							aq.id(R.id.warage).invisible();
							isage = false;

						}

					} else {
						aq.id(R.id.warage).invisible();
						isage = false;

					}
					if (Activity_WomenView.regtype != 2)
						setrisk();
				} else if (s == aq.id(R.id.etheight).getEditable()) {
					if (aq.id(R.id.etheight).getText().toString().length() > 0) {
						if (Double.parseDouble(aq.id(R.id.etheight).getText().toString()) > 275) {
							Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.max_height),
									context);
							aq.id(R.id.etheight).text("");
						} else {
							if (Double.parseDouble(aq.id(R.id.etheight).getText().toString()) > 176
									|| Double.parseDouble(aq.id(R.id.etheight).getText().toString()) < 146) {
								aq.id(R.id.warheight).visible();

								isheight = true;
							} else {
								aq.id(R.id.warheight).invisible();
								isheight = false;
							}
						}
					} else {
						aq.id(R.id.warheight).invisible();
						aq.id(R.id.warheight).invisible();
						isheight = false;
					}
					if (Activity_WomenView.regtype != 2)
						setrisk();
				}

				else if (s == aq.id(R.id.etweight).getEditable()) {
					if (aq.id(R.id.etweight).getText().toString().trim().length() > 0) {
						double weight = Double.parseDouble(aq.id(R.id.etweight).getText().toString());
						if (aq.id(R.id.etweight).getText().toString().length() > 0) {

							if (weight < 35 || weight == 35 || weight > 80) {
								aq.id(R.id.imgweight).visible();

								isweight = true;
							} else {
								aq.id(R.id.imgweight).invisible();
								isweight = false;

							}

						} else {
							// updated on 1Aug2016 by Arpitha
							isweight = false;
							aq.id(R.id.imgweight).invisible();
						}
					} else {
						// updated on 1Aug2016 by Arpitha
						isweight = false;
						aq.id(R.id.imgweight).invisible();
					}
					if (Activity_WomenView.regtype != 2)
						setrisk();
				} else if (s == aq.id(R.id.etpara).getEditable()) {
					int para = Integer.parseInt(aq.id(R.id.etpara).getText().toString());
					int gravida = Integer.parseInt(aq.id(R.id.etgravida).getText().toString());
					if (count > 0) {
						if (para >= gravida) {
							Toast.makeText(getApplicationContext(), getResources().getText(R.string.para_validation),
									Toast.LENGTH_LONG).show();
							aq.id(R.id.etpara).text("");
						}
					}
				} else if (s == aq.id(R.id.etgestationage).getEditable()) {
					if (count > 0 || (aq.id(R.id.etgestationagedays).getText().length()) > 0) {
						chkgest.setEnabled(false);
						chkgest.setChecked(false);

						// if((aq.id(R.id.etgestationagedays).getText().length())
						// > 0)
						if (aq.id(R.id.etgestationage).getText().toString().trim().length() <= 0
								&& (!chkgest.isChecked())
								&& aq.id(R.id.etgestationagedays).getText().toString().trim().length() <= 0) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.pls_ent_gestationage), Toast.LENGTH_LONG).show();
							// displayAlertDialog(getResources().getString(R.string.pls_ent_gestationage),
							// "");
							// aq.id(R.id.etgestationage).getEditText().requestFocus();
							// return false;
						}

						/*
						 * if ((aq.id(R.id.etgestationage).getText().toString().
						 * length()) > 0) { if
						 * (Integer.parseInt(aq.id(R.id.etgestationage).getText(
						 * ).toString()) < 27 ||
						 * Integer.parseInt(aq.id(R.id.etgestationage).getText()
						 * .toString()) > 44) {
						 * Toast.makeText(getApplicationContext(),
						 * getResources().getString(R.string.gestationvalid),
						 * Toast.LENGTH_LONG).show(); //
						 * displayAlertDialog(getResources().getString(R.string.
						 * gestationvalid), // ""); //
						 * aq.id(R.id.etgestationage).getEditText().requestFocus
						 * (); // return false; } }
						 */

						if (aq.id(R.id.etgestationagedays).getText().length() > 0) {
							if (Integer.parseInt(aq.id(R.id.etgestationagedays).getText().toString()) > 6) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.gestationvaliddays), Toast.LENGTH_LONG)
										.show();
								// displayAlertDialog(getResources().getString(R.string.gestationvaliddays),
								// "");
								// aq.id(R.id.etgestationagedays).getEditText().requestFocus();
								// return false;
							}

						}
					} else
						chkgest.setEnabled(true);
				} else if (s == aq.id(R.id.etgestationagedays).getEditable()) {
					if (count > 0 || (aq.id(R.id.etgestationage).getText().length()) > 0) {
						chkgest.setEnabled(false);
						chkgest.setChecked(false);

						if (aq.id(R.id.etgestationage).getText().toString().trim().length() <= 0
								&& (!chkgest.isChecked())
								&& aq.id(R.id.etgestationagedays).getText().toString().trim().length() <= 0) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.pls_ent_gestationage), Toast.LENGTH_LONG).show();
							// displayAlertDialog(getResources().getString(R.string.pls_ent_gestationage),
							// "");
							// aq.id(R.id.etgestationage).getEditText().requestFocus();
							// return false;
						}

						if ((aq.id(R.id.etgestationage).getText().toString().length()) > 0) {
							if (Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) < 27
									|| Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) > 44) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.gestationvalid), Toast.LENGTH_LONG).show();
								// displayAlertDialog(getResources().getString(R.string.gestationvalid),
								// "");
								// aq.id(R.id.etgestationage).getEditText().requestFocus();
								// return false;
							}
						}

						if (aq.id(R.id.etgestationagedays).getText().length() > 0) {
							if (Integer.parseInt(aq.id(R.id.etgestationagedays).getText().toString()) > 6) {
								Toast.makeText(getApplicationContext(),
										getResources().getString(R.string.gestationvaliddays), Toast.LENGTH_LONG)
										.show();
								// displayAlertDialog(getResources().getString(R.string.gestationvaliddays),
								// "");
								// aq.id(R.id.etgestationagedays).getEditText().requestFocus();
							}

						}
					} else
						chkgest.setEnabled(true);

				} else if (s == aq.id(R.id.etgravida).getEditable()) {
					if (aq.id(R.id.etpara).getText().length() > 0) {
						para = Integer.parseInt(aq.id(R.id.etpara).getText().toString());
					}
					int gravida = Integer.parseInt(aq.id(R.id.etgravida).getText().toString());
					if (count > 0) {
						if (para >= gravida) {
							Toast.makeText(getApplicationContext(), getResources().getText(R.string.para_validation),
									Toast.LENGTH_LONG).show();
							aq.id(R.id.etpara).text("");
						}

					}
					setRiskOptions();// 05May2017 Arpitha - v2.6
				}
				// 19Oct2016 Arpitha
				else if (s == aq.id(R.id.etchildwt1).getEditable()) {
					if (count > 0) {
						int babywt1 = Integer.parseInt(aq.id(R.id.etchildwt1).getText().toString());

						if (babywt1 > 0 && (babywt1 < 2500 || babywt1 > 4000)) {
							imgwt1_warning.setVisibility(View.VISIBLE);
						} else {
							imgwt1_warning.setVisibility(View.INVISIBLE);
						}
					} else
						imgwt1_warning.setVisibility(View.INVISIBLE);

				} else if (s == aq.id(R.id.etchildwt2).getEditable()) {
					if (count > 0) {
						int babywt2 = Integer.parseInt(aq.id(R.id.etchildwt2).getText().toString());

						if (babywt2 > 0 && (babywt2 < 2500 || babywt2 > 4000)) {
							imgwt2_warning.setVisibility(View.VISIBLE);
						} else {
							imgwt2_warning.setVisibility(View.INVISIBLE);
						}
					} else
						imgwt2_warning.setVisibility(View.INVISIBLE);
				}

				// 19Oct2016 Arpitha

			} catch (Exception e) {
				AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
						+ this.getClass().getSimpleName(), e);
				e.printStackTrace();
			}

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {

			/*
			 * if ((aq.id(R.id.etgestationage).getText().toString().length()) >
			 * 0) { if
			 * (Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()
			 * ) < 27 ||
			 * Integer.parseInt(aq.id(R.id.etgestationage).getText().toString())
			 * > 44) { Toast.makeText(getApplicationContext(),
			 * getResources().getString(R.string.gestationvalid),
			 * Toast.LENGTH_LONG).show(); //
			 * displayAlertDialog(getResources().getString(R.string.
			 * gestationvalid), // ""); //
			 * aq.id(R.id.etgestationage).getEditText().requestFocus(); //
			 * return false; } }
			 */

		}

	};

	void clearAllFields() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		aq.id(R.id.etadmittedwith).text("");

		aq.id(R.id.etaddress).text("");
		aq.id(R.id.etattendant).text("");
		aq.id(R.id.etphone).text("");
		aq.id(R.id.ethospno).text("");
		aq.id(R.id.etdocname).text("");
		aq.id(R.id.etnursename).text("");

		// 18jan2016
		aq.id(R.id.etspecialinstructions).text("");

		if (Activity_WomenView.regtype == 2) {
			aq.id(R.id.spnHeightDecimal).setSelection(0);
			aq.id(R.id.spnHeightNumeric).setSelection(0);
			rd_htdontknow.setChecked(false);
			rd_feet.setChecked(false);
			rd_cm.setChecked(false);
			rgheight.clearCheck();// 25April2017 Arpitha
			aq.id(R.id.tr_heightcm).gone();
			aq.id(R.id.tr_height).gone();
			aq.id(R.id.etheight).text("");

			if (aq.id(R.id.chkdelinfo).isChecked())// 19April2017 Arpitha
			{

				aq.id(R.id.spndeltype).setSelection(0);
				mspndeltypereason.setSelected(false);
				mspndeltypereason.setVisibility(View.GONE);
				aq.id(R.id.etdeltypeotherreasons).text("");
				aq.id(R.id.etdeltypeotherreasons).gone();
				aq.id(R.id.spnnoofchild).setSelection(0);
				aq.id(R.id.spndelresult).setSelection(0);
				aq.id(R.id.spndelresult2).setSelection(0);

				rdfemale1.setChecked(false);
				rdfemale2.setChecked(false);
				rdmale1.setChecked(true);
				rdmale2.setChecked(true);
				aq.id(R.id.etdeltime).text(Partograph_CommonClass.getCurrentTime());
				aq.id(R.id.etdeltime2).text(Partograph_CommonClass.getCurrentTime());
				aq.id(R.id.etdelcomments).text("");
				aq.id(R.id.etdeldate).text(Partograph_CommonClass.getConvertedDateFormat(
						Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));
				rdepino.setChecked(true);
				rdepiyes.setChecked(false);
				rdvicryl.setChecked(false);
				rdcatgut.setChecked(false);
				rdvicryl.setVisibility(View.GONE);
				rdcatgut.setVisibility(View.GONE);
				rdmotheralive.setChecked(true);
				rdmotherdead.setChecked(false);
				aq.id(R.id.etmothersdeathreason).text("");
				aq.id(R.id.etmothersdeathreason).gone();

				rgnormaltype.clearCheck();// 27Feb2017 Arpitha
				rdvertext.setChecked(true);
				rdbreech.setChecked(false);
				rdbreech.setVisibility(View.VISIBLE);
				rdvertext.setVisibility(View.VISIBLE);

				aq.id(R.id.txtmothersdeathreason).gone();
				aq.id(R.id.txtsuturematerial).gone();
				mspntears.setSelected(false);
				mspntears.setSelection(20);// 12April2017 Arpitha
			}
		}

		// updated on 27Feb2016 by Arpitha
		aq.id(R.id.etweight).text("");
		aq.id(R.id.etedd).text("");

		strlmpdate = "";// 17July2017 Arpitha - bug fixing - v2.6.1
		stredddate = "";// 17July2017 Arpitha - bug fixing - v2.6.1

		aq.id(R.id.etother).text("");
		aq.id(R.id.etchildwt2).text("");

		aq.id(R.id.etlmp).text("");
		aq.id(R.id.etthayicardno).text("");

		// 18Feb2017 Arpitha
		aq.id(R.id.etextracoments).text("");
		// aq.id(R.id.etcomments).text("");
		if (!(isage || isheight || isweight)) {
			aq.id(R.id.mspnriskoptions).setSelection(20);
			rd_highrisk.setChecked(false);
			rd_lowrisk.setChecked(true);
			risk = 0;// 11Aug2017 Arpitha
			aq.id(R.id.etcomments).text("");
		}

		aq.id(R.id.mspnadmittedwith).setSelection(20);

		rd_memb_pre.setChecked(false);
		rd_memb_abs.setChecked(true);

		/*
		 * if (risk == 1) {// 22Mayy2017 Arpitha - v2.6 if (ishighrisk &&
		 * !(isheight || isweight || isage)) { risk = 0; ishighrisk = false;
		 * 
		 * } } // 22Mayy2017 Arpitha - v2.6
		 * 
		 */ rgbabypresentation.clearCheck();
		presentation = 0;// 22May2017 Arpitha
		memb_pre_abs = 0;// 22May2017 Arpitha

	}

	// Calculate EDD and display in EDD Edittext
	// updated on 28july2016 by Arpitha
	private void populateEDD(String xLMP) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());

			Date LmpDate;
			LmpDate = sdf.parse(xLMP);
			Calendar cal = Calendar.getInstance();
			cal.setTime(LmpDate);
			cal.add(Calendar.DAY_OF_MONTH, 280);

			int day = cal.get(Calendar.DAY_OF_MONTH);
			int month = cal.get(Calendar.MONTH) + 1;
			int year = cal.get(Calendar.YEAR);

			stredddate = String.valueOf(year) + "-" + String.format("%02d", month) + "-" + String.format("%02d", day);
			aq.id(R.id.etedd).text(
					Partograph_CommonClass.getConvertedDateFormat(stredddate, Partograph_CommonClass.defdateformat));

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	public void display_messagedialog(final String womenid) throws Exception {
		try {
			// 24Sep2016 Arpitha - addtotrace
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			final Dialog sms_dialog = new Dialog(this);
			sms_dialog.setTitle(getResources().getString(R.string.send_sms));// 24Sep2016
																				// Arpitha
																				// -
																				// fetching
																				// string
																				// value
																				// from
																				// strings.xml

			sms_dialog.setContentView(R.layout.activity_registartionsms);

			sms_dialog.setCancelable(false); // 23Aug2016 - bindu

			sms_dialog.show();

			TextView txt1 = (TextView) sms_dialog.findViewById(R.id.txtval);
			TextView txt2 = (TextView) sms_dialog.findViewById(R.id.txtval1);
			final EditText etphno = (EditText) sms_dialog.findViewById(R.id.etphnno);

			EditText etopt = (EditText) sms_dialog.findViewById(R.id.etreasonoptions);
			TextView txtreason = (TextView) sms_dialog.findViewById(R.id.txtval6);
			ImageButton imgsend = (ImageButton) sms_dialog.findViewById(R.id.imgsend);
			ImageButton imgcancel = (ImageButton) sms_dialog.findViewById(R.id.imgcancel);
			EditText etgest = (EditText) sms_dialog.findViewById(R.id.etgest);
			EditText etfm = (EditText) sms_dialog.findViewById(R.id.etfacility);

			txt2.setText("" + aq.id(R.id.etage).getText().toString());
			txt1.setText(aq.id(R.id.etwname).getText().toString());
			String w_risk = null;
			if (risk == 1) {
				w_risk = getResources().getString(R.string.high);// 24Sep2016
																	// Arpitha -
																	// strings
																	// value
																	// from
																	// strings.xml
				etopt.setText(w_risk);
			} else {
				etopt.setVisibility(View.GONE);
				txtreason.setVisibility(View.GONE);
			}
			String strgest = aq.id(R.id.etgestationage).getText().toString();
			if (strgest.trim().length() <= 0) {
				strgest = getResources().getString(R.string.dontknow);
			}
			etgest.setText(strgest);
			etfm.setText(aq.id(R.id.etfacilityname).getText().toString());

			imgsend.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					try {

						// 25Aug2016 - bindu
						if (etphno.getText().toString().length() > 0 && etphno.getText().toString().length() >= 10) {
							InserttblMessageLog(etphno.getText().toString());
							if (isMessageLogsaved)
								sendSMSFunction();
							sms_dialog.cancel();// 04Oct2016 Arpitha

						} else if (etphno.getText().toString().length() <= 0) {
							Toast.makeText(getApplicationContext(), getResources().getString(R.string.entr_phno),
									Toast.LENGTH_LONG).show();
						} else if (etphno.getText().toString().length() > 0
								&& etphno.getText().toString().length() < 10) {
							Toast.makeText(getApplicationContext(),
									getResources().getString(R.string.plz_entr_valid_phn_number), Toast.LENGTH_LONG)
									.show();
						}

					}

					catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}

				}
			});

			imgcancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sms_dialog.cancel();
					try {

						// 26Sep2016 Arpitha
						int options = mspnriskoptions.getSelectedStrings().size();
						String risks = mspnriskoptions.getSelectedItemsAsString();
						/*if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
								&& (ishighrisk || options > 1
										|| (!(risks.contains(getResources().getString(R.string.other_risk_option))))))
								&& Activity_WomenView.regtype != 2)// 22oct2016
						// Arpitha
*/
						
						if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
								&& (options > 0
										&& ((risks.contains(getResources().getString(R.string.other_risk_option)) && options>1) || ishighrisk) ))
								&& Activity_WomenView.regtype != 2)// 22oct2016
						// Arpitha
						{
							displayAlertDialog(
									getResources().getString(R.string.risk_high_would_you_like_to_refer)
											+ mspnriskoptions.getSelectedStrings()
											+ getResources().getString(R.string.would_you_like_to_refer),
									getResources().getString(R.string.isrefer));
						} else {
							Intent viewWomenList = new Intent(RegistrationActivity.this, Activity_WomenView.class);
							startActivity(viewWomenList);

							finish();
						}

					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);
						e.printStackTrace();
					}
				}
			});
			sms_dialog.setCancelable(false);// 29Sep2016 Arpitha
		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}

	}

	// Display Confirmation 28Sep2016 Arpitha
	private void displayConfirmationAlert(String exit_msg, final String classname) throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		final Dialog dialog = new Dialog(RegistrationActivity.this);
		dialog.setTitle(Html.fromHtml("<font color='" + getResources().getColor(R.color.appcolor) + "'>"
				+ getResources().getString(R.string.registration_alert) + "</font>"));// 15Feb2017
																						// Arpitha
		dialog.setContentView(R.layout.temp_alertdialog);

		int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
		View divider = dialog.findViewById(dividerId);
		divider.setBackgroundColor(getResources().getColor(R.color.appcolor));// 08Feb2017

		dialog.show();

		final TextView txtdialog = (TextView) dialog.findViewById(R.id.txtpulse);
		Button imgbtnyes = (Button) dialog.findViewById(R.id.imgbtnyes);
		Button imgbtnno = (Button) dialog.findViewById(R.id.imgbtnno);
		// Arpitha 27may16
		final TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtpulseval);

		final TextView txtdialog6 = (TextView) dialog.findViewById(R.id.txt);

		if (!(classname.equalsIgnoreCase(getResources().getString(R.string.reg))
				|| classname.equalsIgnoreCase(getResources().getString(R.string.confirmation)))) {
			txtdialog1.setVisibility(View.GONE);
			txtdialog.setVisibility(View.GONE);
			txtdialog6.setText(exit_msg);
			imgbtnyes.setText(getResources().getString(R.string.yes));
			imgbtnno.setText(getResources().getString(R.string.no));

		}

		if (classname.equalsIgnoreCase(getResources().getString(R.string.reg))) {
			String alertmsg = "";
			String values = "";

			if (age < 18 || age > 40) {
				alertmsg = alertmsg + getResources().getString(R.string.age_is) + "\n";
				values = values + "" + age + getResources().getString(R.string.years) + "\n";
			}
			if ((height > 0 && (height > 176 || height < 146))) {
				alertmsg = alertmsg + getResources().getString(R.string.height_is) + "\n";
				values = values + "" + height + getResources().getString(R.string.centimeter) + "\n";
			}
			if (d > 0 && (d > 5.08 || d < 4.08)) {
				alertmsg = alertmsg + getResources().getString(R.string.height_is) + "\n";
				values = values + "" + feet_height + getResources().getString(R.string.feet) + "\n";
			}

			// 22Sep2016 Arpitha
			if (weight > 0 && (weight <= 35 || weight > 80)) {
				alertmsg = alertmsg + getResources().getString(R.string.weight_is) + "\n";
				values = values + "" + weight + getResources().getString(R.string.kg) + "\n";

			}

			// 19Oct2016 Arpitha
			if (baby1weight > 0 && (baby1weight < 2500 || baby1weight > 4000)) {
				values = values + "" + baby1weight + "\n";
				if (baby2weight > 0)

					alertmsg = alertmsg + getResources().getString(R.string.first_baby_weight) + "\n";
				else
					alertmsg = alertmsg + getResources().getString(R.string.baby_weight) + "\n";

			}

			if (baby2weight > 0 && (baby2weight < 2500 || baby2weight > 4000)) {
				alertmsg = alertmsg + getResources().getString(R.string.second_baby_weight) + "\n";
				values = values + "" + baby2weight + "\n";

			}
			txtdialog.setText(alertmsg);
			txtdialog1.setText(values);
			txtdialog6.setText(getResources().getString(R.string.bp_val));

			imgbtnyes.setText(getResources().getString(R.string.save));
			imgbtnno.setText(getResources().getString(R.string.recheck));
		}

		if (classname.equalsIgnoreCase(getResources().getString(R.string.confirmation))) {
			String strwarningmess = "";
			String strval = "";

			if (pulval > 120 || pulval < 50) {
				strwarningmess = strwarningmess + getResources().getString(R.string.pul_val) + "\n";
				strval = strval + "" + pulval + "\n";

			}
			if (bpsysval < 80 || bpsysval > 160) {
				strwarningmess = strwarningmess + getResources().getString(R.string.bp_sys) + "\n";
				strval = strval + "" + bpsysval + "\n";

			}
			if (bpdiaval < 50 || bpdiaval >= 110) {

				strwarningmess = strwarningmess + getResources().getString(R.string.bp_dia) + "\n";
				strval = strval + "" + bpdiaval + "\n";
			}
			txtdialog6.setText(getResources().getString(R.string.bp_val));

			// 22Sep2016 Arpitha
			if (heartrate > 0 && (heartrate < 50 || heartrate > 200)) {
				strwarningmess = strwarningmess + getResources().getString(R.string.heart_rate_val_is) + "\n";
				strval = strval + "" + heartrate + "\n";
			}
			if (spo2 > 0 && (spo2 < 40 || spo2 > 100)) {
				strwarningmess = strwarningmess + getResources().getString(R.string.spo2_val_is) + "\n";
				strval = strval + "" + spo2 + "\n";

			}

			txtdialog.setText(strwarningmess);
			txtdialog1.setText(strval);
			imgbtnyes.setText(getResources().getString(R.string.save));
			imgbtnno.setText(getResources().getString(R.string.recheck));
		}

		imgbtnyes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					dialog.cancel();

					if (classname.equalsIgnoreCase(getResources().getString(R.string.reg))) {
						savedata();
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.confirmation))) {
						String msg = getResources().getString(R.string.refdatetimerecheck)
								+ etdateofreferral.getText().toString() + " / " + ettimeofreferral.getText().toString()
								+ getResources().getString(R.string.confirmrecheck);
						ConfirmAlert(msg, getResources().getString(R.string.referral));// 02Oct2016
																						// Arpitha
																						// -
																						// string
																						// value
																						// from
																						// strings.xml

					}

					if (classname.equalsIgnoreCase(getResources().getString(R.string.save))) {
						savedata();

					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.clear)))

					{
						try {
							clearAllFields();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.main)))

					{
						Intent i = new Intent(getApplicationContext(), Activity_WomenView.class);
						startActivity(i);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.login))) {
						Intent i = new Intent(getApplicationContext(), LoginActivity.class);
						startActivity(i);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.sms))) {
						try {

							if (values != null && values.size() > 0) {

								smscontentforwoman();
								String phno = "";
								for (int i = 0; i < values.size(); i++) {
									phno = phno + values.get(i) + ",";
								}
								displayAlertDialog(getResources().getString(R.string.following_sms_will_be_sent) + phno
										+ " \n " + messagetobesent, getResources().getString(R.string.issmssend));

							} else {
								display_messagedialog(womenId);
							}

						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					// 10May2017 Arpitha - v2.6
					if (classname.equalsIgnoreCase(getResources().getString(R.string.comments))) {
						Intent comments = new Intent(getApplicationContext(), Comments_Activity.class);
						startActivity(comments);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.info))) {
						Intent comments = new Intent(getApplicationContext(), GraphInformation.class);
						startActivity(comments);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.settings))) {
						Intent comments = new Intent(getApplicationContext(), Settings_parto.class);
						startActivity(comments);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.summary))) {
						Intent comments = new Intent(getApplicationContext(), Summary_Activity.class);
						startActivity(comments);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.user_manual))) {
						Intent comments = new Intent(getApplicationContext(), UseManual.class);
						startActivity(comments);
					}
					if (classname.equalsIgnoreCase(getResources().getString(R.string.about))) {
						Intent comments = new Intent(getApplicationContext(), About.class);
						startActivity(comments);
					} // 10May2017 Arpitha - v2.6

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		imgbtnno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
				try {
					if (classname.equalsIgnoreCase(getResources().getString(R.string.sms))) {
						String risks = mspnriskoptions.getSelectedItemsAsString();
						int options = mspnriskoptions.getSelectedStrings().size();
						/*if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
								&& (ishighrisk || options > 1
										|| (!(risks.contains(getResources().getString(R.string.other_risk_option))))))
								&& Activity_WomenView.regtype != 2)// 22oct2016
*/
						
						if ((aq.id(R.id.etfacility).getText().toString().equalsIgnoreCase("PHC") && risk == 1
								&& (options > 0
										&& ((risks.contains(getResources().getString(R.string.other_risk_option)) && options>1) || ishighrisk) ))
								&& Activity_WomenView.regtype != 2)// 22oct2016
						// Arpitha
						{

							displayAlertDialog(
									getResources().getString(R.string.risk_high_would_you_like_to_refer)
											+ mspnriskoptions.getSelectedStrings()
											+ getResources().getString(R.string.would_you_like_to_refer),
									getResources().getString(R.string.isrefer));

						} else {
							Intent viewWomenList = new Intent(RegistrationActivity.this, Activity_WomenView.class);
							startActivity(viewWomenList);

							finish();
						}
					}

				} catch (NotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		dialog.show();

		dialog.setCancelable(false);// 29Sep2016 Arpitha

	}

	// 01Oct2016 Arpitha
	// Show dialog to update delivery status
	private void showDialogToUpdateReferral() {
		try {
			AppContext.addToTrace(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
					+ this.getClass().getSimpleName());
			referral_dialog = new Dialog(RegistrationActivity.this);
			referral_dialog.setContentView(R.layout.activity_reference);
			referral_dialog.setTitle(getResources().getString(R.string.app_name) + " - "
					+ getResources().getString(R.string.referredto));

			ReferralInfo_Activity.isdip = true;

			AssignIdsToReferralWidgets(referral_dialog);

			initialViewReferral();
			
			imgpdf.setVisibility(View.GONE);//13oct2017 Arpitha

			referral_dialog.show();

			etdateofreferral.setText(Partograph_CommonClass.getConvertedDateFormat(
					Partograph_CommonClass.getTodaysDate(), Partograph_CommonClass.defdateformat));

			btnCancelRef.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					referral_dialog.cancel();
					Intent i = new Intent(RegistrationActivity.this, Activity_WomenView.class);
					startActivity(i);
				}
			});
			
//			imgpdf.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					Partograph_CommonClass.pdfAlertDialog(context, woman, "referral", null, null);
//					
//				}
//			});

			etdateofreferral.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						try {
							v.performClick();// 26April2017 Arpitha - included
												// to remove deprecation
							isDateRefClicked = true;
							isDeldate = false;// 19Oct2016 Arpitha
							isedd = false;// 22oct2016 Arpitha
							islmp = false;// 22oct2016 Arpitha
							caldatepicker();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);
							e.printStackTrace();
						}
					}
					return true;
				}
			});

			ettimeofreferral.setOnTouchListener(new View.OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (MotionEvent.ACTION_UP == event.getAction()) {
						// isDelTime1 = true;
						v.performClick();// 26April2017 Arpitha - included to
											// remove remove deprecation
						reftime = ettimeofreferral.getText().toString();

						isTimeRefClicked = true;
						if (reftime != null) {
							Calendar calendar = Calendar.getInstance();
							Date d = null;
							d = dbh.getTime(reftime);
							calendar.setTime(d);
							hour = calendar.get(Calendar.HOUR_OF_DAY);
							minute = calendar.get(Calendar.MINUTE);
						}
						// showDialog(TIME_DIALOG_ID);
						showtimepicker();// 26April2017 Arpitha
					}
					return true;
				}
			});

			// Referred to facility
			spnreferralfacility.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> adapter, View arg1, int arg2, long arg3) {
					referralfacility = adapter.getSelectedItemPosition();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});

			// 04jan2016
			rd_motherconscious.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofmother = 1;
				}
			});

			rd_motherunconscious.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofmother = 0;
				}
			});

			rd_babyalive.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofbaby = 1;
					aq.id(etspo2).visible();
					aq.id(etheatrate).visible();
					trspo2.setVisibility(View.VISIBLE);
					trheartrate.setVisibility(View.VISIBLE);
				}
			});

			rd_babydead.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					conditionofbaby = 0;
					aq.id(etspo2).gone();
					aq.id(etheatrate).gone();
					trspo2.setVisibility(View.GONE);
					trheartrate.setVisibility(View.GONE);
					etheatrate.setText("");// 29Sep2016 Arpitha
					etspo2.setText("");// 29Sep2016 Arpitha
				}
			});

			btnSaveRef.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					try {

						spo2 = 0;
						heartrate = 0;
						// 22Sep2016 Arpitha -
						if (etpulsereferral.getText().toString().length() > 0) {
							pulval = Integer.parseInt(etpulsereferral.getText().toString());
						}

						if (etbpsystolicreferral.getText().toString().length() > 0
								&& etbpdiastolicreferral.getText().toString().length() > 0) {
							bpsysval = Integer.parseInt(etbpsystolicreferral.getText().toString());
							bpdiaval = Integer.parseInt(etbpdiastolicreferral.getText().toString());
						}

						// 22Sep2016 Arpitha -
						if (etheatrate.getText().toString().trim().length() > 0) {
							heartrate = Integer.parseInt(etheatrate.getText().toString());
						}
						// 22Sep2016 Arpitha
						if (etspo2.getText().toString().trim().length() > 0) {
							spo2 = Integer.parseInt(etspo2.getText().toString());
						}

						refdate = (strRefDate == null ? todaysDate : strRefDate) + " "
								+ ettimeofreferral.getText().toString();

						if (validateReferralFields()) {

							// 27Sep2016 Arpitha
							if (Partograph_CommonClass.autodatetime(getApplicationContext())) {

								Date lastregdate = null;
								String strlastinserteddate = dbh.getlastmaxdate(user.getUserId(),
										getResources().getString(R.string.referral));
								SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss",
										java.util.Locale.getDefault());// 26April2017
																		// Arpitha
																		// -
																		// changed
																		// deprecated
																		// code
								if (strlastinserteddate != null) {
									lastregdate = format.parse(strlastinserteddate);
								}
								Date currentdatetime = new Date();
								if (lastregdate != null && currentdatetime.before(lastregdate)) {
									Partograph_CommonClass.showSettingsAlert(getApplicationContext(), getResources()
											.getString(R.string.plz_enable_automatic_date_and_set_current_date));
								} else {// 27Sep2016 Arpitha

									// 22Sep2016 Arpitha
									if ((pulval > 120 || pulval < 50)
											|| ((bpsysval < 80 || bpsysval > 160) || (bpdiaval < 50 || bpdiaval >= 110))
											|| (heartrate > 0 && (heartrate < 50 || heartrate > 200))
											|| (spo2 > 0 && (spo2 < 40 || spo2 > 100)))

									{
										displayConfirmationAlert("", getResources().getString(R.string.confirmation));
									}

									else {
										// updated bindu - 25Aug2016 - check
										// confirmation of Ref date and time
										String msg = getResources().getString(R.string.refdatetimerecheck)
												+ etdateofreferral.getText().toString() + " / "
												+ ettimeofreferral.getText().toString()
												+ getResources().getString(R.string.confirmrecheck);
										ConfirmAlert(msg, getResources().getString(R.string.referral));
									}

								}
							}
						}
					} catch (Exception e) {
						AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
								+ this.getClass().getSimpleName(), e);

						e.printStackTrace();
					}
				}
			});
			referral_dialog.setCancelable(false);

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// Assign Ids to Referral Widgets
	// Arpitha - 02Oct2016 Arpitha
	private void AssignIdsToReferralWidgets(Dialog dialog) throws Exception {
		// TODO Auto-generated method stub
		etplaceofreferral = (EditText) dialog.findViewById(R.id.etplaceofreferral);
		etreasonforreferral = (EditText) dialog.findViewById(R.id.etreasonforreferral);
		etdateofreferral = (EditText) dialog.findViewById(R.id.etdateofreferrence);
		ettimeofreferral = (EditText) dialog.findViewById(R.id.ettimeofreferrence);
		spnreferralfacility = (Spinner) dialog.findViewById(R.id.spnreferredtofacility);
		btnCancelRef = (Button) dialog.findViewById(R.id.btncancelref);
		btnSaveRef = (Button) dialog.findViewById(R.id.btnsaveref);
		trspo2 = (TableRow) dialog.findViewById(R.id.trspo2);
		trheartrate = (TableRow) dialog.findViewById(R.id.trheartrate);

		// 04jan2016
		etdescriptionofreferral = (EditText) dialog.findViewById(R.id.etdescforreferral);
		etbpsystolicreferral = (EditText) dialog.findViewById(R.id.etbpsystolicreferral);
		etbpdiastolicreferral = (EditText) dialog.findViewById(R.id.etbpdiastolicreferral);
		etpulsereferral = (EditText) dialog.findViewById(R.id.etpulsereferral);
		rd_motherconscious = (RadioButton) dialog.findViewById(R.id.rd_motherconscious);
		rd_motherunconscious = (RadioButton) dialog.findViewById(R.id.rd_motherunconscious);
		rd_babyalive = (RadioButton) dialog.findViewById(R.id.rd_babyalive);
		rd_babydead = (RadioButton) dialog.findViewById(R.id.rd_babydead);
		mspnreasonforreferral = (MultiSelectionSpinner) dialog.findViewById(R.id.mspnreasonforreferral);

		// 24jan2016
		txtrefmode = (TextView) dialog.findViewById(R.id.txtmode);

		// updated on 27Feb2016 by Arpitha
		etheatrate = (EditText) dialog.findViewById(R.id.etheartrate);
		etspo2 = (EditText) dialog.findViewById(R.id.etspo2);

		// 23Aug2016 - bindu
		txtwage = (TextView) dialog.findViewById(R.id.wage);
		txtwdoa = (TextView) dialog.findViewById(R.id.wdoa);
		txtwgravida = (TextView) dialog.findViewById(R.id.wgravida);
		txtwname = (TextView) dialog.findViewById(R.id.wname);
		txtwrisk = (TextView) dialog.findViewById(R.id.wtrisk);
		txtwtoa = (TextView) dialog.findViewById(R.id.wtoa);
		txtwgest = (TextView) dialog.findViewById(R.id.wgest);

//		txtheadingref = (TextView) dialog.findViewById(R.id.txtheadingref);// 16Nov2016
//																			// Arpitha
//		txtheadingref.setVisibility(View.GONE);// 16Nov2016 Arpitha

		llbaby = (LinearLayout) dialog.findViewById(R.id.llbaby);// 20March2017
																	// Arpitha
		
		imgpdf = (ImageView) dialog.findViewById(R.id.imgpdf);//13Oct2017 Arpitha
	}

	// Initial View of the referral Screen
	// Arpitha - 02Oct2016 Arpitha
	private void initialViewReferral() throws Exception {

		// 23Aug2016
		getwomanbasicdata();

		// 4jan2016
		etreasonforreferral.setVisibility(View.GONE);
		setReasonForReferral();

		todaysDate = Partograph_CommonClass.getTodaysDate(); // 12jan2016
		ettimeofreferral.setText(Partograph_CommonClass.getCurrentTime());

		// 14Jan2016
		String refTime = ettimeofreferral.getText().toString();
		Calendar calendar = Calendar.getInstance();
		Date d = null;
		d = dbh.getTime(refTime);
		calendar.setTime(d);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);

		spnreferralfacility.setEnabled(true);
		etplaceofreferral.setEnabled(true);
		etreasonforreferral.setEnabled(true);
		etdateofreferral.setEnabled(true);
		ettimeofreferral.setEnabled(true);
		btnSaveRef.setEnabled(true);
		// btnSaveRef.setBackgroundColor(ContextCompat.getColor(this,
		// R.color.bgcolor));

		btnSaveRef.setBackgroundColor(getResources().getColor(R.color.brown));

		// 24jan2016
		txtrefmode.setText(getResources().getString(R.string.editmode));
		// updated on 27Feb2016 by Arpitha
		etheatrate.setEnabled(true);
		etspo2.setEnabled(true);

		// 20March2017 Arpitha
		if (!(isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked())) {
			llbaby.setVisibility(View.GONE);
		} // 20March2017 Arpitha

	}

	// Set options for reason for referral - //02Oct2016 Arpitha
	protected void setReasonForReferral() throws Exception {
		int i = 0;
		reasonforreferralMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;

		// 06jul2016 - assign diff array to get the position.
		List<String> reasonStrArrvalues = null;

		reasonStrArrvalues = Arrays.asList(getResources().getStringArray(R.array.reasonforreferralvalues));

		// to get the values(pos) for the options
		if (reasonStrArrvalues != null) {
			for (String str : reasonStrArrvalues) {
				reasonforreferralMap.put(str, i);
				i++;
			}
		}

		// reasonStrArr =
		// Arrays.asList(getResources().getStringArray(R.array.reasonforreferral));
		if (isDelinfoadd && aq.id(R.id.chkdelinfo).isChecked())// 03April2017
																// Arpitha
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.reasonforreferraldelivered));// 03April2017
		// Arpitha

		else// 03April2017 Arpitha

			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.reasonforreferraldip));// 03April2017
		// Arpitha

		if (reasonStrArr != null) {
			mspnreasonforreferral.setItems(reasonStrArr);
		}

	}

	// Validate Referral mandatory fields- Arpitha - 02Oct2016 Arpitha
	private boolean validateReferralFields() throws Exception {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (etplaceofreferral.getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.ent_placeofreferral), context);
			etplaceofreferral.requestFocus();
			return false;
		}

		// 24Aug2016 - bindu make reason for referral mandatory
		if (mspnreasonforreferral.getSelectedIndicies().size() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.plsselectreasonforref),
					context);
			etdescriptionofreferral.requestFocus();
			return false;
		}

		// 4jan2016
		if (etdescriptionofreferral.getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterdescofreferral), context);
			etdescriptionofreferral.requestFocus();
			return false;
		}

		// 12Jan2016
		if (etpulsereferral.getText().toString().trim().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterpulseval), context);
			etpulsereferral.requestFocus();
			return false;
		} // 12jan2016

		// Changes made by Arpitha
		if (etbpsystolicreferral.getText().toString().trim().length() <= 0
				&& etbpdiastolicreferral.getText().length() <= 0) {
			Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enter_bp), context);
			etbpsystolicreferral.requestFocus();
			return false;
		}

		if (etbpsystolicreferral.getText().toString().trim().length() > 0) {
			if (etbpdiastolicreferral.getText().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterbpdiaval), context);
				etbpdiastolicreferral.requestFocus();
				return false;
			}
		}

		if (etbpdiastolicreferral.getText().toString().trim().length() > 0) {
			if (etbpsystolicreferral.getText().length() <= 0) {
				Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.enterbpsysval), context);
				etbpsystolicreferral.requestFocus();
				return false;
			}
		}

		if (etspo2.getText().toString().equalsIgnoreCase("0")) {
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_value), Toast.LENGTH_LONG)
					.show();
			etspo2.requestFocus();
			return false;
		}

		String currenttime = Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime();
		if (etdateofreferral.getText().length() >= 1) {
			if (!isDeliveryTimevalid(
					aq.id(R.id.etdoa).getText().toString() + " " + aq.id(R.id.ettoa).getText().toString(), refdate, 3))
				return false;

			if (!isDeliveryTimevalid(refdate, currenttime, 5))
				return false;
		}

		if (ettimeofreferral.getText().length() >= 1) {

			if (!(isDeliveryTimevalid(
					aq.id(R.id.etdoa).getText().toString() + " " + aq.id(R.id.ettoa).getText().toString(), refdate, 3)))
				return false;

			if (!isDeliveryTimevalid(refdate, currenttime, 5))
				return false;
		}

		return true;
	}

	// Arpitha - 02Oct2016 Arpitha
	void referral_data() throws Exception {

		wrefpojo = new WomenReferral_pojo();

		wrefpojo.setWomenname(aq.id(R.id.etwname).getText().toString());
		wrefpojo.setWomenid(womenId);
		wrefpojo.setUserid(user.getUserId());
		wrefpojo.setFacility(aq.id(R.id.etfacility).getText().toString());
		wrefpojo.setFacilityname(aq.id(R.id.etfacilityname).getText().toString());
		wrefpojo.setReferredtofacilityid(referralfacility);
		wrefpojo.setPlaceofreferral(etplaceofreferral.getText().toString());

		// updated on 27Feb2016 by Arpitha

		wrefpojo.setHeartrate(etheatrate.getText().toString());
		wrefpojo.setSpo2(etspo2.getText().toString());

		String rdate = (strRefDate == null ? todaysDate : strRefDate);

		wrefpojo.setDateofreferral(rdate);
		wrefpojo.setTimeofreferral(ettimeofreferral.getText().toString());
		wrefpojo.setCreated_date(Partograph_CommonClass.getCurrentDateandTime());
		wrefpojo.setLastupdateddate(Partograph_CommonClass.getCurrentDateandTime());

		// 04jan2016
		List<String> selectedriskoption = mspnreasonforreferral.getSelectedStrings();
		String selIds = "";
		if (selectedriskoption != null && selectedriskoption.size() > 0) {
			for (String str : selectedriskoption) {
				if (selectedriskoption.indexOf(str) != selectedriskoption.size() - 1)
					selIds = selIds + reasonforreferralMap.get(str) + ",";
				else
					selIds = selIds + reasonforreferralMap.get(str);
			}
		}
		wrefpojo.setReasonforreferral(selIds);
		String bp = etbpsystolicreferral.getText().toString() + "/" + etbpdiastolicreferral.getText().toString();
		wrefpojo.setBp(bp);
		wrefpojo.setPulse(etpulsereferral.getText().toString() + "");
		wrefpojo.setConditionofmother(conditionofmother);
		wrefpojo.setConditionofbaby(conditionofbaby);
		wrefpojo.setDescriptionofreferral(etdescriptionofreferral.getText().toString());

		dbh.db.beginTransaction();
		int transId = dbh.iCreateNewTrans(user.getUserId());
		String updateRef = "";
		if (womenrefpojoItems != null && womenrefpojoItems.size() > 0) {
			updateRef = dbh.updateWomenReferralData(wrefpojo, transId);

			if (updateRef.length() > 0) {

				commitTrans(getResources().getString(R.string.referral));
			} else
				rollbackTrans();

		} else {
			boolean isAdded = dbh.addReferral(wrefpojo, transId);

			if (isAdded) {
				dbh.iNewRecordTrans(user.getUserId(), transId, Partograph_DB.TBL_REFERRAL);
				commitTrans(getResources().getString(R.string.referral));

			} else {
				rollbackTrans();
			}
		}

		referral_dialog.cancel();

	}

	// Arpitha - 02Oct2016 Arpitha
	private void ConfirmAlert(String msg, final String goToScreen) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegistrationActivity.this);

		// set dialog message
		alertDialogBuilder.setMessage(msg).setCancelable(false)
				.setNegativeButton(getResources().getString(R.string.recheck), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						etdateofreferral.requestFocus();

						dialog.cancel();
					}
				}).setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							referral_data();
							dialog.cancel();
						} catch (Exception e) {
							AppContext.addLog(new RuntimeException().getStackTrace()[0].getMethodName() + " - "
									+ this.getClass().getSimpleName(), e);

							e.printStackTrace();
						}

					}
				});
		AlertDialog alertDialog1 = alertDialogBuilder.create();
		alertDialog1.show();
	}

	// updated on 04Oct2016 by Arpitha
	void getwomanbasicdata() throws Exception {

		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		String doa = "", toa = "", risk_is = "";

		txtwname.setText(aq.id(R.id.etwname).getText().toString());
		txtwage.setText(aq.id(R.id.etage).getText().toString());

		if (Activity_WomenView.regtype != 2) {
			doa = Partograph_CommonClass.getConvertedDateFormat(todaysDate, Partograph_CommonClass.defdateformat);
			toa = aq.id(R.id.ettoa).getText().toString();
			txtwdoa.setText(getResources().getString(R.string.reg) + ": " + doa + "/" + toa);
		}
		if (aq.id(R.id.etgestationage).getText().toString().trim().length() > 0) {
			String gestdays = "";// 26April2017 Arpitha
			if (aq.id(R.id.etgestationagedays).getText().toString().trim().length() > 0)// 26April2017
																						// Arpitha
			{
				gestdays = aq.id(R.id.etgestationagedays).getText().toString()
						+ getResources().getString(R.string.days);
			} // 26April2017 Arpitha
			txtwgest.setText(aq.id(R.id.etgestationage).getText().toString() + getResources().getString(R.string.wks)
					+ " " + gestdays);
		} else
			txtwgest.setText(getResources().getString(R.string.notknown));

		txtwgravida.setText(getResources().getString(R.string.gravida_short_label) + ":"
				+ aq.id(R.id.etgravida).getText().toString() + ", "
				+ getResources().getString(R.string.para_short_label) + ":" + aq.id(R.id.etpara).getText().toString());
		if (risk == 1) {
			risk_is = getResources().getString(R.string.high);
		} else
			risk_is = getResources().getString(R.string.low);
		txtwrisk.setText(getResources().getString(R.string.risk_short_label) + ":" + risk_is);

	}

	// 04Oct2016 Arpitha
	@SuppressLint("SimpleDateFormat")
	private boolean isDeliveryTimevalid(String date1, String date2, int i) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {

			Date d1 = sdf.parse(date1);
			Date d2 = sdf.parse(date2);
			if (d2.before(d1)) {
				if (i == 3)

					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.refdate_cant_be_before_reg_date) + " - " + date1,
							Toast.LENGTH_LONG).show();

				// displayAlertDialog(getResources().getString(R.string.refdate_cant_be_before_reg_date)
				// + " - " + date1,
				// "");
				if (i == 4)
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.reftime_bef_regtim) + " - " + date1, Toast.LENGTH_LONG)
							.show();
				// displayAlertDialog(getResources().getString(R.string.reftime_bef_regtim)
				// + " - " + date1, "");
				if (i == 5)
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.refdate_after_currentdate) + " - " + date1,
							Toast.LENGTH_LONG).show();
				// displayAlertDialog(getResources().getString(R.string.refdate_after_currentdate)
				// + " - " + date2,
				// "");
				return false;
			} else
				return true;

		} catch (Exception e) {
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);

			e.printStackTrace();
			return false;
		}
	}

	// Set options for tears - 18Oct2016 Arpitha
	protected void setTears() throws Exception {
		int i = 0;
		tearsMap = new LinkedHashMap<String, Integer>();
		List<String> tearsStrArr = null;

		// updated on 6july2016

		List<String> tearsStrArrvalues = null;

		tearsStrArrvalues = Arrays.asList(getResources().getStringArray(R.array.tearsvalues));

		if (tearsStrArrvalues != null) {
			for (String str : tearsStrArrvalues) {
				tearsMap.put(str, i);
				i++;
			}
		}

		tearsStrArr = Arrays.asList(getResources().getStringArray(R.array.tears));

		if (tearsStrArr != null) {
			mspntears.setItems(tearsStrArr);
		}
	}

	// Set Junk Items - 18Oct2016 Arpitha
	private void setDelTypeReason(int dtype) throws Exception {
		int i = 0;
		deltypereasonMap = new LinkedHashMap<String, Integer>();
		List<String> reasonStrArr = null;
		if (dtype == 2) {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoncesareanValues));// 29Sep2016
																											// Arpitha
		} else if (dtype == 3) {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoninstrumental));
		}

		if (reasonStrArr != null) {
			for (String str : reasonStrArr) {
				deltypereasonMap.put(str, i);
				i++;
			}
		}

		// 29Sep2016 Arpitha
		if (dtype == 2)

		{
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoncesarean));
		} else {
			reasonStrArr = Arrays.asList(getResources().getStringArray(R.array.spnreasoninstrumental));
		}
		if (reasonStrArr != null) {
			mspndeltypereason.setItems(reasonStrArr);
		}
	}

	// 20oct2016 Arpitha - action to be performed on click of back button
	@Override
	public void onBackPressed() {
		try {
			displayConfirmationAlert(getResources().getString(R.string.exit_msg),
					getResources().getString(R.string.main));
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
			e.printStackTrace();
		}
	}

	// to set risk automatically based on height, age, weight values - Arpitha
	void setrisk() {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());
		try {
			risk_observed = new ArrayList<String>();
			if (risk == 1) {
				if (isage || isheight || isweight) {
					String pos = mspnriskoptions.getSelectedItemsAsString();
					if (!pos.contains("Other"))// 26April2017 Arpitha
					{
						String[] otheroption = { pos, "Other", "अन्य" };
						mspnriskoptions.setSelection(otheroption);
					}

				} else {

					if (!ishighrisk) {
						rd_highrisk.setChecked(false);
						risk = 0;
						rd_lowrisk.setChecked(true);
						mspnriskoptions.setSelected(false);
						mspnriskoptions.setSelection(20);
						aq.id(R.id.tr_riskoptions).gone();// 26April2017 Arpitha
						mspnriskoptions.setVisibility(View.GONE);// 24Sep2016-ARpitha
					}
				}
			} else {
				if (isage || isweight || isheight) {
					rd_highrisk.setChecked(true);
					risk = 1;
					setRiskOptions();
					String[] otheroption = { "Other", "अन्य" };
					mspnriskoptions.setSelection(otheroption);
					aq.id(R.id.tr_riskoptions).visible();// 26April2017 Arpitha
					mspnriskoptions.setVisibility(View.VISIBLE);// 24Sep2016-ARpitha
				} else {
					rd_highrisk.setChecked(false);
					rd_lowrisk.setChecked(true);
					risk = 0;
					mspnriskoptions.setSelected(false);
					// trriskoptions.setVisibility(View.GONE);
					aq.id(R.id.tr_riskoptions).gone();// 26April2017 Arpitha
					// 24Sep2016-ARpitha
					mspnriskoptions.setVisibility(View.GONE);
				}
			}

			if (isage) {
				risk_observed.add("Age");
			} else {
				risk_observed.remove("Age");
			}
			if (isheight) {
				risk_observed.add("Height");
			} else {
				risk_observed.remove("Height");
			}

			if (isweight) {
				risk_observed.add("Weight");
			} else {
				risk_observed.remove("Weight");
			}

			if (risk_observed != null && risk_observed.size() > 0) {
				robserved = "";
				for (int i = 0; i < risk_observed.size(); i++) {
					robserved = robserved + risk_observed.get(i) + " ";
				}
				aq.id(R.id.etcomments).text(robserved);
			} else {
				robserved = "";
				aq.id(R.id.etcomments).text(robserved);
			}
		} catch (Exception e) {
			e.printStackTrace();
			AppContext.addLog(
					new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName(),
					e);
		}
	}

	public void showtimepicker() {
		DialogFragment newFragment = new TimePickerFragment();
		FragmentManager fm = getSupportFragmentManager();
		newFragment.show(fm, "timePicker");
	}

	private static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
		public TimePickerFragment() {
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {

			// isTimeRefClicked = false;//19May2017 Arpitha - 2.6

			hour = hourOfDay;
			minute = minutes;

			try {

				if (isTimeRefClicked) {

					Date selected;
					String strselectedtime;
					Date currenttime;
					SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm", java.util.Locale.getDefault());// 26April2017
																													// Arpitha
																													// -
																													// chnaged
																													// deprecated
																													// code
					currenttime = date.parse(
							Partograph_CommonClass.getTodaysDate() + " " + Partograph_CommonClass.getCurrentTime());
					if (strRefDate != null) {
						strselectedtime = strRefDate + " "
								+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));
					} else
						strselectedtime = todaysDate + " "
								+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));

					selected = date.parse(strselectedtime);

					Date reg = date.parse(todaysDate + " " + aq.id(R.id.ettoa).getText().toString());

					if (selected.after(currenttime)) {
						Toast.makeText(getActivity(), getResources().getString(R.string.invalid_date),
								Toast.LENGTH_LONG).show();
						// Partograph_CommonClass.displayAlertDialog(getResources().getString(R.string.invalid_date),
						// context);

					} else if (selected.before(reg)) {
						Toast.makeText(getActivity(),
								getResources().getString(R.string.refdate_cant_be_before_reg_date), Toast.LENGTH_LONG)
								.show();
						// Partograph_CommonClass.displayAlertDialog(
						// getResources().getString(R.string.refdate_cant_be_before_reg_date),
						// context);

					} else
						ettimeofreferral.setText(
								new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute)));

				} else {
					String strdeltime;
					if (strDeldate != null) {
						strdeltime = strDeldate + " "
								+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));
					} else
						strdeltime = todaysDate + " "
								+ new StringBuilder().append(padding_str(hour)).append(":").append(padding_str(minute));
					SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm", java.util.Locale.getDefault());// 26April2017
																													// Arpitha
																													// -
																													// changed
																													// deprecated
																													// code
					Date deltime = date.parse(strdeltime);

					// 19Oct2016 Arpitha
					if (isDelTime1) {

						if (deltime.after(new Date())) {
							Partograph_CommonClass.displayAlertDialog(
									getResources().getString(R.string.deltime_after_currenttime), context);
							aq.id(R.id.etdeltime).text(Partograph_CommonClass.getCurrentTime());
						} else
							aq.id(R.id.etdeltime).getEditText().setText(new StringBuilder().append(padding_str(hour))
									.append(":").append(padding_str(minute)));
					} else {
						if (deltime.after(new Date())) {
							Partograph_CommonClass.displayAlertDialog(
									getResources().getString(R.string.deltime_after_currenttime), context);
							aq.id(R.id.etdeltime2).text(Partograph_CommonClass.getCurrentTime());
						} else

							aq.id(R.id.etdeltime2).getEditText().setText(new StringBuilder().append(padding_str(hour))
									.append(":").append(padding_str(minute)));
					}
				} // 19Oct2016
					// Arpitha

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	// 26April2017 Arpitha
	private static String padding_str(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	// 10May2017 Arpitha - v2.6
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		AppContext.addToTrace(
				new RuntimeException().getStackTrace()[0].getMethodName() + " - " + this.getClass().getSimpleName());

		if (!(AppContext.checkSimState() == TelephonyManager.SIM_STATE_READY)) {
			menu.findItem(R.id.sms).setVisible(false);
		}

		menu.findItem(R.id.delete).setVisible(false);
		menu.findItem(R.id.expertcomments).setVisible(false);
		menu.findItem(R.id.viewpartograph).setVisible(false);
		menu.findItem(R.id.ic_msg).setVisible(false);
		menu.findItem(R.id.addcomments).setVisible(false);
		menu.findItem(R.id.viewprofile).setVisible(false);
		menu.findItem(R.id.sync).setVisible(false);
		menu.findItem(R.id.registration).setVisible(false);
		menu.findItem(R.id.search).setVisible(false);// 14Jun2017 Arpitha

		menu.findItem(R.id.save).setVisible(false);// 08Aug2017 Arpitha
		return super.onPrepareOptionsMenu(menu);
	}

	// Alert dialog
	private void displayAlertDialog1(String message, final String classname) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set dialog message
		alertDialogBuilder.setMessage(message)

				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
						if (classname.equalsIgnoreCase("breech"))// 18May2017
																	// v2.6
						{
							normaltype = 2;

						} else if (classname.equalsIgnoreCase("vertex")) {
							normaltype = 1;

						}
					}
				});// 18May2017 Arpitha - v2.6

		// create alert dialog

		// 18May2017 Arpitha - v2.6
		if (classname.equalsIgnoreCase("breech") || classname.equalsIgnoreCase("vertex")) {
			alertDialogBuilder.setMessage(message).setNegativeButton(getResources().getString(R.string.no),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.cancel();
							if (classname.equalsIgnoreCase("breech")) {
								normaltype = 1;
								rdvertext.setChecked(true);
								rdbreech.setChecked(false);
							} else if (classname.equalsIgnoreCase("vertex")) {
								normaltype = 2;
								rdvertext.setChecked(false);
								rdbreech.setChecked(true);
							}

						}
					});
		}

		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
		alertDialog.setCancelable(false);// 31oct2016 Arpitha
	}

	// 12Oct2017 Arpitha
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub

		if (v == aq.id(R.id.etgestationage).getEditText()) {
			if ((aq.id(R.id.etgestationage).getText().toString().length()) > 0) {
				if (Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) < 27
						|| Integer.parseInt(aq.id(R.id.etgestationage).getText().toString()) > 44) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.gestationvalid),
							Toast.LENGTH_LONG).show();
					aq.id(R.id.etgestationage).text("");
					//aq.id(R.id.etgestationage).getEditText().requestFocus();

					// displayAlertDialog(getResources().getString(R.string.gestationvalid),
					// "");
					// aq.id(R.id.etgestationage).getEditText().requestFocus();
					// return false;
				}
			}
		}

	}

}
